package com.sucho.placesautocomplete.exception

class InitializationException(message: String?) : Exception(message)
