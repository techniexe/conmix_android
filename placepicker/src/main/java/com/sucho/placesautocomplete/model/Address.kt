package com.sucho.placesautocomplete.model


data class Address(val longName: String, val shortName: String, val type: ArrayList<String>)