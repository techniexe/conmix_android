package com.sucho.placesautocomplete.model


data class Place(
    val id: String,
    val description: String
) {
    override fun toString(): String {
        return ""
    }
}