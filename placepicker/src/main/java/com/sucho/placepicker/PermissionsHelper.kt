package com.sucho.placepicker

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.OnDialogButtonClickListener
import com.karumi.dexter.listener.single.BasePermissionListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener


object PermissionsHelper {

    fun checkForLocationPermission(activity: Activity, listener: BasePermissionListener?) {

        val dialogPermissionListener = DialogOnDeniedPermissionListener.Builder
            .withContext(activity)
            .withTitle(R.string.permission_fine_location_title)
            .withMessage(R.string.permission_fine_location_message)
            .withButtonText(android.R.string.ok, OnDialogButtonClickListener {
                checkForLocationPermission(activity, listener)
            })

            .withIcon(R.drawable.ic_map_marker)
            .build()


        val compositeListener =
            if (listener != null) {

                CompositePermissionListener(dialogPermissionListener, listener)
            } else {

                CompositePermissionListener(dialogPermissionListener)
            }

        Dexter.withContext(activity)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(compositeListener)
            .withErrorListener {
                Log.e("Dexter", "There was an error: " + it.toString());
            }
            .onSameThread()
            .check()
    }

}
