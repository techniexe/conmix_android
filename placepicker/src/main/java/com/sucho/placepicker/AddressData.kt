package com.sucho.placepicker

import android.location.Address
import android.os.Parcel
import android.os.Parcelable




data class AddressData(
    var latitude: Double,
    var longitude: Double,
    var addressList: List<Address>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.createTypedArrayList(Address.CREATOR)
    ) {
    }

    override fun toString(): String {
        return latitude.toString() + "\n" +
                longitude.toString() + "\n" +
                getAddressString()
    }

    private fun getAddressString(): String {
        addressList?.let { if (it.isNotEmpty()) return it[0].getAddressLine(0) }
        return ""
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeTypedList(addressList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AddressData> {
        override fun createFromParcel(parcel: Parcel): AddressData {
            return AddressData(parcel)
        }

        override fun newArray(size: Int): Array<AddressData?> {
            return arrayOfNulls(size)
        }
    }
}