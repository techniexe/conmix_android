package com.conmix.app.utils


/**
 * Created by Hitesh Patel on 18,May,2021
 */
enum class OrderItemStatusEnm {
    PLACED,PROCESSING,CONFIRMED,TRUCK_ASSIGNED_FOR_PICKUP,PICKUP,DELAYED,DELIVERED,CANCELLED,REJECTED
}