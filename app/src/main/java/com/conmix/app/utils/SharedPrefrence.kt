package com.conmix.app.utils

import android.content.Context
import android.content.SharedPreferences
import com.conmix.app.R
import com.conmix.app.data.CustomMixPostObj
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject


/**
 * Created by Hitesh Patel on 24,February,2021
 */
object SharedPrefrence {

    private val AUTHTOKEN = "authtoken"
    const val SESSION_TOKEN = "session_token"
    const val FCM_TOKEN = "fcm_token"
    const val FCM_COUNT = "fcm_count"
    const val FCM_DEVICE_ID = "fcm_device"
    const val IS_LOGED_IN = "is_logged_in"
    const val IS_FIRST_LAUNCH = "IS_FIRST_LAUNCH"
    const val ISMOBILE_SIGNUP = "mobSignUp"
    const val LATITUDE = "latitude"
    const val CITYID = "cityid"
    const val STATEID = "stateid"
    const val LONGITUDE = "longitude"
    const val SITEID = "siteid"
    const val SITENAME = "sitename"
    const val CITYNAME = "cityname"
    const val STATENAME = "statename"
    const val SELECTEDDATE="selectedDate"
    const val SELECTEDENDDATE = "selectedEndDate"
    const val SELECTEDDATESERVER="selectedDateserver"
    const val CUSTOMMIXOBJ="cust_mix_obj"
    const val DRIVERNAME="driver_name"
    const val DRIVERMOBILE="driver_mobile"
    const val OPRETORNAME="opretor_name"
    const val OPRETORMOBILE="opretor_mobile"
    const val TRANSISTNO="transist_no"
    const val FULLADDRESS="fulladdress"
    const val SELECTEDQTY="selectedqty"

    private fun getSharedPref(context: Context?): SharedPreferences? {
        return context?.getSharedPreferences(
            context?.getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
    }

    fun getSELECTEDQTY(context: Context?): Int? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getInt(SELECTEDQTY,3)
    }

    fun setSELECTEDQTY(context: Context, number: Int){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putInt(SELECTEDQTY, number)
        editor.apply()
    }


    fun getTRANSISTNo(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(TRANSISTNO, null)
    }
    fun setTRANSISTNo(context: Context, number: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(TRANSISTNO, number)
        editor.apply()
    }


    fun getOPRETORName(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(OPRETORNAME, null)
    }
    fun setOPRETORName(context: Context, name: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(OPRETORNAME, name)
        editor.apply()
    }

    fun getFullAdd(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(FULLADDRESS, null)
    }
    fun setFullAdd(context: Context, name: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(FULLADDRESS, name)
        editor.apply()
    }


    fun getOPRETORMobile(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(OPRETORMOBILE, null)
    }
    fun setOPRETORMobile(context: Context, number: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(OPRETORMOBILE, number)
        editor.apply()
    }


    fun getDriverName(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(DRIVERNAME, null)
    }
    fun setDriverName(context: Context, name: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(DRIVERNAME, name)
        editor.apply()
    }


    fun getDriverMobile(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(DRIVERMOBILE, null)
    }
    fun setDriverMobile(context: Context, number: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(DRIVERMOBILE, number)
        editor.apply()
    }


    fun getSelectedDate(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SELECTEDDATE, null)
    }
    fun setSelectedDate(context: Context, date: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SELECTEDDATE, date)
        editor.apply()
    }

    fun getSelectedENDDate(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SELECTEDENDDATE, null)
    }
    fun setSelectedENDDate(context: Context, date: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SELECTEDENDDATE, date)
        editor.apply()
    }

    fun getSelectedDateServer(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SELECTEDDATESERVER, null)
    }
    fun setSelectedDateServer(context: Context, date: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SELECTEDDATESERVER, date)
        editor.apply()
    }

    fun setSessionToken(context: Context, title: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SESSION_TOKEN, title)
        editor.apply()
    }

    fun getSessionToken(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SESSION_TOKEN, null)
    }

    fun setFCMtokem(context: Context, title: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(FCM_TOKEN, title)
        editor.apply()
    }

    fun getFCMtoken(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(FCM_TOKEN, null)
    }

    fun setCount(context: Context, count: Int) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putInt(FCM_COUNT, count)
        editor.apply()
    }

    fun getCount(context: Context?): Int {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getInt(FCM_COUNT, 0)
    }

    fun setDeviceID(context: Context, id: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(FCM_DEVICE_ID, id)
        editor.apply()
    }

    fun getDeviceId(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(FCM_DEVICE_ID, null)
    }

    fun setStateId(context: Context, stateId: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(STATEID, stateId)
        editor.apply()
    }

    fun getStateId(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(STATEID, null)
    }
    fun setCityId(context: Context, cityId: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(CITYID, cityId)
        editor.apply()
    }

    fun getCityId(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(CITYID, null)
    }
    fun setlat(context: Context, lat: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(LATITUDE, lat)
        editor.apply()
    }

    fun getlat(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(LATITUDE, null)
    }
    fun setlang(context: Context, lang: String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(LONGITUDE, lang)
        editor.apply()
    }

    fun getlang(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(LONGITUDE, null)
    }

    fun setLogin(context: Context, flag: Boolean){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putBoolean(IS_LOGED_IN, flag)
        editor.apply()
    }

    fun getLogin(context: Context?): Boolean {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getBoolean(IS_LOGED_IN, false)
    }


    fun setIsMobileSignUp(context: Context, flag: Boolean){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putBoolean(ISMOBILE_SIGNUP, flag)
        editor.apply()
    }

    fun getIsMobileSignUp(context: Context?): Boolean {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getBoolean(ISMOBILE_SIGNUP, false)
    }

    fun setSiteId(context: Context, siteId: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SITEID, siteId)
        editor.apply()
    }

    fun getSiteId(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SITEID, null)
    }

    fun setSiteName(context: Context, siteName: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SITENAME, siteName)
        editor.apply()
    }

    fun getSiteName(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SITENAME, null)
    }

    fun setStateName(context: Context, siteName: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(STATENAME, siteName)
        editor.apply()
    }

    fun getStateName(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(STATENAME, null)
    }

    fun setCityName(context: Context, siteName: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(CITYNAME, siteName)
        editor.apply()
    }

    fun getCityname(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(CITYNAME, null)
    }

    fun setCustomMixObj(context: Context, cnObj: CustomMixPostObj?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        val gson = Gson()
        val json = gson.toJson(cnObj)
        editor.putString(CUSTOMMIXOBJ, json)
        editor.apply()
    }



    fun getCustomMixObj(context: Context?): JSONObject? {
        val sharedPref = getSharedPref(context)
        val jsonString: String? = sharedPref!!.getString(CUSTOMMIXOBJ, "{}")
        return try {
            JSONObject(jsonString)
        } catch (e: JSONException) {
            null
        }
    }

}