package com.conmix.app.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.conmix.app.R
import com.conmix.app.data.ErrorRes
import com.conmix.app.data.ProductCategoryWiseListPickupLocation
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern


/**
 * Created by Hitesh Patel on 24,February,2021
 */
object Utils {

    fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    fun showToast(context: Context?, mesaage: String, time: Int) {
        if (context != null) {

            val inflater: LayoutInflater = (context as Activity).layoutInflater
            val layout: View = inflater.inflate(
                R.layout.custom_toast,
                (context as Activity).findViewById(R.id.toast_layout_root) as ViewGroup?
            )


            val text = layout.findViewById<View>(R.id.text) as TextView
            text.text = mesaage

            val toast = Toast(context)

            //toast.setGravity(Gravity.BOTTOM, 0, 0)
            toast.duration = Toast.LENGTH_LONG
            toast.view = layout
            toast.show()
        }

    }

    fun ignoreFirstWhiteSpace(): InputFilter {
        return InputFilter { source, start, end, dest, dstart, dend ->
            for (i in start until end) {
                if (Character.isWhitespace(source[i])) {
                    if (dstart == 0) return@InputFilter ""
                }
            }
            null
        }
    }

    fun setStringSpanable(
        textView: TextView,
        links: Array<String>,
        clickableSpans: Array<ClickableSpan>
    ) {
        val spannableString = SpannableString(textView.text)
        for (i in links.indices) {
            val clickableSpan = clickableSpans[i]
            val link = links[i]

            val startIndexOfLink = textView.text.toString().indexOf(link)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink,
                startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            val foregroundSpan = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                ForegroundColorSpan(textView.context.getColor(R.color.login_btn_color))

               // ForegroundColorSpan(Color.parseColor("#D01418"))
            } else {
                //  ForegroundColorSpan(textView.context.getColor(R.color.pink_theme_color))
            }
            spannableString.setSpan(
                foregroundSpan, startIndexOfLink,
                startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

        }

        textView.highlightColor =
            Color.TRANSPARENT // prevent TextView change background when highlight
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    fun hideKeyboard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun getFirstLetter(value: String): String? {
        return if (TextUtils.isEmpty(value)) {
            ""
        } else {
            val firstChar = value.substring(0, 1).toUpperCase()
            if (!Pattern.matches("[a-zA-Z]", firstChar)) {

                ""
            } else {

                firstChar
            }
        }
    }
    fun setErrorData(context: Context, data: ResponseBody?) {
        val res = data!!.string().toString()
        val gson = Gson()
        val type = object : TypeToken<ErrorRes>() {}.type
        val err = gson.fromJson<ErrorRes>(res, type)
        if (err?.error?.message != null && err.error.message.isNotEmpty())
            showToast(context, err.error.message, Toast.LENGTH_SHORT)
    }
    fun isValidEmail(target: CharSequence?): Boolean {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    fun isValidPan(target: CharSequence?): Boolean {
        val pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}")
        return (!TextUtils.isEmpty(target) && pattern.matcher(target).matches());
    }

    fun isValidGST(target: CharSequence?): Boolean {
        val pattern = Pattern.compile("[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}")
        return (!TextUtils.isEmpty(target) && pattern.matcher(target).matches());
    }


    fun setImageUsingGlide(context: Context, imageUrl: String?, imageView: ImageView) {
        val requestOptions = RequestOptions().apply {
            placeholder(R.color.colorGrey)
            error(R.color.borderColor)
            diskCacheStrategy(DiskCacheStrategy.ALL)
        }


        Glide.with(context)
            .setDefaultRequestOptions(requestOptions)
            .load(imageUrl ?: "")
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(imageView)


    }

    fun setImageUsingGradeGlide(context: Context, imageUrl: String?, imageView: ImageView) {
        val requestOptions = RequestOptions().apply {
            placeholder(R.drawable.ic_category_new)
            error(R.drawable.ic_category_new)
            diskCacheStrategy(DiskCacheStrategy.ALL)
        }


        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(imageUrl ?: "")
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                            e: GlideException?,
                            model: Any,
                            target: com.bumptech.glide.request.target.Target<Drawable>,
                            isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(
                            resource: Drawable,
                            model: Any,
                            target: com.bumptech.glide.request.target.Target<Drawable>,
                            dataSource: DataSource,
                            isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                })
                .into(imageView)


    }

    fun setImageUsingGlide(context: Context, imageUrl: Int?, imageView: ImageView) {
        val requestOptions = RequestOptions().apply {
            placeholder(R.color.borderColor)
            error(R.color.borderColor)
            diskCacheStrategy(DiskCacheStrategy.ALL)
        }


        Glide.with(context)
            .setDefaultRequestOptions(requestOptions)
            .load(imageUrl ?: "")
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(imageView)


    }

    fun GlidewithLoadUriImageNew(context: Context?, url: Uri?, imageView: ImageView) {
        if (context != null) {
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_user_placeholder)
            requestOptions.error(R.drawable.ic_user_placeholder)
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE)
            requestOptions.skipMemoryCache(true)
            Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(url ?: "")
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {


                        return false
                    }
                })
                .into(imageView)
        }
    }

    fun setPrecesionFormate(number: Double?): String {
       // val form = DecimalFormat("##,##,##,##,###.##")
        val form = DecimalFormat("##,##,##,##,##0.00")
        return form.format(number)
    }

    fun roundOffDecimal(number: Double?): Double? {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

    fun roundOffBIGDecimal(number: Double?): Int? {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toInt()
    }

    fun redirectToMap(context: Context, pickupLocation: ProductCategoryWiseListPickupLocation?) {
        if (pickupLocation != null) {
            val dLatitude = BigDecimal(SharedPrefrence.getlat(context)).toDouble()
            val dLongitude = BigDecimal(SharedPrefrence.getlang(context)).toDouble()

            val pLatitude = pickupLocation?.coordinates?.get(1).toString()
            val pLongitude = pickupLocation?.coordinates?.get(0).toString()
            var uri =
                "http://maps.google.com/maps?saddr=$dLatitude,$dLongitude+&daddr=$pLatitude,$pLongitude&directionsmode=driving&zoom=14&views=traffic"

            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(uri)
            )
            try {
                (context as Activity).startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                try {
                    val unrestrictedIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    (context as Activity).startActivity(unrestrictedIntent)
                } catch (innerEx: ActivityNotFoundException) {
                    showToast(
                            context = context,
                            mesaage = "Please install a maps application",
                            time = Toast.LENGTH_SHORT
                    )
                }
            }
        }
    }


    fun covertTimeToText(dataDate: String?): String? {
        var convTime: String? = null
        val prefix = ""
        val suffix = "Ago"
        try {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            dateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val pasTime = dateFormat.parse(dataDate)
            val nowTime = Date()
            val dateDiff = nowTime.time - pasTime.time
            val second: Long = TimeUnit.MILLISECONDS.toSeconds(dateDiff)
            val minute: Long = TimeUnit.MILLISECONDS.toMinutes(dateDiff)
            val hour: Long = TimeUnit.MILLISECONDS.toHours(dateDiff)
            val day: Long = TimeUnit.MILLISECONDS.toDays(dateDiff)
            if (second < 60) {
                convTime = "$second Seconds $suffix"
            } else if (minute < 60) {
                convTime = "$minute Minutes $suffix"
            } else if (hour < 24) {
                convTime = "$hour Hours $suffix"
            } else if (day >= 7) {
                convTime = if (day > 360) {
                    (day / 360).toString() + " Years " + suffix
                } else if (day > 30) {
                    (day / 30).toString() + " Months " + suffix
                } else {
                    (day / 7).toString() + " Week " + suffix
                }
            } else if (day < 7) {
                convTime = "$day Days $suffix"
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("ConvTimeE", e.message?:"")
        }
        return convTime
    }


    fun getConvertDateWithoutTime(time: String): String {
       /* val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)
            val birthdateFormat = SimpleDateFormat("dd MMM yyyy")
            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""*/

        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)
            val birthdateFormat = SimpleDateFormat("dd/MM/yyyy")
            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }


    fun getIsoDate(time: Long): String {

        /*val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        //dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = Date(time)
            // val longMiliSeconds = date.time
            val birthdateFormat = SimpleDateFormat("dd MMM yyyy")
            //birthdateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val formatedDate = birthdateFormat.format(date)
            //val timeString: String = DateUtils.getRelativeTimeSpanString(longMiliSeconds, System.currentTimeMillis(), 0).toString()
            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""*/

        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        //dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = Date(time)
            // val longMiliSeconds = date.time
            val birthdateFormat = SimpleDateFormat("dd/MM/yyyy")
            //birthdateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val formatedDate = birthdateFormat.format(date)
            //val timeString: String = DateUtils.getRelativeTimeSpanString(longMiliSeconds, System.currentTimeMillis(), 0).toString()
            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }

    fun sendDeliveryDate(dte:String):String{

        /*val dateFormat = SimpleDateFormat("dd MMM yyyy")
        //dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = Date(dte)
            // val longMiliSeconds = date.time
            val birthdateFormat = SimpleDateFormat("yyyy-MM-dd")
            //birthdateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val formatedDate = birthdateFormat.format(date)
            //val timeString: String = DateUtils.getRelativeTimeSpanString(longMiliSeconds, System.currentTimeMillis(), 0).toString()
            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""*/


        var datetime: String? = null
        val inputFormat: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
       // val inputFormat: SimpleDateFormat = SimpleDateFormat("dd MMM yyyy")
        val d = SimpleDateFormat("yyyy-MM-dd")
        try {
            val convertedDate: Date = inputFormat.parse(dte)
            datetime = d.format(convertedDate)
            return datetime
        } catch (e: ParseException) {
        }
        return ""

    }



    fun getDateTime(year: Int, month: Int, dayOfMonth: Int): Long {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        return calendar.time.time
    }

    fun makeLinks(textView: TextView, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        val spannableString = SpannableString(textView.text)
        for (i in links.indices) {
            val clickableSpan = clickableSpans[i]
            val link = links[i]

            val startIndexOfLink = textView.text.toString().indexOf(link)
            spannableString.setSpan(
                    clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            //val boldSpan = StyleSpan(Typeface.BOLD)
            val foregroundSpan = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ForegroundColorSpan(textView.context.getColor(R.color.login_btn_color))
            } else {
                //  ForegroundColorSpan(textView.context.getColor(R.color.pink_theme_color))
            }
            spannableString.setSpan(
                    foregroundSpan, startIndexOfLink,
                    startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            /*spannableString.setSpan(boldSpan, startIndexOfLink,
                startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)*/
        }

        textView.highlightColor =
                Color.TRANSPARENT // prevent TextView change background when highlight

        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spannableString, TextView.BufferType.SPANNABLE)
    }


    fun getConvertDate(time: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)

            val birthdateFormat = SimpleDateFormat("dd MMM yyyy, hh:mm:ss a")

            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }

    fun getMaskMobileNumber(mobNum:String?,isEmail:Boolean,context: Context):String{
        if(!isEmail) {

            val lengthmob = mobNum.toString().length
            if(lengthmob >= 10){

                val newString = mobNum?.drop(lengthmob - 10)
                val newFirststr = newString?.take(2)
                val newLatStr = newString?.takeLast(2)
                val newxxStr ="XXXXXX"

                val newStrlatest = newFirststr+newxxStr+newLatStr
                return String.format(context.getString(R.string.txt_description_sms), newStrlatest)

            }
        }else{
            return String.format(context.getString(R.string.txt_description_email), mobNum)
        }

        return ""
    }


    fun setSpanWithClick(
        context: Context,
        view: TextView,
        fulltext: String,
        subtext: String,
        color: Int,
        phoneNumber: String
    ) {

        view.setText(fulltext, TextView.BufferType.SPANNABLE)
        val str: Spannable = view.text as Spannable
        val i = fulltext.indexOf(subtext)


        str.setSpan(
            object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val i = Intent(
                        Intent.ACTION_DIAL,
                        Uri.parse("tel:+$phoneNumber")
                    )
                    context.startActivity(i)
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.color = ContextCompat.getColor(context, color)
                }
            }, i, i + subtext.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )
        view.setMovementMethod(LinkMovementMethod.getInstance())
    }


    fun setSpanColor(
        context: Context,
        view: TextView,
        fulltext: String?,
        subtext: String?,
        color: Int
    ) {
        view.setText(fulltext, TextView.BufferType.SPANNABLE)
        val str: Spannable = view.text as Spannable
        val i = subtext?.let { fulltext?.indexOf(it) }
        if (i != null) {
            str.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, color)),
                i,
                i + subtext.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }


     fun isValidUsername(name: String?): Boolean {

        // Regex to check valid username.
        val regex = "^([A-Za-z]+)(\\s[A-Za-z]+)*\\s?\$"

        // Compile the ReGex
        val p = Pattern.compile(regex)

        // If the username is empty
        // return false
        if (name == null) {
            return false
        }

        // Pattern class contains matcher() method
        // to find matching between given username
        // and regular expression.
        val m = p.matcher(name)

        // Return if the username
        // matched the ReGex
        return m.matches()
    }

    fun getTimeInLong(time: String): Long {
        //2020-06-25T12:41:00
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)
            return date.time
        } catch (e: ParseException) {

        }
        return Date().time
    }

    fun comparedateToday(enddate:String,currentDate:String):Boolean{
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val endDate = sdf.parse(enddate)
        val crntDate = sdf.parse(currentDate)


        val result = endDate.compareTo(crntDate)
        //println("result: $result")

        if (result == 0) {
            println("Date1 is equal to Date2")
            return true
        } else if (result > 0) {
            println("Date1 is after Date2")
            return true
        } else if (result < 0) {
            return false
            // println("Date1 is before Date2")
        } else {
            return false
        }
        return false
    }

    fun comparenddateToStartday(enddate:String,startDate:String):Int{
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val endDate = sdf.parse(enddate)
        val strDate = sdf.parse(startDate)


        val result = endDate.compareTo(strDate)
        val results =  ((endDate.time - strDate.time)/(24 * 60 * 60 * 1000)).toInt()
        //println("result: $result")


        return results
    }

    fun getDateTimeMilLi(vdate:String):Long{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val vfdate = sdf.parse(vdate)
        val results =  vfdate.time
        //println("result: $result")
        return results
    }

    fun getDateShangeQtyAssign(vdate:String):String{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        try {
            val date = sdf.parse(vdate)
            val format = SimpleDateFormat("dd/MM/yyyy");
            val dateString = format.format(date);
            return dateString
        } catch (e: ParseException) {

        }
        return ""
    }

    fun getParsedSendServerFormate(time: String): String {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)
            val format = SimpleDateFormat("yyyy-MM-dd");
            val dateString = format.format(date);
            return dateString
        } catch (e: ParseException) {

        }
        return ""
    }

    fun getAddHoursafter(time: String,hours:Int):String?{
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")


        try {
            val date = dateFormat.parse(time)
            val c = Calendar.getInstance()
            try {
                c.time = date
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(
                Calendar.HOUR,
                hours
            ) // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE

            val sdf1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val output = sdf1.format(c.time)
            return output.toString()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }


    fun getAddDaysafter(time: String):String?{
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")


        try {
            val date = dateFormat.parse(time)
            val c = Calendar.getInstance()
            try {
                c.time = date
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(
                Calendar.DATE,
                1
            ) // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE

            val sdf1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val output = sdf1.format(c.time)
            return output.toString()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }

    fun getAdd30Daysafter(time: String):String?{
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")


        try {
            val date = dateFormat.parse(time)
            val c = Calendar.getInstance()
            try {
                c.time = date
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(
                Calendar.DATE,
                30
            ) // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE

            val sdf1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val output = sdf1.format(c.time)
            return output.toString()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }

    fun comparedateTodayNew(startdate:String,currentDate:String):Boolean{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val srtDate = sdf.parse(startdate)
        val crntDate = sdf.parse(currentDate)

        val format = SimpleDateFormat("yyyy-MM-dd")
        val SdateString = format.format(srtDate)
        val sDatev = format.parse(SdateString)


        val CdateString = format.format(crntDate)
        val cDatev = format.parse(CdateString)


        val result = sDatev.compareTo(cDatev)
        //println("result: $result")

        if (result == 0) {
            println("Date1 is equal to Date2")
            return true
        } else if (result > 0) {
            println("Date1 is after Date2")
            return false
        } else if (result < 0) {
            return false
            // println("Date1 is before Date2")
        } else {
            return false
        }
        return false
    }


    fun comparetwoDates(startdate:String,currentDate:String):Int{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val srtDate = sdf.parse(startdate)
        val crntDate = sdf.parse(currentDate)

        val format = SimpleDateFormat("yyyy-MM-dd")
        val SdateString = format.format(srtDate)
        val sDatev = format.parse(SdateString)


        val CdateString = format.format(crntDate)
        val cDatev = format.parse(CdateString)


        val result = sDatev.compareTo(cDatev)
        //println("result: $result")


        return result
    }

    fun getConvertDateTime(time: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

        try {
            val date = dateFormat.parse(time)

            val birthdateFormat = SimpleDateFormat("dd MMM yyyy, hh:mm:ss a")

            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }

    fun dateRangeError(context:Context,day:Int,txtVW:TextView){

        if(day == 1){
            txtVW.text = String.format(context.getString(R.string.date_range_info_oneday), day)
        }else{
            txtVW.text = String.format(context.getString(R.string.date_range_info_onedays), day)
        }

    }

    fun dateRangeErrorTost(context:Context,day:Int,txtVW:TextView){

        if(day == 1){

            showToast(context,String.format(context.getString(R.string.date_range_info_oneday), day),
                Toast.LENGTH_SHORT)

        }else{
            showToast(context,String.format(context.getString(R.string.date_range_info_onedays), day),
                Toast.LENGTH_SHORT)
        }

    }
}