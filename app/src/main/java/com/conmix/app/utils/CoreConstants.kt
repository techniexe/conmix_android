package comCoreConstants.conmix.utils


/**
 * Created by Hitesh Patel on 24,February,2021
 */
object CoreConstants {

    class Intent {
        companion object {
            const val INTENT_DELIVERY_BUTTON_INVISIBLE = "deliverybutton"
            const val INTENT_AUTH_ERROR = "authError"
            const val INTENT_MOB_NUMBER = "mobNum"
            const val INTENT_EMAIL_ADDRESS = "emailadd"
            const val INTENT_PRODUCT_ID = "productId"
            const val INTENT_SUB_CATEGORY_ID = "subcategoryId"
            const val ISFROMPRODUCTDETAIL = "productDetail"
            const val INTENT_ABOUT_US_URL = "aboutusurl"
            const val INTENT_ABOUT_US_TITLE = "aboutustitle"
            const val INTENT_GRADE_ID="gradeId"
            const val INTENT_CONCRETE_GRADE_TITLE = "concrategradetitle"
            const val INTENT_CONCRETE_GRADE_OBJ = "concrategradeobj"
            const val INTENT_CONCRETE_GRADE_ID = "concrategradeid"
            const val INTENT_VENDER_ID = "venderId"
            const val INTENT_REVIEW_LIST = "intentreviewlist"
            const val INTENT_ORDER_ITEM_ID = "orderitemId"
            const val INTENT_TRACK_ID = "trackId"
            const val INTENT_ORDER_DIAPLAY_ID = "display"
            const val INTENT_CREATED_DATE = "createddate"
            const val INTENT_ORDER_ID = "orderId"
            const val INTENT_CHANGE_PASSWORD="changepassword"




        }
    }

    class FRAGMENTS {
        companion object {
            const val HOME = "HOMEFRAGMENT"
            const val WISHLIST = "WISHLIST"
            const val CART = "CARTFRAGMENT"
            const val PROFILE = "PROFILE"
            const val NOTIFICATION = "NOTIFICATION"

        }
    }

    class ORDERSTATUS {
        companion object {
            const val PROCESSING = "PROCESSING"
            const val TRUCK_ASSIGNED_FOR_PICKUP = "TRUCK_ASSIGNED_FOR_PICKUP"
            const val TM_ASSIGNED = "TM_ASSIGNED"
            const val PICKUP = "PICKUP"
            const val DELAYED = "DELAYED"
            const val DELIVERED = "DELIVERED"
            const val REJECTED = "REJECTED"
            const val CPTRUCK_ASSIGNED = "CP_ASSIGNED"

        }
    }

    class NOTIFICATIONTYPE {
        companion object {
            //Order placed
            const val ORDERPLACE = 1
            const val ORDERPLACETYPEORDERPLACE = "ORDER PLACED"

            const val ORDERPROCESS = 2
            const val ORDERPLACETYPEORDERPROCESS = "ORDER PROCESS"

            const val ORDERACCEPT = 3
            const val ORDERACCEPTTYPE = "ORDER ACCEPT BY VENDOR"

            const val ASSIGNQTY= 4
            const val ORDERASSIGNQTYTYPE = "ASSIGN QUANTITY"

            const val orderRejectedForBuyer = 5
            const val ORDERREJECTEDTYPE = "ORDER REJECTED BY VENDOR"

            const val ORDERCONFIRM = 6
            const val ORDERPLACETYPEORDERCONFIRM = "ORDER CONFIRM"

            const val TRUCKASSIGNTOORDERFORBUYER = 7
            const val ORDERTRUCKASSIGNED = "TM ASSIGNED"


            const val pickupOrderForBuyer = 8
            const val ORDERPICKUP = "ORDER PICKUP"

            const val orderItemRejectForBuyer = 9
            const val ORDERITEMREJECT = "PRODUCT REJECT"

            const val orderRejectForBuyer = 10
            const val ORDERREJECT = "ORDER REJECT"


            const val orderDelayForBuyer = 11
            const val ORDERDELAY = "ORDER DELAY"

            const val partiallyOrderDeliveredForBuyer = 12
            const val ORDERPARTIALLYORDERDELIVER = "PARTIALLY PRODUCT DELIVERED"

            const val orderFullyProductDeliver = 13
            const val orderFullyProductDeliverTitle = "FULLY PRODUCT DELIVERED"

            const val orderDeliveredForBuyer = 14
            const val ORDERDELIVERED = "ORDER DELIVERED"

            const val partiallyOrderCancelled = 15
            const val ORDERPARTIALLYCANCELLED = "PARTIALLY ORDER CANCELLED"

            const val orderCancelledBuyer = 16
            const val orderCancelled = "ORDER CANCELLED"

            const val orderShortCloseBuyer = 17
            const val orderShortClose = "ORDER SHORT CLOSE"

            const val CPASSIGNTOORDERFORBUYER = 18
            const val ORDERCPASSIGNED = "CP ASSIGNED"

            const val reminderBeforSevenDayForClientAssignedQty = 19
            const val reminderBeforSevenDayForClientAssignedQtyTITLE="REMINDER QTY ASSIGN"

            const val reminderBeforOneDayForClientAssignedQty = 20
            const val reminderBeforOneDayForClientAssignedQtyTITLE="REMINDER QTY ASSIGN"

            const val orderCpPickup = 21
            const val orderCpPickupTITLE="CP PICKUP"


            const val orderCpDeliver = 22
            const val orderCpDeliverTITLE="CP DELIVERED"

            const val productlapsed = 23
            const val productlapsedTITLE="PRODUCT LAPSED"

            const val orderlapsed = 24
            const val orderlapsedTITLE="ORDER LAPSED"

            const val orderCpDelay = 25
            const val orderCpDelayTITLE="CP DELAY"

            const val replyOfSupportTicketByAdmin = 26
            const val ReplyOfSupportTicketByAdminTitle="SUPPORT TICKET - REPLY BY ADMIN"

            const val resolveSupportTicketByAdmin = 27
            const val ResolveSupportTicketByAdminTitle="SUPPORT TICKET - RESOLVE BY ADMIN"


        }
    }
}