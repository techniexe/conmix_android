package com.conmix.app.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.conmix.app.data.ProductCategoryWiseDetailsProductCategory
import com.conmix.app.data.StateModel
import com.conmix.app.databinding.RegistrationSpinnerRowBinding
import android.widget.TextView
import com.conmix.app.R


/**
 * Created by Hitesh Patel on 04,October,2021
 */
class SelectStateBillAdapter (ctx: Context, moods: ArrayList<StateModel>?) :
    ArrayAdapter<StateModel>(ctx, 0, moods!!) {

    private var _binding: RegistrationSpinnerRowBinding? = null

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {

        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val stateModel = getItem(position)

        _binding = RegistrationSpinnerRowBinding.inflate(LayoutInflater.from(context),parent,false)

        val view = _binding!!.root
        _binding!!.titleSpinnerItem.setText(stateModel?.state_name.toString())
        return view
    }
}