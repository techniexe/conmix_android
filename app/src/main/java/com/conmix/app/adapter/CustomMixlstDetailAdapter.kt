package com.conmix.app.adapter

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.CartItemLst
import com.conmix.app.data.CartObjDta
import com.conmix.app.data.CustomMixResObjData
import com.conmix.app.data.ProductCategoryWiseListPickupLocation
import com.conmix.app.dialogs.UpdateQtyDialog
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 02,April,2021
 */
class CustomMixlstDetailAdapter(val context: Context,
                                val designList: ArrayList<CustomMixResObjData>,
                                val title:String?,
                                var supportFragmentManager: FragmentManager) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), UpdateQtyDialog.DialogToFragment {
    private val ROW_TYPE = 1


    var mListener: SelectCustomMixInterface? = null
    lateinit var pickupLocation: ProductCategoryWiseListPickupLocation


    fun addLoadMoreLikes(list: java.util.ArrayList<CustomMixResObjData>) {
        designList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_mix_detail_lst_row, parent, false)


        return designMixViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {

        return ROW_TYPE
    }
    override fun getItemCount(): Int {
        return designList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is designMixViewHolder) {
            val dta = designList[position]

            if(position == 0 && designList.size > 1 ){
                holder.suggestion_txt_ly.visibility = View.VISIBLE
                holder.suggestion_view_ly.visibility = View.VISIBLE
            }else{
                holder.suggestion_txt_ly.visibility = View.GONE
                holder.suggestion_view_ly.visibility = View.GONE
            }

            holder.btnAddToCart.visibility = View.VISIBLE
            //holder.item_grade_main.text = dta.concrete_grade_name+" - Custom Mix"

            val sellingPrice = dta.selling_price_with_margin?.let { Utils.setPrecesionFormate(it) }

            if(sellingPrice != null){
                val spanText = SpannableStringBuilder("Price : ₹"+sellingPrice?:"0"+" / Cu.Mtr ")
                spanText.setSpan( StyleSpan(Typeface.BOLD), 7, (spanText.length-9), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                spanText.setSpan(AbsoluteSizeSpan(14,true),7,(spanText.length-9), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                holder.item_price.setText(spanText, TextView.BufferType.SPANNABLE)
            }else{
                holder.item_price.visibility = View.GONE
            }


            //holder.item_price.text = "Price : ₹"+sellingPrice.toString() +" / Cu.Mtr"
            holder.item_grade_by_group_name.text= dta.company_name?:""
            var strBuffer1 = StringBuffer()

            if(dta?.line1?.isEmpty() == true) {
                strBuffer1.append("")
            }else{
                strBuffer1.append(dta?.line1?:"")
            }
            if(dta?.city_name?.isEmpty() == true) {
                strBuffer1.append("")
            }else{
                strBuffer1.append(", ")
                strBuffer1.append(dta?.city_name?:"")
            }
            if(dta?.state_name?.isEmpty() == true) {
                strBuffer1.append("")
            }else{
                strBuffer1.append(", ")
                strBuffer1.append(dta?.state_name?:"")
            }


            holder.item_location.text = strBuffer1.toString()

            if(dta.with_TM?:false && dta.with_CP?:false){
                holder.item_with_tm_pump.text = "With TM & CP"
            }else if(dta.with_TM?:false){
                holder.item_with_tm_pump.text = "With TM"
            }else if(dta.with_CP?:false){
                holder.item_with_tm_pump.text = "With CP"
            }else{
                holder.item_with_tm_pump.text = "Without TM & CP"
            }
            holder.design_mix_title_txt.text = dta.concrete_grade_name+" - Custom Mix"
            holder.cement_value_txt.text = dta?.cement_quantity?.let { Utils.setPrecesionFormate(it) }
            holder.sand_value_txt.text = dta?.sand_quantity?.let { Utils.setPrecesionFormate(it) }

            if(dta?.fly_ash_quantity?.toString().isNullOrEmpty()){
                holder.flyash_value_txt.text = "-"
            }else{
                holder.flyash_value_txt.text = dta?.fly_ash_quantity?.let { Utils.setPrecesionFormate(it) }
            }


            if(dta?.admix_quantity?.toString().isNullOrEmpty()){
                holder.admixure_value_txt.text = "-"
            }else{
                holder.admixure_value_txt.text = dta?.admix_quantity?.let { Utils.setPrecesionFormate(it) }
            }


            holder.admixure_value_txt.text = dta?.admix_quantity?.let { Utils.setPrecesionFormate(it) }
            holder.aggregate1TitleTxt.text = dta?.aggregate1_sub_category_name+" (Kg) : "
            holder.aggregate1_value_txt.text = dta?.aggregate1_quantity?.let { Utils.setPrecesionFormate(it) }
            holder.aggregate2TitleTxt.text = dta?.aggregate2_sub_category_name+" (Kg) : "
            holder.aggregate2_value_txt.text = dta?.aggregate2_quantity?.let { Utils.setPrecesionFormate(it) }
            holder.water_value_txt.text = dta?.water_quantity?.let { Utils.setPrecesionFormate(it) }
            holder.grade_value_txt.text = dta.concrete_grade_name

            holder.tvKm.text = String.format(
                    context.getString(R.string.txt_km),
                    Utils.setPrecesionFormate(dta?.distance)
            )

           // holder.design_mix_desc_value.text = "Cement Brand : "+dta?.cement_brand_name+" , Sand Source : "+dta?.sand_source_name+" , Aggregate Source : "+dta.aggregate_source_name+" , Admixture Brand : "+dta.admix_brand_name+" , Fly Ash Source Name : "+dta.fly_ash_source_name+" , Water : Regular"


            val stringBuffer = StringBuffer()
            stringBuffer.append("Cement Brand : ")
            stringBuffer.append(dta.cement_brand_name?:" - ")
            stringBuffer.append("; Sand Source : ")
            stringBuffer.append(dta.sand_source_name?:" - ")
            stringBuffer.append("; Aggregate Source : ")
            stringBuffer.append(dta.aggregate_source_name?:" - ")
            stringBuffer.append("; Admixture Brand : ")
            stringBuffer.append(dta.admix_brand_name?:" - ")
            stringBuffer.append("; Fly Ash Source Name : ")
            stringBuffer.append(dta.fly_ash_source_name?:" - ")


            holder.design_mix_desc_value.text = stringBuffer.toString()


            if(dta?.quantity != null){
                holder.tvProductCount.text = dta?.quantity.toString()
            }else{
                holder.tvProductCount.text = "3"
            }

            holder.tvProductCount?.setOnClickListener {
                var updateQty = UpdateQtyDialog(holder.tvProductCount.text.toString().toInt(),position)
                updateQty.setListener(this@CustomMixlstDetailAdapter)
                updateQty.show(supportFragmentManager, "updateQty")
            }

            holder.tvKm.setOnClickListener {
                pickupLocation = dta?.location!!
                if (::pickupLocation.isInitialized)
                    Utils.redirectToMap(context, pickupLocation)
            }

            holder.icAdd.setOnClickListener {
                var totalMaxQty = 100
                var productQty = (dta?.quantity?:"3").toString()?.toInt()
                if (productQty >= totalMaxQty) {
                    Utils.showToast(
                            context,
                        context.getString(R.string.maxmumqty100),
                            Toast.LENGTH_LONG
                    )
                } else {
                    productQty = holder.tvProductCount.text.toString().toInt()
                    productQty += 1
                    holder.tvProductCount.setText(productQty.toString())
                    /*dta?.quantity = productQty
                    notifyItemChanged(position)*/
                    mListener?.onProductAddedCart(dta,position,productQty)
                }
            }

            holder.icRemove.setOnClickListener {

                var productQty = holder.tvProductCount.text.toString()?.toInt()
                if (productQty <= 3) {
                    Utils.showToast(
                            context,
                            context.getString(R.string.mimmumqty3),
                            Toast.LENGTH_LONG
                    )
                } else {
                    productQty = holder.tvProductCount.text.toString().toInt()
                    productQty -= 1
                    holder.tvProductCount.setText(productQty.toString())
                    /*dta?.quantity = productQty
                    notifyItemChanged(position)*/
                    mListener?.onProductRemovedFromCart(dta,position,productQty)
                }
            }

            holder.btnAddToCart.setOnClickListener {
                mListener?.onClickDesignListAddtoCart(dta,position,holder.tvProductCount.text.toString().toInt())
            }
        }

    }

    internal inner class designMixViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var lnly: LinearLayout = view.findViewById<LinearLayout>(R.id.lnly)
        //var item_grade_main: TextView = view.findViewById<TextView>(R.id.item_grade_main)
        var item_location:TextView = view.findViewById<TextView>(R.id.item_location)
        var item_price: TextView = view.findViewById<TextView>(R.id.item_price)
        var item_grade_by_group_name: TextView = view.findViewById<TextView>(R.id.item_grade_by_group_name)
        var item_with_tm_pump: TextView = view.findViewById<TextView>(R.id.item_with_tm_pump)
        var design_mix_title_txt: TextView = view.findViewById<TextView>(R.id.design_mix_title_txt)
        var cement_value_txt: TextView = view.findViewById<TextView>(R.id.cement_value_txt)
        var sand_value_txt: TextView = view.findViewById<TextView>(R.id.sand_value_txt)
        var flyash_value_txt: TextView =  view.findViewById<TextView>(R.id.flyash_value_txt)
        var admixure_value_txt: TextView =  view.findViewById<TextView>(R.id.admixure_value_txt)
        var aggregate1_value_txt: TextView = view.findViewById<TextView>(R.id.aggregate1_value_txt)
        var aggregate2_value_txt: TextView = view.findViewById<TextView>(R.id.aggregate2_value_txt)
        var water_value_txt: TextView =  view.findViewById<TextView>(R.id.water_value_txt)
        var grade_value_txt: TextView =  view.findViewById<TextView>(R.id.grade_value_txt)
        var aggregate1TitleTxt: TextView =  view.findViewById<TextView>(R.id.aggregate1_title_txt)
        var aggregate2TitleTxt: TextView =  view.findViewById<TextView>(R.id.aggregate2_title_txt)
        var icRemove: ImageView = view.findViewById<ImageView>(R.id.icRemove)
        var icAdd: ImageView = view.findViewById<ImageView>(R.id.icAdd)
        var tvProductCount: TextView =  view.findViewById<TextView>(R.id.tvProductCount)
        var design_mix_desc_value: TextView = view.findViewById<TextView>(R.id.design_mix_desc_value)
        var btnAddToCart:AppCompatButton = view.findViewById<AppCompatButton>(R.id.btnAddToCart)
        var tvKm:TextView = view.findViewById<TextView>(R.id.tvKm)
        var suggestion_txt_ly:TextView = view.findViewById(R.id.suggestion_txt_ly)
        var suggestion_view_ly:View = view.findViewById(R.id.suggestion_view_ly)


    }

    fun setListener(listener: SelectCustomMixInterface) {

        mListener = listener
    }

    interface SelectCustomMixInterface{
        fun onClickDesignListAddtoCart(site: CustomMixResObjData, position: Int, qty:Int)
        fun onProductAddedCart(site: CustomMixResObjData,position: Int, qty:Int)
        fun onProductRemovedFromCart(site: CustomMixResObjData,position: Int, qty:Int)
        fun onQtyDialog(site: CustomMixResObjData,position: Int, qty:Int)
    }

    override fun dialogDismiss() {

    }

    override fun dialogSave(count: Int, position: Int) {
        /*designList.get(position).quantity = count
        notifyItemChanged(position)*/

        mListener?.onQtyDialog(designList.get(position),position,count)
    }

}