package com.conmix.app.adapter

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.bottomsheet.GradeBottomsheet
import com.conmix.app.data.DesignListObj
import com.conmix.app.utils.Utils
import java.math.RoundingMode


/**
 * Created by Hitesh Patel on 17,March,2021
 */
class DesignMixListAdapter(var supportFragmentManager: FragmentManager, val context: Context,
                           val designList: ArrayList<DesignListObj>, val title:String?

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false

    var loadViewHolder: LoadHolder? = null
    var mListener: SelectDesignMixInterface? = null

    fun addLoadMoreLikes(list: java.util.ArrayList<DesignListObj>) {
        designList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.concreate_grade_list_row, parent, false)
            designMixViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == designList.size) {
            return LOAD_TYPE
        } else {
            return ROW_TYPE
        }
        return ROW_TYPE
    }
    override fun getItemCount(): Int {
        return designList.size+1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is designMixViewHolder) {
            val dta = designList[position]
           // holder.item_grade_main.text = title
            holder.item_grade_by_Mode_Tyape_Name.text = dta.product_name.toString()
            holder.item_grade_by_group_name.text = dta.vendor?.company_name

            if(dta.vendor_media?.size?:0 > 0){
                dta.vendor_media?.forEach {
                    if(it.type =="PRIMARY"){
                        Utils.setImageUsingGradeGlide(context,it.media_url,holder.grade_img)
                    }
                }
            }


            if(dta.selling_price_with_margin != null){
                val sellingPrice = dta.selling_price_with_margin?.let { Utils.setPrecesionFormate(it) }
                val spanText = SpannableStringBuilder("Price : ₹ "+sellingPrice+" / Cu.Mtr ")
                spanText.setSpan( StyleSpan(Typeface.BOLD), 8, (spanText.length-9), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                spanText.setSpan(AbsoluteSizeSpan(14,true),8,(spanText.length-9),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                holder.item_sales_price.setText(spanText, TextView.BufferType.SPANNABLE)
            }else{
                val sellingPrice = 0.0
                val spanText = SpannableStringBuilder("Price : ₹ "+sellingPrice+" / Cu.Mtr ")
                spanText.setSpan( StyleSpan(Typeface.BOLD), 8, (spanText.length-9), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                spanText.setSpan(AbsoluteSizeSpan(14,true),8,(spanText.length-9),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                holder.item_sales_price.setText(spanText, TextView.BufferType.SPANNABLE)
            }


           // holder.item_sales_price.text = "Price : ₹"+sellingPrice+" / Cu.Mtr"
           // holder.outRatingtxt.text = Utils.setPrecesionFormate(dta.vendor?.rating?:0.0)

            if(dta.vendor?.rating != null){
                holder.outRatingtxt.text = dta.vendor?.rating?.toBigDecimal()?.setScale(1, RoundingMode.UP)?.toDouble().toString()
            }else{
                holder.outRatingtxt.text = "0.0"
            }

            holder.outFromRatingtxt.text= "/5"

            holder.grade_info_img?.setOnClickListener {
                popup(dta,title)
            }

            holder.lnly.setOnClickListener {
                mListener?.onClickDesignList(dta)
            }


        } else if (holder is LoadHolder) {
            loadViewHolder = holder as LoadHolder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }
    }


    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }


    inner class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar

        init {
            progressBar = view.findViewById<ProgressBar>(R.id.progressBarReview) as ProgressBar
        }

    }

    internal inner class designMixViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var lnly:LinearLayout = view.findViewById<LinearLayout>(R.id.lnly)
       // var item_grade_main: TextView = view.findViewById<TextView>(R.id.item_grade_main)
        var grade_img: ImageView = view.findViewById<ImageView>(R.id.grade_img)
        var item_grade_by_company_name: TextView = view.findViewById<TextView>(R.id.item_grade_by_company_name)
        var item_grade_by_group_name: TextView = view.findViewById<TextView>(R.id.item_grade_by_group_name)
        var item_sales_price: TextView = view.findViewById<TextView>(R.id.item_sales_price)
        var ratingbar: ImageView = view.findViewById<ImageView>(R.id.ratingbar)
        var outRatingtxt:TextView = view.findViewById<TextView>(R.id.outRatingtxt)
        var outFromRatingtxt:TextView = view.findViewById<TextView>(R.id.outFromRatingtxt)
        var grade_info_img:ImageView = view.findViewById<ImageView>(R.id.grade_info_img)
        var item_grade_by_Mode_Tyape_Name:TextView = view.findViewById(R.id.item_grade_by_Mode_Tyape_Name)

    }

    fun setListener(listener: SelectDesignMixInterface) {

        mListener = listener
    }

    interface SelectDesignMixInterface{
        fun onClickDesignList(site: DesignListObj)
    }


    private fun popup(dta: DesignListObj, title: String?) {

        var infoOption = GradeBottomsheet(dta, title!!)

        infoOption.show(supportFragmentManager, "BottomDialogFragment")



    }
}