package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.conmix.app.databinding.RegistrationSpinnerRowBinding
import java.util.HashMap


/**
 * Created by Hitesh Patel on 17,August,2021
 */
class SelectTimeAdapter(ctx: Context, moods: ArrayList<HashMap<Int, String>>?) : ArrayAdapter<HashMap<Int, String>>(ctx, 0, moods!!) {

    private var _binding: RegistrationSpinnerRowBinding? = null

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val stateModel = getItem(position)

        _binding = RegistrationSpinnerRowBinding.inflate(LayoutInflater.from(context),parent,false)

        val view = _binding!!.root
        _binding!!.titleSpinnerItem.setText(stateModel?.get(position).toString())
        return view
    }

    private fun getPosition(position: Int): Int {
            return  position
    }
}