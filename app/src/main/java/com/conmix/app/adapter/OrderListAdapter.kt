package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.OrderLstObj
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class OrderListAdapter(
        val context: Context,
        val myOrderList: ArrayList<OrderLstObj>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false

    var loadViewHolder: LoadHolder? = null
    var mListener: SelectMyOrderListInterface? = null

    fun addLoadMoreLikes(list: java.util.ArrayList<OrderLstObj>) {
        myOrderList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_order_row, parent, false)
            orderListViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == myOrderList.size) {
            return LOAD_TYPE
        } else {
            return ROW_TYPE
        }
        return ROW_TYPE
    }
    override fun getItemCount(): Int {
        return myOrderList.size+1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is orderListViewHolder) {
            val dta = myOrderList[position]




           /* holder.orderListRowLnLy?.setOnClickListener {

                mListener?.onClicked(dta?._id)



            }*/


            holder.orderIdNameTxt.text =String.format(context.getString(R.string.txt_order), dta?.display_id)

            val time = Utils.getConvertDate(dta?.created_at?:"")
            holder.orderDateTxt.text = "Order Date: "+time
            holder.totalPrice.text = "\u20B9"+ Utils.setPrecesionFormate(dta?.selling_price_With_Margin?:0.0).toString()

            holder.itemView?.setOnClickListener {

               /* val intent = Intent(context, OrderDetailActivity::class.java)
                intent.putExtra("OrderID", dta?._id)
                context.startActivity(intent)
                Bungee.slideLeft(context)*/

                mListener?.onClicked(dta?._id)

            }


        } else if (holder is LoadHolder) {
            loadViewHolder = holder as LoadHolder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }

    }


    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }


    inner class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar

        init {
            progressBar = view.findViewById<ProgressBar>(R.id.progressBarReview) as ProgressBar
        }

    }

    internal inner class orderListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var orderListRowLnLy: LinearLayout = view.findViewById(R.id.orderListRowLnLy)
        var orderIdNameTxt: TextView = view.findViewById(R.id.orderId)
        var orderDateTxt: TextView = view.findViewById(R.id.orderdate)
        var orderQtyTxt: TextView = view.findViewById(R.id.orderQtyTxt)
        var totalPrice: TextView = view.findViewById(R.id.totalPrice)



    }

    fun setListener(listener: SelectMyOrderListInterface) {

        mListener = listener
    }

    interface SelectMyOrderListInterface{
        fun onClicked(orderId:String?)
    }

}