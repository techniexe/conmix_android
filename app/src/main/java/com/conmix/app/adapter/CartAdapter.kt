package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.CartItemLst
import com.conmix.app.data.CartObjDta
import com.conmix.app.dialogs.BuyerInfoDetailDialog
import com.conmix.app.dialogs.ChooseTimeDialog
import com.conmix.app.dialogs.UpdateQtyDialog
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 13,April,2021
 */
class CartAdapter(
    var context: Context,
    var cartItemsSchema: CartObjDta?,
    var supportFragmentManager: FragmentManager) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>(), UpdateQtyDialog.DialogToFragment,
    ChooseTimeDialog.DialogToFragment {
    private var listener: ProductsAdapterInterface? = null
    var productQty = 0
    var totalMinQty = 0
    var totalMaxQty = 0
    private val ROW_TYPE = 1
    var dateCartItems: CartItemLst? = null


    //var productId: String? = null
    // var userId: String? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
        return CartItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cartItemsSchema?.items?.size!!
    }

    override fun getItemViewType(position: Int): Int {

        return ROW_TYPE
    }

    fun setProductsAdapterListener(mListner: ProductsAdapterInterface) {
       listener = mListner
    }
    interface ProductsAdapterInterface {
        fun onProductAddedCart(cartItems: CartItemLst?, cartItemsSchema: CartObjDta?)
        fun onProductRemovedFromCart(cartItems: CartItemLst?, cartItemsSchema: CartObjDta?)
        fun onProductDelete(index: Int, itemId: String?,cart_id:String?)
        fun onDateChange(date:String?, cartItems: CartItemLst?, cartItemsSchema: CartObjDta?,endt:String)
        fun onBuyerInfoAdd(cartItems: CartItemLst?)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if( holder is CartItemViewHolder){
            /*val dta = cartItemsSchema?.items?.get(position)
            var withTMPUMPTXT = "With TM & CP Available Date"

            if(dta?.with_TM?:false && dta?.with_CP?:false){
                withTMPUMPTXT = "With TM & CP Available Date"
            }else if(dta?.with_TM?:false){
                withTMPUMPTXT = "With TM Available Date"
            }else if(dta?.with_CP?:false){
                withTMPUMPTXT = "With CP Available Date"
            }else{
                withTMPUMPTXT = "Without TM & CP Available Date"
            }
*/
            val dta = cartItemsSchema?.items?.get(position)
            var withTMPUMPTXT = "Your RMC order with Transit Mixer & Concrete Pump is available on"

            if(dta?.with_TM?:false && dta?.with_CP?:false){
                withTMPUMPTXT = "Your RMC order with Transit Mixer & Concrete Pump is available on"
            }else if(dta?.with_TM?:false){
                withTMPUMPTXT = "Your RMC order with Transit Mixer is available on"
            }else if(dta?.with_CP?:false){
                withTMPUMPTXT = "Your RMC order with Concrete Pump is available on"
            }else{
                withTMPUMPTXT = "Sorry, Your RMC order with Transit Mixer & Concrete Pump is not available on"
            }



            if(dta?.design_mix_id != null){
                holder.tvTitle.text = dta?.concrete_grade_name
                holder.venderNameTxt.text = dta?.company_name?:""
                holder.tvPriceValue.text = String.format(
                        context.getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(dta?.unit_price_With_Margin?:0.0)
                )
                holder.tvProductCount.text = dta?.quantity?.toString()

                //Utils.setImageUsingGradeGlide(context,dta?.vendor_media?.get(0)?.media_url?:"",holder.imgCart)




                if(dta?.vendor_media?.size?:0 > 0 ){
                    Utils.setImageUsingGradeGlide(context,dta?.vendor_media?.get(0)?.media_url?:"",holder.imgCart)
                }else{
                    Utils.setImageUsingGradeGlide(context,"",holder.imgCart)
                }

                val datevalue = dta?.delivery_date?.let { Utils.getConvertDateWithoutTime(it) }

               /* if(dta?.suggestion?.size?:0 > 1){
                    holder.calendarlnly.setBackgroundResource(R.drawable.date_not_available_drawable)
                    holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_not_avilable_color))
                    holder.datetxt.text = dta?.suggestion?.get(0)?.ErrMsgForTM +" on Date : "+datevalue
                }else{
                    holder.calendarlnly.setBackgroundResource(R.drawable.date_available_square_drawable)
                    holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                    holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                }*/



                if(dta?.with_TM?:false){
                    holder.calendarlnly.setBackgroundResource(R.drawable.date_available_square_drawable)
                    holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                    holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                    holder.addBuyerlnly.visibility = View.GONE
                    holder.addBuyerTxt.visibility = View.GONE
                    holder.tmBuyerLnly.visibility = View.GONE
                }else{
                    val buyerInFo = dta?.TM_no
                    if(buyerInFo != null){
                        holder.calendarlnly.setBackgroundResource(R.drawable.date_available_square_drawable)
                        holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                        holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                        holder.addBuyerlnly.visibility = View.GONE
                        holder.addBuyerTxt.visibility = View.GONE
                        holder.tmBuyerLnly.visibility = View.GONE
                    }else{
                        holder.calendarlnly.setBackgroundResource(R.drawable.date_not_available_drawable)
                        holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_not_avilable_color))
                        holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                        holder.addBuyerlnly.visibility = View.GONE
                        holder.addBuyerTxt.visibility = View.VISIBLE
                        holder.tmBuyerLnly.visibility = View.GONE
                    }
                }


            }else if(dta?.custom_mix != null){

                holder.tvTitle.text = dta?.custom_mix?.concrete_grade_name
                holder.venderNameTxt.text = dta?.vendor_name
                holder.tvPriceValue.text = String.format(
                        context.getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(dta?.unit_price_With_Margin?:0.0)
                )
                holder.tvProductCount.text = dta?.quantity?.toString()
                val datevalue = dta?.delivery_date?.let { Utils.getConvertDateWithoutTime(it) }

                //Utils.setImageUsingGradeGlide(context,dta?.vendor_media?.get(0)?.media_url,holder.imgCart)


                if(dta?.vendor_media?.size?:0 > 0 ){
                    Utils.setImageUsingGradeGlide(context,dta?.vendor_media?.get(0)?.media_url?:"",holder.imgCart)
                }else{
                    Utils.setImageUsingGradeGlide(context,"",holder.imgCart)
                }

               /* if(dta?.suggestion?.size?:0 > 1){
                    holder.calendarlnly.setBackgroundResource(R.drawable.date_not_available_drawable)
                    holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_not_avilable_color))
                    holder.datetxt.text = dta?.suggestion?.get(0)?.ErrMsgForTM +" on Date : "+datevalue
                }else{
                    holder.calendarlnly.setBackgroundResource(R.drawable.date_available_square_drawable)
                    holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                    holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                }*/



                if(dta?.with_TM?:false){
                    holder.calendarlnly.setBackgroundResource(R.drawable.date_available_square_drawable)
                    holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                    holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                    holder.addBuyerlnly.visibility = View.GONE
                    holder.addBuyerTxt.visibility = View.GONE
                    holder.tmBuyerLnly.visibility = View.GONE
                }else{
                    val buyerInFo = dta?.TM_no
                    if(buyerInFo != null){
                        holder.calendarlnly.setBackgroundResource(R.drawable.date_available_square_drawable)
                        holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                        holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                        holder.addBuyerlnly.visibility = View.GONE
                        holder.addBuyerTxt.visibility = View.GONE
                        holder.tmBuyerLnly.visibility = View.GONE
                    }else{
                        holder.calendarlnly.setBackgroundResource(R.drawable.date_not_available_drawable)
                        holder.datetxt.setTextColor(ContextCompat.getColor(context,R.color.date_not_avilable_color))
                        holder.datetxt.text = withTMPUMPTXT+" : "+datevalue
                        holder.addBuyerlnly.visibility = View.GONE
                        holder.addBuyerTxt.visibility = View.VISIBLE
                        holder.tmBuyerLnly.visibility = View.GONE
                    }
                }
            }

            holder.addBuyerlnly?.setOnClickListener {
                listener?.onBuyerInfoAdd(dta)
            }


            holder.tmBuyerLnly.setOnClickListener {
                var buyerdlog = BuyerInfoDetailDialog(dta?.TM_no,dta?.driver_name,dta?.driver_mobile)
                buyerdlog.show(supportFragmentManager, "buyerdlog")
            }

            holder.icDelete.setOnClickListener {
                listener?.onProductDelete(position,dta?._id,cartItemsSchema?._id)
            }

           holder.calendarlnly.setOnClickListener {

               dateCartItems = dta
               var chooseTimeSlotDialog = ChooseTimeDialog(dta?.TM_price, dta?.CP_price,dta)
               chooseTimeSlotDialog.setListener(this)
               chooseTimeSlotDialog.show(supportFragmentManager, "chooseTimeSlotDialog")
           }
        }
    }


    internal inner class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view),
            View.OnClickListener{

         var tvTitle: TextView = view.findViewById(R.id.tvTitle)
         var venderNameTxt:TextView = view.findViewById(R.id.venderNameTxt)
         var tvPriceValue: TextView = view.findViewById(R.id.tvPriceValue)

         var icRemove: ImageView = view.findViewById(R.id.icRemove)

         var icAdd: ImageView = view.findViewById(R.id.icAdd)

         var icDelete: ImageView = view.findViewById(R.id.icDelete)
         var tvProductCount: TextView = view.findViewById(R.id.tvProductCount)
         var imgCart: ImageView = view.findViewById(R.id.imgCart)

         var datetxt:TextView = view.findViewById(R.id.datetxt)
         var calendarlnly:LinearLayout = view.findViewById(R.id.calendarlnly)
         var addBuyerlnly:LinearLayout = view.findViewById(R.id.addBuyerlnly)
         var addBuyerTxt:TextView = view.findViewById(R.id.addBuyerTxt)
         var tmBuyerLnly:LinearLayout = view.findViewById(R.id.tmBuyerLnly)

        init {
            icAdd.setOnClickListener(this)
            icRemove.setOnClickListener(this)
            tvProductCount.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {

                R.id.icAdd -> {
                    totalMaxQty = 100
                    productQty = cartItemsSchema?.items?.get(adapterPosition)?.quantity?.toInt()!!
                    if (productQty >= totalMaxQty) {
                        Utils.showToast(
                                context,
                            context.getString(R.string.maxmumqty100),
                                Toast.LENGTH_LONG
                        )
                    } else {
                        productQty = tvProductCount.text.toString().toInt()
                        productQty += 1
                        tvProductCount.setText(productQty.toString())
                        var cartItem = cartItemsSchema?.items?.get(adapterPosition)
                        cartItem?.quantity = productQty.toInt()
                        //productId = cartItemsSchema?.items!![adapterPosition].product_id
                        // userId = cartItemsSchema?.items!![adapterPosition].productInfo?.user_id
                        listener!!.onProductAddedCart(cartItem,cartItemsSchema)
                    }

                }
                R.id.icRemove -> {
                    totalMinQty = 3
                    productQty = cartItemsSchema?.items?.get(adapterPosition)?.quantity?.toInt()!!
                    if (productQty <= totalMinQty) {
                        Utils.showToast(
                                context,
                            context.getString(R.string.mimmumqty3),
                                Toast.LENGTH_LONG
                        )
                    } else {
                        productQty = tvProductCount.text.toString().toInt()
                        productQty -= 1
                        tvProductCount.text = productQty.toString()
                        var cartItem = cartItemsSchema?.items?.get(adapterPosition)
                        cartItem?.quantity = productQty.toInt()
                        // productId = cartItemsSchema?.items!![adapterPosition].product_id
                        //  userId = cartItemsSchema?.items!![adapterPosition].productInfo?.user_id
                        listener!!.onProductRemovedFromCart(cartItem,cartItemsSchema)
                    }

                }
                R.id.tvProductCount -> {
                    var updateQty = UpdateQtyDialog(tvProductCount.text.toString().toInt(),adapterPosition)
                    updateQty.setListener(this@CartAdapter)
                    updateQty.show(supportFragmentManager, "updateQty")
                }
            }
        }
    }



    override fun dialogDismiss() {

    }

    override fun dialogSave(count: Int, position: Int) {
        var cartItem = cartItemsSchema?.items?.get(position)
        cartItem?.quantity = count
        notifyItemChanged(position)
        listener!!.onProductAddedCart(cartItem,cartItemsSchema)
    }

    override fun dialogChangeDateSave(date: String,endt:String) {
        listener?.onDateChange(date,dateCartItems,cartItemsSchema,endt)
    }
}