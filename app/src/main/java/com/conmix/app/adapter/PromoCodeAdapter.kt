package com.conmix.app.adapter

import android.content.Context
import android.content.Intent
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.activities.AboutUsWebView
import com.conmix.app.data.CouponDataModel
import com.conmix.app.utils.Utils
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class PromoCodeAdapter(
    val context: Context,
    val couponList: ArrayList<CouponDataModel>,
    val listener: PromocodeInterface
) :RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ITEM = 1
    var couponCode:String? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PromoCodeHolder) {
            try {
                val promoCode = couponList.get(position)
                holder.promocodeInfo.text = promoCode.info
                holder.promocodeName.text = promoCode.code
                holder.promocodeTnc.text = promoCode.tnc + " T&C"
                setTncClick(holder.promocodeTnc)

                if (!couponCode.isNullOrEmpty()) {
                    if (promoCode.code.equals(couponCode)) {
                        holder.promocodeApply.text = "Applied"
                        holder.promocodeApply.isEnabled = false
                        holder.promocodeApply.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color_light))
                    } else {
                        holder.promocodeApply.text = "Apply"
                        holder.promocodeApply.isEnabled = true
                        holder.promocodeApply.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                    }
                } else {
                    holder.promocodeApply.text = "Apply"
                    holder.promocodeApply.isEnabled = true
                    holder.promocodeApply.setTextColor(ContextCompat.getColor(context,R.color.date_avilable_color))
                }


                holder.promocodeApply.setOnClickListener {
                    listener.applyPromoCode(promoCode.code!!)
                }

            } catch (e: Exception) {

            }
            // holder.validForLabel.text = service.discount_info


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.promo_code_lst_row, parent, false)
        return PromoCodeHolder(view)

    }

    override fun getItemCount(): Int {
        return couponList.size
    }

    override fun getItemViewType(position: Int): Int {
        return ITEM
    }
    /* interface TryoutsListener {
         fun onPostClicked(tryout: TryoutsDataModel)
     }*/

    internal inner class PromoCodeHolder(view: View) : RecyclerView.ViewHolder(view) {
        var promocodeApply: TextView
        var promocodeInfo: TextView
        var promocodeName: TextView
        var promocodeTnc: TextView
        //var promoCodeRadioBtn: RadioButton


        init {
            promocodeInfo = view.findViewById(R.id.promocodeInfo)
            promocodeApply = view.findViewById(R.id.promocodeApply)
            promocodeName = view.findViewById(R.id.promocodeName)
            promocodeTnc = view.findViewById(R.id.promocodeTnc)
            //  promoCodeRadioBtn = view.findViewById(R.id.promoCodeRadioBtn)

        }
    }

    interface PromocodeInterface {
        fun applyPromoCode(coupon: String)

    }

    fun setTncClick(textView: TextView) {
        val signupLinkClickSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                /*val intent = Intent(context, TermsAndConditionsActivity::class.java)
                val url = "http://192.168.1.154:3030/v1/common/service/discountTnc/d"
                intent.putExtra("url", url)
                intent.putExtra("name", "Terms And Conditions")
                context.startActivity(intent)
                Bungee.slideLeft(context)*/


                val intent = Intent(context, AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, context.getString(R.string.terms_condition_txt))
                context.startActivity(intent)
                Bungee.slideLeft(context)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true


            }

        }
        val clickableText = arrayOf("T&C")
        val clickableSpan: Array<ClickableSpan> = arrayOf(signupLinkClickSpan)
        Utils.makeLinks(textView, clickableText, clickableSpan)
    }
}