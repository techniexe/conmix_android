package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.SiteAddressObjLst


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class SelectSiteAdapter(val context: Context,
                        val siteAddList: ArrayList<SiteAddressObjLst>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false

    var loadViewHolder: LoadHolder? = null
    var mListener: SelectSiteAddressInterface? = null

    fun addLoadMoreLikes(list: java.util.ArrayList<SiteAddressObjLst>) {
        siteAddList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_select_address, parent, false)
            siteAddViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == siteAddList.size) {
            return LOAD_TYPE
        } else {
            return ROW_TYPE
        }
        return ROW_TYPE
    }
    override fun getItemCount(): Int {
        return siteAddList.size+1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is siteAddViewHolder) {
            val dta = siteAddList[position]

            if(dta.isSelected?:false){
                holder.selected_add_btn.setImageResource(R.drawable.ic_checked)
                holder.llBox.setBackgroundResource(R.drawable.plain_border_square)
            }else{
                holder.selected_add_btn.setImageResource(R.drawable.ic_checkbox)
                holder.llBox.setBackgroundResource(R.drawable.plain_border_gray_square)
            }


            holder.selected_add_btn?.setOnClickListener {

                //when data.isSelected = null
                if(dta.isSelected?:false){
                    dta.isSelected = false
                    mListener?.onAddSelected(dta)
                }else {
                    dta.isSelected = true
                    mListener?.onAddSelected(dta)
                }

                notifyDataSetChanged()

            }

            holder.companyname.text = dta.company_name.toString()
            holder.siteNameTxt.text = dta.site_name.toString()
            holder.addTxt.text = dta.address_line1+",\n"+dta.address_line2+"\n"+
                    dta.city_name+" - "+dta.pincode+"\n"+dta.state_name+", "+
                    dta.country_name
            holder.personNameTxt.text = dta.person_name.toString()
            holder.phoneNumberTxt.text = "Mobile No : " + dta.mobile_number.toString()


        } else if (holder is LoadHolder) {
            loadViewHolder = holder as LoadHolder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }

    }


    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }


    inner class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar

        init {
            progressBar = view.findViewById<ProgressBar>(R.id.progressBarReview) as ProgressBar
        }

    }

    internal inner class siteAddViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var companyname: TextView = view.findViewById<TextView>(R.id.tvCompanyName)
        var selected_add_btn: ImageView = view.findViewById<ImageView>(R.id.selected_add_btn)
        var llBox: LinearLayout = view.findViewById<LinearLayout>(R.id.llbox)
        var siteNameTxt: TextView = view.findViewById<TextView>(R.id.siteNameTxt)
        var addTxt: TextView = view.findViewById<TextView>(R.id.addTxt)
        var personNameTxt: TextView = view.findViewById<TextView>(R.id.sellerNameTxt)
        var phoneNumberTxt: TextView = view.findViewById<TextView>(R.id.tvPhoneNumber)

    }

    fun setListener(listener: SelectSiteAddressInterface) {

        mListener = listener
    }

    interface SelectSiteAddressInterface{
        fun onAddSelected(site: SiteAddressObjLst)
    }

}