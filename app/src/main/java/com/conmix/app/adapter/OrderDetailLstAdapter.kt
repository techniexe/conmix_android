package com.conmix.app.adapter

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R

import com.conmix.app.bottomsheet.GradeOrderBottomSheet
import com.conmix.app.bottomsheet.OrderStatusBottomsheet
import com.conmix.app.data.OrderDetailItemDta

import com.conmix.app.dialogs.BuyerInfoDetailDialog


import com.conmix.app.utils.Utils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 22,April,2021
 */
class OrderDetailLstAdapter(
    var context: Context,
    var myOrderList: ArrayList<OrderDetailItemDta>?,
    var supportFragmentManager: FragmentManager
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    var isCanceled = false

   // var mListener: SelectMyOrderListInterface? = null
    var mListener: SelectOrderItemStatus? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_order_detail_row, parent, false)
        return  orderListViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {

        return ROW_TYPE
    }
    override fun getItemCount(): Int {
        return myOrderList?.size?:0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is orderListViewHolder) {
            val dta = myOrderList?.get(position)

            holder.orderIdTxt?.text = String.format(context.getString(R.string.txt_order_item), dta?.display_item_id)
            val time = Utils.getConvertDate(dta?.created_at ?:"")
            holder.orderdateTxt.text = "Order date : "+time
            holder.statusTxt.text = dta?.item_status

            if(dta?.vendor_media?.size?:0 > 0){
                dta?.vendor_media?.forEach {
                    if(it.type =="PRIMARY"){
                        Utils.setImageUsingGradeGlide(context,it.media_url,holder.imgOrderV)
                    }
                }
            }

            if(dta?.item_status == "CANCELLED"){
                isCanceled = false
                holder.menuBtn.visibility = View.GONE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.red_satus_rounded_btn)

            }else if(dta?.item_status == "PLACED"){
                isCanceled = true
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.orange_rounded_btn)
            }else if(dta?.item_status == "PROCESSING"){
                isCanceled = true
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.yellow_rounded_btn)
            }else if(dta?.item_status == "CONFIRMED"){
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.green_rounded_btn)

            }
            else if(dta?.item_status == "TRUCK_ASSIGNED_FOR_PICKUP"){
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = "ASSIGNED"
                holder.statusLnLy.setBackgroundResource(R.drawable.yellow_rounded_btn)
            }else if(dta?.item_status == "TM_ASSIGNED"){
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = "TM ASSIGNED"
                holder.statusLnLy.setBackgroundResource(R.drawable.yellow_rounded_btn)
            }
            else if(dta?.item_status == "PICKUP"){
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.blue_rounded_btn)
            }
            else if(dta?.item_status == "DELAYED"){
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.orange_rounded_btn)
            }
            else if(dta?.item_status == "DELIVERED"){
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.green_rounded_btn)
            }
            else if(dta?.item_status == "REJECTED"){
                isCanceled = false
                holder.menuBtn.visibility = View.GONE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.red_satus_rounded_btn)
            }else if(dta?.item_status == "LAPSED"){
                isCanceled = false
                holder.menuBtn.visibility = View.GONE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.red_satus_rounded_btn)
            }else{
                isCanceled = false
                holder.menuBtn.visibility = View.VISIBLE
                holder.statusTxt.text = dta?.item_status
                holder.statusLnLy.setBackgroundResource(R.drawable.orange_rounded_btn)
            }


            if(dta?.design_mix != null){
                holder.tvTitle.text = dta.concrete_grade?.name +" - "+dta.design_mix?.product_name
            }else{
                holder.tvTitle.text = dta?.concrete_grade?.name+" - Custom Mix"
            }

            holder.venderNameTxt.text = dta?.vendor?.company_name
            holder.tvUnitPriceValue.text = String.format(
                    context.getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(dta?.unit_price_With_Margin?:0.0)
            )
           /*holder.tvQtyValue.text = Utils.setPrecesionFormate(dta?.quantity?:0.0)+" Cu.Mtr"
            holder.tvDeliverQtyValue.text = Utils.setPrecesionFormate(dta?.pickup_quantity?:0.0)+" Cu.Mtr"
            holder.tvRemainingQtyValue.text = Utils.setPrecesionFormate(dta?.remaining_quantity?:0.0)+" Cu.Mtr"
            holder.tvPenAssignQtyValue.text = Utils.setPrecesionFormate(dta?.part_quantity?.toDouble()?:0.0)+" Cu.Mtr"*/

            holder.tvQtyValue.text = (dta?.quantity?:0.0).toInt().toString()+" Cu.Mtr"
            holder.tvDeliverQtyValue.text = (dta?.pickup_quantity?:0.0).toInt().toString()+" Cu.Mtr"
            holder.tvRemainingQtyValue.text = (dta?.remaining_quantity?:0.0).toInt().toString()+" Cu.Mtr"
            holder.tvPenAssignQtyValue.text = (dta?.part_quantity?:0.0).toInt().toString()+" Cu.Mtr"

            holder.tvTotalAmtValue.text = String.format(
                    context.getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(dta?.selling_price_With_Margin?:0.0)
            )

            var withTMPUMPTXT = "With TM & CP "

            if(dta?.with_TM ?:false && dta?.with_CP ?:false){
                withTMPUMPTXT = "With TM & CP "
            }else if(dta?.with_TM?:false){
                withTMPUMPTXT = "With TM "
            }else if(dta?.with_CP?:false){
                withTMPUMPTXT = "With CP "
            }else{
                withTMPUMPTXT = "Without TM & CP"
            }

            holder.tmdetailTxt.text = withTMPUMPTXT

            if(dta?.with_TM ?:false){
                holder.tmBuyerDetailLnly.visibility = View.GONE
            }else{
                val buyerInFo = dta?.TM_no
                if(buyerInFo != null){
                    holder.tmBuyerDetailLnly.visibility = View.GONE
                }
            }
          /* holder.tmBuyerDetailLnly.setOnClickListener {
                val buyerdlog = BuyerInfoDetailDialog(dta?.TM_no, dta?.driver_name, dta?.driver_mobile)
                buyerdlog.show(supportFragmentManager, "buyerdlog")
            }*/

            holder.grade_info_img?.setOnClickListener {

                var infoOption = GradeOrderBottomSheet(dta!!)
                infoOption.show(supportFragmentManager, "BottomDialogFragment")
            }


            /*if(dta?.item_status == "DELIVERED"){
                if(dta?.review == null){
                    holder.menuBtn?.visibility = View.VISIBLE
                }else{
                    holder.menuBtn?.visibility = View.GONE
                }
            }else{
                if(isCanceled){
                    holder.menuBtn?.visibility = View.VISIBLE
                }else{
                    holder.menuBtn?.visibility = View.GONE
                }
            }*/

            holder.menuBtn?.setOnClickListener {

                popup(dta,isCanceled)
            }



            if(dta?.item_status == "CONFIRMED" || dta?.item_status == "TM_ASSIGNED" ||
                dta?.item_status == "PICKUP" || dta?.item_status == "DELAYED" || dta?.item_status == "LAPSED") {

                if(dta?.part_quantity?:0 >= 3) {

                    val calendar = Calendar.getInstance()
                    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    val Cdate = simpleDateFormat.format(calendar.time)
                    val newDeliverDate = Utils.comparetwoDates(dta?.end_date!!, Cdate)
                    val endDate = Utils.comparetwoDates(dta?.end_date!!, Cdate)

                    /*if (newDeliverDate == 0 || endDate <= 0) {
                        holder.tmAssignLnly.visibility = View.GONE
                    } else {
                        holder.tmAssignLnly.visibility = View.VISIBLE
                    }*/


                    if(newDeliverDate == 0){
                        holder.tmAssignLnly.visibility = View.VISIBLE
                    }else if(endDate < 0){
                        holder.tmAssignLnly.visibility = View.GONE
                    }else{
                        holder.tmAssignLnly.visibility = View.VISIBLE
                    }


                    //holder.tmAssignLnly.visibility = View.VISIBLE

                }else{
                    holder.tmAssignLnly.visibility = View.GONE
                }
            }else{
                holder.tmAssignLnly.visibility = View.GONE
            }

           /* holder.track_menuBtn?.setOnClickListener {
                mListener?.onTrackOrderItem(dta)
            }*/


            holder.tmAssignLnly.setOnClickListener {
                //mListener?.onTrackOrderItem(dta)
                mListener?.onAssignQtyItem(dta)

            }
        }
    }


    internal inner class orderListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var orderIdTxt: TextView = view.findViewById(R.id.orderIdTxt)
        var orderdateTxt: TextView = view.findViewById(R.id.orderdateTxt)
        var statusLnLy: LinearLayout = view.findViewById(R.id.statusLnLy)
        var statusTxt: TextView = view.findViewById(R.id.statusTxt)
        var menuBtn: ImageView = view.findViewById(R.id.menuBtn)
        var imgOrderV:ImageView = view.findViewById(R.id.imgOrderV)
        var tvTitle:TextView = view.findViewById(R.id.tvTitle)
        var grade_info_img:ImageView = view.findViewById(R.id.grade_info_img)
        var venderNameTxt:TextView = view.findViewById(R.id.venderNameTxt)
        var tvUnitPriceValue:TextView = view.findViewById(R.id.tvUnitPriceValue)
        var tvQtyValue:TextView = view.findViewById(R.id.tvQtyValue)
        var tvDeliverQtyValue:TextView = view.findViewById(R.id.tvDeliverQtyValue)
        var tvRemainingQtyValue:TextView = view.findViewById(R.id.tvRemainingQtyValue)
        var tvTotalAmtValue:TextView = view.findViewById(R.id.tvTotalAmtValue)
        var tmdetailTxt:TextView = view.findViewById(R.id.tmdetailTxt)
        var tmBuyerDetailLnly:LinearLayout = view.findViewById(R.id.tmBuyerDetailLnly)
        var tmAssignLnly:LinearLayout = view.findViewById(R.id.tmAssignLnly)
       // var track_menuBtn:ImageView = view.findViewById(R.id.track_menuBtn)
        var tvPenAssignQtyValue:TextView = view.findViewById(R.id.tvPenAssignQtyValue)

    }

    fun setOrderListener(listener: SelectOrderItemStatus) {
        mListener = listener
    }

    /*fun showMenuPopup(v: View) {
        val popup = PopupMenu(context, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.order_menu_main, popup.menu)

        popup.show()

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
               *//* R.id.download_invoice_menu -> {



                    true
                }*//*
                R.id.review_feedback_menu -> {

                    true
                }
                R.id.cancel_order_menu ->{

                    val fragment = BreakAlertDialog(
                            object : BreakAlertDialog.ClickListener {
                                override fun onDoneClicked() {

                                }

                                override fun onCancelClicked() {


                                }
                            },
                            context.getString(R.string.alert_cancel_order_txt),
                            context.getString(R.string.txt_yes),
                            context.getString(R.string.txt_cancel)
                    )

                    fragment.show(supportFragmentManager, "alert")
                    fragment.isCancelable = false


                    true
                }
                else -> false
            }
        }
    }*/


    interface SelectOrderItemStatus {
        fun onCancelOrdeItem(orderItemListData: OrderDetailItemDta?)
        fun onReviewItem(orderItemListData: OrderDetailItemDta?)
        fun onTrackOrderItem(orderItemListData: OrderDetailItemDta?)
        fun onAssignQtyItem(orderItemListData: OrderDetailItemDta?)
        fun onAssignOrderTrack(orderItemListData: OrderDetailItemDta?)
        fun onRepeatOrderItem(orderItemListData: OrderDetailItemDta?)
    }


    private fun popup(dta: OrderDetailItemDta?, isCanceled:Boolean) {

        var orderOption = OrderStatusBottomsheet(mListener,dta,isCanceled)
        orderOption.show((context as AppCompatActivity).supportFragmentManager,"BottomDialogFragment")

    }

}