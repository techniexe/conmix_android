package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.FeatureProduct
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class FeatureProductAdapter(
    val context: Context,
    private val featureProductData: ArrayList<FeatureProduct>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        holder as HorizontalTryoutsAdapter
        val featureProduct = featureProductData[position]

        Utils.setImageUsingGlide(context,featureProduct.image ,holder.imgFeatureIcon)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.dash_item_feature_product, parent, false)
        return HorizontalTryoutsAdapter(view)
    }

    override fun getItemCount(): Int {
        return featureProductData.size
    }


    internal class HorizontalTryoutsAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var imgFeatureIcon: ImageView =
            view.findViewById(R.id.imgFeatureIcon)


    }
}
