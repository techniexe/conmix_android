package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.VenderReviewData
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 13,April,2021
 */
class ReviewdapterWithPaging (var context: Context, private var reviewList: ArrayList<VenderReviewData>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2

     var loadViewHolder: LoadHolder? = null


    fun addLoadMoreLikes(list: ArrayList<VenderReviewData>) {
        reviewList=list
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {

        return reviewList.size + 1

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_review, parent, false)
            CartItemViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == reviewList.size) {
            LOAD_TYPE
        } else {
            ROW_TYPE
        }
    }
    class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
         var progressBar: ProgressBar = view.findViewById(R.id.progressBarReview) as ProgressBar

    }



    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val review=reviewList[position]

            if(review.buyer!=null)
                holder.tvName.text=review.buyer!!.full_name

            holder.tvDescription.text=review.review_text
            // Utils.setImageUsingGlide(context,review?.image_url,holder.imgCategory)

            val time = Utils.covertTimeToText(review.created_at ?: "")
            holder.tvTime.text = time

            holder.ratingBar.rating= review.rating!!.toFloat()


        }
        else if (holder is LoadHolder) {
            loadViewHolder = holder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }
    }

    internal  class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvTime: TextView = view.findViewById(R.id.tvTime)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var ratingBar: RatingBar = view.findViewById(R.id.ratting)



    }


}