package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.NotificationData
import com.conmix.app.utils.Utils
import comCoreConstants.conmix.utils.CoreConstants


/**
 * Created by Hitesh Patel on 19,May,2021
 */
class NotificationListAdapter(
    val context: Context,
    private var notificationList: ArrayList<NotificationData>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2

    private var notificationClick: NotificationItemClick? = null
    private var loadViewHolder: LoadHolder? = null

    fun addLoadMoreLikes(list: ArrayList<NotificationData>) {
        notificationList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return notificationList.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NotificationHolder) {
            val notificationData = notificationList[position]

            var fullNameOrCmpName = ""

            fullNameOrCmpName = if(notificationData.to_user?.account_type == "Individual"){
                notificationData.to_user?.full_name?:""
            }else{
                notificationData.to_user?.company_name?:""
            }

            var fullString = ""

            if (notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.ORDERPLACE) {
                holder.notificationMsg.text =
                    CoreConstants.NOTIFICATIONTYPE.ORDERPLACETYPEORDERPLACE

                fullString = String.format(
                    context.getString(
                        R.string.txt_notification,
                        fullNameOrCmpName,
                        notificationData.order?.display_id
                    )
                )

            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.ORDERPROCESS){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERPLACETYPEORDERPROCESS

                fullString = String.format(
                    context.getString(
                        R.string.txt_order_process_notification,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id
                    )
                )


            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.ORDERACCEPT){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERACCEPTTYPE

                fullString = String.format(
                    context.getString(
                        R.string.orderAcceptedbyVendor,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.ASSIGNQTY){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERASSIGNQTYTYPE

                fullString = String.format(
                    context.getString(
                        R.string.orderAssignQty,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderRejectedForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERREJECTEDTYPE

                fullString = String.format(
                    context.getString(
                        R.string.orderRejectedbyVendor,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.ORDERCONFIRM){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERPLACETYPEORDERCONFIRM

                fullString = String.format(
                    context.getString(
                        R.string.txt_order_confirm_notification,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.TRUCKASSIGNTOORDERFORBUYER){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERTRUCKASSIGNED

                fullString = String.format(
                    context.getString(
                        R.string.truckAssignedToOrderForBuyer,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData?.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.pickupOrderForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERPICKUP

                fullString = String.format(
                    context.getString(
                        R.string.pickupOrderForBuyer,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderItemRejectForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERITEMREJECT

                fullString = String.format(
                    context.getString(
                        R.string.orderItemRejectForBuyer,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData?.vendor?.full_name,
                        notificationData.order?.display_id

                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderRejectForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERREJECT

                fullString = String.format(
                    context.getString(
                        R.string.orderRejectForBuyer,
                        fullNameOrCmpName,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderDelayForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERDELAY

                fullString = String.format(
                    context.getString(
                        R.string.orderDelayForBuyer,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData.order_track?.delayTime,
                        notificationData.order_track?.reasonForDelay

                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.partiallyOrderDeliveredForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERPARTIALLYORDERDELIVER
                fullString = String.format(
                    context.getString(
                        R.string.partiallyOrderDeliveredForBuyer,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderFullyProductDeliver){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderFullyProductDeliverTitle

                fullString = String.format(
                    context.getString(
                        R.string.orderFullyProductDelivered,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }
            else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderDeliveredForBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERDELIVERED
                fullString = String.format(
                    context.getString(
                        R.string.orderDeliveredForBuyer,
                        fullNameOrCmpName,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.partiallyOrderCancelled){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERPARTIALLYCANCELLED
                fullString = String.format(
                    context.getString(
                        R.string.ordercancelpartially,
                        fullNameOrCmpName,
                        notificationData?.order_item?.display_item_id,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderCancelledBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderCancelled
                fullString = String.format(
                    context.getString(
                        R.string.ordercanceluser,
                        fullNameOrCmpName,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderShortCloseBuyer){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderShortClose
                fullString = String.format(
                    context.getString(
                        R.string.ordershortcloseduser,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.CPASSIGNTOORDERFORBUYER){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ORDERCPASSIGNED
                fullString = String.format(
                    context.getString(
                        R.string.ordercpassignnotify,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData?.vendor?.full_name
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.reminderBeforSevenDayForClientAssignedQty){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.reminderBeforSevenDayForClientAssignedQtyTITLE
                fullString = String.format(
                    context.getString(
                        R.string.reminder_7days_noti,
                        fullNameOrCmpName,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.reminderBeforOneDayForClientAssignedQty){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.reminderBeforOneDayForClientAssignedQtyTITLE
                fullString = String.format(
                    context.getString(
                        R.string.reminder_1days_noti,
                        fullNameOrCmpName,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderCpPickup){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderCpPickupTITLE
                fullString = String.format(
                    context.getString(
                        R.string.cp_pickup_noti,
                        fullNameOrCmpName,
                        notificationData?.vendor?.full_name,
                        notificationData?.site?.site_name,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderCpDeliver){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderCpDeliverTITLE
                fullString = String.format(
                    context.getString(
                        R.string.cp_deliver_noti,
                        fullNameOrCmpName,
                        notificationData?.vendor?.full_name,
                        notificationData?.site?.site_name,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id
                    )
                )
            }
            else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.productlapsed){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.productlapsedTITLE
                fullString = String.format(
                    context.getString(
                        R.string.product_lapsed_noti,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderlapsed){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderlapsedTITLE
                fullString = String.format(
                    context.getString(
                        R.string.ordrer_lapsed_noti,
                        fullNameOrCmpName,
                        notificationData.order?.display_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderCpDelay){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.orderCpDelayTITLE

                fullString = String.format(
                    context.getString(
                        R.string.cpDelayForNoti,
                        fullNameOrCmpName,
                        notificationData.order_item?.display_item_id,
                        notificationData.order?.display_id,
                        notificationData.CP_order_track?.delayTime,
                        notificationData.CP_order_track?.reasonForDelay
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.replyOfSupportTicketByAdmin){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ReplyOfSupportTicketByAdminTitle

                fullString = String.format(
                    context.getString(
                        R.string.replyadminForNoti,
                        fullNameOrCmpName,
                        notificationData.supportTicket?.ticket_id
                    )
                )
            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.resolveSupportTicketByAdmin){
                holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.ResolveSupportTicketByAdminTitle

                fullString = String.format(
                    context.getString(
                        R.string.resolvedbyForNoti,
                        fullNameOrCmpName,
                        notificationData.supportTicket?.ticket_id
                    )
                )
            }
            else{
                holder.notificationMsg.text = ""
                fullString = fullNameOrCmpName.toString()
            }

            fullNameOrCmpName?.let {
                Utils.setSpanColor(
                    context,
                    holder.notificationText,
                    fullString?:"",
                    fullNameOrCmpName,
                    R.color.colorPrimary
                )
            }
            val time = Utils.covertTimeToText(notificationData.created_at ?: "")
            holder.notificationTime.text = time
            holder.itemView?.setOnClickListener {
                notificationData.seen_on = "seenOn"
                notificationData._id?.let { it1 ->
                    notificationClick?.itemClick(
                        it1,
                        notificationData.order?._id
                    )
                }
                notifyItemChanged(position)
            }
            if (notificationData.seen_on != null) {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            } else {
                holder.itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.bordercolor
                    )
                )
            }
        } else if (holder is LoadHolder) {
            loadViewHolder = holder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification_list, parent, false)
            NotificationHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
         var progressBar: ProgressBar = view.findViewById(R.id.progressBarReview) as ProgressBar
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == notificationList.size) {
            LOAD_TYPE
        } else {
            ROW_TYPE
        }
    }

    internal class NotificationHolder(view: View) : RecyclerView.ViewHolder(view) {

        var notificationMsg: TextView = view.findViewById(R.id.notificationMsg)
        var notificationTime: TextView = view.findViewById(R.id.notificationTime)
        var notificationText: TextView = view.findViewById(R.id.notificationText)

    }

    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }

    interface NotificationItemClick {
        fun itemClick(notificationId: String, _id: String?)
    }

    fun notificationItemClick(notificationItemClick: NotificationItemClick) {
        this.notificationClick = notificationItemClick
    }
}