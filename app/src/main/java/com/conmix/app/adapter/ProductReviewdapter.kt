package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.ProductReciewLstDta
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 07,April,2021
 */
class ProductReviewdapter(var context: Context, private var reviewList: ArrayList<ProductReciewLstDta>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {




    override fun getItemCount(): Int {

        return if(reviewList.size >= 3){
            3
        }else{
            reviewList.size
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        var view= LayoutInflater.from(parent.context)
                .inflate(R.layout.item_review, parent, false)
        return   CartItemViewHolder(view)

    }





    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val review=reviewList[position]

            if(review.buyer!=null)
                holder.tvName.text=review.buyer!!.full_name

            holder.tvDescription.text=review.review_text
            // Utils.setImageUsingGlide(context,review?.image_url,holder.imgCategory)

            val time = Utils.covertTimeToText(review.created_at ?: "")
            holder.tvTime.text = time

            holder.ratingBar.rating= review.rating!!.toFloat()


        }

    }



    internal  class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvTime: TextView = view.findViewById(R.id.tvTime)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var ratingBar: RatingBar = view.findViewById(R.id.ratting)



    }


}