package com.conmix.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.activities.CustomDesignMix1Activity
import com.conmix.app.data.ProductCategoryWiseDetailsProductCategory
import com.conmix.app.services.interfaces.RecyclerOnSubItemClickListener
import de.hdodenhof.circleimageview.CircleImageView
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class ProductCategoryAdapter(val context: Context,
                                private val productCategoryData: ArrayList<ProductCategoryWiseDetailsProductCategory>,
                                private var isFromHome:Boolean
) :
RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    lateinit var recyclerOnItemClickListener: RecyclerOnSubItemClickListener

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        holder as HorizontalTryoutsAdapter
       // val productCategory = productCategoryData[position]

        //Utils.setImageUsingGlide(context, productCategory.main_image_url, holder.imgeProductIcon)

       // holder.title.text = productCategory.name

        if (position == 0) {
            holder.imgeProductIcon.visibility = View.GONE
            holder.first_img_Production.visibility = View.VISIBLE

            holder.title.text = "Custom RMC"
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary))


            /*holder.backgroundImage.setOnClickListener {
                listener.onCameraClicked()
            }*/

            holder.first_img_Production?.setOnClickListener {
                val intent = Intent(context, CustomDesignMix1Activity::class.java)
                context.startActivity(intent)
                Bungee.slideLeft(context)

            }



        } else {

            holder.imgeProductIcon.visibility = View.VISIBLE
            holder.first_img_Production.visibility = View.GONE
            holder.title.text = productCategoryData.get(position -1).name
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.black))

            /*val image = selectedImages.get(position-1)
            val mediaPath = image.path
            holder.imgAsk.visibility = View.VISIBLE
            holder.deleteImg.visibility = View.VISIBLE
            holder.cameraIcon.visibility = View.GONE
            holder.backgroundImage.visibility = View.GONE
            holder.deleteImg.setOnClickListener {
                listener.onDeleteClick(position-1)
            }

            val requestOption = RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()
            Glide.with(context).load(mediaPath)
                    .apply(requestOption)
                    //.transition(DrawableTransitionOptions.withCrossFade())
                    .into(holder.imgAsk)*/

            holder.imgeProductIcon.setOnClickListener {
                productCategoryData.get(position -1).name?.let { recyclerOnItemClickListener.onItemClick(holder.imgeProductIcon, id = productCategoryData.get(position -1)._id,subId = "",categoryName = productCategoryData.get(position -1).name) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.dash_item_product_category, parent, false)
        return HorizontalTryoutsAdapter(view)
    }

    override fun getItemCount(): Int {
        if(isFromHome){
            return 6
        }else{
            return productCategoryData.size + 1
        }

    }
    fun setListner(recyclerOnItemClickListener: RecyclerOnSubItemClickListener) {
        this.recyclerOnItemClickListener = recyclerOnItemClickListener
    }


    internal inner class HorizontalTryoutsAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var imgeProductIcon: CircleImageView = view.findViewById(R.id.imgProductIcon)
        var first_img_Production:CircleImageView = view.findViewById(R.id.firstImgProductIcon)
        var title: TextView = view.findViewById(R.id.tvText)

       /* init {
            view.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
        }*/
    }
}
