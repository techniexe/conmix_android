package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.conmix.app.data.AddmixureCategoryObj
import com.conmix.app.databinding.RegistrationSpinnerRowBinding


/**
 * Created by Hitesh Patel on 30,March,2021
 */
class AddMixureCodeAdapter(ctx: Context, moods: ArrayList<AddmixureCategoryObj>?) :
        ArrayAdapter<AddmixureCategoryObj>(ctx, 0, moods!!) {

    private var _binding: RegistrationSpinnerRowBinding? = null

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val stateModel = getItem(position)

        _binding = RegistrationSpinnerRowBinding.inflate(LayoutInflater.from(context),parent,false)

        val view = _binding!!.root
        _binding!!.titleSpinnerItem.setText(stateModel?.category_name.toString()+"-"+stateModel?.admixture_type.toString())
        return view
    }
}