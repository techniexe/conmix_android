package com.conmix.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.activities.CategorySeeAllActivity
import com.conmix.app.activities.ConcrateGradeListActivity
import com.conmix.app.data.BannerData
import com.conmix.app.data.DashBoardResponse
import com.conmix.app.services.interfaces.RecyclerOnSubItemClickListener
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.ArrayList


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class DashboardAdapter(
    private val mContext: Activity,
    private val dashboardResponse: DashBoardResponse?,
    private val lifecycle: Lifecycle
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), RecyclerOnSubItemClickListener {


    private val BANNER_TYPE = 1
    private val PRODUCT_CATEGORY_TYPE = 2
    private val FEATURE_PRODUCT_TYPE = 3
    //private val TOP_BRAND_TYPE = 4
    private var bannerList: ArrayList<BannerData> = ArrayList()
   // private var imageList = ArrayList<String>()
    val imageList = ArrayList<SlideModel>()

    init {
        dashboardResponse?.banners?.let { bannerList.addAll(it) }
        dashboardResponse?.banners?.forEach {
            imageList.add(SlideModel(it.media_url ?: "","", ScaleTypes.CENTER_CROP))
        }
    }

    /*private fun getAvenger(): MutableList<Movie> {
        val items = mutableListOf<Movie>()
        items.add(Movie("https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-1.jpg"))
        items.add(Movie("https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-2.jpg"))
        items.add(Movie("https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-3.jpg"))
        items.add(Movie("https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-4.jpg"))
        return items
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            BANNER_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.dash_banner, parent, false)
                return BannerViewHolder(view)
            }
            PRODUCT_CATEGORY_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.dash_include_list, parent, false)
                return ProductCategoryViewHolder(view)
            }
             FEATURE_PRODUCT_TYPE -> {
                 val view = LayoutInflater.from(parent.context)
                     .inflate(R.layout.dash_include_list, parent, false)
                 return FeatureProductViewHolder(view)
             }

            /*TOP_BRAND_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.dash_include_list, parent, false)
                return TopbrandViewHolder(view)
            }*/
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.dash_include_list, parent, false)
                return ProductCategoryViewHolder(view)
            }
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BannerViewHolder -> {

               /* holder.imageSlider.setSliderAdapter(SliderAdapterExample(mContext, imageList))
                holder.imageSlider.indicatorUnselectedColor  = ContextCompat.getColor(mContext, R.color.white);
                holder.imageSlider.setIndicatorAnimation(IndicatorAnimationType.SLIDE)
                holder.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)*/

                holder.imageSlider.setImageList(imageList)

               /* val adapter = SliderAdapterPlutoExample(imageList, object : OnItemClickListener<Movie> {
                    override fun onItemClicked(item: Movie?, position: Int) {
                        //Log.d(TAG, "on Item clicked $position ${item?.imdbRating}")
                    }
                })

                holder.imageSlider.create(adapter,4000,lifecycle)
                *//*holder.infinity_gauntlet_indicator.visibility = View.VISIBLE
                holder.imageSlider.setCustomIndicator(holder.infinity_gauntlet_indicator)*/

            }
            is ProductCategoryViewHolder -> {
                holder.tvTitle.visibility = View.VISIBLE
                holder.tvTitle.text = mContext.getString(R.string.txt_our_Product_title)
                holder.tvSellAll.visibility = View.VISIBLE
                val adapter = dashboardResponse?.productCategory?.let { ProductCategoryAdapter(mContext, it,true) }
                val gridLayoutManager = GridLayoutManager(mContext, 3, RecyclerView.VERTICAL, false)
                val layoutParams = gridLayoutManager.spanSizeLookup
                holder.rvProductCategoryy?.layoutManager = gridLayoutManager
                adapter!!.setListner(this)

               /* holder.rvProductCategoryy.layoutManager = LinearLayoutManager(
                    mContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )*/

                holder.rvProductCategoryy.adapter = adapter

                holder.tvSellAll.setOnClickListener {
                    var intent =  Intent(mContext, CategorySeeAllActivity::class.java)
                    intent.putExtra("catlst",dashboardResponse?.productCategory)
                    mContext.startActivity(intent)
                    Bungee.slideLeft(mContext)
                }
            }
            is FeatureProductViewHolder -> {
                holder.tvTitle.visibility = View.GONE
                holder.tvTitle.text = mContext.getString(R.string.txt_feature_product)
                holder.tvSellAll.visibility = View.GONE
                holder.rvProductCategoryy.layoutManager = LinearLayoutManager(
                    mContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
                holder.rvProductCategoryy.adapter = dashboardResponse?.featuredVenue?.let { FeatureProductAdapter(mContext, it) }
            }


            /*is TopbrandViewHolder -> {
                holder.tvTitle.visibility = View.VISIBLE
                holder.tvTitle.text = mContext.getString(R.string.txt_top_brand)
                holder.tvSellAll.visibility = View.GONE
                holder.rvProductCategoryy.layoutManager = LinearLayoutManager(
                    mContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
                holder.rvProductCategoryy.adapter =
                    dashboardResponse?.topBrand?.let { TopBrandAdapter(mContext, it) }
            }*/
        }
    }

    override fun getItemViewType(position: Int): Int {


        return when (position) {
            0 -> {
                BANNER_TYPE
            }
            1 -> {
                PRODUCT_CATEGORY_TYPE
            }
             2 -> {
                 FEATURE_PRODUCT_TYPE
             }
            /* 2 -> {
                 TOP_SERVICE_TYPE
             }*/
            /*3 -> {
                TOP_BRAND_TYPE
            }*/
            else -> {
                FEATURE_PRODUCT_TYPE
            }
        }
    }


    override fun getItemCount(): Int {
        return 3
    }



    internal class BannerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageSlider: com.denzcoskun.imageslider.ImageSlider = view.findViewById(R.id.imageSlider) as com.denzcoskun.imageslider.ImageSlider


        /*val imageSlider:PlutoView = view.findViewById<PlutoView>(R.id.slider_view)
        val infinity_gauntlet_indicator:PlutoIndicator = view.findViewById(R.id.custom_indicator)*/


    }


    internal class ProductCategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var rvProductCategoryy: RecyclerView = view.findViewById(R.id.rvdashCommonList)
        var tvTitle: TextView = view.findViewById(R.id.tvDashTitle)
        var tvSellAll: TextView = view.findViewById(R.id.tvSeeAll)
    }


    internal  class FeatureProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var rvProductCategoryy: RecyclerView = view.findViewById(R.id.rvdashCommonList)
        var tvTitle: TextView = view.findViewById(R.id.tvDashTitle)
        var tvSellAll: TextView = view.findViewById(R.id.tvSeeAll)
    }



    internal class TopServiceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var rvProductCategoryy: RecyclerView = view.findViewById(R.id.rvdashCommonList)
        var tvTitle: TextView = view.findViewById(R.id.tvDashTitle)
        var tvSellAll: TextView = view.findViewById(R.id.tvSeeAll)
    }

    internal  class TopbrandViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var rvProductCategoryy: RecyclerView = view.findViewById(R.id.rvdashCommonList)
        var tvTitle: TextView = view.findViewById(R.id.tvDashTitle)
        var tvSellAll: TextView = view.findViewById(R.id.tvSeeAll)

    }


    override fun onItemClick(childView: View?, id: String?, subId: String?, categoryName: String?) {

/*      val intent = Intent(mContext, SelectLocationActivity::class.java)
        intent.putExtra(CoreConstants.Intent.INTENT_CATEGORY_ID,id)
        intent.putExtra(CoreConstants.Intent.INTENT_CATEGORY_NAME,categoryName)
        mContext.startActivity(intent)
        Bungee.slideLeft(mContext)*/

       /* Utils.setFacebookAnalytics(
            context = mContext,
            mContext.getString(R.string.home_ServiceCategoryList)
        )
        var intent =Intent(mContext, CategoryActivtity::class.java)
        intent.putExtra(CoreConstants.Intent.INTENT_CATEGORY_ID,id)
        intent.putExtra(CoreConstants.Intent.INTENT_CATEGORY_NAME,categoryName)
        mContext.startActivity(intent)
        Bungee.slideLeft(mContext)*/

        var intent =  Intent(mContext, ConcrateGradeListActivity::class.java)
        intent.putExtra(CoreConstants.Intent.INTENT_GRADE_ID,id)
        intent.putExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_TITLE,categoryName)
        mContext.startActivity(intent)
        Bungee.slideLeft(mContext)
    }

}