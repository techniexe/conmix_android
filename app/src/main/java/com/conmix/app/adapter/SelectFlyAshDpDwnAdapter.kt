package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R

import com.conmix.app.data.FlyAshSourceObj


/**
 * Created by Hitesh Patel on 30,March,2021
 */
class SelectFlyAshDpDwnAdapter(val context: Context, private var flyAshListadp: ArrayList<FlyAshSourceObj>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1

    var mListener: SelectFlyAshInterface? = null
    var counter = 0


    init {

        flyAshListadp?.forEach {
            if(it.isSelected == true){
                counter++
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.multi_selection_dropdown_row, parent, false)

        return siteAddViewHolder(view)


    }

    override fun getItemViewType(position: Int): Int {

        return ROW_TYPE
    }
    override fun getItemCount(): Int {
        return flyAshListadp.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is siteAddViewHolder) {
            val dta = flyAshListadp[position]



            if (counter >= 2) {
                if( dta.isSelected == true ){
                    holder.checkbox.isChecked = true
                    holder.checkbox.isClickable = true
                    holder.checkbox.isEnabled = true

                }else{
                    holder.checkbox.isChecked = false
                    holder.checkbox.isClickable = false
                    holder.checkbox.isEnabled = false

                }
            }else{
                holder.checkbox.isClickable = true
                holder.checkbox.isEnabled = true
                if(dta.isSelected == true ){
                    holder.checkbox.isChecked = true

                }else{
                    holder.checkbox.isChecked = false
                }
            }


            holder.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                dta.isSelected = isChecked
                if(isChecked == true){
                    counter ++
                }else{
                    counter --
                }
                // mListener?.onAddSelected(isChecked,dta._id!!)
                notifyDataSetChanged()

            }

            holder.itemtext.text = dta?.fly_ash_source_name

        }
    }


    internal inner class siteAddViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var checkbox: CheckBox = view.findViewById<CheckBox>(R.id.checkbox)
        var itemtext: TextView = view.findViewById<TextView>(R.id.itemtext)


    }

    fun setListener(listener: SelectFlyAshInterface) {

        mListener = listener
    }

    interface SelectFlyAshInterface{
        fun onAddSelectedFlyAsh(ischecked:Boolean,position:String)
    }

}