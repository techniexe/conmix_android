package com.conmix.app.adapter

import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.conmix.app.R
import com.conmix.app.data.Movie
import com.opensooq.pluto.base.PlutoAdapter
import com.opensooq.pluto.base.PlutoViewHolder
import com.opensooq.pluto.listeners.OnItemClickListener


/**
 * Created by Hitesh Patel on 19,March,2021
 */
class SliderAdapterPlutoExample(items: MutableList<Movie>, onItemClickListener: OnItemClickListener<Movie>) : PlutoAdapter<Movie, SliderAdapterPlutoExample.ViewHolder>(items, onItemClickListener) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent, R.layout.image_slider_layout_item)
    }

    class ViewHolder(parent: ViewGroup, itemLayoutId: Int) : PlutoViewHolder<Movie>(parent, itemLayoutId) {
        private var ivPoster: ImageView = getView(R.id.iv_auto_image_slider)


        override fun set(item: Movie, position: Int) {
            Glide.with(context).load(item.url).into(ivPoster)

        }
    }
}
