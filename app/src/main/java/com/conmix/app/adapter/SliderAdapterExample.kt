package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.conmix.app.R
import com.conmix.app.databinding.ImageSliderLayoutItemBinding
import com.smarteist.autoimageslider.SliderViewAdapter

/**
 * Created by Hitesh Patel on 08,March,2021
 */

public class SliderAdapterExample(context: Context?, private var mSliderItems: ArrayList<String>) :
    SliderViewAdapter<SliderAdapterExample.SliderAdapterVH>() {

    var binding :ImageSliderLayoutItemBinding?= null

    fun renewItems(sliderItems: ArrayList<String>) {
        mSliderItems = sliderItems
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        mSliderItems.removeAt(position)
        notifyDataSetChanged()
    }

    fun addItem(sliderItem: String) {
        mSliderItems.add(sliderItem)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {


         binding = ImageSliderLayoutItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SliderAdapterVH(binding!!)
    }

    override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int) {

        val sliderItem = mSliderItems[position]

        val requestOptions: RequestOptions = RequestOptions()
            .placeholder(R.color.borderColor)
            .error(R.color.borderColor)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(viewHolder.itemView.context)
            .load(sliderItem)
            .apply(requestOptions)
            .centerCrop()
            .into(binding!!.ivAutoImageSlider)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { v: View? -> })
    }

    override fun getCount(): Int {
        //slider view count could be dynamic size
        return mSliderItems.size
    }

    class SliderAdapterVH( binding: ImageSliderLayoutItemBinding) : ViewHolder(binding.root)
}