package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.VenderReviewData
import com.conmix.app.data.VenderReviewResData
import com.conmix.app.utils.ColorGenerator
import com.conmix.app.utils.TextDrawable
import com.conmix.app.utils.Utils





/**
 * Created by Hitesh Patel on 12,April,2021
 */
class VenderReviewAdapter(var context: Context, private var reviewList: VenderReviewResData?) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var reviewsData:ArrayList<VenderReviewData> = ArrayList()

    init {
        reviewsData.addAll(reviewList?.reviews?:java.util.ArrayList())
    }



    override fun getItemCount(): Int {

        return if(reviewsData.size >= 3){
            3
        }else{
            reviewsData.size
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        var view= LayoutInflater.from(parent.context)
                .inflate(R.layout.item_review, parent, false)
        return   CartItemViewHolder(view)

    }





    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val review=reviewsData[position]

            if(review.buyer!=null)
                holder.tvName.text=review.buyer!!.full_name

            holder.tvDescription.text=review.review_text
            // Utils.setImageUsingGlide(context,review?.,holder.imgCategory)

            val time = Utils.covertTimeToText(review.created_at ?: "")
            holder.tvTime.text = time

            holder.ratingBar.rating= review.rating!!.toFloat()

            val generator = ColorGenerator.MATERIAL // or use DEFAULT
            val color1 = generator.randomColor

            val drawable = TextDrawable.builder().beginConfig()
                .withBorder(4)
                .textColor(ContextCompat.getColor(context, R.color.white))
                .bold().toUpperCase().endConfig()
                .buildRound(review.buyer!!.full_name?.first().toString(),color1)
            holder.imgprf.setImageDrawable(drawable)
        }
    }



    internal  class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvTime: TextView = view.findViewById(R.id.tvTime)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var ratingBar: RatingBar = view.findViewById(R.id.ratting)
        var imgprf:ImageView = view.findViewById(R.id.imgProfile)

    }


}