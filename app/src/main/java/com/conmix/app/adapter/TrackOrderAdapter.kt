package com.conmix.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.activities.TrackOrderDetailActivity
import com.conmix.app.data.TrackOrderLstobj
import com.conmix.app.utils.Utils
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 17,May,2021
 */
class TrackOrderAdapter (
    val context: Context,
    private var trackOrderList: ArrayList<TrackOrderLstobj>?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return trackOrderList?.size!!

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_track_lst_row, parent, false)
        return CartItemViewHolder(view)
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val trackOrderDetail = trackOrderList?.get(position)


            holder.tvName.text = String.format(
                context.getString(R.string.txt_truck),
                trackOrderDetail?.TM_rc_number
            )
            holder.tvCapacity.text = String.format(
                context.getString(R.string.txt_capacity),
                trackOrderDetail?.TM_category,
                trackOrderDetail?.TM_sub_category

            )
            holder.tvDriveer.text =
                String.format(context.getString(R.string.txt_driver), trackOrderDetail?.driver_name)
            holder.tvMobile.text = String.format(
                context.getString(R.string.txt_mobile_trak),
                trackOrderDetail?.driver_mobile_number
            )
            if (!trackOrderDetail?.delivered_at.isNullOrEmpty()) {
                holder.tvDelivered.visibility = View.VISIBLE
                holder.tvDelivered.text = String.format(
                    context.getString(R.string.txt_delivery_track),
                    Utils.getConvertDateWithoutTime(trackOrderDetail?.delivered_at.toString())
                )
            } else {
                if (!trackOrderDetail?.event_status.isNullOrBlank()) {
                    holder.tvDelivered.visibility = View.VISIBLE
                    holder.tvDelivered.text = trackOrderDetail?.event_status.toString()
                } else {
                    holder.tvDelivered.visibility = View.GONE
                }
            }

            if (trackOrderDetail?.pickup_quantity!! > 0) {


                holder.tvQty.text = String.format(
                    context.getString(R.string.quanity),
                    trackOrderDetail.pickup_quantity
                )
            }
            /*  Utils.setImageUsingGlide(
                  context,
                  trackOrderDetail?.sub_category_image_url,
                  holder.imgTrack
              )*/
        }
    }

    internal inner class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var tvName: TextView = view.findViewById(R.id.tvTitle)
        var tvCapacity: TextView = view.findViewById(R.id.tvCapacity)
        var tvDriveer: TextView = view.findViewById(R.id.tvDriver)
        var tvQty: TextView = view.findViewById(R.id.tvQty)
        var tvMobile: TextView = view.findViewById(R.id.tvMobile)
        var tvDelivered: TextView = view.findViewById(R.id.tvDelivered)
        var imgTrack: ImageView = view.findViewById(R.id.imgTrackOrder)

        var imgNext: ImageView = view.findViewById(R.id.imgNext)
        var rlItemView: RelativeLayout = view.findViewById(R.id.relativeLayout)

        init {
            rlItemView.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.relativeLayout -> {
                    if (trackOrderList?.get(adapterPosition)?.event_status != CoreConstants.ORDERSTATUS.REJECTED) {
                        val intent = Intent(context, TrackOrderDetailActivity::class.java)
                        intent.putExtra(
                            CoreConstants.Intent.INTENT_ORDER_ITEM_ID,
                            trackOrderList?.get(adapterPosition)?.order_item_id
                        )
                        intent.putExtra(
                            CoreConstants.Intent.INTENT_TRACK_ID,
                            trackOrderList?.get(adapterPosition)?._id
                        )
                        intent.putExtra(
                            CoreConstants.Intent.INTENT_ORDER_DIAPLAY_ID,
                            trackOrderList?.get(adapterPosition)?.display_id
                        )
                        intent.putExtra(
                            CoreConstants.Intent.INTENT_CREATED_DATE,
                            trackOrderList?.get(adapterPosition)?.created_at
                        )
                        context.startActivity(intent)
                        Bungee.slideLeft(context)
                    } else {
                        Toast.makeText(
                            context,
                            context.getString(R.string.txt_truck_msg),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }

    }


}