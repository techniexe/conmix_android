package com.conmix.app.adapter

import android.content.Context
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.AssignTrackDlObj

import com.conmix.app.utils.Utils
import comCoreConstants.conmix.utils.CoreConstants


/**
 * Created by Hitesh Patel on 20,August,2021
 */
class TrackCPOrderDetailAdapter(
    val context: Context,
    private var trackOrderDetail: AssignTrackDlObj?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return 3

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cp_track_order_detail, parent, false)
        return TrackItemViewHolder(view)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is TrackItemViewHolder) {

           if (position == 0) {

               setOrderStatus(
                   holder.tvOrderStatus,
                   context.getString(R.string.cp_order_assigned),
                   true,
                   false
               )
               setOrderDescription(
                   holder.tvSubTitle,
                   context.getString(R.string.cp_order_assigned_description),
                   true
               )
               setStatusDate(holder.tvDate, trackOrderDetail?.assigned_at, true)
               setVehicleData(holder.tvVehicle, trackOrderDetail, true, false)

               dortColor(true, holder.imgTrack, false)
               lineColor(true, holder.view, true)

            } else if (position == 1) {


                if (trackOrderDetail?.event_status!! == CoreConstants.ORDERSTATUS.PICKUP ||
                    trackOrderDetail?.event_status!! == CoreConstants.ORDERSTATUS.DELIVERED ||
                    trackOrderDetail?.event_status!! == CoreConstants.ORDERSTATUS.DELAYED
                ) {
                    setOrderStatus(
                        holder.tvOrderStatus,
                        "Picked up",
                        true, false
                    )
                    setOrderDescription(
                        holder.tvSubTitle,
                        context.getString(R.string.cp_order_status_pickup_description),
                        true
                    )
                    setStatusDate(holder.tvDate, trackOrderDetail?.pickedup_at, true)

                    dortColor(true, holder.imgTrack, false)
                    lineColor(true, holder.view, true)

                    if (trackOrderDetail?.event_status!! == CoreConstants.ORDERSTATUS.DELAYED)
                        setVehicleData(holder.tvVehicle, trackOrderDetail, true, true)

                } else {

                    setOrderStatus(
                        holder.tvOrderStatus,
                        context.getString(R.string.order_status_pickup),
                        false,
                        false
                    )
                    setOrderDescription(
                        holder.tvSubTitle,
                        context.getString(R.string.cp_order_status_pickup_description),
                        false
                    )
                    setStatusDate(holder.tvDate, trackOrderDetail?.pickedup_at, false)
                    setVehicleData(holder.tvVehicle, trackOrderDetail, false, false)

                    dortColor(false, holder.imgTrack, false)
                    lineColor(false, holder.view, true)

                }
            } else if (position == 2) {

                if (trackOrderDetail?.event_status!! == CoreConstants.ORDERSTATUS.DELIVERED) {
                    setOrderStatus(
                        holder.tvOrderStatus,
                        context.getString(R.string.delivered),
                        true,
                        true
                    )
                    setOrderDescription(
                        holder.tvSubTitle,
                        context.getString(R.string.cp_delivered_description),
                        true
                    )
                    setStatusDate(holder.tvDate, trackOrderDetail?.delivered_at, true)
                    setVehicleData(holder.tvVehicle, trackOrderDetail, false, false)
                    dortColor(true, holder.imgTrack, true)
                    lineColor(true, holder.view, false)
                } else {

                    setOrderStatus(
                        holder.tvOrderStatus,
                        "Deliver",
                        false,
                        false
                    )
                    setOrderDescription(
                        holder.tvSubTitle,
                        context.getString(R.string.cp_delivered_description),
                        false
                    )
                    //setStatusDate(holder.tvDate, trackOrderDetail?.delivered_at, false)
                    setVehicleData(holder.tvVehicle, trackOrderDetail, false, false)
                    dortColor(false, holder.imgTrack, false)
                    lineColor(false, holder.view, false)
                }
            }
        }
    }

    internal inner class TrackItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvOrderStatus: TextView = view.findViewById(R.id.tvOrderStatus)
        var tvSubTitle: TextView = view.findViewById(R.id.tvOrderString)
        var tvDate: TextView = view.findViewById(R.id.tvOrderDate)
        var tvVehicle: TextView = view.findViewById(R.id.tvOrderDetail)
        var imgTrack: ImageView = view.findViewById(R.id.imgTrackProgress)
        var view: View = view.findViewById(R.id.vwLine)
    }

    fun dortColor(isFilled: Boolean, imageView: ImageView, isDelivered: Boolean) {

        if (!isDelivered) {
            if (isFilled) {
                imageView.setBackgroundResource(R.drawable.ic_circle_fill)
                imageView.setColorFilter(
                    ContextCompat.getColor(context, R.color.colorDort),
                    android.graphics.PorterDuff.Mode.MULTIPLY
                )
            } else {
                imageView.setBackgroundResource(R.drawable.ic_circle_border)
                imageView.setColorFilter(
                    ContextCompat.getColor(context, R.color.borderColor),
                    android.graphics.PorterDuff.Mode.MULTIPLY
                )
            }
        } else {
            imageView.setBackgroundResource(R.drawable.ic_circle_check)
        }
    }

    fun lineColor(isFilled: Boolean, view: View, isVisible: Boolean) {
        if (isVisible) {
            view.visibility = View.VISIBLE
            if (isFilled) {
                view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDort))

            } else {
                view.setBackgroundColor(ContextCompat.getColor(context, R.color.borderColor))
            }
        } else {
            view.visibility = View.INVISIBLE
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun setOrderStatus(
        tvOrderStatus: TextView,
        status: String,
        isVisible: Boolean,
        isDelivered: Boolean
    ) {
        if (isVisible) {
            if (isDelivered)
                tvOrderStatus.setTextColor(context.getColor(R.color.colorDort))
            else
                tvOrderStatus.setTextColor(context.getColor(R.color.black))
                tvOrderStatus.text = status
        } else {
                tvOrderStatus.setTextColor(context.getColor(R.color.borderColor))
                tvOrderStatus.text = status
        }
    }

    fun setOrderDescription(
        tvOrderStatusDescription: TextView,
        description: String,
        isVisible: Boolean
    ) {
        if (isVisible) {
            tvOrderStatusDescription.visibility = View.VISIBLE
            tvOrderStatusDescription.text = description
        } else {
            tvOrderStatusDescription.visibility = View.INVISIBLE
        }
    }

    fun setStatusDate(tvOrderDate: TextView, date: String?, isVisible: Boolean) {

        if (isVisible) {
            tvOrderDate.visibility = View.VISIBLE
            tvOrderDate.text = date?.let { Utils.getConvertDate(it) }
        } else {
            tvOrderDate.visibility = View.INVISIBLE
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun setVehicleData(
        tvVehicle: TextView,
        vehicle: AssignTrackDlObj?,
        isVisible: Boolean,
        isDelay: Boolean
    ) {
        if (isVisible) {
            tvVehicle.visibility = View.VISIBLE

            if (!isDelay) {
                val strBuffer = StringBuffer()

                if(vehicle?.serial_number?.isEmpty() == false){
                    strBuffer.append("CP Serial No")
                    strBuffer.append(" ")
                    strBuffer.append(":")
                    strBuffer.append(" ")
                    strBuffer.append(vehicle.serial_number ?:"")
                    strBuffer.append("\n")
                }

                if(vehicle?.operator_name?.isEmpty() == false){
                    strBuffer.append("Operator Name")
                    strBuffer.append(" ")
                    strBuffer.append(":")
                    strBuffer.append(" ")
                    strBuffer.append(vehicle.operator_name ?:"")
                    strBuffer.append("\n")
                }

                if(vehicle?.operator_mobile_number?.isEmpty() == false){
                    strBuffer.append("Mobile No")
                    strBuffer.append(" ")
                    strBuffer.append(":")
                    strBuffer.append(" ")
                    strBuffer.append(vehicle.operator_mobile_number ?:"")

                    vehicle.operator_mobile_number?.toString()?.let {
                        Utils.setSpanWithClick(
                            context = context,
                            view = tvVehicle,
                            fulltext = strBuffer.toString(),
                            subtext = it,
                            color = R.color.site_add_owner_txt_color,
                            phoneNumber = vehicle.operator_mobile_number!!
                        )
                    }
                }else{
                    tvVehicle.setText(strBuffer)
                }
                tvVehicle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10F)
            } else {
                tvVehicle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10F)
                tvVehicle.text = String.format(
                    context.getString(R.string.delay),
                    trackOrderDetail?.delayTime,
                    trackOrderDetail?.reasonForDelay
                )
                tvVehicle.setTextColor(
                    context.getColor(R.color.rejected_color)
                )
            }
        } else {
            tvVehicle.visibility = View.INVISIBLE
        }
    }
}