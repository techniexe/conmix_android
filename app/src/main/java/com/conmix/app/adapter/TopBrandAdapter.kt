package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.TopBrand
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class TopBrandAdapter(
    val context: Context,
    private val topBrand: ArrayList<TopBrand>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        holder as HorizontalTryoutsAdapter
        val topBrand = topBrand[position]

        Utils.setImageUsingGlide(context, topBrand.image,holder.imgTopBrand)




    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.dash_item_top_brand, parent, false)
        return HorizontalTryoutsAdapter(view)
    }

    override fun getItemCount(): Int {
        return topBrand.size
    }


    internal class HorizontalTryoutsAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var imgTopBrand: ImageView =
            view.findViewById(R.id.imgTopBrand)


    }
}
