package com.conmix.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.activities.AssignTrackOrderDetail
import com.conmix.app.activities.TrackOrderDetailActivity
import com.conmix.app.data.orderItemPartDataObj
import com.conmix.app.utils.Utils
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 19,August,2021
 */
class TMCPAssignAdapter (
    val context: Context,
    private var trackOrderList: ArrayList<orderItemPartDataObj>?,
    private var displayId:String?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return trackOrderList?.size!!

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_tm_cp_assign_list_row, parent, false)
        return CartItemViewHolder(view)
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val dta = trackOrderList?.get(position)



            holder.tvAssignstartTimeValue.text = Utils.getConvertDateTime(dta?.start_time?:"")
            holder.tvAssignendTimeValue.text = Utils.getConvertDateTime(dta?.end_time?:"")
            holder.tvdeliverQtyValue.text = dta?.assigned_quantity?.toString()

            if(dta?.CPtrack_details?.size?:0 == 0){
                holder.imgNext.visibility  = View.GONE
            }else{

                if(dta?.CPtrack_details?.get(0)?.event_status?.equals("REJECTED") == true){
                    holder.imgNext.visibility = View.GONE
                }else{
                    holder.imgNext.visibility = View.VISIBLE
                }

            }


        }
    }

    internal inner class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {

        var tvAssignstartTimeValue: TextView = view.findViewById(R.id.tvAssignstartTimeValue)
        var tvAssignendTimeValue: TextView = view.findViewById(R.id.tvAssignendTimeValue)
        var tvdeliverQtyValue: TextView = view.findViewById(R.id.tvdeliverQtyValue)


        var imgNext: ImageView = view.findViewById(R.id.imgNext)
        var rlItemView: RelativeLayout = view.findViewById(R.id.relativeLayout)


        init {
            rlItemView.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.relativeLayout -> {


                    if(trackOrderList?.get(adapterPosition)?.CPtrack_details?.size?:0 == 0){

                    }else{

                        if(trackOrderList?.get(adapterPosition)?.CPtrack_details?.get(0)?.event_status?.equals("REJECTED") == true){
                            Toast.makeText(
                                context,
                                context.getString(R.string.txt_truck_msg),
                                Toast.LENGTH_LONG
                            ).show()
                        }else{
                            val intent = Intent(context, AssignTrackOrderDetail::class.java)
                            intent.putExtra(
                                "assignObj",
                                trackOrderList?.get(adapterPosition)?.CPtrack_details?.get(0)
                            )
                            intent.putExtra("trackId",trackOrderList?.get(adapterPosition)?._id)
                            intent.putExtra("displayId",displayId)
                            context.startActivity(intent)
                            Bungee.slideLeft(context)
                        }

                    }
                }
            }
        }

    }


}