package com.conmix.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.bottomsheet.BillSiteOptionBottomsheet
import com.conmix.app.bottomsheet.SiteOptionBottomsheet
import com.conmix.app.data.BillAddressObjLst
import com.conmix.app.data.SiteAddressObjLst
import com.conmix.app.utils.SharedPrefrence


/**
 * Created by Hitesh Patel on 29,September,2021
 */
class SelectBillingSiteProfileAdapter (
    var supportFragmentManager: FragmentManager,
    val context: Context,
    val siteAddList: ArrayList<BillAddressObjLst>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false

    var loadViewHolder: LoadHolder? = null
    var mListener: SelectSiteProfileAddressInterface? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_select_billaddress_profile, parent, false)
            siteAddViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
       /* if (position == siteAddList.size) {
            return LOAD_TYPE
        } else {
            return ROW_TYPE
        }*/
        return ROW_TYPE
    }

    override fun getItemCount(): Int {
       // return siteAddList.size + 1
        return siteAddList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is siteAddViewHolder) {
            val dta = siteAddList[position]


            holder.selected_delete_btn?.setOnClickListener {
                popup(dta)
            }


            if(dta?.buyerDetails?.account_type == "Individual"){
                //holder.companyName.text = dta.full_name.toString()

                if(dta?.full_name?.trim().isNullOrEmpty()){
                    holder.companyName.text  = dta.company_name
                }else{
                    holder.companyName.text  = dta.full_name
                }


                if(!dta.gst_number?.trim().isNullOrEmpty()){
                    holder.gstNumberTxt.visibility = View.VISIBLE
                    holder.gstNumberTxt.text = "GST No : " + dta.gst_number.toString()
                }else{
                    holder.gstNumberTxt.visibility = View.GONE
                }


            }else{
                // holder.companyName.text = dta.company_name.toString()
                if(dta?.company_name?.trim().isNullOrEmpty()){
                    holder.companyName.text  = dta.full_name
                }else{
                    holder.companyName.text  = dta.company_name
                }



               /* holder.gstNumberTxt.visibility = View.VISIBLE
                holder.gstNumberTxt.text = "GST No : " + dta.gst_number.toString()*/

                if(!dta.gst_number?.trim().isNullOrEmpty()){
                    holder.gstNumberTxt.visibility = View.VISIBLE
                    holder.gstNumberTxt.text = "GST No : " + dta.gst_number.toString()
                }else{
                    holder.gstNumberTxt.visibility = View.GONE
                }

            }



            /*holder.addTxt.text = dta.line1 + ",\n" + dta.line2 + "\n" +
                    dta.cityDetails?.city_name + " - " + dta.pincode + "\n" + dta.stateDetails?.state_name + ", " +
                    dta.stateDetails?.country_name*/

            holder.addTxt.text = "${dta.line1?:""},\n${dta.line2?:""}\n${dta.cityDetails?.city_name?:""} - ${dta.pincode?:""}\n${dta.stateDetails?.state_name?:""}, ${dta?.stateDetails?.country_name?:""}"




        } else if (holder is LoadHolder) {
            loadViewHolder = holder as LoadHolder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }

    }


    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }


    inner class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar

        init {
            progressBar = view.findViewById<ProgressBar>(R.id.progressBarReview) as ProgressBar
        }

    }

    internal inner class siteAddViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var companyName: TextView = view.findViewById<TextView>(R.id.tvCompanyName)
        var selected_delete_btn: ImageView = view.findViewById<ImageView>(R.id.imgDelete)

        var addTxt: TextView = view.findViewById<TextView>(R.id.addTxt)
       // var phoneNumberTxt: TextView = view.findViewById<TextView>(R.id.tvPhoneNumber)
        var gstNumberTxt: TextView = view.findViewById<TextView>(R.id.tvGSTNumber)




    }


    private fun popup(dta: BillAddressObjLst) {

        var siteOption = BillSiteOptionBottomsheet(mListener, dta)
        siteOption.show(supportFragmentManager, "BottomDialogFragment")



    }

    interface SelectSiteProfileAddressInterface{
        fun onDeleteAdd(siteAddressId:String)
        fun onEditAdd(siteAddressobj: BillAddressObjLst)
    }

    fun setListener(listener: SelectSiteProfileAddressInterface) {

        mListener = listener
    }
}