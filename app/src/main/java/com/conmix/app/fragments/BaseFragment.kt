package com.conmix.app.fragments

import android.content.Context
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext


/**
 * Created by Hitesh Patel on 08,March,2021
 */
open class BaseFragment : Fragment(), CoroutineScope {
    private lateinit var mJob: Job

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + mJob

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mJob = Job()
        // Utils.hideKeyboard(requireActivity())
    }

    override fun onDestroyView() {
        mJob.cancel()
        super.onDestroyView()
    }
}