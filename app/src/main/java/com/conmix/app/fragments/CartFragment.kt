package com.conmix.app.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.activities.*
import com.conmix.app.adapter.CartAdapter
import com.conmix.app.data.*
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel

import com.conmix.app.databinding.CartListActivityBinding
import com.conmix.app.databinding.NoCartitemBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ApplyCouponViewModel
import com.conmix.app.viewmodels.OrderViewModel
import com.conmix.app.viewmodels.UserProfileFrgViewModel
import com.razorpay.Checkout
import comCoreConstants.conmix.utils.CoreConstants
import gun0912.tedimagepicker.util.ToastUtil
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class CartFragment:Fragment(), CartAdapter.ProductsAdapterInterface {

    private lateinit var binding: CartListActivityBinding
    private lateinit var bindingEmptyCart: NoCartitemBinding
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel
    var userProfileObj: UserProfileObj? = null
    private lateinit var userProfileFrgViewModel: UserProfileFrgViewModel
    private lateinit var cartAdapter: CartAdapter
    var stateId:String?= null
    var siteId:String? = null
    var lat:Double?= null
    var long:Double?= null
    var deliveryDate:String?= null
    var cartItemsSchema: ArrayList<CartItemLst> = ArrayList<CartItemLst>()
    var cartObjDta: CartObjDta?= null
    private lateinit var orderViewModel: OrderViewModel
    private lateinit var applyCouponViewModel: ApplyCouponViewModel
    val TAG:String = CartFragment::class.toString()
    private var mListner: DialogToFragment? = null
    var sucessMsg:String = "Product added to cart successfully"
    var endDate:String?= null


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = CartListActivityBinding.inflate(layoutInflater)
        bindingEmptyCart = binding.emptyCartLy
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupViewModel()
        getUserPrfdataApiCall()
        Checkout.preload(activity?.applicationContext)
        binding.cartToolbar.visibility = View.GONE
        stateId = SharedPrefrence.getStateId(requireContext())
        lat = SharedPrefrence.getlat(requireContext())?.toDouble()
        long = SharedPrefrence.getlang(requireContext())?.toDouble()
        deliveryDate = SharedPrefrence.getSelectedDate(requireContext())
        endDate = SharedPrefrence.getSelectedENDDate(requireContext())


        startAnim()
        getCartdata()



        binding.btnAddNewSite.setOnClickListener {
            val intent = Intent(requireContext(), CartSelectSiteAddressActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE,true)
            intent.putExtra("siteId",cartObjDta?.site_id)
            startActivityForResult(intent,240)
            Bungee.slideLeft(requireContext())
        }

        binding.btnAddNewBiilingAddress.setOnClickListener {

            val intent = Intent(requireContext(), CartSelectBillingAddressActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE,true)
            intent.putExtra("billId",cartObjDta?.billing_address_id)
            intent.putExtra("cartStateId",cartObjDta?.siteInfo?.state_id)
            startActivityForResult(intent,480)
            Bungee.slideLeft(requireContext())
        }


        binding.applyCoupanTxt.setOnClickListener {
            val intent = Intent(requireContext(), ApplyCouponActivity::class.java)
            intent.putExtra("couponCode",cartObjDta?.couponInfo?.code)
            startActivityForResult(intent,420)
            Bungee.slideLeft(requireContext())
        }


        binding.btnCheckOut.setOnClickListener {
            var siteId = binding.tvAddress.text
            if (siteId.isNullOrEmpty()) {
               // processApi()
              //  StartPayment()
                Utils.showToast(requireContext(), getString(R.string.txt_delivery), Toast.LENGTH_SHORT)

            } else if(cartObjDta?.billing_address_id == null) {

                Utils.showToast(requireContext(), getString(R.string.txt_bii_delivery), Toast.LENGTH_SHORT)
            }else{
                mListner!!.sortDialogDismiss(cartObjDta,userProfileObj,binding.avLoading)
            }
        }
        binding.cancelCoupon.setOnClickListener {
            postCouponCode()
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
     /*   if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {

                PostSiteId()
            }
        }else if(requestCode == 420){
            if (resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }else if(requestCode == 480){
            if (resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }
        else if(requestCode == 244){
            if (resultCode == Activity.RESULT_OK) {
                val objItems: CartItemLst? = data?.getParcelableExtra("cartItems")
                val tmNo = data?.getStringExtra("tmNo")
                val driverName = data?.getStringExtra("driverName")
                val driverMoNo = data?.getStringExtra("driverMoNo")
              //  val opretorName = data?.getStringExtra("opretorName")
               // val opretorMoNo = data?.getStringExtra("opretorMoNo")
                onProductBuyerAddedCart(objItems,tmNo,driverName,driverMoNo)

            }
        }*/


        if(requestCode == 480){
            if (resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }else if(requestCode == 420){
            if (resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }

    }

    private fun setupViewModel() {

        customMixAddToCartViewModel = ViewModelProvider(
                this,
                CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(CustomMixAddToCartViewModel::class.java)

        orderViewModel = ViewModelProvider(
                this,
                OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(OrderViewModel::class.java)

        applyCouponViewModel = ViewModelProvider(
                this,
                ApplyCouponViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(ApplyCouponViewModel::class.java)
        userProfileFrgViewModel =
            ViewModelProvider(
                this,
                UserProfileFrgViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(UserProfileFrgViewModel::class.java)


    }
     fun startAnim() {
        if (binding.avLoading != null)

            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE

    }

     fun stopAnim() {

        if (binding.avLoading != null)
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE

    }



    private fun setResposerData(response: CartObjDta) {
        cartObjDta = response
        cartItemsSchema = response.items?: ArrayList()
        emptyView()
        setupUI()

    }

    private fun setupUI() {

        binding.rvCartList.layoutManager = LinearLayoutManager(requireContext())
        cartAdapter = CartAdapter(requireContext(), cartObjDta, requireActivity().supportFragmentManager)
        cartAdapter.setProductsAdapterListener(this)
        binding.rvCartList.adapter = cartAdapter
        cartItemsSchema?.let { setData() }
    }


    public fun processApi(razorpayPaymentId: String?) {
        ApiClient.setToken()

        val obj = GatewayTranstedIdObj(razorpayPaymentId)

        orderViewModel.postOrderData(obj).observe(requireActivity(), Observer{

            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                       // startAnim()

                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.code() == 200) {

                            redirectToDialog(resource.data.body()?.data!!)
                        } else {
                            Utils.setErrorData(requireContext(), resource.data?.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }
        })

    }

    private fun redirecToMainActivity() {
        val intent = Intent(requireContext(), MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        //finish()
        Bungee.slideRight(requireContext())
    }

    private fun redirectToDialog(dOrder: OrderResObj) {
        val fragment = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {
                processApiNew(dOrder)
            }

            override fun onCancelClicked() {


            }
        }, String.format(getString(R.string.alert_order_place), dOrder.display_id), "Ok", "")


        fragment.show(requireActivity().supportFragmentManager, "alert")
        fragment.isCancelable = false
    }

    private fun processApiNew(order: OrderResObj) {
        ApiClient.setToken()
        orderViewModel.processorder(order._id!!).observe(this, Observer{
                response ->
            when(response.status){
                Status.LOADING -> {
                    startAnim()

                }
                Status.SUCCESS -> {
                    stopAnim()
                    if (response.data?.isSuccessful!!) {
                        response.data.let { it1 ->
                            // redirectToDialog(order.display_id)
                            redirecToMainActivity()
                        }
                    } else {
                        Utils.setErrorData(requireContext(), response.data.errorBody())
                    }
                }
                Status.ERROR -> {
                    stopAnim()
                    Utils.showToast(requireContext(), response.message.toString(), Toast.LENGTH_LONG)
                }


            }
        })

    }


    private fun PostSiteId() {

        val cartAddressIdObj = CartAddressIdObj(SharedPrefrence.getSiteId(requireContext()))

        ApiClient.setToken()

        customMixAddToCartViewModel.cartAddressAdd(cartAddressIdObj).observe(this, Observer{

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {
                        stopAnim()

                        if (resource.data?.isSuccessful!!) {

                            getCartdata()

                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                            //Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }


    private fun setData() {
        if (cartItemsSchema != null) {

            if (cartObjDta?.totalNoOfItems?:0 > 1) {
                binding.tvCartItemLable.text = String.format(
                        getString(R.string.txt_cart_items),
                        cartItemsSchema.size
                )
            } else {
                binding.tvCartItemLable.text = String.format(
                        getString(R.string.txt_cart_item),
                        cartItemsSchema.size
                )
            }

            binding.tvCartItem.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.selling_price_With_Margin?:0.0)
            )

            binding.txtTransistAmtValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.TM_price?:0.0)
            )

            binding.tvConcretePumValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.CP_price?:0.0)
            )

            if(cartObjDta?.cgst_price != null && cartObjDta?.cgst_price?:0.0 > 0){
                binding.cGSTLnLy.visibility = View.VISIBLE
                binding.sGSTLnLy.visibility = View.VISIBLE
                binding.iGSTLnLy.visibility = View.GONE
                binding.tvItemCGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.cgst_price?:0.0)
                )

                binding.tvItemSGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.sgst_price?:0.0)
                )



            }else{
               binding.cGSTLnLy.visibility = View.GONE
                binding.sGSTLnLy.visibility = View.GONE

                if(cartObjDta?.igst_price != null && cartObjDta?.igst_price?:0.0 > 0){
                    binding.iGSTLnLy.visibility = View.VISIBLE

                    binding.tvItemIGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.igst_price?:0.0)
                    )
                }else{
                    binding.iGSTLnLy.visibility = View.GONE

                }
            }



            /*if(cartObjDta?.coupon_amount != null && cartObjDta?.coupon_amount?:0.0 > 0) {

                 binding.discountLnLy.visibility = View.VISIBLE
                 binding.txtDiscountValue.text = String.format(
                         getString(R.string.txt_unit_price),
                         Utils.setPrecesionFormate(cartObjDta?.coupon_amount?:0.0)
                 )
             }else{
                 binding.discountLnLy.visibility = View.GONE
             }*/
            if(cartObjDta?.coupon_amount != null && cartObjDta?.coupon_amount?:0.0 > 0) {

                binding.discountLnLy.visibility = View.VISIBLE
                binding.amountLnLy.visibility = View.VISIBLE
                binding.txtDiscountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.coupon_amount?:0.0)
                )
                binding.txtAmountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.amount?:0.0)
                )
            }else{
                binding.discountLnLy.visibility = View.GONE
                binding.amountLnLy.visibility = View.GONE
            }

           /* binding.discountLnLy.visibility = View.VISIBLE
            binding.txtDiscountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.coupon_amount)
            )*/


            if(cartObjDta?.couponInfo != null){
                binding.applyCoupanTxt.text = cartObjDta?.couponInfo?.code
                binding.arrowleft.visibility = View.GONE
                binding.cancelCoupon.visibility = View.VISIBLE
                binding.applyCoupanTxt.setTextColor(ContextCompat.getColor(requireContext(),R.color.avalibity_color))
            }else{
                binding.applyCoupanTxt.text = "APPLY PROMOCODE"
                binding.arrowleft.visibility = View.VISIBLE
                binding.cancelCoupon.visibility = View.GONE
                binding.applyCoupanTxt.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorPrimary))
            }



            binding.tvItemTotal.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.total_amount?:0.0)
            )



            if (cartObjDta?.siteInfo != null) {

                /*val savedStateId = SharedPrefrence.getStateId(requireContext())
                val saveCityId = SharedPrefrence.getCityId(requireContext())

                if (!savedStateId.equals(cartObjDta?.siteInfo?.state_id) && !saveCityId.equals(cartObjDta?.siteInfo?.city_id)) {

                }*/

                /*setPrefrence(
                    cartObjDta?.siteInfo?._id,
                    cartObjDta?.siteInfo?.state_id,
                    cartObjDta?.siteInfo?.city_id,
                    cartObjDta?.siteInfo?.location?.coordinates?.get(1)?.toString(),
                    cartObjDta?.siteInfo?.location?.coordinates?.get(0)?.toString(),
                    cartObjDta?.siteInfo?.site_name,
                    cartObjDta?.siteInfo?.city_name
                )*/

                binding.btnAddNewSite.text = getString(R.string.change_new_site)
                binding.llAddress.visibility = View.VISIBLE

                binding.tvCompanyName?.text = cartObjDta?.siteInfo?.company_name
                binding.tvBuyerName?.text = cartObjDta?.siteInfo?.site_name

                var strBuffer = StringBuffer()
                strBuffer.append(cartObjDta?.siteInfo?.address_line1)
                strBuffer.append("\n")
                strBuffer.append(cartObjDta?.siteInfo?.address_line2)
                strBuffer.append("\n")
                strBuffer.append(cartObjDta?.siteInfo?.city_name)
                strBuffer.append(" - ")
                strBuffer.append(cartObjDta?.siteInfo?.pincode)
                strBuffer.append("\n")
                strBuffer.append(cartObjDta?.siteInfo?.state_name)
                strBuffer.append(",\n")
                strBuffer.append(cartObjDta?.siteInfo?.country_name)
                binding.tvAddress.setText(strBuffer.toString())
                binding.tvsitePersonName?.text = String.format(
                    getString(R.string.sitepersonName_str),
                    cartObjDta?.siteInfo?.person_name?:""
                )
                /*binding.tvPhoneNumber?.text =
                    String.format(
                        getString(R.string.site_phone_str),
                        cartObjDta?.siteInfo?.mobile_number?:""
                    )
*/
                if(!cartObjDta?.siteInfo?.mobile_number.isNullOrEmpty()){


                            val str = String.format(
                                getString(R.string.site_phone_str),
                                cartObjDta?.siteInfo?.mobile_number?:""
                            )


                    cartObjDta?.siteInfo?.mobile_number?.let {
                        Utils.setSpanWithClick(
                            context = requireContext(),
                            view = binding.tvPhoneNumber,
                            fulltext = str,
                            subtext = it,
                            color = R.color.site_add_owner_txt_color,
                            phoneNumber = cartObjDta?.siteInfo?.mobile_number!!
                        )
                    }

                   /* binding.tvPhoneNumber?.setOnClickListener {
                        val number = cartObjDta?.siteInfo?.mobile_number
                        val i = Intent(
                            Intent.ACTION_DIAL,
                            Uri.parse("tel:+$number")
                        )
                        requireContext().startActivity(i)
                    }*/
                }else{
                    binding.tvPhoneNumber?.visibility = View.GONE
                }

            } else {
                binding.btnAddNewSite.text = getString(R.string.add_new_site)
                binding.llAddress.visibility = View.GONE
            }

            if (cartObjDta?.billingAddressInfo != null) {
                binding.btnAddNewBiilingAddress.text = getString(R.string.cart_change_billing_add_btn)
                binding.llbillAddress.visibility = View.VISIBLE




                if(cartObjDta?.billingAddressInfo?.full_name != null &&  cartObjDta?.billingAddressInfo?.full_name != " "){
                    binding.tvBillingCompanyName.text = cartObjDta?.billingAddressInfo?.full_name
                }else {
                    binding.tvBillingCompanyName.text = cartObjDta?.billingAddressInfo?.company_name
                }


                binding.tvbillingAddress.text = cartObjDta?.billingAddressInfo?.line1+",\n"+cartObjDta?.billingAddressInfo?.line2+"\n"+
                        cartObjDta?.billingAddressInfo?.city_name+" - "+cartObjDta?.billingAddressInfo?.pincode+"\n"+cartObjDta?.billingAddressInfo?.state_name+", "+
                        cartObjDta?.billingAddressInfo?.country_name
                //holder.personNameTxt.text = dta.person_name.toString()
                //binding.tvGstBilling.text = "GST No : " + cartObjDta?.billingAddressInfo?.gst_number.toString()

                if(cartObjDta?.billingAddressInfo?.gst_number ==  " " ){
                    binding.tvGstBilling.visibility = View.GONE
                }else if(cartObjDta?.billingAddressInfo?.gst_number != null){
                    binding.tvGstBilling.visibility = View.VISIBLE
                    binding.tvGstBilling.text = "GST No : " + cartObjDta?.billingAddressInfo?.gst_number.toString()
                }else{
                    binding.tvGstBilling.visibility = View.GONE
                }


            }else{
                binding.btnAddNewBiilingAddress.text = getString(R.string.add_new_billing_btn)
                binding.llbillAddress.visibility = View.GONE
            }

        }
    }

    private fun getCartdata() {

        siteId = SharedPrefrence.getSiteId(requireContext())
        stateId = SharedPrefrence.getStateId(requireContext())
        lat = SharedPrefrence.getlat(requireContext())?.toDouble()
        long = SharedPrefrence.getlang(requireContext())?.toDouble()

        ApiClient.setToken()

        customMixAddToCartViewModel.getCartData(stateId,long, lat, null,null,null).observe(requireActivity(),Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {
                        stopAnim()

                        if (resource.data?.isSuccessful!!) {

                            setResposerData(response = resource.data.body()?.data!!)

                        } else {
                            emptyView()
                            //Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }


    private fun emptyView() {

        if (cartItemsSchema.size >0) {
            bindingEmptyCart.constrainEmptyCart.visibility = View.GONE
            binding.llCart.visibility = View.VISIBLE
            binding.llbutton.visibility = View.VISIBLE
        }else {
            binding.llCart.visibility = View.GONE
            bindingEmptyCart.constrainEmptyCart.visibility = View.VISIBLE
            binding.llbutton.visibility = View.GONE
        }
    }

    override fun onProductAddedCart(cartItems: CartItemLst?, cartItemsSchema: CartObjDta?) {
        stateId  = SharedPrefrence.getStateId(requireContext())
        siteId = SharedPrefrence.getSiteId(requireContext())
        long = SharedPrefrence.getlang(requireContext())?.toDouble()
        lat = SharedPrefrence.getlat(requireContext())?.toDouble()

        val objpost = AddToCartCustomMixObj(
                cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartItemsSchema?._id,
                cartItems?._id,
                cartItemsSchema?.user_id,
                siteId,
                long,
                lat,
                cartItems?.with_CP,cartItems?.with_TM,
                cartItems?.delivery_date!!,cartItems?.TM_no,
                cartItems?.driver_name,cartItems?.driver_mobile,Utils.sendDeliveryDate(endDate!!))
        sucessMsg = ""
        postCustomMixData(objpost)
    }


     fun onProductBuyerAddedCart(cartItems: CartItemLst?, tmNo:String?, driverName:String?, driverMoNo:String?) {
        stateId  = SharedPrefrence.getStateId(requireContext())
        siteId = SharedPrefrence.getSiteId(requireContext())
        long = SharedPrefrence.getlang(requireContext())?.toDouble()
        lat = SharedPrefrence.getlat(requireContext())?.toDouble()

        val objpost = AddToCartCustomMixObj(
                cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartObjDta?._id,
                cartItems?._id,
                cartObjDta?.user_id,
                siteId,
                long,
                lat,
                cartItems?.with_CP,
                cartItems?.with_TM,
                cartItems?.delivery_date!!,
                tmNo,
                driverName,
                driverMoNo,Utils.sendDeliveryDate(endDate!!))
         sucessMsg = ""
        postCustomMixData(objpost)
    }

    override fun onProductRemovedFromCart(cartItems: CartItemLst?, cartItemsSchema: CartObjDta?) {

        stateId  = SharedPrefrence.getStateId(requireContext())
        siteId = SharedPrefrence.getSiteId(requireContext())
        long = SharedPrefrence.getlang(requireContext())?.toDouble()
        lat = SharedPrefrence.getlat(requireContext())?.toDouble()


        val objpost = AddToCartCustomMixObj(cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartItemsSchema?._id,
                cartItems?._id,
                cartItemsSchema?.user_id,
                siteId,
                long,
                lat,
                cartItems?.with_CP,cartItems?.with_TM,
                cartItems?.delivery_date!!,cartItems?.TM_no,
                cartItems?.driver_name,cartItems?.driver_mobile,Utils.sendDeliveryDate(endDate!!))
        sucessMsg = ""
        postCustomMixData(objpost)

    }

    override fun onProductDelete(index: Int, itemId: String?,cart_Id:String?) {

        val fragment = BreakAlertDialog(
                object : BreakAlertDialog.ClickListener {
                    override fun onDoneClicked() {
                        deleteCartItem(index,itemId,cart_Id)
                    }
                    override fun onCancelClicked() {
                    }
                },
                getString(R.string.alert_msg_cart_delete),
                getString(R.string.txt_yes),
                getString(R.string.txt_cancel)
        )

        fragment.show(requireActivity().supportFragmentManager, "alert")
        fragment.isCancelable = false
    }

    override fun onDateChange(date: String?, cartItems: CartItemLst?, cartItemsSchema: CartObjDta?,endt:String) {

        stateId  = SharedPrefrence.getStateId(requireContext())
        siteId = SharedPrefrence.getSiteId(requireContext())
        long = SharedPrefrence.getlang(requireContext())?.toDouble()
        lat = SharedPrefrence.getlat(requireContext())?.toDouble()


        val objpost = AddToCartCustomMixObj(cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartItemsSchema?._id,
                cartItems?._id,
                cartItemsSchema?.user_id,
                siteId,
                long,
                lat,
                true,true,
                Utils.sendDeliveryDate(date!!),cartItems?.TM_no,
                cartItems?.driver_name,cartItems?.driver_mobile,Utils.sendDeliveryDate(endt!!))
        sucessMsg = "Date Updated Successfully"
        postCustomMixData(objpost)

    }

    override fun onBuyerInfoAdd(cartItems: CartItemLst?) {
        val intent = Intent(requireContext(), BuyerInfoActivity::class.java)
        intent.putExtra("isFromCart",true)
        intent.putExtra("cartItems",cartItems)
        startActivityForResult(intent,244)
        requireActivity().overridePendingTransition(R.anim.bottom_up, R.anim.nothing)
    }


    private fun deleteCartItem(index: Int, itemId: String?,cart_Id:String?) {
        ApiClient.setToken()

        val obj = DeleteCartObj(itemId,cart_Id)

        customMixAddToCartViewModel.deleteCartItem(obj)
                .observe(this, Observer{

                    it?.let { resource ->
                        when (resource.status) {

                            Status.SUCCESS -> {
                                stopAnim()
                                if (resource.data?.isSuccessful!!) {
                                    deleteItem(index)
                                } else {
                                    Utils.setErrorData(requireContext(), resource.data.errorBody())
                                }
                            }
                            Status.LOADING -> {
                                startAnim()
                            }
                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }
                    }
                })
    }


    private fun deleteItem(position: Int) {
        if (!cartObjDta?.items.isNullOrEmpty()) {
            cartObjDta!!.items!!.removeAt(position)
            cartAdapter.notifyDataSetChanged()
            getCartdata()
        }
    }


    private fun setPrefrence(
            siteId: String?,
            stateId: String?,
            cityId: String?,
            lat: String?,
            long: String?,
            siteName: String?,
            cityname: String?
    ) {
        if (!siteId.isNullOrEmpty()) {
            SharedPrefrence.setSiteId(
                    requireContext(),
                    siteId
            )
        }
        if (!stateId.isNullOrEmpty()) {
            SharedPrefrence.setStateId(
                    requireContext(),
                    stateId
            )
        }
        if (!cityId.isNullOrEmpty()) {
            SharedPrefrence.setCityId(
                    requireContext(),
                    cityId
            )
        }
        if (!lat.isNullOrEmpty()) {
            SharedPrefrence.setlat(
                    requireContext(),
                    lat
            )
        }
        if (!long.isNullOrEmpty()) {
            SharedPrefrence.setlang(
                    requireContext(),
                    long
            )
        }
        if (!siteName.isNullOrEmpty() && !cityname.isNullOrEmpty()) {
            val strBuffer = StringBuffer()
            strBuffer.append((siteName))
            strBuffer.append(" ")
            strBuffer.append(",")
            strBuffer.append(" ")
            strBuffer.append((cityname))
            SharedPrefrence.setSiteName(
                    requireContext(),
                    strBuffer.toString()
            )
        }
    }



    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.body()?.data != null) {

                                    if(!sucessMsg.isNullOrEmpty()) {
                                        Utils.showToast(
                                            requireContext(),
                                            sucessMsg,
                                            Toast.LENGTH_SHORT
                                        )
                                    }
                                    getCartdata()

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        binding.avLoading.visibility = View.GONE
                        binding.avLoading.hide()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        /* binding.avLoading.visibility = View.VISIBLE
                         binding.avLoading.show()*/
                    }
                }
            }
        })
    }


    fun postCouponCode() {

        val cpObj = CouponObj("")

        ApiClient.setToken()
        applyCouponViewModel.postCouponCode(cpObj).observe(requireActivity(),Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {
                        stopAnim()

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.code() == 201) {

                                    Utils.showToast(requireContext(),"Coupon code removed successfully",Toast.LENGTH_SHORT)

                                    getCartdata()

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }



    private fun getUserPrfdataApiCall() {

            ApiClient.setToken()
            userProfileFrgViewModel.getUserPrfData().observe(requireActivity(), Observer {

                it?.let { resource ->
                    when (resource.status) {

                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                userProfileObj = resource.data.body()?.data
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(context, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
    }
    interface DialogToFragment {
        fun sortDialogDismiss(orderData: CartObjDta?, userData: UserProfileObj?,avLoading:com.wang.avi.AVLoadingIndicatorView?)

    }

    fun setListener(mListener: DialogToFragment) {
        this.mListner = mListener
    }
}
