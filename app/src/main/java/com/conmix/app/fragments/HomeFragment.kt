package com.conmix.app.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.activities.LoginSelectSiteAddressActivity
import com.conmix.app.activities.PlaceAutoCompleteActivity
import com.conmix.app.adapter.DashboardAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.*

import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.HomeViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class HomeFragment : Fragment() {

    lateinit var homeViewModel: HomeViewModel
    var dashboardResponse = DashBoardResponse()
    lateinit var productCategory: ArrayList<ProductCategoryWiseDetailsProductCategory>
    private var _binding: FragmentHomeBinding? = null
    private var nodataBinding: EmptyViewBinding? = null
    private var noInternetBinding: NoInternetBinding?= null
    private lateinit var dashboardAdapter: DashboardAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        nodataBinding = _binding!!.emptyViewLy
        noInternetBinding = _binding!!.noInternetLy
        val view = _binding!!.root
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        stopAnim()
        setUpViewModel()
        getDashBoardData()

        noInternetBinding?.btTryAgain?.setOnClickListener {
            getDashBoardData()
        }
        _binding?.llLocation?.setOnClickListener {

            var siteId = SharedPrefrence.getSiteId(requireContext())

            if (!siteId.isNullOrEmpty()) {
                val intent =Intent(requireContext(), LoginSelectSiteAddressActivity::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE, false)
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            } else {

              /*  locationDialog = PlaceAutoCompleteDialog()
                locationDialog.setListner(this)
                locationDialog.show(
                        childFragmentManager,
                        PlaceAutoCompleteDialog::class.java.simpleName
                )*/

                val intent = Intent(context, PlaceAutoCompleteActivity::class.java)
                startActivity(intent)
               // Bungee.slideUp(context)
               (context as Activity).overridePendingTransition(R.anim.bottom_up, R.anim.nothing);


            }

        }
    }


    override fun onResume() {
        super.onResume()
        val isLoogedIn = SharedPrefrence.getLogin(requireContext())
        val siteName = SharedPrefrence.getSiteName(requireContext())
        val siteId = SharedPrefrence.getSiteId(context)
        val stateName = SharedPrefrence.getStateName(requireContext())
        val cityname = SharedPrefrence.getCityname(context)

        if (isLoogedIn && !siteId.isNullOrEmpty() && !siteName.isNullOrEmpty()) {
            _binding?.llLocation?.visibility = View.VISIBLE
            _binding?.tvLocation?.text = siteName
        } else {


            if (!stateName.isNullOrEmpty() && !cityname.isNullOrEmpty()) {

                val strBuffer = StringBuffer()
                strBuffer.append((cityname))
                strBuffer.append(",")
                strBuffer.append(" ")
                strBuffer.append((stateName))

                _binding?.llLocation?.visibility = View.VISIBLE
                _binding?.tvLocation?.text = strBuffer.toString()
            } else {
                _binding?.llLocation?.visibility = View.GONE
            }

            navigateToDiaog()
        }
    }


    private fun setupUI() {
        _binding?.rvDashBoardList?.layoutManager = LinearLayoutManager(requireContext())
        dashboardAdapter = DashboardAdapter(requireActivity(), dashboardResponse,requireActivity().lifecycle)
        _binding?.rvDashBoardList?.adapter = dashboardAdapter
    }



    private fun navigateToDiaog() {
        val lastLatitude = SharedPrefrence.getlat(context)
        val lastLongitude = SharedPrefrence.getlang(context)
        val stateId = SharedPrefrence.getStateId(context)
        val cityId = SharedPrefrence.getCityId(context)


        val isLoogedIn = SharedPrefrence.getLogin(context)
        val siteId = SharedPrefrence.getSiteId(context)
        if (isLoogedIn && siteId.isNullOrEmpty()) {

            val intent = Intent(context, LoginSelectSiteAddressActivity::class.java)
            startActivity(intent)
            Bungee.slideLeft(context)

        } else {
            if (lastLatitude.isNullOrEmpty() && lastLongitude.isNullOrEmpty() && stateId.isNullOrEmpty() && cityId.isNullOrEmpty()) {
               /* locationDialog = PlaceAutoCompleteDialog()
                locationDialog.setListner(this)
                locationDialog.show(
                        childFragmentManager,
                        PlaceAutoCompleteDialog::class.java.simpleName
                )*/

                val intent = Intent(context, PlaceAutoCompleteActivity::class.java)
                startActivity(intent)
                //Bungee.slideUp(context)
                (context as Activity).overridePendingTransition(R.anim.bottom_up, R.anim.nothing);

            }
        }
    }

    private fun getDashBoardData() {
        if (Utils.isNetworkAvailable(context)) {

            _binding?.llMain?.visibility = View.VISIBLE
            noInternetBinding?.llnointernet?.visibility = View.GONE

            ApiClient.setToken()
            homeViewModel.getProductHome(null, null, null).observe(requireActivity(), Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->
                                    setResposerData(response.body()?.data)
                                }
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(context, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                    }
                }
            })
        } else {
            _binding?.llMain?.visibility = View.GONE
            noInternetBinding?.llnointernet?.visibility = View.VISIBLE
        }
    }

    private fun startAnim() {
        if (_binding?.avLoadingHome != null) {
            _binding?.avLoadingHome?.show()
            _binding?.avLoadingHome?.visibility = View.VISIBLE

        }
    }

    private fun setResposerData(data: ArrayList<ProductCategoryWiseDetailsProductCategory>?) {
        if (::productCategory.isInitialized) {
            productCategory.clear()
        }
        if (data != null) {
            productCategory = data
        }

        setUpdata()

    }

    private fun setUpdata() {

        val bannerDataList = java.util.ArrayList<BannerData>()
        val featureProductList = java.util.ArrayList<FeatureProduct>()
        val topBrand = java.util.ArrayList<TopBrand>()


        val bannerData1 = BannerData()
        bannerData1._id = "1"
        bannerData1.media_url =
                "https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-1.jpg"

        val bannerData2 = BannerData()
        bannerData2._id = "2"
        bannerData2.media_url =
                "https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-2.jpg"

        val bannerData3 = BannerData()
        bannerData3._id = "3"
        bannerData3.media_url =
                "https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-3.jpg"

        val bannerData4 = BannerData()
        bannerData4._id = "4"
        bannerData4.media_url =
                "https://do0hszra1gpv3.cloudfront.net/d/banner/slider-banner-4.jpg"

        bannerDataList.add(bannerData1)
        bannerDataList.add(bannerData2)
        bannerDataList.add(bannerData3)
        bannerDataList.add(bannerData4)

         val featureProduct1 = FeatureProduct()
         featureProduct1._id = "1"
         featureProduct1.image = R.drawable.banner_advt
         featureProductList.add(featureProduct1)

      /*  val featureProduct2 = FeatureProduct()
         featureProduct2._id = "1"
         featureProduct2.image = R.drawable.top_image2

         featureProductList.add(featureProduct1)
         featureProductList.add(featureProduct2)*/

        val topBrand1 = TopBrand()
        topBrand1._id = "1"
        topBrand1.image = R.drawable.top_image1

        val topBrand2 = TopBrand()
        topBrand2._id = "2"
        topBrand2.image = R.drawable.top_image2

        val topBrand3 = TopBrand()
        topBrand3._id = "3"
        topBrand3.image = R.drawable.top_image3

        topBrand.add(topBrand1)
        topBrand.add(topBrand2)
        topBrand.add(topBrand3)



        dashboardResponse.banners = bannerDataList
        dashboardResponse.featuredVenue = featureProductList
        dashboardResponse.topBrand = topBrand


        if (productCategory.isNotEmpty())
            dashboardResponse.productCategory = productCategory
        emptyView()
        setupUI()

    }




    override fun onDestroyView() {
        // Consider not storing the binding instance in a field
        // if not needed.
        _binding = null
        nodataBinding = null
        noInternetBinding = null
        super.onDestroyView()
    }

    private fun stopAnim() {
        if (_binding?.avLoadingHome != null) {
            _binding?.avLoadingHome?.hide()
            _binding?.avLoadingHome?.visibility = View.GONE

        }
    }

    private fun emptyView() {
        if (dashboardResponse.banners?.isNotEmpty()!! ||
            dashboardResponse.featuredVenue?.isNotEmpty()!! ||
            dashboardResponse.productCategory?.isNotEmpty()!! ||
            dashboardResponse.topBrand?.isNotEmpty()!!
        ) {
            nodataBinding?.llNodata?.visibility = View.GONE
            _binding?.rvDashBoardList?.visibility = View.VISIBLE

        } else {
            nodataBinding?.llNodata?.visibility = View.VISIBLE
            _binding?.tvLocation?.visibility = View.GONE
            _binding?.rvDashBoardList?.visibility = View.GONE
        }
    }


    private fun setUpViewModel() {

        homeViewModel =
            ViewModelProvider(this, HomeViewModel.ViewModelFactory(ApiClient().apiService))
                .get(HomeViewModel::class.java)

    }

    override fun onStop() {
        super.onStop()

    }
}