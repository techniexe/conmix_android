package com.conmix.app.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.activities.*
import com.conmix.app.data.LoginResponse
import com.conmix.app.data.UserProfileData
import com.conmix.app.data.UserProfileObj
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.RealPathUtil
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.NotificationViewModel
import com.conmix.app.viewmodels.UserProfileFrgViewModel
import com.conmix.app.databinding.MyProfileFgBinding
import com.conmix.app.databinding.NoInternetBinding
import com.conmix.app.utils.TextDrawable
import com.rajat.pdfviewer.PdfViewerActivity
import com.yalantis.ucrop.UCrop
import comCoreConstants.conmix.utils.CoreConstants
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import spencerstudios.com.bungeelib.Bungee
import java.io.File


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class ProfileFragment :Fragment(),View.OnClickListener{

    private lateinit var binding:MyProfileFgBinding
    var menuProfile: ImageView? = null
    private var noInternetBinding: NoInternetBinding?= null
    lateinit var userProfileFrgViewModel: UserProfileFrgViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    var userProfileObj: UserProfileObj? = null
    var isLogging: Boolean? = false
    private var SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage"
    private var lockAspectRatio = true
    private  var setBitmapMaxWidthHeight:Boolean = false
    private var ASPECT_RATIO_X = 1
    private  var ASPECT_RATIO_Y:Int = 1
    private  var bitmapMaxWidth:Int = 1000
    private  var bitmapMaxHeight:Int = 1000
    private var IMAGE_COMPRESSION = 80

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MyProfileFgBinding.inflate(inflater, container, false)
        noInternetBinding = binding.noInternetLy
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        menuProfile = activity?.findViewById<ImageView>(R.id.profile_more_menu)

        stopAnim()
        setupViewModel()
        binding.userNameTxt.setOnClickListener(this)
        binding.userNameSubTitle.setOnClickListener(this)
        binding.llMyOrderUs.setOnClickListener(this)
        binding.llSiteAdd.setOnClickListener(this)
        binding.llbillAdd.setOnClickListener(this)
        //binding.llwarrantyAdd.setOnClickListener(this)
        binding.llDeliveryAdd.setOnClickListener(this)
        binding.llprivacyAdd.setOnClickListener(this)
        binding.llhelpAdd.setOnClickListener(this)
        binding.llsupportTicket.setOnClickListener(this)
       // binding.editProfilePic.setOnClickListener(this)

        isLogging = SharedPrefrence.getLogin(requireContext())

        if (isLogging == true) {
            getUserPrfdataApiCall()
        } else {
            setupUI()
        }

        menuProfile?.setOnClickListener {
            showMenuPopup(menuProfile!!)
        }

        noInternetBinding?.btTryAgain?.setOnClickListener {
            if (isLogging == true) {
                getUserPrfdataApiCall()
            } else {
                setupUI()
            }
        }
    }

    private fun setupViewModel() {
        userProfileFrgViewModel =
            ViewModelProvider(
                this,
                UserProfileFrgViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(UserProfileFrgViewModel::class.java)

        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)
    }

    private fun getUserPrfdataApiCall() {

        if (Utils.isNetworkAvailable(context)) {

            binding.scrollMain.visibility = View.VISIBLE
            noInternetBinding?.llnointernet?.visibility = View.GONE


            ApiClient.setToken()
            userProfileFrgViewModel.getUserPrfData().observe(requireActivity(), Observer {

                it?.let { resource ->
                    when (resource.status) {

                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                setResposerData(response = resource.data.body())
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(context, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
        } else {
            binding.scrollMain.visibility = View.GONE
            noInternetBinding?.llnointernet?.visibility = View.VISIBLE
        }
    }
    private fun setResposerData(response: UserProfileData?) {
        this.userProfileObj = response!!.data
        setupUI()
    }

    private fun setupUI(){

        if(isLogging == true){
            binding.userPrfLnly.visibility = View.VISIBLE

            if(userProfileObj?.account_type =="Individual"){
                binding.userNameTxt.text = userProfileObj?.full_name
            }else{
                binding.userNameTxt.text = userProfileObj?.company_name
            }



            binding.userNameSubTitle.text = userProfileObj?.email
           // binding.editProfilePic.visibility = View.VISIBLE
            binding.llSiteAdd.visibility = View.VISIBLE
            binding.llbillAdd.visibility = View.VISIBLE
            binding.billlineView.visibility = View.VISIBLE
            binding.orderView.visibility = View.VISIBLE
            binding.siteboarderView.visibility = View.VISIBLE
            binding.llsupportTicket.visibility = View.VISIBLE
            binding.supportlineView.visibility = View.VISIBLE

            //logOutLnly?.visibility= View.VISIBLE

            /*val requestOptions = RequestOptions()
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            Glide.with(this)
                .load(userProfileObj?.identity_image_url ?: "").apply(requestOptions)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                })
                .into(binding.imgUser)*/



            val drawable = TextDrawable.builder().beginConfig()
                .textColor(ContextCompat.getColor(requireContext(), R.color.prf_img_txt))
                .bold().toUpperCase().endConfig()
                .buildRect(userProfileObj?.full_name?.first().toString(),ContextCompat.getColor(requireContext(), R.color.prf_img_back))


            binding.imgUser.setImageDrawable(drawable)

        }else{
            binding.userPrfLnly.visibility = View.VISIBLE
            binding.llMyOrderUs.visibility = View.GONE
            binding.orderView.visibility = View.GONE
          //  binding.editProfilePic.visibility = View.GONE
            binding.llSiteAdd.visibility = View.GONE
            binding.llbillAdd.visibility = View.GONE
            binding.billlineView.visibility = View.GONE
            binding.siteboarderView.visibility = View.GONE
            binding.llsupportTicket.visibility = View.GONE
            binding.supportlineView.visibility = View.GONE
            //logOutLnly?.visibility= View.GONE
            binding.userNameTxt.text = "Welcome!"
            binding.userNameSubTitle.text = "Login / Signup"
            val drawable = TextDrawable.builder().beginConfig()
                .textColor(ContextCompat.getColor(requireContext(), R.color.prf_img_txt))
                .bold().toUpperCase().endConfig()
                .buildRect("W",ContextCompat.getColor(requireContext(), R.color.prf_img_back))


            binding.imgUser.setImageDrawable(drawable)
        }
    }

    private fun startAnim() {
        binding.avLoading.show()

    }

    private fun stopAnim() {
        binding.avLoading.hide()

    }
    fun showMenuPopup(v: View) {
        val popup = PopupMenu(requireContext(), v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.profile_menu, popup.menu)
        popup.show()

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit_profile_menu -> {

                    val intent =  Intent(requireContext(), EditProfileActivity::class.java)
                    intent.putExtra("userObj",userProfileObj)
                    startActivityForResult(intent, 240)
                    Bungee.slideLeft(requireContext())

                    true
                }
                R.id.change_Password_menu -> {
                    val intent =  Intent(requireContext(), ChangePasswordActivity::class.java)
                    startActivity(intent)
                    Bungee.slideLeft(requireContext())
                    true
                }
                R.id.logOut_menu ->{

                    val fragment = BreakAlertDialog(
                        object : BreakAlertDialog.ClickListener {
                            override fun onDoneClicked() {
                                removeFCMToken()

                            }

                            override fun onCancelClicked() {


                            }
                        },
                        getString(R.string.alert_profile_log_out),
                        getString(R.string.txt_yes),
                        getString(R.string.txt_cancel)
                    )

                    fragment.show(childFragmentManager, "alert")
                    fragment.isCancelable = false


                    true
                }
                else -> false
            }
        }
    }


    private fun removeFCMToken() {
        ApiClient.setToken()
        val deviceId = SharedPrefrence.getDeviceId(requireContext())
        notificationViewModel.deleteToken(deviceId).observe(this,{
            it?.let {
                    resource ->
                when (resource.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {

                            SharedPrefrence.setlat(requireActivity(), null)
                            SharedPrefrence.setlang(requireActivity(), null)
                            SharedPrefrence.setStateId(requireActivity(), null)
                            SharedPrefrence.setCityId(requireActivity(), null)
                            SharedPrefrence.setSiteId(requireActivity(), null)
                            SharedPrefrence.setSiteName(requireActivity(), null)
                            SharedPrefrence.setStateName(requireActivity(), null)
                            SharedPrefrence.setCityName(requireActivity(), null)
                            SharedPrefrence.setLogin(requireActivity(), false)
                            SharedPrefrence.setDeviceID(requireActivity(),null)
                            SharedPrefrence.setCustomMixObj(requireContext(),null)
                            SharedPrefrence.setSessionToken(requireActivity(),null)
                            SharedPrefrence.setFCMtokem(requireContext(),null)

                            val intent = Intent(context, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            requireActivity().finish()
                            Bungee.slideRight(requireActivity())


                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }


    fun showSingleImage(urig: Uri) {


        //uploadImagesfun(urig)

        cropImage(urig)

    }


    private fun cropImage(sourceUri: Uri) {


        var destinationFileName = SAMPLE_CROPPED_IMAGE_NAME
        destinationFileName += ".jpg";

        val destinationUri = Uri.fromFile( File(requireContext().cacheDir, destinationFileName))

        // val destinationUri = Uri.fromFile(File(cacheDir, queryName(contentResolver, sourceUri)))
        val options = UCrop.Options()
        options.setCompressionQuality(IMAGE_COMPRESSION)
        options.setToolbarColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
        options.setStatusBarColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
       // options.setActiveWidgetColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
        if (lockAspectRatio) options.withAspectRatio(
            ASPECT_RATIO_X.toFloat(),
            ASPECT_RATIO_Y.toFloat()
        )
        if (setBitmapMaxWidthHeight) options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight)
        UCrop.of(sourceUri, destinationUri)
            .withOptions(options)
            .start(requireActivity())
    }

    override fun onClick(v: View?) {
        when(v!!.id){

            R.id.userNameTxt->{
                if(!isLogging!!) {
                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    startActivity(intent)
                    requireActivity().finish()
                    Bungee.slideRight(requireActivity())
                }
            }
            R.id.userNameSubTitle->{

                if(!isLogging!!) {
                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    startActivity(intent)
                    requireActivity().finish()
                    Bungee.slideRight(requireActivity())
                }

            }
            R.id.llMyOrderUs -> {
                if (isLogging!!) {
                    val intent = Intent(requireContext(), OrderListActivity::class.java)
                    intent.putExtra("userId", userProfileObj?._id)
                    startActivity(intent)
                    Bungee.slideLeft(requireContext())

                } else {
                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    startActivity(intent)
                    requireActivity().finish()
                    Bungee.slideRight(requireActivity())
                }
            }

            R.id.llSiteAdd -> {
                val intent = Intent(requireContext(), SelectSiteAddressProfileActivity::class.java)
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            }


            R.id.llbillAdd -> {
                val intent = Intent(requireContext(), SelectBillingAddProfileActivity::class.java)
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            }



            /*R.id.logOutLnly -> {
                val fragment = BreakAlertDialog(
                    object : BreakAlertDialog.ClickListener {
                        override fun onDoneClicked() {
                            // removeFCMToken()
                            SharedPrefrence.setlat(requireActivity(), "")
                            SharedPrefrence.setlang(requireActivity(), "")
                            SharedPrefrence.setStateId(requireActivity(), "")
                            SharedPrefrence.setCityId(requireActivity(), "")
                            SharedPrefrence.setSiteId(requireActivity(), "")
                            SharedPrefrence.setSiteName(requireActivity(), "")
                            SharedPrefrence.setStateName(requireActivity(), "")
                            SharedPrefrence.setCityName(requireActivity(), "")
                            SharedPrefrence.setLogin(requireActivity(), false)

                            val intent = Intent(context, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            requireActivity().finish()
                            Bungee.slideRight(requireActivity())
                        }

                        override fun onCancelClicked() {


                        }
                    },
                    getString(R.string.alert_profile_log_out),
                    getString(R.string.txt_yes),
                    getString(R.string.txt_cancel)
                )

                fragment.show(childFragmentManager, "alert")
                fragment.isCancelable = false
            }*/

            R.id.llwarrantyAdd->{
                /*val intent = Intent(requireContext(), AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, getString(R.string.txt_warenty))
                startActivity(intent)
                Bungee.slideLeft(requireContext())*/

               /* val intent = Intent(requireContext(), PDFViewActivity::class.java)
                startActivity(intent)
                Bungee.slideLeft(requireContext())*/

                startActivity(
                    PdfViewerActivity.launchPdfFromUrl(
                        requireContext(), "https://d2tlw91f21wjoy.cloudfront.net/d/order_track/Invoice_2204539307?i=1630056256820",
                        "Invoice", "dir",false
                    )
                )

            }
            R.id.llDeliveryAdd->{
                val intent = Intent(requireContext(), AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, getString(R.string.txt_delivry_policy))
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            }
            R.id.llprivacyAdd->{
                val intent = Intent(requireContext(), AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, getString(R.string.txt_privacy_policy))
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            }
            R.id.llhelpAdd->{
                val intent = Intent(requireContext(), AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, getString(R.string.txt_help))
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            }
            R.id.llsupportTicket->{
                val intent = Intent(requireContext(), SupprtNewActivity::class.java)
                startActivity(intent)
                Bungee.slideLeft(requireContext())
            }

          /*  R.id.edit_profile_pic->{
                TedImagePicker.with(requireContext())
                    .mediaType(MediaType.IMAGE)
                    .zoomIndicator(false)
                    .max(1, "Media can not be add more than 1")
                    //.scrollIndicatorDateFormat("YYYYMMDD")
                    //.buttonGravity(ButtonGravity.BOTTOM)
                    //.buttonBackground(R.drawable.btn_sample_done_button)
                    //.buttonTextColor(R.color.sample_yellow)
                    .errorListener { message -> Log.d("ted", "message: $message") }
                    .start { uri -> showSingleImage(uri) }
            }
*/
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {
                handleUCropResult(data)
            }
        }else  if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {
                getUserPrfdataApiCall()
            }
        }

    }

    fun handleUCropResult(data: Intent?){
        if (data == null) {
            setResultCancelled()
            return
        }
        val resultUri = UCrop.getOutput(data)
        Utils.GlidewithLoadUriImageNew(context, resultUri, binding.imgUser)
        uploadImagesfun(resultUri!!)
    }

    private fun setResultCancelled() {
        val intent = Intent()
        activity?.setResult(0, intent)

    }

    fun uploadImagesfun(selectedImg: Uri) {


        var parts: MultipartBody.Part? = null

        val realpath = RealPathUtil.getRealPath(requireContext(), Uri.parse(selectedImg.toString()))
        val file = File(realpath)
        val surveyBody = RequestBody.create("image/jpeg".toMediaTypeOrNull(), file)
        parts = MultipartBody.Part.createFormData("identity_image", file.name, surveyBody)
        val fullNameEdtRB =
            RequestBody.create(okhttp3.MultipartBody.FORM, binding.userNameTxt.text.toString())

        ApiClient.setToken()

   /*     userProfileFrgViewModel.uploadUserImage(fullNameEdtRB,parts)
            .observe(this) {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                Utils.showToast(
                                    requireContext(),
                                    "Successfully Upload Image",
                                    Toast.LENGTH_SHORT
                                )

                                resource.data.let { response -> setData(response.body()?.data) }

                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(
                                requireContext(),
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            }*/

        userProfileFrgViewModel.uploadUserImage(fullNameEdtRB,parts).observe(this,Observer{
            response ->
            when(response.status){
                Status.SUCCESS -> {

                    if (response.data?.isSuccessful!!) {
                        Utils.showToast(
                                requireContext(),
                                "Successfully Upload Image",
                                Toast.LENGTH_SHORT
                        )

                        response.data.let { response -> setData(response.body()?.data) }

                    } else {
                        Utils.setErrorData(requireContext(), response.data.errorBody())
                    }

                }
                Status.ERROR -> {

                    Utils.showToast(
                            requireContext(),
                            response.message.toString(),
                            Toast.LENGTH_LONG
                    )
                }
                Status.LOADING -> {

                }
            }
        })
    }

    private fun setData(response: LoginResponse?) {
        SharedPrefrence.setSessionToken(requireContext(), response?.customToken)
    }
}


