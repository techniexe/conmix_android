package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.repositories.ConcreateGradeListRepository

import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 17,March,2021
 */
class  ConcreateGradeListViewModel(private val concreateGradeListRepository: ConcreateGradeListRepository) : ViewModel() {


    fun getConcrateGradeList(site_id: String?, long: String?, lat: String?, garde_id: String?, before: String?, after: String?, order_by: String?, page_num: String?,delivery_date: String?,end_date: String?,quantity: Int?) =
            liveData(Dispatchers.IO) {
                emit(Resource.loading(data = null))
                try {
                    emit(
                            Resource.success(
                                    data = concreateGradeListRepository.getConcrateGradeList(
                                            site_id, long, lat, garde_id, before, after, order_by, page_num,delivery_date,end_date,quantity
                                    )
                            )
                    )
                } catch (exception: Exception) {
                    emit(
                        Resource.error(data = null, message = exception.message
                            ?: "Error Occurred!"))
                }
            }

    fun getConcrateGradeObj(designmix_id: String?, site_id: String?, long: String?, lat: String?,
                            delivery_date: String?,stateId:String?,with_TM:Boolean?,with_CP:Boolean?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = concreateGradeListRepository.getConcrateGradeObj(designmix_id, site_id, long, lat, delivery_date,stateId,with_TM,with_CP)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ConcreateGradeListViewModel::class.java)) {
                return ConcreateGradeListViewModel(ConcreateGradeListRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}