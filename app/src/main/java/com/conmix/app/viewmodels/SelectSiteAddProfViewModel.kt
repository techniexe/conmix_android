package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.repositories.SelectSiteAddProfileRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class SelectSiteAddProfViewModel (private val selectSiteAddProfileRepository: SelectSiteAddProfileRepository): ViewModel(){


    fun getSiteAdressdata(beforeTime:String?,afterTime:String?,search:String?)= liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = selectSiteAddProfileRepository.getSiteAddressList(beforeTime,afterTime,search)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun deleteSiteAddress(siteId:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try{
            emit(Resource.success(data = selectSiteAddProfileRepository.deleteSiteAddress(siteId)))
        } catch (exception:Exception){
            emit(Resource.error(data = null,message = exception.message?:"Error Occurred!"))
        }
    }

    fun deleteBillAddress(billAddressId:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try{
            emit(Resource.success(data = selectSiteAddProfileRepository.deleteBillAddress(billAddressId)))
        } catch (exception:Exception){
            emit(Resource.error(data = null,message = exception.message?:"Error Occurred!"))
        }
    }

    fun getBillAdressdata()= liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = selectSiteAddProfileRepository.getBillAddressList()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SelectSiteAddProfViewModel::class.java)) {
                return SelectSiteAddProfViewModel(SelectSiteAddProfileRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}