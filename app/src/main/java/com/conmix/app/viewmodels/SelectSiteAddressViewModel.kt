package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.repositories.SelectSiteAddressRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class SelectSiteAddressViewModel(private val selectSiteAddressRepositary: SelectSiteAddressRepository): ViewModel(){


    fun getSiteAdressdata(beforeTime: String?, afterTime: String?, search: String?) =
            liveData(Dispatchers.IO) {
                emit(Resource.loading(data = null))
                try {
                    emit(
                            Resource.success(
                                    data = selectSiteAddressRepositary.getSiteAddressList(
                                            beforeTime,
                                            afterTime,
                                            search
                                    )
                            )
                    )
                } catch (exception: Exception) {
                    emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }


  /*  fun addSiteAddress(siteaddressObject: OrderListObj) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                    Resource.success(
                            data = selectSiteAddressRepositary.addSiteAddress(
                                    siteaddressObject
                            )
                    )
            )
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }*/

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SelectSiteAddressViewModel::class.java)) {
                return SelectSiteAddressViewModel(SelectSiteAddressRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}