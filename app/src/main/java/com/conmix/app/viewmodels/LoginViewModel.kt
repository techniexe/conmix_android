package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.LoginReqModel
import com.conmix.app.repositories.LoginRepositary
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class LoginViewModel (private val loginRepository: LoginRepositary) : ViewModel() {

    fun loginWithIdentifier(userName: String, loginReqModel: LoginReqModel)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = loginRepository.loginWithIdentifier(userName,loginReqModel)))
        }catch (exception:Exception){
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
                return LoginViewModel(LoginRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }
    }
}