package com.conmix.app.viewmodels


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.CustomMixPostObj
import com.conmix.app.data.customMixFixObj
import com.conmix.app.repositories.CustomConmixRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 22,March,2021
 */
class CustomConmixViewModel(private val customConmixRepository: CustomConmixRepository): ViewModel() {


    fun getAdmixtureBandData(search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getAdmixtureBandData(search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getAggregateSandCategoryData(search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getAggregateSandCategoryData(search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getAggregateSandSubCategoryData(categoryId:String?,search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getAggregateSandSubCategoryData(categoryId,search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun getAggregateSandSubCategory2Data(id:String?,search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getAggregateSandSubCategory2Data(id,search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun getCementBandData(search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getCementBandData(search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getAggregateSourceData() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getAggregateSourceData()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getFlyAshSourceData() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getFlyAshSourceData()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getSandSourceData() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getSandSourceData()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun getAdmixureCategoryData(brandId:String?,search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getAdmixureCategoryData(brandId,search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getCementGradeData() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getCementGradeData()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getProductHome(search:String?,before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getProductHome(search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun postCustomMixData(customMixPostObjRequest: CustomMixPostObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.postCustomMixData(customMixPostObjRequest)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getCustomMixDetailData(vendorId:String?,customMixPostObjRequest: CustomMixPostObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getCustomMixDetailData(vendorId,customMixPostObjRequest)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun getCustomMixFixData(customMixFixObj: customMixFixObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customConmixRepository.getCustommixFixData(customMixFixObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }



    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CustomConmixViewModel::class.java)) {
                return CustomConmixViewModel(CustomConmixRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}