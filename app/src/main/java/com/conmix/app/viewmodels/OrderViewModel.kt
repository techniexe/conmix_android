package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.AssignTMOBJ
import com.conmix.app.data.GatewayTranstedIdObj
import com.conmix.app.data.PaymentTranstedIdObj
import com.conmix.app.data.checkTmAvlObj
import com.conmix.app.repositories.OrderRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers
import retrofit2.http.Path


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class OrderViewModel(private val orderRepository: OrderRepository): ViewModel() {


    fun postOrderData(razorpayPaymentId: GatewayTranstedIdObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.postOrderData(razorpayPaymentId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getOrderlstData(user_id: String?,before: String?,after: String?,order_status: String?,payment_status: String?,gateway_transaction_id: String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.getOrderlstData(user_id,before,after,order_status,payment_status,gateway_transaction_id)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getOrderDetailLstData(orderId:String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.getOrderDetailLstData(orderId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun cancelOrderdta(paymentTranstedIdObj: PaymentTranstedIdObj?,order_item_id:String?, order_id:String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.cancelOrderdta(paymentTranstedIdObj,order_item_id,order_id)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun cancelOrderItem(orderId:String?,itemId:String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.cancelOrderItem(orderId,itemId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
    fun processorder(orderId: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.processOrder(orderId)))
        } catch (exception: java.lang.Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun checkTmAvalibity(checkTmAvlObj: checkTmAvlObj?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.checkTmAvalibity(checkTmAvlObj)))
        } catch (exception: java.lang.Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun UpdateAssignTm( order_id:String?, order_item_id:String?,assignTmOBJ: AssignTMOBJ?)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = orderRepository.UpdateAssignTm(order_id,order_item_id,assignTmOBJ)))
        } catch (exception: java.lang.Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }





    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(OrderViewModel::class.java)) {
                return OrderViewModel(OrderRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}