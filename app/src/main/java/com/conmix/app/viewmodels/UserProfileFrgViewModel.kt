package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.repositories.UserProfileFrgRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.lang.Exception


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class UserProfileFrgViewModel(val userProfileFrgRepository: UserProfileFrgRepository): ViewModel() {


    fun getUserPrfData()= liveData(Dispatchers.IO){
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = userProfileFrgRepository.getUserPrfData()))
        }
        catch (e: Exception){
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun uploadUserImage(fullName: RequestBody?, parts: MultipartBody.Part) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = userProfileFrgRepository.uploadUserImageServer(fullName,parts)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }



    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(UserProfileFrgViewModel::class.java)) {
                return UserProfileFrgViewModel(UserProfileFrgRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }
    }
}