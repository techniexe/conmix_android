package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.FcmData
import com.conmix.app.repositories.NotificationRepositary
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 19,May,2021
 */
class NotificationViewModel(private val notificationRepositary: NotificationRepositary) :
    ViewModel() {


    fun ragisterToken(fcmData: FcmData) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.ragisterNotification(fcmData)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun deleteToken(deviceId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.deleteToken(deviceId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun notificationCount() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.notificationCount()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getNotification(beforeTime: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.notificationList(beforeTime)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun notificationItemClick(notificationId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.notificationSeen(notificationId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {
                return NotificationViewModel(NotificationRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }

}