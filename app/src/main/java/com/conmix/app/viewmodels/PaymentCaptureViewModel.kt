package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.PaymentCaptureObj
import com.conmix.app.repositories.PaymentCaptureRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception


/**
 * Created by Hitesh Patel on 08,June,2021
 */
class PaymentCaptureViewModel(private val paymentCaptureRepository: PaymentCaptureRepository): ViewModel() {

    fun paymentCapture(authheader:String,payment_id:String,paymentCaptureObj: PaymentCaptureObj) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = paymentCaptureRepository.paymentCapture(authheader,payment_id,paymentCaptureObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PaymentCaptureViewModel::class.java)) {
                return PaymentCaptureViewModel(PaymentCaptureRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}