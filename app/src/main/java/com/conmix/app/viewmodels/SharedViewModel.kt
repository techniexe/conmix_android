package com.conmix.app.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.conmix.app.data.PaymentError
import com.conmix.app.data.PaymentSuccess


/**
 * Created by Hitesh Patel on 07,June,2021
 */
class SharedViewModel : ViewModel() {
    private val onSuccess: MutableLiveData<Boolean> = MutableLiveData()
    private val onError: MutableLiveData<Boolean> = MutableLiveData()
    private var paymentError: PaymentError? = null
    private var paymentSuccess: PaymentSuccess? = null
    fun getOnSuccess(): LiveData<Boolean> {
        return onSuccess
    }

    fun getOnError(): LiveData<Boolean> {
        return onError
    }

    fun setOnSuccess(b: Boolean?) {
        onSuccess.setValue(b!!)
    }

    fun setOnError(b: Boolean?) {
        onError.setValue(b!!)
    }

    fun getPaymentError(): PaymentError? {
        return paymentError
    }

    fun setPaymentError(paymentError: PaymentError?) {
        this.paymentError = paymentError
    }

    fun getPaymentSuccess(): PaymentSuccess? {
        return paymentSuccess
    }

    fun setPaymentSuccess(paymentSuccess: PaymentSuccess?) {
        this.paymentSuccess = paymentSuccess
    }
}