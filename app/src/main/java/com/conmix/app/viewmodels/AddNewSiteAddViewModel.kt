package com.conmix.app.viewmodels


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.*
import com.conmix.app.repositories.AddNewSiteAddressRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 09,March,2021
 */
class AddNewSiteAddViewModel(private val addNewSiteAddressRepository: AddNewSiteAddressRepository): ViewModel() {


    fun addStateCity(citydetails: Citydetails) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = addNewSiteAddressRepository.addStateCity(citydetails)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }

    }

    fun getCountryData() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = addNewSiteAddressRepository.getCountryvalue()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }

    }

    fun getStateData(countryId:String?)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = addNewSiteAddressRepository.getStateDataValue(countryId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getEditStateData()= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = addNewSiteAddressRepository.getBillStateDataValue()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getCityData(stateId: String?)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = addNewSiteAddressRepository.getCityDataValue(stateId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun postAddNewAddress(siteobj: AddSiteAddPostData) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= addNewSiteAddressRepository.postAddNewData(siteobj)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun postAddNewBillAddress(billAddobj: AddBillAddPostObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= addNewSiteAddressRepository.postAddBillNewData(billAddobj)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun updateBillAddNewAdd(id:String?,billAddobj: AddBillAddPostObj) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= addNewSiteAddressRepository.updateBillAddNewAdd(id,billAddobj)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun addBillAddressCart(addBillAddIdCart: AddBillAddressIdCart?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= addNewSiteAddressRepository.addBillAddressCart(addBillAddIdCart)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun checkSiteAdd(checkSiteObj: CheckSiteNameObj) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= addNewSiteAddressRepository.checkSiteAdd(checkSiteObj)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    /*class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AddNewSiteAddViewModel::class.java)) {
                return AddNewSiteAddViewModel(AddNewSiteAddressRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }*/

    class ViewModelFactory(private val apiInterface: ApiInterface) :
        ViewModelProvider.NewInstanceFactory() {

         override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AddNewSiteAddViewModel::class.java)) {
                return AddNewSiteAddViewModel(AddNewSiteAddressRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}