package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.SignUpRegisterModel
import com.conmix.app.data.UpdateUserProfileObj
import com.conmix.app.repositories.SignUpRegisterRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class SignUpRegisterViewModel(private val  signUpRegisterRepository: SignUpRegisterRepository): ViewModel() {

    fun signUpRegisterUser(signupType:String,signupRegisterModel: SignUpRegisterModel) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = signUpRegisterRepository.postSignupUserData(signupType,signupRegisterModel)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun updateUserProfileData(signupRegisterModel: UpdateUserProfileObj) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = signUpRegisterRepository.updateUserProfileData(signupRegisterModel)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
    fun getPaymnetType(search:String?,before:String?,after:String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = signUpRegisterRepository.getPaymnetType(search,before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SignUpRegisterViewModel::class.java)) {
                return SignUpRegisterViewModel(SignUpRegisterRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}