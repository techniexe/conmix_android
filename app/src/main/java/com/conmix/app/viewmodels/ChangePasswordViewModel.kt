package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.ChnagePasswordProfileObj
import com.conmix.app.data.UpdateUserPasswordProfileObj
import com.conmix.app.repositories.ChangePasswordRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class ChangePasswordViewModel(private val changePasswordRepository: ChangePasswordRepository): ViewModel() {

    fun updatePasswordData(passwordObj: UpdateUserPasswordProfileObj?) = liveData(
            Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = changePasswordRepository.changePassword(passwordObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun changePasswordData(passwordObj: ChnagePasswordProfileObj?) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = changePasswordRepository.changePasswordNew(passwordObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

     fun getUserOtp() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = changePasswordRepository.getUserOTP()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) :
        ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ChangePasswordViewModel::class.java)) {
                return ChangePasswordViewModel(ChangePasswordRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}