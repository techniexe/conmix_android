package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.AddSiteAddPostData
import com.conmix.app.data.Citydetails
import com.conmix.app.repositories.EditNewSiteAddressRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class EditSiteAddViewModel(private val editNewSiteAddressRepository: EditNewSiteAddressRepository): ViewModel() {

    fun getCountryData()= liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= editNewSiteAddressRepository.getCountryvalue()))
        }catch (exception:Exception){
            emit(Resource.error(data = null,message = exception.message?:"Error Occurred!"))
        }

    }

    fun getStateData(countryId:String)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = editNewSiteAddressRepository.getStateDataValue(countryId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getCityData(stateId: String)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = editNewSiteAddressRepository.getCityDataValue(stateId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getNewAddress(siteId: String) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= editNewSiteAddressRepository.getSiteAddressValue(siteId)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun updateSiteAddress(siteId: String,addSiteAddPostData: AddSiteAddPostData) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data= editNewSiteAddressRepository.updateSiteAddressValue(siteId,addSiteAddPostData)))
        }catch (exception:Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun addStateCity(citydetails: Citydetails) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = editNewSiteAddressRepository.addStateCity(citydetails)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }

    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(EditSiteAddViewModel::class.java)) {
                return EditSiteAddViewModel(EditNewSiteAddressRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }


}