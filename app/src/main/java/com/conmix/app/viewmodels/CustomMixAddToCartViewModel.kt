package com.conmix.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmix.app.data.AddToCartCustomMixObj
import com.conmix.app.data.CartAddressIdObj
import com.conmix.app.data.DeleteCartObj

import com.conmix.app.repositories.CustomMixAddToCartRepository
import com.conmix.app.services.api.ApiInterface
import com.conmix.app.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 05,April,2021
 */
class CustomMixAddToCartViewModel(private val customMixAddToCartRepository: CustomMixAddToCartRepository): ViewModel() {


    fun postCustomMixAddToCartObj(customMixPostObjRequest: AddToCartCustomMixObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customMixAddToCartRepository.postCustomMixAddToCartObj(customMixPostObjRequest)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun getCartData(state_id:String?,long:Double?,lat:Double?,with_TM:Boolean?,with_CP:Boolean?,delivery_date:String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customMixAddToCartRepository.getCartData(state_id,long,lat,with_TM,with_CP,delivery_date)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getCartDataWithId(cartId:String?,state_id:String?,long:Double?,lat:Double?,with_TM:Boolean?,with_CP:Boolean?,delivery_date:String?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customMixAddToCartRepository.getCartDataWithId(cartId,state_id,long,lat,with_TM,with_CP,delivery_date)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun deleteCartItem(deleteCartObj: DeleteCartObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customMixAddToCartRepository.deleteCartItem(deleteCartObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun cartAddressAdd(cartAddressIdObj: CartAddressIdObj?) = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = customMixAddToCartRepository.cartAddressAdd(cartAddressIdObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CustomMixAddToCartViewModel::class.java)) {
                return CustomMixAddToCartViewModel(CustomMixAddToCartRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}