package com.conmix.app.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.conmix.app.data.DesignListObj
import com.conmix.app.databinding.ProductGradeInfoBottomBinding
import com.conmix.app.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 * Created by Hitesh Patel on 17,March,2021
 */
class GradeBottomsheet(
    var dta: DesignListObj,
    var title: String
) : BottomSheetDialogFragment() {

    private var _binding: ProductGradeInfoBottomBinding? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            @Nullable container: ViewGroup?,
            @Nullable savedInstanceState: Bundle?
    ): View? {
        _binding = ProductGradeInfoBottomBinding.inflate(inflater, container, false)
        val view = _binding?.root


        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupUI(view)
    }


    private fun setupUI(view: View) {

        _binding?.titleTxt?.text = title+" - "+dta.product_name.toString()
        _binding?.cementValueTxt?.text = dta.cement_quantity?.let { Utils.setPrecesionFormate(it) }
        _binding?.sandValueTxt?.text = dta.sand_quantity?.let { Utils.setPrecesionFormate(it) }
        _binding?.flyashValueTxt?.text = dta.fly_ash_quantity?.let { Utils.setPrecesionFormate(it) }
        _binding?.admixureValueTxt?.text = dta.ad_mixture_quantity?.let { Utils.setPrecesionFormate(it) }
        _binding?.aggregate1TitleTxt?.text = dta.aggregate1_sand_category?.sub_category_name+" (Kg)"
        _binding?.aggregate1ValueTxt?.text = dta.aggregate1_quantity?.let { Utils.setPrecesionFormate(it) }
        _binding?.aggregate2TitleTxt?.text = dta.aggregate2_sand_category?.sub_category_name+" (Kg)"
        _binding?.aggregate2ValueTxt?.text = dta.aggregate2_quantity?.let { Utils.setPrecesionFormate(it) }
        _binding?.waterValueTxt?.text = dta.water_quantity?.let { Utils.setPrecesionFormate(it) }

        _binding?.closeBtn?.setOnClickListener {
            dismiss()
        }

    }

}