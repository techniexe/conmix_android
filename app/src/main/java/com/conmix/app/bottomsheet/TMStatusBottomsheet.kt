package com.conmix.app.bottomsheet

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.conmix.app.activities.TMReportActivity
import com.conmix.app.data.TrackOrderLstobj
import com.conmix.app.databinding.TmDetailOptionSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rajat.pdfviewer.PdfViewerActivity
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 03,September,2021
 */
class TMStatusBottomsheet(
    var mcontext:Context,
    var orderItemListData: TrackOrderLstobj?

) : BottomSheetDialogFragment() {

    private var bindingInfo: TmDetailOptionSheetBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        bindingInfo = TmDetailOptionSheetBinding.inflate(inflater, container, false)
        val view = bindingInfo?.root
        return view


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(view)
    }

    private fun setupUI(view: View) {


        if(orderItemListData?.invoice_url.isNullOrEmpty()){
            bindingInfo?.tvViewInvoice?.visibility = View.GONE
            bindingInfo?.tvViewInvoiceView?.visibility = View.GONE
        }else{
            bindingInfo?.tvViewInvoice?.visibility = View.VISIBLE
            bindingInfo?.tvViewInvoiceView?.visibility = View.VISIBLE
        }


        if(orderItemListData?.qube_test_report_7days.isNullOrEmpty()){
            bindingInfo?.sevenCubeReportBtn?.visibility = View.GONE
            bindingInfo?.sevenCubeReportView?.visibility = View.GONE
        }else{
            bindingInfo?.sevenCubeReportBtn?.visibility = View.VISIBLE
            bindingInfo?.sevenCubeReportView?.visibility = View.VISIBLE
        }


        if(orderItemListData?.qube_test_report_28days.isNullOrEmpty()){
            bindingInfo?.twentyeightCubeReportBtn?.visibility = View.GONE
            bindingInfo?.twentyeightCubeReportView?.visibility = View.GONE
        }else{
            bindingInfo?.twentyeightCubeReportBtn?.visibility = View.VISIBLE
            bindingInfo?.twentyeightCubeReportView?.visibility = View.VISIBLE
        }




        bindingInfo?.sevenCubeReportBtn?.setOnClickListener {
            dismiss()
            val intent = Intent(mcontext, TMReportActivity::class.java)
            intent.putExtra(
                "image",
                orderItemListData?.qube_test_report_7days
            )

            mcontext.startActivity(intent)
            Bungee.slideLeft(mcontext)


        }


        bindingInfo?.twentyeightCubeReportBtn?.setOnClickListener {
            dismiss()
            val intent = Intent(mcontext, TMReportActivity::class.java)
            intent.putExtra(
                "image",
                orderItemListData?.qube_test_report_28days
            )

            mcontext.startActivity(intent)
            Bungee.slideLeft(mcontext)
        }

        bindingInfo?.tvViewInvoice?.setOnClickListener {
            dismiss()
            startActivity(
                PdfViewerActivity.launchPdfFromUrl(
                    requireContext(), orderItemListData?.invoice_url,
                    "Invoice", "dir",false
                )
            )

        }


        bindingInfo?.tvCancel?.setOnClickListener {
            dismiss()

        }

    }

}