package com.conmix.app.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.conmix.app.adapter.OrderDetailLstAdapter
import com.conmix.app.data.OrderDetailItemDta
import com.conmix.app.databinding.LayoutOrderBottomsheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 * Created by Hitesh Patel on 23,April,2021
 */
class OrderStatusBottomsheet(
    var mListener: OrderDetailLstAdapter.SelectOrderItemStatus?,
    var orderItemListData: OrderDetailItemDta?,
    var isCancel:Boolean
) : BottomSheetDialogFragment() {

    private var bindingInfo: LayoutOrderBottomsheetBinding? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            @Nullable container: ViewGroup?,
            @Nullable savedInstanceState: Bundle?
    ): View? {
        bindingInfo = LayoutOrderBottomsheetBinding.inflate(inflater, container, false)
        val view = bindingInfo?.root
        return view


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(view)
    }

    private fun setupUI(view: View) {


        if(orderItemListData?.item_status == "PLACED" || orderItemListData?.item_status == "PROCESSING"){
            bindingInfo?.tvCancelItemorder?.visibility = View.VISIBLE
            bindingInfo?.cancelItemorderVw?.visibility = View.VISIBLE
        }else{
            bindingInfo?.tvCancelItemorder?.visibility = View.GONE
            bindingInfo?.cancelItemorderVw?.visibility = View.GONE
        }


        if(orderItemListData?.item_status == "CANCELLED" || orderItemListData?.item_status == "REJECTED" || orderItemListData?.item_status == "LAPSED" ) {
            bindingInfo?.trackOrderMenuBtn?.visibility = View.GONE
            bindingInfo?.trackOrderMenuView?.visibility = View.GONE
            bindingInfo?.assignOrderMenuBtn?.visibility = View.GONE
            bindingInfo?.assignOrderMenuView?.visibility = View.GONE
        }else{
            bindingInfo?.trackOrderMenuBtn?.visibility = View.VISIBLE
            bindingInfo?.trackOrderMenuView?.visibility = View.VISIBLE
            bindingInfo?.assignOrderMenuBtn?.visibility = View.VISIBLE
            bindingInfo?.assignOrderMenuView?.visibility = View.VISIBLE
        }


        if(orderItemListData?.item_status == "DELIVERED"){
            bindingInfo?.tvRepeatItemorder?.visibility = View.VISIBLE
            bindingInfo?.repeatItemorderVw?.visibility = View.VISIBLE
            if(orderItemListData?.review == null){
                bindingInfo?.tvWriteReview?.visibility = View.VISIBLE
                bindingInfo?.writeReviewView?.visibility = View.VISIBLE

            }else{
                bindingInfo?.tvWriteReview?.visibility = View.GONE
                bindingInfo?.writeReviewView?.visibility = View.GONE

            }


        }else{
            bindingInfo?.tvRepeatItemorder?.visibility = View.GONE
            bindingInfo?.repeatItemorderVw?.visibility = View.GONE
            bindingInfo?.tvWriteReview?.visibility = View.GONE
            bindingInfo?.writeReviewView?.visibility = View.GONE

        }


        bindingInfo?.tvWriteReview?.setOnClickListener {
            dismiss()
            mListener?.onReviewItem(orderItemListData)
        }

        bindingInfo?.tvRepeatItemorder?.setOnClickListener {
            dismiss()
            mListener?.onRepeatOrderItem(orderItemListData)
        }

        bindingInfo?.tvCancelItemorder?.setOnClickListener {
            dismiss()
            mListener?.onCancelOrdeItem(orderItemListData)
        }

        bindingInfo?.assignOrderMenuBtn?.setOnClickListener {
            dismiss()
            mListener?.onAssignOrderTrack(orderItemListData)
        }

        bindingInfo?.trackOrderMenuBtn?.setOnClickListener {
            dismiss()
            mListener?.onTrackOrderItem(orderItemListData)
        }

        bindingInfo?.tvCancel?.setOnClickListener {
            dismiss()

        }

    }

}