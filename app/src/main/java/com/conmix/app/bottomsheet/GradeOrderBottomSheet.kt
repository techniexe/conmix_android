package com.conmix.app.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable

import com.conmix.app.data.OrderDetailItemDta
import com.conmix.app.databinding.DesignMixDescBottomBinding
import com.conmix.app.utils.Utils

import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 * Created by Hitesh Patel on 22,April,2021
 */
class GradeOrderBottomSheet(
        var designListObj: OrderDetailItemDta
) : BottomSheetDialogFragment() {

    private var bindingInfo: DesignMixDescBottomBinding? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            @Nullable container: ViewGroup?,
            @Nullable savedInstanceState: Bundle?
    ): View? {
        bindingInfo = DesignMixDescBottomBinding.inflate(inflater, container, false)
        val view = bindingInfo?.root


        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupUI(view)
    }


    private fun setupUI(view: View) {



        if(designListObj?.design_mix != null){
            bindingInfo?.designMixTitleTxt?.text = designListObj.concrete_grade?.name +" - "+designListObj.design_mix?.product_name
            //bindingInfo?.titleTxt?.text = designListObj.concrete_grade?.name +" - "+designListObj.design_mix?.product_name
        }else{
            bindingInfo?.designMixTitleTxt?.text = designListObj?.concrete_grade?.name+" - Custom Mix"
            //bindingInfo?.titleTxt?.text = designListObj?.concrete_grade?.name+" - Custom Mix"
        }

        bindingInfo?.cementValueTxt?.text = designListObj?.cement_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.sandValueTxt?.text = designListObj?.sand_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.flyashValueTxt?.text = designListObj?.fly_ash_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.admixureValueTxt?.text = designListObj?.admix_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.aggregate1TitleTxt?.text = designListObj?.aggregate1_sub_cat?.sub_category_name+" (kg) : "
        bindingInfo?.aggregate1ValueTxt?.text = designListObj?.aggregate1_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.aggregate2TitleTxt?.text = designListObj?.aggregate2_sub_cat?.sub_category_name+" (kg) : "
        bindingInfo?.aggregate2ValueTxt?.text = designListObj?.aggregate2_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.waterValueTxt?.text = designListObj?.water_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo?.gradeValueTxt?.text = designListObj.concrete_grade?.name


        val stringBuffer = StringBuffer()
        stringBuffer.append("Cement Brand : ")
        stringBuffer.append(designListObj?.cement_brand?.name?:" - ")
        stringBuffer.append("; Sand Source : ")
        stringBuffer.append(designListObj?.sand_source?.sand_source_name?:" - ")
        stringBuffer.append("; Aggregate Source : ")
        stringBuffer.append(designListObj?.aggregate_source?.aggregate_source_name?:" - ")
        stringBuffer.append("; Admixture Brand : ")
        stringBuffer.append(designListObj?.admix_brand?.name?:" - ")
        stringBuffer.append("; Fly Ash Source Name : ")
        stringBuffer.append(designListObj?.fly_ash_source?.fly_ash_source_name?:" - ")


        bindingInfo?.designMixDescValue?.text = stringBuffer.toString()


        bindingInfo?.closeBtn?.setOnClickListener {
            dismiss()
        }
    }
}