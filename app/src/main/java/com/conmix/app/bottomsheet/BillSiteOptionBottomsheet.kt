package com.conmix.app.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.conmix.app.adapter.SelectBillingSiteProfileAdapter
import com.conmix.app.data.BillAddressObjLst
import com.conmix.app.data.SiteAddressObjLst
import com.conmix.app.databinding.LayoutSiteBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 * Created by Hitesh Patel on 29,September,2021
 */
class BillSiteOptionBottomsheet(
    var mListener: SelectBillingSiteProfileAdapter.SelectSiteProfileAddressInterface?,
    var dta: BillAddressObjLst?
) : BottomSheetDialogFragment() {

    private var _binding: LayoutSiteBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        _binding = LayoutSiteBinding.inflate(inflater, container, false)
        val view = _binding?.root
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(view)
    }

    private fun setupUI(view: View) {



        _binding?.tvEdit?.setOnClickListener {
            dismiss()
            mListener?.onEditAdd(dta!!)
        }
        _binding?.tvDelete?.setOnClickListener {
            dismiss()
            mListener?.onDeleteAdd(dta!!._id!!)
        }
        _binding?.tvCancel?.setOnClickListener {
            dismiss()

        }

    }

}