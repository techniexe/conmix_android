package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 12,March,2021
 */
@Parcelize
data class SiteAddressListData(val data: ArrayList<SiteAddressObjLst>) : Parcelable

@Parcelize
data class SiteAddressObjLst(
    var user_id: String,
    var _id: String,
    var company_name: String?,
    var site_name: String?,
    var address_line1: String?,
    var address_line2: String?,
    var country_id: Int? = 0,
    var state_id: String?,
    var city_id: String?,
        //var sub_city_id: String?,
    var city_name: String?,
    var country_code: String?,
    var country_name: String?,
    var state_name: String?,
    var pincode: Int?,
    var location: ProductCategoryWiseListPickupLocation,
    var person_name: String?,
    var title: String?,
    var email: String?,
    var email_verified: Boolean? = false,
    var mobile_number: String?,
    var mobile_number_verified: Boolean? = false,
    var alt_mobile_number: String?,
    var alt_mobile_number_verified: Boolean? = false,
    var whatsapp_number: String?,
    var whatsapp_number_verified: Boolean? = false,
    var landline_number: String?,
    var profile_pic: String?,
    var created_at: String?,
    var isSelected: Boolean? = false
) : Parcelable

@Parcelize
data class SiteAddressObjModel(val data : SiteAddressObjLst): Parcelable


//  Address Billing Objects
@Parcelize
data class BillAddressListData(val data: ArrayList<BillAddressObjLst>) : Parcelable

@Parcelize
data class BillAddressObjLst(
    var _id: String?,
    var company_name: String?,
    var line1: String?,
    var line2: String?,
    var state_id: String?,
    var city_id: String?,
    var pincode: Int?,
    var gst_number:String?,
    var user:BillAddUserObj?,
    var created_at:String?,
    var stateDetails:StateModel?,
    var cityDetails:CityModel?,
    var isSelected: Boolean? = false,
    var buyerDetails:buyerBillAccountType?,
    var full_name:String?
) : Parcelable


@Parcelize
data class buyerBillAccountType(var account_type:String?):Parcelable

@Parcelize
data class BillAddUserObj(var user_type:String?,
                          var user_id:String?):Parcelable

@Parcelize
data class BillAddressObjInfoCart(
    var _id: String?,
    var company_name: String?,
    var line1: String?,
    var line2: String?,
    var state_id: String?,
    var city_id: String?,
    var pincode: Int?,
    var gst_number:String?,
    var user:BillAddUserObj?,
    var city_name:String?,
    var country_code:String?,
    var country_name:String?,
    var state_name:String?,
    var full_name: String?

):Parcelable



