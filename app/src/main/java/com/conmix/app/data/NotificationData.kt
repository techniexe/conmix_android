package com.conmix.app.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


/**
 * Created by Hitesh Patel on 19,May,2021
 */
@Parcelize
data class NotificationData(
    val _id: String? = null,
    val notification_type: Int? = null,
    val created_at: String? = null,
    val to_user: NotificationDataFromUser? = null,
    val order: Order? = null,
    var seen_on: String? = null,
    val order_item: NotificationOrderItem? = null,
    val vendor:NotiVendorObj?= null,
    val order_track:OrderTrackObj?= null,
    val site:SiteNotiObj?= null,
    val CP_order_track:CPOrderTrackObj?= null,
    val supportTicket:supportTicketObj?=null
) : Parcelable


@Parcelize
data class supportTicketObj(val _id:String?,val ticket_id:String?):Parcelable


@Parcelize
data class CPOrderTrackObj(val _id:String?,val delayTime:String?,val reasonForDelay:String?):Parcelable

@Parcelize
data class SiteNotiObj(val _id:String?,val site_name:String?):Parcelable

@Parcelize
data class OrderTrackObj(val _id:String?,val delayTime:String?,val reasonForDelay:String?):Parcelable

@Parcelize
data class NotiVendorObj(val _id:String?,val full_name:String?):Parcelable

@Parcelize
data class NotificationDataList(var notifications: ArrayList<NotificationData>) : Parcelable

@Parcelize
data class NotificationOrderItem(val _id:String?,val display_item_id:String?): Parcelable

@Parcelize
data class NotificationDataFromUser(
    val _id: Boolean? = null,
    val full_name: String? = null,
    val company_name:String?= null,
    val company_type:String? = null,
    val account_type:String?= null
) : Parcelable

@Parcelize
data class Order(
    var _id: String? = "",
    var display_id: String? = "") : Parcelable