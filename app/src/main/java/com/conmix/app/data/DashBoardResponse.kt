package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,March,2021
 */
@Parcelize
data class DashBoardResponse(
    var banners: ArrayList<BannerData>?=null,
    var productCategory: ArrayList<ProductCategoryWiseDetailsProductCategory>?=null,
    var featuredVenue: ArrayList<FeatureProduct>?=null,
    var topBrand: ArrayList<TopBrand>?=null
) : Parcelable
