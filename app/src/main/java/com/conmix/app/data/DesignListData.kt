package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 16,March,2021
 */
@Parcelize
data class DesignListData(val data: ArrayList<DesignListObj>) : Parcelable


@Parcelize
data class DesignListObjMain(val data: DesignListObj?) : Parcelable

@Parcelize
data class DesignListObj(val _id: String?,
                         var product_name: String?,
                         var cement_quantity: Double?,
                         var sand_quantity: Double?,
                         var aggregate1_quantity: Double?,
                         var aggregate2_quantity: Double?,
                         var fly_ash_quantity: Double?,
                         var ad_mixture_quantity: Double?,
                         var water_quantity: Double?,
                         var selling_price: Double?,
                         var is_available: Boolean?,
                         var description: String?,
                         var is_custom: Boolean?,
                         var design_mix_id: String?,
                         var calculated_distance: Double?,
                         var vendor: DisignVendorObj?,
                         var vendor_media: ArrayList<DisignVendorMediaObj>?,
                         var concrete_grade: DisignConcreteGradeObj?,
                         var cement_brand: DisignCementBrandObj?,
                         var sand_source: DisignSandSourceObj?,
                         var aggregate_source: DisignAggregateSourceObj?,
                         var aggregate1_sand_category: DisignAggreSorcCat1Obj?,
                         var aggregate2_sand_category: DisignAggreSorcCat1Obj?,
                         var fly_ash_source: DisignFlyAshSourceObj?,
                         var admixture_brand: DisignAdmixtureBrandObj?,
                         var cement_grade: DisignCementGradeObj?,
                         var admixture_category: DisignAdmixtureCatObj?,
                         var address: DisignAddressObj?,
                         var with_TM: Boolean?,
                         var errmsgForTM: String?,
                         var with_CP: Boolean?,
                         var errmsgForCP: String?,
                         var margin_price: Double?,
                         var quantity: Int? = 3,
                         var address_id: String?,
                         var vendor_id: String?,
                         var updated_at: String?,
                         var updated_by_id: String?,
                         var TM_price: Double?,
                         var CP_price: Double?,
                         var selling_price_With_TM_CP: Double?,
                         var selling_price_with_margin: Double?,
var distance:Double?) : Parcelable


@Parcelize
data class DisignVendorObj(var _id: String?, var full_name: String?,var company_name:String?,var rating:Double?) : Parcelable

@Parcelize
data class DisignVendorMediaObj(var _id: String?, var media_url: String?, var media_type: String?, var vendor_id: String?, var type: String?, var created_at: String?) : Parcelable

@Parcelize
data class DisignConcreteGradeObj(var _id: String?, var name: String?) : Parcelable

@Parcelize
data class DisignCementBrandObj(var _id: String?, var name: String?) : Parcelable

@Parcelize
data class DisignSandSourceObj(var _id: String?, var sand_source_name: String?) : Parcelable

@Parcelize
data class DisignAggregateSourceObj(var _id: String?,
                                    var aggregate_source_name: String?) : Parcelable

@Parcelize
data class DisignAggreSorcCat1Obj(var _id: String?, var sub_category_name: String?) : Parcelable

@Parcelize
data class DisignFlyAshSourceObj(var _id: String?, var fly_ash_source_name: String?) : Parcelable


@Parcelize
data class DisignAdmixtureBrandObj(var _id: String?, var name: String?) : Parcelable

@Parcelize
data class DisignCementGradeObj(var _id: String?, var name: String?) : Parcelable

@Parcelize
data class DisignAdmixtureCatObj(var _id: String?, var category_name: String?) : Parcelable

@Parcelize
data class DisignAddressObj(var _id: String?, var location: ProductCategoryWiseListPickupLocation?,
                            var line1: String?, var line2: String?, var pincode: Int?,
                            var state_id: String?, var state_name: String?, var city_id: String?,
                            var city_name: String?) : Parcelable