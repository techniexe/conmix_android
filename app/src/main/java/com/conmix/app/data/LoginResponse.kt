package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Mala Ruparel on 24/7/20.
 */
@Parcelize
data class LoginResponse(val customToken: String) : Parcelable