package com.conmix.app.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


/**
 * Created by Hitesh Patel on 07,December,2021
 */
@Parcelize
data class UpdateProfileOtpObj(var old_mobile_no:String?,var new_mobile_no:String?,var old_email:String?,var new_email:String?):Parcelable
