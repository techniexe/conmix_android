package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,March,2021
 */
@Parcelize
data class ProductCategoryWiseListPickupLocation(
    /* It's \"Point\" */
    var type: String? = null,
    var coordinates: ArrayList<Double>? = null
): Parcelable


@Parcelize
data class AddresslocationObj(var type: String? = null,
                              var coordinates: ArrayList<Double>? = null
): Parcelable