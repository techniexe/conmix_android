package com.conmix.app.data;

import com.razorpay.PaymentData;

/**
 * Created by Hitesh Patel on 07,June,2021
 */
public class PaymentSuccess {
    private String razorpayPaymentId;
    private PaymentData paymentData;

    public PaymentSuccess(String razorpayPaymentId, PaymentData paymentData) {
        this.razorpayPaymentId = razorpayPaymentId;
        this.paymentData = paymentData;
    }

    public String getRazorpayPaymentId() {
        return razorpayPaymentId;
    }

    public PaymentData getPaymentData() {
        return paymentData;
    }
}
