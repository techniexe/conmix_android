package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,March,2021
 */
@Parcelize
data class ProductCategoryWiseDetailsProductCategory(
    val _id: String?,
    val name: String?,
    val created_at: String?,
    val created_by: String?,
    val is_deleted: String?,
    val deleted_by: String?

) : Parcelable


@Parcelize
data class ProductlistCategory(var data: ArrayList<ProductCategoryWiseDetailsProductCategory>?) :
    Parcelable