package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize



/**
 * Created by Hitesh Patel on 31,March,2021
 */


@Parcelize
data class CustomMixPostObj(var site_id: String?,
                            var long: Double?,
                            var lat: Double?,
                            var concrete_grade_id: String?,
                            var cement_grade_id: ArrayList<String>?,
                            var cement_brand_id: ArrayList<String>?,
                            var cement_quantity: Double?,
                            var sand_source_id: ArrayList<String>?,
                            var sand_quantity: Double?,
                            var aggregate1_sub_cat_id: ArrayList<String>?,
                            var aggregate2_sub_cat_id: ArrayList<String>?,
                            var aggregate_source_id: ArrayList<String>?,
                            var aggregate1_quantity: Double?,
                            var aggregate2_quantity: Double?,
                            var fly_ash_source_id: ArrayList<String>?,
                            var fly_ash_quantity: Double?,
                            var admix_brand_id: ArrayList<String>?,
                            var admix_category_id: ArrayList<String>?,
                            var admix_quantity: Double?,
                            var water_quantity: Double?,
                            var with_CP: Boolean?,
                            var with_TM: Boolean?,
                            var delivery_date: String?,
                            var address_id: String?,
                            var admix_code_id:ArrayList<String>?) : Parcelable


@Parcelize
data class CustomMixResLstData(var data:ArrayList<CustomMixResObjData>):Parcelable


@Parcelize
data class CustomMixResLstDetailData(var data: CustomMixDestailResLstData):Parcelable

@Parcelize
data class CustomMixDestailResLstData(var response:ArrayList<CustomMixResObjData>):Parcelable




@Parcelize
data class CustomMixResObjData(var concrete_grade_id:String?,
                               var selling_price_with_margin:Double?,
                               var final_admix_brand_id:String?,
                               var final_admix_cat_id:String?,
                               var final_fly_ash_source_id:String?,
                               var final_agg_source_id:String?,
                               var final_aggregate1_sub_cat_id:String?,
                               var final_aggregate2_sub_cat_id:String?,
                               var final_sand_source_id:String?,
                               var final_cement_brand_id:String?,
                               var final_cement_grade_id:String?,
                               var vendor_id:String?,
                               var address_id:String?,
                               var cement_per_kg_rate:Double?,
                               var sand_per_kg_rate:Double?,
                               var aggregate_per_kg_rate:Double?,
                               var fly_ash_per_kg_rate:Double?,
                               var admix_per_kg_rate:Double?,
                               var water_per_ltr_rate:Double?,
                               var cement_price:Double?,
                               var cement_quantity:Double?,
                               var sand_price:Double?,
                               var sand_quantity:Double?,
                               var aggreagte_price:Double?,
                               var aggregate1_quantity:Double?,
                               var aggregate2_quantity:Double?,
                               var fly_ash_price:Double?,
                               var fly_ash_quantity:Double?,
                               var admix_price:Double?,
                               var admix_quantity:Double?,
                               var water_price:Double?,
                               var water_quantity:Double?,
                               var CP_price:Double?,
                               var TM_price:Double?,
                               var selling_price:Double?,
                               var margin_price:Double?,
                               var vendor_name:String?,
                               var vendor_media:ArrayList<DisignVendorMediaObj>?,
                               var concrete_grade_name:String?,
                               var cement_grade_name:String?,
                               var cement_brand_name:String?,
                               var sand_source_name:String?,
                               var aggregate1_sub_category_name:String?,
                               var aggregate2_sub_category_name:String?,
                               var aggregate_source_name:String?,
                               var fly_ash_source_name:String?,
                               var admix_category_name:String?,
                               var admix_brand_name:String?,
                               var with_CP:Boolean?,
                               var with_TM:Boolean?,
                               var aggreagte1_price:Double?,
                               var aggreagte2_price:Double?,
                               var distance:Double?,
                               var unit_price:Double?,
                               var margin_price_for_selling:Double?,
                               var margin_price_for_unit:Double?,
                               var quantity:Int? = 3,
                               var isSelectAddToCart:Boolean?= false,
                               var aggregate1_per_kg_rate:Double?,
                               var aggregate2_per_kg_rate:Double?,
                               var TM_price_for_unit:Double?,
                               var delivery_date:String?,
                               var company_name:String?,
                               var line1:String?,
                               var line2:String?,
                               var state_name:String?,
                               var city_name:String?,
                               var location: ProductCategoryWiseListPickupLocation?,
                               var isSelected:Boolean?=false):Parcelable



@Parcelize
data class customMixFixObj(var site_id:String?,var long:Double?,var lat:Double?):Parcelable

@Parcelize
data class CustomMixFixObjMainData(var data:CustomMixFixObjData?):Parcelable

@Parcelize
data class CustomMixFixObjData(var cement_brand:ArrayList<CementBrandDataObj>,
                               var sand_source:ArrayList<SandSourceDataObj>,
                               var agg_source:ArrayList<AggregateSourceObj>,
                               var agg1_sub_cat:ArrayList<AggregateSubCategoryDataObj>,
                               var agg2_sub_cat:ArrayList<AggregateSubCategoryDataObj>,
                               var fly_ash_source:ArrayList<FlyAshSourceObj>,
                               var admix_brand:ArrayList<AddmixureBrandObj>,
                               var cement_grade:ArrayList<CementGradeDataObj>,
                               var admix_category:ArrayList<AddmixureCategoryObj>):Parcelable


