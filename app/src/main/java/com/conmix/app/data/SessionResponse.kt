package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Mala Ruparel on 23/7/20.
 */
@Parcelize
data class SessionResponse(val token: String) : Parcelable