package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 05,April,2021
 */


/*@Parcelize
data class AddToCartCustomMixObj(var design_mix_id: String?,
                                 var custom_mix: CustomMixToCartObj?,
                                 var quantity: Int?,
                                 var state_id: String?,
                                 var cart_id: String?,
                                 var item_id: String?,
                                 var user_id: String?,
                                 var site_id: String?,
                                 var long: Double?,
                                 var lat: Double?,
                                 var with_CP: Boolean?,
                                 var with_TM: Boolean?,
                                 var delivery_date: String?,
                                 var TM_no: String?,
                                 var driver_name: String?,
                                 var driver_mobile: String?,
                                 var TM_operator_name: String?,
                                 var TM_operator_mobile_no: String?) : Parcelable*/


@Parcelize
data class AddToCartCustomMixObj(var design_mix_id: String?,
                                 var custom_mix: CustomMixResObjData?,
                                 var quantity: Int?,
                                 var state_id: String?,
                                 var cart_id: String?,
                                 var item_id: String?,
                                 var user_id: String?,
                                 var site_id: String?,
                                 var long: Double?,
                                 var lat: Double?,
                                 var with_CP: Boolean?,
                                 var with_TM: Boolean?,
                                 var delivery_date: String?,
                                 var TM_no: String?,
                                 var driver_name: String?,
                                 var driver_mobile: String?,
                                 var end_date: String?) : Parcelable

@Parcelize
data class CustomMixToCartObj(var concrete_grade_id: String?,
                              var final_admix_brand_id: String?,
                              var final_admix_cat_id: String?,
                              var admix_quantity: Double?,
                              var admix_per_kg_rate: Double?,
                              var final_fly_ash_source_id: String?,
                              var fly_ash_per_kg_rate: Double?,
                              var fly_ash_quantity: Double?,
                              var final_aggregate2_sub_cat_id: String?,
                              var aggregate2_quantity: Double?,
                              var aggregate2_per_kg_rate: Double?,
                              var final_agg_source_id: String?,
                              var final_aggregate1_sub_cat_id: String?,
                              var aggregate1_quantity: Double?,
                              var aggregate1_per_kg_rate: Double?,
                              var final_sand_source_id: String?,
                              var sand_quantity: Double?,
                              var sand_per_kg_rate: Double?,
                              var final_cement_brand_id: String?,
                              var final_cement_grade_id: String?,
                              var cement_per_kg_rate: Double?,
                              var cement_quantity: Double?,
                              var water_quantity: Double?,
                              var water_per_ltr_rate: Double?,
                              var vendor_id: String?,
                              var address_id: String?) : Parcelable

@Parcelize
data class CustomMixAddtoCartRepData(val data: CustomMixAddtoCartRep):Parcelable

@Parcelize
data class CustomMixAddtoCartRep(var status:String?,var _id:String?,var suggestion:ArrayList<CustomMixAddtoCartSuggestionLst>?):Parcelable

@Parcelize
data class CustomMixAddtoCartSuggestionLst(var ErrMsgForCp:String?,var ErrMsgForTM:String?):Parcelable





