package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,March,2021
 */
@Parcelize
data class Citydetails(
    val _id: String? = null,
    var location: ProductCategoryWiseListPickupLocation? = null,
    var city_name: String? = null,
    val state_id: String? = null,
    val created_at: String? = null,
    val country_code: String? = null,
    val country_id: String? = null,
    var country_name: String? = null,
    var state_name: String? = null
) : Parcelable
