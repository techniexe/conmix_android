package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 12,March,2021
 */
@Parcelize
data class UserProfileData(val data: UserProfileObj) : Parcelable

@Parcelize
data class UserProfileObj(
    var _id: String?,
    var full_name: String?,
    var mobile_number: String?,
    var password: String?,
    var mobile_verified: Boolean? = false,
    var email_verified: Boolean? = false,
    var signup_type: String?,
    var account_type: String?,
    var identity_image_url: String?,
    var gst_number: String?,
    var pan_number: String?,
    var email: String?,
    var created_at: String?,
    var company_type:String?,
    var company_name:String?,
    var pay_method_id:String?
) : Parcelable



@Parcelize
data class UpdateUserProfileObj(
        var account_type:String?,
        var company_type:String?,
        var company_name:String?,
        var full_name:String?,
        var email:String?,
        var mobile_number:String?,
        var gst_number:String?,
        var pan_number:String?,
        var landline_number:String?,
        var old_mobile_code:String?,
        var new_mobile_code:String?,
        var old_email_code:String?,
        var new_email_code:String?
       // var pay_method_id:String?
) : Parcelable

