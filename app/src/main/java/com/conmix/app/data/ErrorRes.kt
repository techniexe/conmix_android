package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 03,March,2021
 */
@Parcelize
data class ErrorRes(val error: ErrorModel) : Parcelable

@Parcelize
data class ErrorModel(val message: String?, val email_exists: Boolean?, val full_name: String?, val facebook_id: String?) : Parcelable
