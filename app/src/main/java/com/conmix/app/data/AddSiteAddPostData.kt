package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 12,March,2021
 */
@Parcelize
data class AddSiteAddPostData(val site: AddSiteAddPostObj) : Parcelable


@Parcelize
data class AddSiteAddPostObj(
    var company_name: String?,
    var site_name: String?,
    var address_line1: String?,
    var address_line2: String?,
    var country_id: Int?,
    var state_id: String?,
    var city_id: String?,
    var sub_city_id: String?,
    var pincode: Int?,
    var person_name: String?,
    var title: String?,
    var email: String?,
    var mobile_number: String?,
    var alt_mobile_number: String?,
    var whatsapp_number: String?,
    var landline_number: String?,
    var profile_pic: String?,
    var location: ProductCategoryWiseListPickupLocation?
) : Parcelable


@Parcelize
data class AddSiteAddPostResult(val data: AddSiteAddPostdata): Parcelable

@Parcelize
data class AddSiteAddPostdata(val id: String) : Parcelable

@Parcelize
data class CheckSiteNameObj(var site_name:String?):Parcelable


@Parcelize
data class AddBillAddPostObj(
    var company_name: String?,
    var line1: String?,
    var line2: String?,
    var state_id: String?,
    var city_id: String?,
    var pincode: String?,
    var gst_number: String?,
    var full_name:String?

) : Parcelable


@Parcelize
data class AddBillAddressPostResult(val data: AddBillAddPostdata?): Parcelable

@Parcelize
data class AddBillAddPostdata(val _id: String?) : Parcelable


@Parcelize
data class AddBillAddressIdCart(var billing_address_id:String?):Parcelable
