package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,March,2021
 */
@Parcelize
data class BannerData(
    var _id: String?=null,
    //val location: mylocation,
    var media_type: String?=null,
    var media_url: String?=null,
    var sequence: Int?=null,
    var start_date: String?=null,
    var end_date: String?=null,
    var created_by_id: String?=null,
    var created_at: String?=null
) : Parcelable
