package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 12,March,2021
 */
@Parcelize
data class FcmData(var token: String, var deviceID: String, var platform: String) : Parcelable

@Parcelize
data class NotificationCount(var notification_count: Int, var unseen_message_count: Int) :
    Parcelable
