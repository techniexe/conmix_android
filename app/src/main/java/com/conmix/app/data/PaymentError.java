package com.conmix.app.data;

import com.razorpay.PaymentData;

/**
 * Created by Hitesh Patel on 07,June,2021
 */
public class PaymentError {
    private int code;
    private String description;
    private PaymentData paymentData;

    public PaymentError(int code, String description, PaymentData paymentData) {
        this.code = code;
        this.description = description;
        this.paymentData = paymentData;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public PaymentData getPaymentData() {
        return paymentData;
    }
}
