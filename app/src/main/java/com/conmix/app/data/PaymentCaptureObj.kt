package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,June,2021
 */
@Parcelize
data class PaymentCaptureObj(var amount:String,var currency:String):Parcelable
