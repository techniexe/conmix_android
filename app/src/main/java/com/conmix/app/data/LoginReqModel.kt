package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 04,March,2021
 */
@Parcelize
data class LoginReqModel(val password: String) : Parcelable

@Parcelize
data class UpdateUserPasswordProfileObj(
        var password: String?
) : Parcelable

@Parcelize
data class ChnagePasswordProfileObj(var old_password:String?,var password:String?,var authentication_code:String?):Parcelable
