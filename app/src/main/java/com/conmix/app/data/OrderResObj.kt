package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 20,April,2021
 */

@Parcelize
data class OrderResDta(var data: OrderResObj?) : Parcelable

@Parcelize
data class OrderResObj(var _id: String?, var display_id: String?) : Parcelable

@Parcelize
data class OrderLstDta(var data: ArrayList<OrderLstObj>?) : Parcelable

@Parcelize
data class OrderLstObj(
    var delivery_location: AddresslocationObj?,
    var order_status: String?,
    var payment_status: String?,
    var payment_attempt: Int?,
    var _id: String?,
    var display_id: String?,
    var user_id: String?,
    var payment_mode: String?,
    var selling_price: Double?,
    var margin_price: Double?,
    var TM_price: Double?,
    var CP_price: Double?,
    var site_id: String?,
    var site_name: String?,
    var address_line1: String?,
    var address_line2: String?,
    var state_id: String?,
    var city_id: String?,
    var pincode: String?,
    var created_at: String?,
    var selling_price_With_Margin:Double?
) : Parcelable

@Parcelize
data class OrderLstDetailDta(var data: OrderlstDetailObj?) : Parcelable

@Parcelize
data class OrderlstDetailObj(
    var orderData: OrderDetailLstObjRes?,
    var orderItemData: ArrayList<OrderDetailItemDta>
) : Parcelable

@Parcelize
data class OrderDetailLstObjRes(
    var _id: String?,
    var delivery_location: AddresslocationObj?,
    var order_status: String?,
    var payment_status: String?,
    var gateway_transaction_id: String?,
    var payment_attempt: Double?,
    var display_id: String?,
    var user_id: String?,
    var payment_mode: String?,
    var selling_price: Double?,
    var margin_price: Double?,
    var TM_price: Double?,
    var CP_price: Double?,
    var site_id: String?,
    var site_name: String?,
    var address_line1: String?,
    var address_line2: String?,
    var state_id: String?,
    var city_id: String?,
    var sub_city_id: String?,
    var pincode: String?,
    var created_at: String?,
    var company_name: String?,
    var city_name: String?,
    var country_code: String?,
    var country_name: String?,
    var state_name: String?,
    var mobile_number: String?,
    var total_amount: Double?,
    var coupon_amount: Double?,
    var cgst_price: Double?,
    var sgst_price: Double?,
    var gst_price: Double?,
    var selling_price_With_Margin:Double?,
    var fee:Double?,
    var tax:Double?,
    var buyer_billing_address:BillAddressObjInfoCart?,
    var buyer_billing_address_city_name:String?,
    var buyer_billing_address_state_name:String?,
    var igst_price:Double?,
    var amount:Double?) : Parcelable

@Parcelize
data class OrderDetailItemDta(
    var _id: String?,
    var location: AddresslocationObj?,
    var item_status: String?,
    var payment_status: String?,
    var display_item_id: String?,
    var buyer_id: String?,
    var order_id: String?,
    var quantity: Double?,
    var cement_per_kg_rate: Double?,
    var sand_per_kg_rate: Double?,
    var aggregate1_per_kg_rate: Double?,
    var aggregate2_per_kg_rate: Double?,
    var fly_ash_per_kg_rate: Double?,
    var admix_per_kg_rate: Double?,
    var water_per_ltr_rate: Double?,
    var cement_price: Double?,
    var sand_price: Double?,
    var aggreagte1_price: Double?,
    var aggreagte2_price: Double?,
    var fly_ash_price: Double?,
    var admix_price: Double?,
    var water_price: Double?,
    var with_TM: Boolean?,
    var with_CP: Boolean?,
    var distance: Double?,
    var selling_price: Double?,
    var margin_price: Double?,
    var TM_price: Double?,
    var CP_price: Double?,
    var gst_type: String?,
    var vendor_id: String?,
    var address_id: String?,
    var cement_quantity: Double?,
    var sand_quantity: Double?,
    var aggregate1_quantity: Double?,
    var aggregate2_quantity: Double?,
    var fly_ash_quantity: Double?,
    var admix_quantity: Double?,
    var water_quantity: Double?,
    var line1: String?,
    var line2: String?,
    var state_id: String?,
    var city_id: String?,
    var pincode: Double?,
    var address_type: String?,
    var business_name: String?,
    var city_name: String?,
    var state_name: String?,
    var created_at: String?,
    var concrete_grade: nameIdObj?,
    var admix_brand: nameIdObj?,
    var admix_cat: nameIdObj?,
    var fly_ash_source: nameIdObj?,
    var aggregate2_sub_cat: nameIdObj?,
    var aggregate_source: nameIdObj?,
    var aggregate1_sub_cat: nameIdObj?,
    var sand_source: nameIdObj?,
    var cement_brand: nameIdObj?,
    var cement_grade: nameIdObj?,
    var design_mix_id: String?,
    var design_mix: desinMixOrderObj?,
    var TM_no: String?,
    var driver_name: String?,
    var driver_mobile: String?,
    var TM_operator_name: String?,
    var TM_operator_mobile_no: String?,
    var unit_price: Double?,
    var sgst_rate: Double?,
    var cgst_price: Double?,
    var gst_price: Double?,
    var vendor_media: ArrayList<DisignVendorMediaObj>?,
    var vendor: DisignVendorObj?,
    var review: VenderReviewData?,
    var pickup_quantity:Double?,
    var remaining_quantity:Double?,
    var write_review:Boolean?,
    var selling_price_With_Margin:Double?,
    var unit_price_With_Margin:Double?,
    var delivery_date:String?,
    var end_date:String?,
    var temp_quantity:Double?,
    var part_quantity:Long?,
    var order:orderItemOrderObj?,
    var orderItemPartData:ArrayList<orderItemPartDataObj>?

) : Parcelable


@Parcelize
data class orderItemPartDataObj(var _id:String?,var order_item_id:String?,var assigned_quantity:Int?,var start_time:String?,var end_time:String?,var buyer_order_id:String?,var buyer_id:String?,var temp_quantity:Double?,var created_at:String?,var CPtrack_details:ArrayList<CPTrackObj>?):Parcelable

@Parcelize
data class CPTrackObj(var _id:String?,var CP_id:String?,var assigned_at:String?,var order_item_id:String?,var order_item_part_id:String?,var start_time:String?,var end_time:String?,var is_already_delivered:Boolean?,var buyer_order_id:String?,var event_status:String?,var vendor_user_id:String?,var vendor_order_id:String?,var created_at:String?):Parcelable

@Parcelize
data class orderItemOrderObj(var gateway_transaction_id:String?):Parcelable

@Parcelize
data class desinMixOrderObj(var _id: String?, var product_name: String?) : Parcelable


@Parcelize
data class nameIdObj(
    var _id: String?,
    var name: String?,
    var category_name: String?,
    var fly_ash_source_name: String?,
    var sub_category_name: String?,
    var aggregate_source_name: String?,
    var sand_source_name: String?
) : Parcelable
/// Tracking Model

@Parcelize
data class TrackOrderLst(var data: ArrayList<TrackOrderLstobj>?) : Parcelable

@Parcelize
data class TrackOrderdtl(var data: TrackOrderLstobj?) : Parcelable

@Parcelize
data class TrackOrderLstobj(
    var _id: String?,
    var pickup_quantity: Double?,
    var TM_id: String?,
    var assigned_at: String?,
    var order_item_id: String?,
    var buyer_order_id: String?,
    var event_status: String?,
    var designMixDetails: desinMixdetailObj?,
    var design_mix_id: String?,
    var display_id: String?,
    var created_at: String?,
    var order_status: String?,
    var processed_at: String?,
    var TM_rc_number:String?,
    var TM_category:String?,
    var TM_sub_category:String?,
    var driver_name:String?,
    var driver_mobile_number:String?,
    var concrete_grade_name:String?,
    var cement_brand_name:String?,
    var sand_source_name:String?,
    var aggregate_source_name:String?,
    var aggregate1_sub_category_name:String?,
    var aggregate2_sub_category_name:String?,
    var fly_ash_source_name:String?,
    var admixture_brand_name:String?,
    var cement_grade_name:String?,
    var admixture_category_name:String?,
    var royality_quantity:Double?,
    var pickedup_at:String?,
    var delivered_at:String?,
    var delayTime:String?,
    var reasonForDelay:String?,
    var delayed_at:String?,
    var qube_test_report_7days:String?,
    var qube_test_report_28days:String?,
    var invoice_url:String?
) : Parcelable


@Parcelize
data class TMDetailsObj(var _id: String?, var TM_rc_number: String?) : Parcelable

@Parcelize
data class TMCategoryDetailsObj(var _id: String?, var category_name: String?) : Parcelable

@Parcelize
data class TMSubCategoryDetailsObj(var _id: String?, var sub_category_name: String?) : Parcelable

@Parcelize
data class TMDriverDetailsObj(
    var _id: String?,
    var driver_name: String?,
    var driver_mobile_number: String?
) : Parcelable


@Parcelize
data class desinMixdetailObj(
    var _id: String?,
    var pickup_location: AddresslocationObj?,
    var grade_id: String?,
    var product_name: String?,
    var cement_brand_id: String?,
    var cement_quantity: Double?,
    var sand_source_id: String?,
    var sand_quantity: Double?,
    var aggregate_source_id: String?,
    var aggregate1_sub_category_id: String?,
    var aggregate1_quantity: Double?,
    var aggregate2_sub_category_id: String?,
    var aggregate2_quantity: Double?,
    var fly_ash_source_id: String?,
    var fly_ash_quantity: Double?,
    var ad_mixture_brand_id: String?,
    var ad_mixture_quantity: Double?,
    var water_quantity: Double?,
    var selling_price: Double?,
    var description: String?,
    var ad_mixture_category_id: String?,
    var cement_grade_id: String?,
    var address_id: String?,
    var is_custom: Boolean?,
    var design_mix_id: String?,
    var is_available: Boolean?,
    var vendor_id: String?,
    var updated_at: String?,
    var updated_by_id: String?
) : Parcelable


@Parcelize
data class pickUpAddInfObj(
    var _id: String?,
    var line1: String?,
    var line2: String?,
    var pincode: String?,
    var pickup_location: AddresslocationObj?,
    var city_name: String?,
    var state_name: String?
) : Parcelable


@Parcelize
data class deliverAddInfObj(
    var _id: String?,
    var line1: String?,
    var line2: String?,
    var pincode: String?,
    var delivery_location: AddresslocationObj?,
    var city_name: String?,
    var state_name: String?
) : Parcelable


@Parcelize
data class GatewayTranstedIdObj(var gateway_transaction_id:String?):Parcelable

@Parcelize
data class checkTmAvlObj(var vendor_id:String?,var delivery_date:String?,var start_time:String?,var end_time:String?):Parcelable

@Parcelize
data class AssignTMOBJ(var delivery_date:String?,var assigned_quantity:Int?,var start_time:String?,var end_time:String?):Parcelable

@Parcelize
data class AssignTrackDlData(val data:AssignTrackDlObj?):Parcelable

@Parcelize
data class AssignTrackDlObj(var _id:String?,
                            var CP_id:String?,
                            var assigned_at:String?,
                            var buyer_order_id:String?,
                            var order_item_id:String?,
                            var event_status:String?,
                            var display_id:String?,
                            var created_at:String?,
                            var order_status:String?,
                            var processed_at:String?,
                            var concrete_pump_model:String?,
                            var manufacture_year:Int?,
                            var pipe_connection:Int?,
                            var concrete_pump_capacity:Double?,
                            var is_Operator:Boolean?,
                            var operator_name:String?,
                            var operator_mobile_number:String?,
var delayTime:String?,var reasonForDelay:String?,var pickedup_at:String?,var delivered_at:String?,var serial_number:String?):Parcelable


@Parcelize
data class PaymentTranstedIdObj(var gateway_transaction_id:String?):Parcelable


