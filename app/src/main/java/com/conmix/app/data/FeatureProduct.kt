package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,March,2021
 */
@Parcelize
data class FeatureProduct(
    var _id: String?=null,
    var image:Int?=0,
    var name:String?=null
) : Parcelable
