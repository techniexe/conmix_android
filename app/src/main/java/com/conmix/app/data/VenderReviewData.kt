package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 07,April,2021
 */


@Parcelize
data class VenderReviewdataRes(val data: VenderReviewResData):Parcelable

@Parcelize
data class VenderReviewResData(var write_review:Boolean?,
                               var reviews:ArrayList<VenderReviewData>):Parcelable

@Parcelize
data class VenderReviewData(var status:String?=null,
                            var _id:String?=null,
                            var vendor_id:String?=null,
                            var review_text:String?=null,
                            var rating:Double?=null,
                            var created_at:String?=null,
                            var modirated_at:String?=null,
                            var buyer: buyerVenderReviewObj?=null,
                            var user_id:String?=null):Parcelable




@Parcelize
data class buyerVenderReviewObj(var _id:String?,
                                var full_name:String?):Parcelable


@Parcelize
data class VenderWriteReviewObj(var vendor_id:String?,var review_text:String?,var rating:Double?,var status:String?):Parcelable


@Parcelize
data class VenderWriteReviewRes(var _id:String?):Parcelable