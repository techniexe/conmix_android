package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 22,March,2021
 */
@Parcelize
data class CementGradeDataList(var data: ArrayList<CementGradeDataObj>) : Parcelable

@Parcelize
data class CementGradeDataObj(var _id: String?, var name: String?, var created_by: String?, var created_at: String?, var is_deleted: Boolean?) : Parcelable


@Parcelize
data class CementBrandList(var data: ArrayList<CementBrandDataObj>) : Parcelable

@Parcelize
data class CementBrandDataObj(var _id: String?, var name: String?, var created_at: String?, var created_by: String?, var is_deleted: Boolean?, var deleted_by: String?, var deleted_at: String?, var image_url: String?, var thumbnail_url: String?,var isSelected:Boolean=false) : Parcelable

@Parcelize
data class SandSourceList(var data: ArrayList<SandSourceDataObj>) : Parcelable


@Parcelize
data class SandSourceDataObj(var _id: String?, var region_id: String?, var sand_source_name: String?, var created_by_id: String?,
                             var created_at: String?, var updated_at: String?, var updated_by_id: String?, var region_name: String?,var isSelected:Boolean=false,var name:String?) : Parcelable

@Parcelize
data class AggregateCategoryList(var data: ArrayList<AggregateCategoryDataObj>) : Parcelable


@Parcelize
data class AggregateCategoryDataObj(var _id: String?, var category_name: String?, var sequence: Int?,
                                    var image_url: String?, var thumbnail_url: String?, var created_at: String?, var created_by: String?) : Parcelable

@Parcelize
data class AggregateSubCategoryList(var data: ArrayList<AggregateSubCategoryDataObj>) : Parcelable

@Parcelize
data class AggregateSubCategoryDataObj(var _id: String, var sub_category_name: String?, var sequence: Int?, var image_url: String?, var thumbnail_url: String?,
                                       var quantity_unit_code: String?, var quantity_units: String?, var gst_slab: GstSlabObj?, var margin_rate: MarginRateObj?, var selling_unit: ArrayList<String>?,
                                       var min_quantity: MinQuantityObj?, var created_by: String?, var created_at: String?, var product_category: ProductCategoryObj?, var isSelected:Boolean=false,var name:String?) : Parcelable


@Parcelize
data class GstSlabObj(var _id: String?, var title: String?, var igst_rate: Double?, var sgst_rate: Double?, var cgst_rate: Double?) : Parcelable

@Parcelize
data class MarginRateObj(var _id: String?, var title: String?, var slab: ArrayList<MarginSlabObj>, var created_by_id: String?, var created_at: String?) : Parcelable

@Parcelize
data class MarginSlabObj(var _id: String?, var upto: Double?, var rate: Double?) : Parcelable

@Parcelize
data class MinQuantityObj(var pcs: Double?, var nos: Double?, var kg: Double?, var MT: Double?, var sqmeter: Double?, var qtl: Double?) : Parcelable

@Parcelize
data class ProductCategoryObj(var _id: String?, var category_name: String?) : Parcelable


@Parcelize
data class AggregateSourceList(var data: ArrayList<AggregateSourceObj>) : Parcelable

@Parcelize
data class AggregateSourceObj(var _id: String?, var region_id: String?, var aggregate_source_name: String?,
                              var created_by_id: String?, var created_at: String?,
                              var updated_at: String?, var updated_by_id: String?, var region_name: String?,var isSelected:Boolean=false,var name:String?) : Parcelable

@Parcelize
data class AddmixureCategoryList(var data: ArrayList<AddmixureCategoryObj>) : Parcelable


@Parcelize
data class AddmixureCategoryObj(var _id: String?, var category_name: String?, var brand_id: String?,
                                var created_by: String?, var created_at: String?,var admixture_type:String?,var name:String?) : Parcelable

@Parcelize
data class AddmixureBrandList(var data: ArrayList<AddmixureBrandObj>) : Parcelable


@Parcelize
data class AddmixureBrandObj(var _id: String, var name: String?, var created_at: String?,
                             var created_by: String?, var is_deleted: Boolean?, var deleted_by: String?,
                             var deleted_at: String?,
                             var image_url: String?, var thumbnail_url: String?) : Parcelable

@Parcelize
data class FlyAshSourceList(var data: ArrayList<FlyAshSourceObj>) : Parcelable


@Parcelize
data class FlyAshSourceObj(var _id: String?, var region_id: String?,
                           var fly_ash_source_name: String?, var created_by_id: String?, var created_at: String?,
                           var updated_at: String?, var updated_by_id: String?, var region_name: String?,var isSelected:Boolean=false,var name:String?) : Parcelable