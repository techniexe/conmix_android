package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 07,April,2021
 */

@Parcelize
data class ProductReviewResData(val data:ArrayList<ProductReciewLstDta>):Parcelable

@Parcelize
data class ProductReciewLstDta(var status:String?,
                               var _id:String?,
                               var product_id:String?,
                               var review_text:String?,
                               var rating:Double?,
                               var sub_category_id:String?,
                               var created_at:String?,
                               var modirated_at:String?,
                               var buyer: buyerVenderReviewObj?):Parcelable