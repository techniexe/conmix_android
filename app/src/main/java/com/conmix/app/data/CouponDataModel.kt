package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 20,April,2021
 */

@Parcelize
data class CouponDataModel(
                           var _id: String,
                           var max_usage:Int?,
                           var unique_use:Boolean?,
                           var is_deleted:Boolean?,
                           var is_active:Boolean?,
                           var code:String?,
                           var discount_type: String?,
                           var discount_value: Double?,
                           var min_order: Double?,
                           var max_discount: Double?,
                           var start_date: String?,
                           var end_date: String?,
                           var info: String?,
                           var tnc: String?,
                           var created_at:String?):Parcelable


@Parcelize
data class CouponObj(var coupon_code:String?):Parcelable
