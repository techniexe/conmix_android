package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 09,March,2021
 */
@Parcelize
data class CountryDataModel(val data:ArrayList<CountryModel>): Parcelable


@Parcelize
data class CountryModel(
    var _id: String? = null,
    var country_id: Int? = 0,
    var country_code: String? = null,
    var country_name: String? = null,
    var created_at: String? = null
): Parcelable

@Parcelize
data class StateDataModel(val data: ArrayList<StateModel>) : Parcelable

@Parcelize
data class StateModel(
    val _id: String? = null,
    val state_name: String? = null,
    val country_id: String? = null,
    val country_code: String? = null,
    val country_name: String? = null,
    val created_at: String? = null,
    var isSelected: Boolean? = false
) : Parcelable


@Parcelize
data class CityDataModel(val data: ArrayList<CityModel>) : Parcelable

@Parcelize
data class CityModel(
    val _id: String?,
    val location: ProductCategoryWiseListPickupLocation?,
    val city_name: String?,
    val state_id: String?,
    val created_at: String?,
    val country_code: String?,
    val country_id: Int? = 0,
    val country_name: String?,
    val state_name: String?,
    val popularity: Int? = 0,
    var isSelected: Boolean? = false
) : Parcelable

@Parcelize
data class LocationObj(var type: String?, var coordinates: ArrayList<Double>) : Parcelable
