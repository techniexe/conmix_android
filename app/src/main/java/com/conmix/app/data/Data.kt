package com.conmix.app.data


/**
 * Created by Hitesh Patel on 02,March,2021
 */
data class Data<out T>(val data: T)
