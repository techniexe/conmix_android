package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 04,March,2021
 */
@Parcelize
data class SignUpRegisterModel(var account_type:String?,
                               var company_type:String?,
                               var company_name:String?,
                               var full_name:String?,
                               var email:String?,
                               var mobile_number:String?,
                               var password:String?,
                               var gst_number:String?,
                               var pan_number:String?,
                               var landline_number:String?
                             //  var pay_method_id:String?
                               ):Parcelable

@Parcelize
data class PaymentTypesArr(var data:ArrayList<PaymentTypesObj>):Parcelable


@Parcelize
data class PaymentTypesObj(var _id:String?,
                           var name:String?,
                           var created_at:String?,
                           var created_by:String?,
                           var is_active:Boolean?,
                           var is_deleted:Boolean?,
                           var deleted_at:String?,
                           var deleted_by:Boolean?):Parcelable
