package com.conmix.app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 24,March,2021
 */

@Parcelize
data class SelectedCementBrandChecked (var isChecked:Boolean,var position:String):Parcelable