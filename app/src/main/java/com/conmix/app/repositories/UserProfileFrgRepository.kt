package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface
import okhttp3.MultipartBody
import okhttp3.RequestBody


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class UserProfileFrgRepository ( private val apiInterface: ApiInterface) {

    suspend fun getUserPrfData() = apiInterface.getUserProfileData()

    suspend fun uploadUserImageServer(fullName: RequestBody?, parts: MultipartBody.Part) = apiInterface.uploadUserPic(fullName,parts)

}