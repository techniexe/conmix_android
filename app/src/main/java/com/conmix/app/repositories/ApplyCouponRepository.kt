package com.conmix.app.repositories

import com.conmix.app.data.CouponObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class ApplyCouponRepository(private val apiInterface: ApiInterface) {

    suspend fun getCouponsData() = apiInterface.getCouponsData()
    suspend fun postCouponCode(couponObj: CouponObj?) = apiInterface.postCouponCode(couponObj)
    suspend fun postCouponCodeWithClientId(cartId:String?,couponObj: CouponObj?) = apiInterface.postCouponCodeWithClientId(cartId,couponObj)

}