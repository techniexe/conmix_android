package com.conmix.app.repositories

import com.conmix.app.data.PaymentCaptureObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 08,June,2021
 */
class PaymentCaptureRepository(private val apiInterface: ApiInterface) {

    suspend fun paymentCapture(authheader:String,payment_id:String,paymentCaptureObj: PaymentCaptureObj) = apiInterface.paymentCapture(authheader,payment_id,paymentCaptureObj)
}