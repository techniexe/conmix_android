package com.conmix.app.repositories

import com.conmix.app.data.SignUpRegisterModel
import com.conmix.app.data.UpdateUserProfileObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class SignUpRegisterRepository(private val apiInterface: ApiInterface) {

    suspend fun postSignupUserData(signup_type:String,signupRegisterModel: SignUpRegisterModel) = apiInterface.signupRegisterUser(signup_type,signUpRegisterModel = signupRegisterModel)

    suspend fun updateUserProfileData(signupRegisterModel: UpdateUserProfileObj) = apiInterface.updateUserProfile(signupRegisterModel)

    suspend fun getPaymnetType(search:String?,before:String?,after:String?) = apiInterface.getPaymentTypes(search,before,after)
}