package com.conmix.app.repositories

import com.conmix.app.data.LoginReqModel
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class LoginRepositary(private val apiInterface: ApiInterface) {

      suspend fun loginWithIdentifier(userName:String,loginReqModel: LoginReqModel) = apiInterface.loginWithIdentifier(userName,loginReqModel = loginReqModel)
}