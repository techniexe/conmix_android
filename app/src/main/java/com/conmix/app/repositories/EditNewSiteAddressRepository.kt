package com.conmix.app.repositories

import com.conmix.app.data.AddSiteAddPostData
import com.conmix.app.data.Citydetails
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class EditNewSiteAddressRepository (val apiInterface: ApiInterface){
    suspend fun addStateCity(citydetails: Citydetails) = apiInterface.addState(citydetails)
    suspend fun getCountryvalue() = apiInterface.getCountryData()
    suspend fun getStateDataValue(countryId:String)= apiInterface.getStateFromCountryData(countryId)
    suspend fun getCityDataValue(stateId:String)= apiInterface.getCityData(stateId)
    suspend fun getSiteAddressValue(siteId:String) = apiInterface.getSiteAddPerticulerId(siteId)
    suspend fun updateSiteAddressValue(stateId: String,addSiteAddPostData: AddSiteAddPostData) = apiInterface.updateAddProfile(stateId,addSiteAddPostData)
}