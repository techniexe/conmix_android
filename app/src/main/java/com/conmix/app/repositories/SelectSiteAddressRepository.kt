package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class SelectSiteAddressRepository (val apiInterface: ApiInterface) {

    suspend fun getSiteAddressList(beforeTime: String?, afterTime: String?, search: String?) =
            apiInterface.getSiteAddressListData(beforeTime, afterTime, search)


   /* suspend fun addSiteAddress(siteaddressObject: OrderListObj) =
            apiInterface.addSiteAddress(siteaddressObject)*/
}