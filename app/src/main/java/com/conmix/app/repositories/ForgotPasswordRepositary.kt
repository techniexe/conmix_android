package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class ForgotPasswordRepositary (val apiInterface: ApiInterface){


    suspend fun forgotpassword(mobNo:String,method:String) = apiInterface.forgotPassword(mobNo,method)
    suspend fun verifyOtp(identifier:String,code:String)=apiInterface.verifyForgotpasswordOtp(identifier = identifier,code = code)
    suspend fun getOtpByMobNo(mobNo:String,method:String)=apiInterface.getOtpByMobNum(mobNo,method)

}