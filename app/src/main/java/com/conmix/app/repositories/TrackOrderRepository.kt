package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 17,May,2021
 */
class TrackOrderRepository (private val apiInterface: ApiInterface) {

    suspend fun getTrackOrderLstData(order_item_id:String?) = apiInterface.getTrackListOrder(order_item_id)

    suspend fun getTrackOrderDtlData(order_item_id:String?,tracking_id:String?) = apiInterface.getTrackDtlOrder(order_item_id,tracking_id)

    suspend fun getAssignTrackOrderDtlData(_id:String?) = apiInterface.getAssignTrackOrderDetail(_id)

}