package com.conmix.app.repositories

import com.conmix.app.data.ChnagePasswordProfileObj
import com.conmix.app.data.UpdateUserPasswordProfileObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class ChangePasswordRepository(private val apiInterface: ApiInterface) {
    suspend fun changePassword(passwordObj: UpdateUserPasswordProfileObj?)= apiInterface.updatePassWordProfile(passwordObj)
    suspend fun changePasswordNew(passwordObj: ChnagePasswordProfileObj?) = apiInterface.changePassWordProfile(passwordObj)
    suspend fun getUserOTP()= apiInterface.getUserOTP()

}