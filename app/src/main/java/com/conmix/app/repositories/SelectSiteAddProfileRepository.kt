package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class SelectSiteAddProfileRepository (val apiInterface: ApiInterface) {

    suspend fun getSiteAddressList(beforeTime:String?,afterTime:String?,search:String?) = apiInterface.getSiteAddressListData(beforeTime,afterTime,search)
    suspend fun  deleteSiteAddress(siteId:String?) = apiInterface.deleteSiteAddProfile(siteId)
    suspend fun  deleteBillAddress(billAddressId:String?) = apiInterface.deleteBillAddProfile(billAddressId)

    suspend fun getBillAddressList() = apiInterface.getBillAddressListData()




}