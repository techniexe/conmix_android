package com.conmix.app.repositories

import com.conmix.app.data.CustomMixPostObj
import com.conmix.app.data.customMixFixObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 22,March,2021
 */
class CustomConmixRepository(val apiInterface: ApiInterface) {

    suspend fun getAdmixtureBandData(search:String?,before:String?,after:String?)= apiInterface.getAdmixtureBandData(search,before,after)

    suspend fun getAggregateSandCategoryData(search:String?,before:String?,after:String?) = apiInterface.getAggregateSandCategoryData(search,before,after)

    suspend fun getAggregateSandSubCategoryData(categoryId:String?,search:String?,before:String?,after:String?) = apiInterface.getAggregateSandSubCategoryData(categoryId,search,before,after)

    suspend fun getAggregateSandSubCategory2Data(id:String?,search:String?,before:String?,after:String?) = apiInterface.getAggregateSandSubCategory2Data(id,search,before,after)

    suspend fun getCementBandData(search:String?,before:String?,after:String?) = apiInterface.getCementBandData(search,before,after)

    suspend fun getAggregateSourceData() = apiInterface.getAggregateSourceData()

    suspend fun getFlyAshSourceData() = apiInterface.getFlyAshSourceData()

    suspend fun getSandSourceData() = apiInterface.getSandSourceData()

    suspend fun getAdmixureCategoryData(brandId:String?,search:String?,before:String?,after:String?) = apiInterface.getAdmixureCategoryData(brandId,search,before,after)

    suspend fun getCementGradeData() = apiInterface.getCementGradeData()

    suspend fun getProductHome(search:String?,before:String?,after:String?) = apiInterface.getProductHome(search,before,after)

    suspend fun postCustomMixData(customMixPostObjRequest: CustomMixPostObj?) =apiInterface.postCustomMixData(customMixPostObjRequest)


    suspend fun getCustomMixDetailData(vendorId:String?,customMixPostObjRequest: CustomMixPostObj?) = apiInterface.getCustomMixDetailData(vendorId,customMixPostObjRequest)

    suspend fun getCustommixFixData(customMixFixObj: customMixFixObj?) = apiInterface.getCustomMixFixData(customMixFixObj)

}