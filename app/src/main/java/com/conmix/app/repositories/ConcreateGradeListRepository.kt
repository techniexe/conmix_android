package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 17,March,2021
 */
class ConcreateGradeListRepository(val apiInterface: ApiInterface) {

    suspend fun getConcrateGradeList(site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?,end_date: String?,quantity: Int?) =
            apiInterface.getDesignMixList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,end_date,quantity)

    suspend fun getConcrateGradeObj(designmix_id:String?,site_id:String?,long:String?,lat:String?,
                                    delivery_date:String?,stateId:String?,with_TM:Boolean?,with_CP:Boolean?)= apiInterface.getDesignMixObj(designmix_id,site_id,long,lat,delivery_date,stateId,with_TM,with_CP )

}