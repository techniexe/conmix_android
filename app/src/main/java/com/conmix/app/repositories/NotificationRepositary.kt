package com.conmix.app.repositories

import com.conmix.app.data.FcmData
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 19,May,2021
 */
class NotificationRepositary(val apiInterface: ApiInterface) {

    suspend fun ragisterNotification(fcmData: FcmData) = apiInterface.registerFCMToken(fcmData)
    suspend fun deleteToken(deviceId: String?) = apiInterface.removeFCMToken(deviceId)
    suspend fun notificationCount() = apiInterface.getNotificationCount()
    suspend fun notificationList(beforeTime: String?) = apiInterface.getNotification(beforeTime)
    suspend fun notificationSeen(notificationId: String?) = apiInterface.notificationItemSeen(notificationId)
}