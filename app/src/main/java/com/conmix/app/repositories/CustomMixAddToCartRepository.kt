package com.conmix.app.repositories

import com.conmix.app.data.AddToCartCustomMixObj
import com.conmix.app.data.CartAddressIdObj
import com.conmix.app.data.DeleteCartObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 05,April,2021
 */
class CustomMixAddToCartRepository(val apiInterface: ApiInterface){

    suspend fun postCustomMixAddToCartObj(addToCartCustomMixObj: AddToCartCustomMixObj?) = apiInterface.postCustomMixAddToCartObj(addToCartCustomMixObj)

    suspend fun getCartData(state_id:String?,long:Double?,lat:Double?,with_TM:Boolean?,with_CP:Boolean?,delivery_date:String?) = apiInterface.getCartData(state_id,long,lat,with_TM,with_CP,delivery_date)

    suspend fun getCartDataWithId(cartId:String?,state_id:String?,long:Double?,lat:Double?,with_TM:Boolean?,with_CP:Boolean?,delivery_date:String?) = apiInterface.getCartDataWithId(cartId,state_id,long,lat,with_TM,with_CP,delivery_date)

    suspend fun deleteCartItem(deleteCartObj: DeleteCartObj?) = apiInterface.deleteCartItem(deleteCartObj)

    suspend fun cartAddressAdd(cartAddressIdObj: CartAddressIdObj?) = apiInterface.cartAddressAdd(cartAddressIdObj)

}