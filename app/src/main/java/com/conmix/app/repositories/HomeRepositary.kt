package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 08,March,2021
 */
class HomeRepositary(val apiInterface: ApiInterface){

    suspend fun getProductHome(search:String?,before:String?,after:String?)=apiInterface.getProductHome(search,before,after)
    //suspend fun searchUser(name: String)= apiInterface.searchUser(name)

}