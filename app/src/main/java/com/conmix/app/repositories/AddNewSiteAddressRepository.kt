package com.conmix.app.repositories

import com.conmix.app.data.*
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 09,March,2021
 */
class AddNewSiteAddressRepository (val apiInterface: ApiInterface){
    suspend fun addStateCity(citydetails: Citydetails) = apiInterface.addState(citydetails)
    suspend fun getCountryvalue() = apiInterface.getCountryData()
    suspend fun getStateDataValue(countryId:String?)= apiInterface.getStateFromCountryData(countryId)
    suspend fun getBillStateDataValue()= apiInterface.getStateData()
    suspend fun getCityDataValue(stateId:String?)= apiInterface.getCityData(stateId)
    suspend fun postAddNewData(siteObj: AddSiteAddPostData) = apiInterface.postAddNewSiteAdd(siteObj)
    suspend fun checkSiteAdd(checkSiteObj:CheckSiteNameObj) = apiInterface.checkSiteAdd(checkSiteObj)
    suspend fun postAddBillNewData(billaddObj: AddBillAddPostObj?) = apiInterface.postBillAddNewAdd(billaddObj)
    suspend fun updateBillAddNewAdd(id:String?,billaddObj: AddBillAddPostObj?) = apiInterface.updateBillAddNewAdd(id,billaddObj)
    suspend fun addBillAddressCart(addBillAddIdCart: AddBillAddressIdCart?) = apiInterface.addBillAddressCart(addBillAddIdCart)

}