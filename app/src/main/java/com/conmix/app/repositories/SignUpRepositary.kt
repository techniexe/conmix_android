package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 03,March,2021
 */
class SignUpRepositary (private val apiInterface: ApiInterface){

    suspend fun getOtpByMobNo(mobNo:String,method:String)=apiInterface.getOtpByMobNum(mobNo,method)
    suspend fun getOtpByEmail(email:String) = apiInterface.getOtpByEmail(email)

}