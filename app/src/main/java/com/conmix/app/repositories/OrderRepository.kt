package com.conmix.app.repositories

import com.conmix.app.data.AssignTMOBJ
import com.conmix.app.data.GatewayTranstedIdObj
import com.conmix.app.data.PaymentTranstedIdObj
import com.conmix.app.data.checkTmAvlObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class OrderRepository(private val apiInterface: ApiInterface) {

    suspend fun postOrderData(razorpayPaymentId: GatewayTranstedIdObj?) = apiInterface.postOrderData(razorpayPaymentId)

    suspend fun getOrderlstData(user_id: String?,before: String?,after: String?,order_status: String?,payment_status: String?,gateway_transaction_id: String?) = apiInterface.getOrderlstData(user_id,before,after,order_status,payment_status,gateway_transaction_id)

    suspend fun getOrderDetailLstData(orderId:String?) = apiInterface.getOrderDetailLstData(orderId)

    suspend fun cancelOrderdta(paymentTranstedIdObj: PaymentTranstedIdObj?,order_item_id:String?, order_id:String?) = apiInterface.cancelOrderdta(paymentTranstedIdObj,order_item_id,order_id)

    suspend fun cancelOrderItem(orderId:String?,itemId:String?) = apiInterface.cancelOrderItem(orderId,itemId)

    suspend fun processOrder(orderid: String) = apiInterface.processOrder(orderid)

    suspend fun checkTmAvalibity(checkTmAvlObj: checkTmAvlObj?) = apiInterface.checkTMAvailbility(checkTmAvlObj)

    suspend fun UpdateAssignTm(order_id:String?, order_item_id:String?,assignTmOBJ: AssignTMOBJ?) = apiInterface.UpdateAssignTm(order_id,order_item_id,assignTmOBJ)


}