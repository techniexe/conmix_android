package com.conmix.app.repositories

import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 07,April,2021
 */
class ProductReviewRepository (val apiInterface: ApiInterface) {

    suspend fun getProductReview(product_id: String?,user_id:String?,before:String?,after:String?) = apiInterface.getProductReview(product_id,user_id,before,after)
}