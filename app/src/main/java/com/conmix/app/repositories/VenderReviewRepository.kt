package com.conmix.app.repositories

import com.conmix.app.data.VenderWriteReviewObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 07,April,2021
 */
class VenderReviewRepository(val apiInterface: ApiInterface) {

    suspend fun getVenderReviewData(vendor_id: String?,before:String?,after:String?) = apiInterface.getVenderReview(vendor_id,before,after)

    suspend fun postVenderReviewData(venderWriteReviewObj: VenderWriteReviewObj?) = apiInterface.postVenderReviewData(venderWriteReviewObj)

}