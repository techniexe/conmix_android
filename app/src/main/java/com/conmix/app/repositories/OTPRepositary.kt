package com.conmix.app.repositories

import com.conmix.app.data.UpdateProfileOtpObj
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class OTPRepositary (val apiInterface: ApiInterface){

    suspend fun verifyOtp(mobNumer:String,code:String) = apiInterface.verifyOtp(mobileNumber = mobNumer,code = code)

    suspend fun getOtpByMobNo(mobNo:String,method:String) = apiInterface.getOtpByMobNum(mobNo,method)

    suspend fun getOtpByEmail(email:String) = apiInterface.getOtpByEmail(email)

    suspend fun getOTPByUpdateEmail(updateProfileOtpObj: UpdateProfileOtpObj) = apiInterface.getUpdateEmailOtp(updateProfileOtpObj)

}