package com.conmix.app.repositories

import com.conmix.app.data.SessionRequest
import com.conmix.app.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 02,March,2021
 */
class SessionRepositary(val apiInterface: ApiInterface){
 suspend fun getSessionToken(sessionRequest: SessionRequest) = apiInterface.getSessionToken(sessionRequest)

}