package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.NotificationListAdapter
import com.conmix.app.data.NotificationData
import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.databinding.NotificationActivityLyBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.NotificationViewModel
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 19,May,2021
 */
class NotificationsActivity:AppCompatActivity(), NotificationListAdapter.NotificationItemClick {
    var notificationListAdapter: NotificationListAdapter? = null
    var beforeTime: String? = null
    var isLoading = true
    var notificationList = ArrayList<NotificationData>()
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var binding: NotificationActivityLyBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = NotificationActivityLyBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)
        setUpViewModel()

        binding.rvList?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = binding.rvList?.layoutManager?.childCount
                    val totalItemCount = binding.rvList?.layoutManager?.itemCount
                    val pastVisiblesItems =
                        (binding.rvList?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                    if (isLoading) {

                        if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {
                            isLoading = false
                            if (notificationListAdapter != null) {

                                notificationListAdapter!!.loading()
                                beforeTime = notificationList[notificationList.size - 1].created_at

                                val handler = Handler()
                                handler.postDelayed(Runnable {
                                    getNotificationList(beforeTime)
                                }, 500)
                            }
                        }
                    }
                }

            }
        })
        getNotificationList(beforeTime)
        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()
    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()
    }

    private fun setUpViewModel() {
        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)
    }

    private fun getNotificationList(beforeTime: String?) {
        ApiClient.setToken()
        notificationViewModel.getNotification(beforeTime).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { it1 -> setUpReviewUI(it1.body()?.data?.notifications) }
                            emptyView()
                        } else {
                            emptyView()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        emptyView()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }

        })
    }

    private fun setUpReviewUI(rvArrayList: ArrayList<NotificationData>?) {
        if (!rvArrayList.isNullOrEmpty()) {


            if (notificationListAdapter == null) {

                notificationList.addAll(rvArrayList)

                notificationListAdapter =
                    NotificationListAdapter(this, notificationList)


                notificationListAdapter?.notificationItemClick(this)
                binding.rvList?.adapter = notificationListAdapter
            } else {

                notificationList.addAll(rvArrayList)
                notificationListAdapter?.addLoadMoreLikes(notificationList)
                isLoading = true
                notificationListAdapter?.loadDone()
            }
        }else{
            isLoading = true
            notificationListAdapter?.loadDone()
        }
    }

    override fun itemClick(notificationId: String, _id: String?) {
        if (!notificationId.isNullOrEmpty()) {
            ApiClient.setToken()
            notificationViewModel.notificationItemClick(notificationId).observe(this, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                _id?.let { it1 -> navigateToMainList(it1) }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                    }
                }

            })
        }
    }

    private fun navigateToMainList(_id: String) {

        val intent = Intent(this@NotificationsActivity, OrderDetailActivity::class.java)
        intent.putExtra("orderId", _id)
        startActivity(intent)
        Bungee.slideLeft(this)
    }

    override fun onBackPressed() {

        binding.imgBack.performClick()
    }
    private fun emptyView() {

        if (notificationList.isNotEmpty()) {
            stopAnim()
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE

        } else {
            stopAnim()
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE
            bindingEmptyOrder.tvNoData.text = getString(R.string.empty_notification_lst)
        }
    }
}