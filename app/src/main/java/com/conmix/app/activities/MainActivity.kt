package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.BuildConfig
import com.conmix.app.R
import com.conmix.app.data.*
import com.conmix.app.databinding.ContentMainBinding
import com.conmix.app.databinding.MainActivityBinding
import com.conmix.app.databinding.ToolbarBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.fragments.CartFragment
import com.conmix.app.fragments.HomeFragment
import com.conmix.app.fragments.ProfileFragment
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.*
import com.fxn.OnBubbleClickListener
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import comCoreConstants.conmix.utils.CoreConstants
import org.json.JSONException
import org.json.JSONObject
import spencerstudios.com.bungeelib.Bungee
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Hitesh Patel on 02,March,2021
 */
class MainActivity : BaseActivity(), PaymentResultWithDataListener, CartFragment.DialogToFragment {
    private lateinit var binding: MainActivityBinding
    private lateinit var bindingToolbar: ToolbarBinding
    private lateinit var bindingContentMainBinding: ContentMainBinding
    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var orderViewModel: OrderViewModel
    private lateinit var paymentCaptureViewModel: PaymentCaptureViewModel
    var cartData: CartObjDta? = null
    var userData: UserProfileObj? = null
    var nwamnt:String? = null
    var avLoading:com.wang.avi.AVLoadingIndicatorView? = null
    var currentTab = R.id.home;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)

        bindingToolbar = binding.toolbarView
        bindingContentMainBinding = bindingToolbar.toolbarContantMain
        Checkout.preload(applicationContext)
        setupViewModel()
        setContentView(binding.root)
        sharedViewModel.setOnSuccess(false)
        sharedViewModel.setOnError(false)

        bindingToolbar.fmNotificationbar.visibility = View.VISIBLE
        val isFromSplash = intent.getBooleanExtra("isFromSplash", false)
        val isFromNotification = intent.getBooleanExtra("isFromNotification", false)
        if (isFromNotification) {
            onNewIntent(intent)
        }

        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE,2)
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val date = simpleDateFormat.format(calendar.time)
        SharedPrefrence.setSelectedDate(this, Utils.getConvertDateWithoutTime(date!!))

        val endDate = SharedPrefrence.getSelectedENDDate(this)
        if(endDate != null){
            val isEndDatevalid = Utils.comparedateToday(endDate,Utils.getConvertDateWithoutTime(date!!))
            if(isEndDatevalid){
                SharedPrefrence.setSelectedENDDate(this, endDate)
            }else{
                SharedPrefrence.setSelectedENDDate(this, Utils.getConvertDateWithoutTime(date!!))
            }

        }else{
            SharedPrefrence.setSelectedENDDate(this, Utils.getConvertDateWithoutTime(date!!))
        }


        var isLogging = SharedPrefrence.getLogin(this)
        if (!isLogging) {
            //registerFCMToken()
            bindingToolbar.fmNotificationbar.visibility = View.GONE
            getSessionToken()
        }else{
            bindingToolbar.fmNotificationbar.visibility = View.VISIBLE
           // registerFCMToken()
        }



        supportFragmentManager.commit {
            addToBackStack("HomeFragment")
            add(R.id.content, HomeFragment())
        }

        bindingToolbar.fmNotificationbar.setOnClickListener {
            var intent = Intent(this, NotificationsActivity::class.java)
            startActivity(intent)
            Bungee.slideLeft(this)
        }

        binding.bubbleTabBar.addBubbleListener(object : OnBubbleClickListener {
            override fun onBubbleClick(id: Int) {
                when (id) {
                    R.id.home -> {
                        /* fmNotificationbar.visibility = View.VISIBLE
                         val count: Int = SharedPrefrence.getCount(this@MainActivity)
                         if (count > 0) {
                             notifications_badge.visibility = View.VISIBLE
                             notifications_badge.text = count.toString()
                         } else {
                             notifications_badge.visibility = View.GONE
                         }
                         bindingToolbar.profile_more_menu.visibility = View.GONE*/
                        currentTab = R.id.home
                        bindingToolbar.profileMoreMenu.visibility = View.GONE
                        bindingToolbar.sellWithUs.visibility = View.GONE
                        bindingToolbar.fmNotificationbar.visibility = View.VISIBLE
                        redirectToHomeFragment()
                    }

                    R.id.shopping -> {
                        val isLogging = SharedPrefrence.getLogin(this@MainActivity)
                        val siteId = SharedPrefrence.getSiteId(this@MainActivity)
                        bindingToolbar.fmNotificationbar.visibility = View.GONE
                        bindingToolbar.profileMoreMenu.visibility = View.GONE
                        bindingToolbar.sellWithUs.visibility = View.GONE

                        if (isLogging) {
                            currentTab = R.id.shopping
                            if (siteId.isNullOrEmpty()) {
                                redirectToHomeFragment()
                            } else {
                                // binding.profile_more_menu.visibility = View.GONE
                                var shoppingFragment =
                                    supportFragmentManager.findFragmentByTag(CoreConstants.FRAGMENTS.CART)
                                if (shoppingFragment == null) {
                                    shoppingFragment = CartFragment()
                                    shoppingFragment.setListener(this@MainActivity)
                                }

                                /*replaceFragmentWithBackStack(
                                    R.id.content, shoppingFragment!!

                                )*/

                                supportFragmentManager.commit {
                                    addToBackStack(CoreConstants.FRAGMENTS.CART)
                                    replace(R.id.content, shoppingFragment)
                                }
                            }

                        } else {

                            Utils.showToast(
                                this@MainActivity,
                                getString(R.string.please_login_message),
                                Toast.LENGTH_SHORT
                            )

                            redirectToLoginActivity()
                        }

                    }
                    R.id.profile -> {
                        currentTab = R.id.profile
                        var isLogging = SharedPrefrence.getLogin(this@MainActivity)
                        bindingToolbar.fmNotificationbar.visibility = View.GONE
                        bindingToolbar.sellWithUs.visibility = View.GONE
                        if (isLogging == true) {
                            bindingToolbar.profileMoreMenu.visibility = View.VISIBLE
                        } else {
                            bindingToolbar.profileMoreMenu.visibility = View.GONE
                        }
                        var profileFragment =
                            supportFragmentManager.findFragmentByTag(CoreConstants.FRAGMENTS.PROFILE)
                        if (profileFragment == null) {
                            profileFragment = ProfileFragment()
                        }
                        /*replaceFragmentWithBackStack(
                            R.id.content, profileFragment

                        )*/

                        supportFragmentManager.commit {
                            addToBackStack(CoreConstants.FRAGMENTS.PROFILE)
                            replace(R.id.content, profileFragment)
                        }
                    }
                }
            }
        })

    }


    private fun setupViewModel() {

        sessionViewModel =
            ViewModelProvider(
                this,
                SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
            ).get(SessionViewModel::class.java)

        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)

        sharedViewModel =
            ViewModelProvider(
                this
            )
                .get(SharedViewModel::class.java)
        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)

        paymentCaptureViewModel = ViewModelProvider(
            this,
            PaymentCaptureViewModel.ViewModelFactory(ApiClient().apiPaymentCapture)
        )
            .get(PaymentCaptureViewModel::class.java)

    }

    private fun getSessionToken() {
        ApiClient.setToken()
        val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

        sessionViewModel.getSession(sessionRequest)
            .observe(this, androidx.lifecycle.Observer { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        if (response.data?.isSuccessful!!) {
                            response.data?.let { response ->
                                if (response.body()?.data?.token != null) {
                                    SharedPrefrence.setSessionToken(
                                        this,
                                        response.body()?.data?.token
                                    )

                                }
                            }
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            })

    }

    private fun redirectToHomeFragment() {
        val isLogging = SharedPrefrence.getLogin(this@MainActivity)
        if(isLogging){
            bindingToolbar.fmNotificationbar.visibility = View.VISIBLE
        }else{
            bindingToolbar.fmNotificationbar.visibility = View.GONE
        }

        var homeFragment = supportFragmentManager.findFragmentByTag(CoreConstants.FRAGMENTS.HOME)
        if (homeFragment == null) {
            homeFragment = HomeFragment()
        }
        supportFragmentManager.commit {
            addToBackStack(CoreConstants.FRAGMENTS.HOME)
            replace(R.id.content, homeFragment)
        }

    }

    private fun registerFCMToken() {

        Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            val secureId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
            SharedPrefrence.setDeviceID(this, secureId)
            SharedPrefrence.setFCMtokem(this, token)
            registerFcmTokenToServer(token, secureId ?: "")
        })
    }

    private fun registerFcmTokenToServer(token: String, deviceId: String) {
        ApiClient.setToken()
        var fcmData = FcmData(token, deviceId, "android")
        notificationViewModel.ragisterToken(fcmData).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {

                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }
        })
    }

    fun getNotifcationCount() {
        ApiClient.setToken()
        notificationViewModel.notificationCount().observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        //stopAnim()
                        resource.data?.let {
                            val count = resource.data.body()?.data?.unseen_message_count ?: 0
                            //val unSeencount = resource.data.body()?.data?.unseen_message_count ?: 0

                            SharedPrefrence.setCount(this, count)
                            runOnUiThread {
                                if (count > 0) {
                                    bindingToolbar.notificationsBadge.visibility = View.VISIBLE

                                    if(count > 9){
                                        bindingToolbar.notificationsBadge.text = "9+"
                                    }else{
                                        bindingToolbar.notificationsBadge.text = count.toString()
                                    }

                                } else {
                                    bindingToolbar.notificationsBadge.visibility = View.GONE
                                }
                            }


                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }

        })
    }

    override fun onBackPressed() {


        if (currentTab == R.id.home) {
            finish()
            Bungee.slideRight(this)
        } else {
            binding.bubbleTabBar.setSelected(0)
            Bungee.slideRight(this)
        }
    }

    private fun redirectToLoginActivity() {
        val intent = Intent(this@MainActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        Bungee.slideRight(this@MainActivity)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        Log.d("tag", "onNewIntent")
        val data: HashMap<String, String> =
            intent?.getSerializableExtra("data") as HashMap<String, String>

        if (!data.isEmpty()) {
            try {

                /*when (data.get("notification_type")) {
                    "1" -> {
                        val intent = Intent(this, OrderDetailActivity::class.java)
                        intent.putExtra("OrderID", data.get("order_id") ?: "")
                        startActivity(intent)
                    }
                    "2"->{

                    }

                }*/


                val intent = Intent(this, OrderDetailActivity::class.java)
                intent.putExtra("orderId", data.get("order_id") ?: "")
                startActivity(intent)

            } catch (e: Exception) {
                Log.i("FCM", "fcm Exception" + e.cause)
                Log.i("FCM", "fcm Exception" + e.localizedMessage)
            }

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onResume() {
        super.onResume()
        val isLogging = SharedPrefrence.getLogin(this@MainActivity)
        if (isLogging) {
            getNotifcationCount()
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?,p1: PaymentData?) {

        val fm = supportFragmentManager
        val f: CartFragment? = fm.findFragmentByTag(CoreConstants.FRAGMENTS.CART) as CartFragment?
        f?.startAnim()

        CallCapture(razorpayPaymentId)

    }


    fun CallCapture(razorpayPaymentId: String?){
        val headerStr = String.format(getString(R.string.header_capture_str),"rzp_test_MDFf9gouEAhcyB","R82pv8IwsTCBeqOSeCftceQX")

        val data: ByteArray = headerStr.toByteArray(charset("UTF-8"))
        val base64 = Base64.encodeToString(data, Base64.NO_WRAP)

        val encodedString = "Basic "+ base64

        val paymentCapObj = PaymentCaptureObj(nwamnt!!,"INR")

        getPaymentCapture(encodedString,razorpayPaymentId!!,paymentCapObj,razorpayPaymentId)
    }


    fun getPaymentCapture(authheader:String, payment_id:String, paymentCaptureObj: PaymentCaptureObj, razorpayPaymentId: String?) {

        paymentCaptureViewModel.paymentCapture(authheader,payment_id,paymentCaptureObj).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        //stopAnim()
                        resource.data?.let {
                            if(resource.data?.code() == 200){
                                processApi(razorpayPaymentId)
                            }else{
                                retryCapturetError(razorpayPaymentId)
                            }

                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }

        })
    }

    override fun onPaymentError(errorCode: Int, response: String?,p1: PaymentData?) {
        /* val paymentError = PaymentError(errorCode, response, paymentData)
         sharedViewModel.setPaymentError(paymentError)
         sharedViewModel.setOnError(true)*/

        try {
            val jsonObject = JSONObject(response.toString())
            val errormsg = jsonObject.getJSONObject("error")
            val discr = errormsg.getString("description")
            try {
                redirectToDialogPaymentError(discr)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } catch (err: JSONException) {
            Log.d("Error", err.toString())
        }




    }


    private fun StartPayment() {
        // Set key id
        val activity: Activity = this
        val co = Checkout()
        co.setKeyID("rzp_test_MDFf9gouEAhcyB")
        //set Image
        co.setImage(R.mipmap.ic_launcher)
        try {
            val options = JSONObject()
            options.put("name", "Conmix")
            options.put("description", "")
            options.put("theme.color", "#136E9D")
            options.put("currency", "INR");
            //options.put("order_id", "order_DBJOWzybf0sJbb");
            //   val amtn = Utils.setPrecesionFormate(cartObjDta?.total_amount?:0.0)
            val amtn1 = (cartData?.total_amount ?: 0.0) * 100
            val amnt = Math.round(amtn1)
            nwamnt = amnt.toString()
            options.put("amount", amnt.toString())//pass amount in currency subunits

            options.put("timeout",200)
            val retryObj = JSONObject()
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            val prefill = JSONObject()
            prefill.put("name", userData?.full_name)
            prefill.put("email", userData?.email)
            prefill.put("contact", userData?.mobile_number)

            options.put("prefill", prefill)

            val notesObj = JSONObject()

            var strBuffer = StringBuffer()
            strBuffer.append(cartData?.siteInfo?.address_line1)
            strBuffer.append(",")
            strBuffer.append(cartData?.siteInfo?.address_line2)
            strBuffer.append(",")
            strBuffer.append(cartData?.siteInfo?.city_name)
            strBuffer.append(" - ")
            strBuffer.append(cartData?.siteInfo?.pincode)
            strBuffer.append(",")
            strBuffer.append(cartData?.siteInfo?.state_name)
            strBuffer.append(",")
            strBuffer.append(cartData?.siteInfo?.country_name)
            notesObj.put("address", strBuffer)
            notesObj.put("merchant_order_id", cartData?.unique_id)

            options.put("notes", notesObj)
            options.put("send_sms_hash", true)

            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }


    private fun processApi(razorpayPaymentId: String?) {
        ApiClient.setToken()
        val obj = GatewayTranstedIdObj(razorpayPaymentId)

        orderViewModel.postOrderData(obj).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                         startAnim()

                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.code() == 200) {
                            redirectToDialog(resource.data.body()?.data!!)
                        } else {
                            Utils.setErrorData(this, resource.data?.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }


            }
        })

    }


    private fun redirecToMainActivity() {

        val fm = supportFragmentManager
        val f: CartFragment? = fm.findFragmentByTag(CoreConstants.FRAGMENTS.CART) as CartFragment?
        f?.stopAnim()

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        //finish()
        Bungee.slideRight(this)
    }


    private fun retryCapturetError(razorpayPaymentId: String?) {
        val fm = supportFragmentManager
        val f: CartFragment? = fm.findFragmentByTag(CoreConstants.FRAGMENTS.CART) as CartFragment?
        f?.stopAnim()

        val dialogFragment: BreakAlertDialog = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {
                CallCapture(razorpayPaymentId)
            }

            override fun onCancelClicked() {


            }
        }, "Please try again", "Ok", "")
        val fragmentManager: FragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(dialogFragment, "BreakAlertDialog.FRAGMENT_TAG").commitAllowingStateLoss()
        dialogFragment.show(this@MainActivity.supportFragmentManager, "Alert")
        dialogFragment.isCancelable = false

    }

    private fun redirectToDialogPaymentError(error: String?) {
        val fm = supportFragmentManager
        val f: CartFragment? = fm.findFragmentByTag(CoreConstants.FRAGMENTS.CART) as CartFragment?
        f?.stopAnim()

        val dialogFragment: BreakAlertDialog = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {
                stopAnim()
            }

            override fun onCancelClicked() {


            }
        }, String.format(error?:""), "Ok", "")
        val fragmentManager: FragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(dialogFragment, "BreakAlertDialog.FRAGMENT_TAG").commitAllowingStateLoss()
        dialogFragment.show(this@MainActivity.supportFragmentManager, "Alert")
        dialogFragment.isCancelable = false

    }

    private fun redirectToDialog(dOrder: OrderResObj) {
        val fragment = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {
                processApiNew(dOrder)
            }

            override fun onCancelClicked() {


            }
        }, String.format(getString(R.string.alert_order_place), dOrder.display_id), "Ok", "")


        fragment.show(this.supportFragmentManager, "alert")
        fragment.isCancelable = false
    }


    private fun processApiNew(order: OrderResObj) {
        ApiClient.setToken()
        orderViewModel.processorder(order._id!!).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->

                when (resource.status) {
                    Status.LOADING -> {
                         startAnim()

                    }
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { it1 ->
                                // redirectToDialog(order.display_id)
                                redirecToMainActivity()
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }


    override fun sortDialogDismiss(cartData: CartObjDta?, userData: UserProfileObj?,avLoading:com.wang.avi.AVLoadingIndicatorView?) {
        this.cartData = cartData
        this.userData = userData
        this.avLoading = avLoading

        startAnim()
        StartPayment()
    }


    companion object {

        private const val TAG = "MainActivity"
    }


    fun startAnim() {
        if (avLoading != null)
            avLoading?.show()
            avLoading?.visibility = View.VISIBLE
    }

    fun stopAnim() {
        if (avLoading != null)
            avLoading?.hide()
            avLoading?.visibility = View.GONE
    }
}