package com.conmix.app.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.BuildConfig
import com.conmix.app.R
import com.conmix.app.data.SessionRequest
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.AutoStartPermissionHelper
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.SessionViewModel
import spencerstudios.com.bungeelib.Bungee
import java.util.*



/**
 * Created by Hitesh Patel on 24,February,2021
 */
class SplashScreenActivity: BaseActivity() {

    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        //fullScreen()
        setupViewModel()
        Handler().postDelayed({
        val  sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE)
        val autoStart = sharedpreferences.getString("autoStart", "yes");
        if (autoStart.equals("yes")) {
            AutoStartPermissionHelper.getInstance().getAutoStartPermission(this)
        }
        getSessionToken()
        }, 2000)

    }

    private fun setupViewModel() {

        sessionViewModel =
            ViewModelProvider(
                this,
                SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
            ).get(SessionViewModel::class.java)



    }

    private fun navigateToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent)
        finish()
        Bungee.slideLeft(this)
    }

    private fun getSessionToken() {

        var customToken = SharedPrefrence.getSessionToken(this)
        if (customToken.isNullOrBlank()) {
            val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

            sessionViewModel.getSession(sessionRequest = sessionRequest)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                if (resource.data?.isSuccessful!!) {
                                    resource.data?.let { response ->
                                        SharedPrefrence.setSessionToken(
                                            this,
                                            response.body()?.data?.token
                                        )
                                    }
                                    navigateToMainActivity()
                                } else {

                                }
                            }
                            Status.ERROR -> {
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                            Status.LOADING -> {
                            }
                        }
                    }
                })
        }else{
            navigateToMainActivity()
        }
    }
}