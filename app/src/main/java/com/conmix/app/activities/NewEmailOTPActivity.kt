package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.R
import com.conmix.app.data.AddToCartCustomMixObj
import com.conmix.app.data.LoginResponse
import com.conmix.app.data.UpdateUserProfileObj
import com.conmix.app.databinding.EmailOtpActivityBinding
import com.conmix.app.services.SMSBroadcast.SmsVerifyCatcher
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.OTPViewModel
import com.conmix.app.viewmodels.SignUpRegisterViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.regex.Pattern


/**
 * Created by Hitesh Patel on 03,December,2021
 */
class NewEmailOTPActivity: BaseActivity(), View.OnClickListener, TextView.OnEditorActionListener {
    private lateinit var binding: EmailOtpActivityBinding
    private var mobNum:String? = null
    private var otp: String? = ""
    private var cTimer: CountDownTimer? = null
    private var smsVerifyCatcher: SmsVerifyCatcher? = null
    private lateinit var otpViewModel: OTPViewModel
    private lateinit var signUpRegisterViewModel: SignUpRegisterViewModel
    var isFromProductDetail: Boolean = false
    var objCart: AddToCartCustomMixObj? = null
    var isEmail = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EmailOtpActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        // fullScreen()
        setupViewModel()
        getIntentdate()
        mobNum = intent.getStringExtra(CoreConstants.Intent.INTENT_MOB_NUMBER)
        binding.btnVerify.isEnabled = false
        binding.btnVerify.alpha = 0.5f
        if (!mobNum.isNullOrEmpty()) {
            binding.tvDescription.visibility = View.VISIBLE

            if(!isEmail) {
                binding.tvDescription.text = Utils.getMaskMobileNumber(mobNum,isEmail,this)
            }else{
                binding.tvDescription.text = Utils.getMaskMobileNumber(mobNum,isEmail,this)
            }
        } else
            binding.tvDescription.visibility = View.GONE

        binding.backBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        smsVerifyCatcher?.setPhoneNumberFilter("VM-DSHSMS")
        //init SmsVerifyCatcher
        smsVerifyCatcher = SmsVerifyCatcher(
            this
        ) { message ->
            val code: String? = parseCode(message)

            //Parse verification code
            binding.codeEdtpin.setText(code) //set code in edit text
            //then you can send verification code to server
        }

        //smsVerifyCatcher.setFilter("regexp");
        reverseTimer(120)
        binding.btnVerify.setOnClickListener(this)
        binding.resendTxt.setOnClickListener(this)
        binding.callWithCodeText.setOnClickListener(this)

        binding.codeEdtpin.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                stopAnim()
                otp = binding.codeEdtpin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    // verifyOtp()
                    if(isEmail){
                        val obj = UpdateUserProfileObj(
                            null, null,
                            null, null,
                            mobNum, null,
                            null, null, null,null,null,null,otp
                        )
                        updateProfilefun(obj)
                    }else{
                        val obj = UpdateUserProfileObj(
                            null, null,
                            null, null,
                            null, mobNum,
                            null, null, null,null,otp,null,null
                        )
                        updateProfilefun(obj)
                    }



                } else {
                    stopAnim()
                }
            }
            false
        }
        binding.codeEdtpin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //binding.btnVerify.isEnabled = s?.length == 6

                if(s?.length == 6){
                    binding.btnVerify.isEnabled = true
                    binding.btnVerify.alpha = 1.0f
                }else{
                    binding.btnVerify.isEnabled = false
                    binding.btnVerify.alpha = 0.5f
                }
            }

        })
    }
    private fun updateProfilefun(obj: UpdateUserProfileObj) {
        ApiClient.setToken()
        signUpRegisterViewModel.updateUserProfileData(obj)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                Utils.showToast(
                                    this,
                                    getString(R.string.edit_profile_sucess),
                                    Toast.LENGTH_LONG
                                )
                                resource.data.let { response -> setData(response.body()?.data) }
                            } else {
                                cTimer?.cancel()
                                binding.resendTxt.visibility = View.VISIBLE
                                binding.callWithCodeText.visibility = View.GONE
                                binding.otpTimer.visibility = View.GONE
                                Utils.setErrorData(this, resource.data?.errorBody())
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            cTimer?.cancel()
                            binding.resendTxt.visibility = View.VISIBLE
                            binding.callWithCodeText.visibility = View.GONE
                            binding.otpTimer.visibility = View.GONE

                            Utils.showToast(this@NewEmailOTPActivity,getString(R.string.error_otp), Toast.LENGTH_SHORT)
                        }
                    }
                }
            })
    }

    private fun getIntentdate() {
        isFromProductDetail = intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)
        isEmail = intent.getBooleanExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS,false)
        objCart = intent.getParcelableExtra("cartObj")


    }
    private fun setupViewModel() {
        otpViewModel =
            ViewModelProvider(this, OTPViewModel.ViewModelFactory(ApiClient().apiService))
                .get(OTPViewModel::class.java)
        signUpRegisterViewModel = ViewModelProvider(this,SignUpRegisterViewModel.ViewModelFactory(ApiClient().apiService)).get(SignUpRegisterViewModel::class.java)
    }
    private fun parseCode(message: String): String? {
        val p = Pattern.compile("\\b\\d{6}\\b")
        val m = p.matcher(message)
        var code: String? = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    override fun onStart() {
        super.onStart()
        smsVerifyCatcher!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        smsVerifyCatcher!!.onStop()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!)
        smsVerifyCatcher!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return false
    }


    private fun reverseTimer(Seconds: Int) {
        // tvResendCode.visibility = View.GONE

        binding.resendTxt.visibility = View.GONE

        binding.callWithCodeText.visibility = View.GONE
        binding.faltuLine.visibility = View.GONE
        binding.otpTimer.visibility = View.VISIBLE
        binding.otpTimer.text = "00:00"
        cTimer?.cancel()
        cTimer = object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val minutes = seconds / 60
                seconds %= 60
                binding.otpTimer.text = String.format("%02d", minutes) + ":" + String.format("%02d", seconds)

            }

            override fun onFinish() {

                binding.resendTxt.visibility = View.VISIBLE
                binding.callWithCodeText.visibility = View.GONE
                binding.faltuLine.visibility = View.GONE
                binding.otpTimer.visibility = View.GONE
                binding.resendTxt.text = getString(R.string.txt_resend_code)
            }
        }.start()
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
        //loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE
        }
        //loaderFrame.visibility = View.GONE
    }

/*    private fun verifyOtp() {
        ApiClient.setToken()
        otp?.let { otpViewModel.verifyOTP(mobNum!!, it) }?.observe(this, Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    stopAnim()
                    if (response.data?.isSuccessful!!) {
                        response.data.let { response -> setData(response.body()?.data) }
                        *//*val intent = Intent(this@NewEmailOTPActivity, RegistrationActivity::class.java)
                        intent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER, mobNum)
                        intent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, isEmail)
                        intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL,isFromProductDetail)
                        intent.putExtra("cartObj",objCart)
                        startActivity(intent)
                        Bungee.slideLeft(this)*//*
                        finish()
                    } else {
                        cTimer?.cancel()
                        binding.resendTxt.visibility = View.VISIBLE
                        binding.callWithCodeText.visibility = View.GONE
                        Utils.setErrorData(this, response.data.errorBody())
                    }
                }
                Status.ERROR -> {
                    stopAnim()
                    cTimer?.cancel()
                    binding.resendTxt.visibility = View.VISIBLE
                    binding.callWithCodeText.visibility = View.GONE

                    Utils.showToast(this@NewEmailOTPActivity,getString(R.string.error_otp), Toast.LENGTH_SHORT)
                }
                Status.LOADING -> {
                    startAnim()
                }
            }
        })
    }*/

    private fun setData(response: LoginResponse?) {
        response?.customToken?.let { SharedPrefrence.setSessionToken(this, it) }
        val intent = Intent(this@NewEmailOTPActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
        Bungee.slideRight(this@NewEmailOTPActivity)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnVerify -> {
                stopAnim()
                otp = binding.codeEdtpin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    if(isEmail){
                        val obj = UpdateUserProfileObj(
                            null, null,
                            null, null,
                            mobNum, null,
                            null, null, null,null,null,null,otp
                        )
                        updateProfilefun(obj)
                    }else{
                        val obj = UpdateUserProfileObj(
                            null, null,
                            null, null,
                            null, mobNum,
                            null, null, null,null,otp,null,null
                        )
                        updateProfilefun(obj)
                    }

                } else {
                    stopAnim()
                }
            }
            R.id.resend_txt -> {

                if(!isEmail){
                    getOtp("sms")
                }else{
                    getOtpEmail()
                }

            }
            R.id.callWithCodeText->{
                if(!isEmail){
                    getOtp("call")
                }else{
                    getOtpEmail()
                }
            }
        }
    }


    private fun getOtpEmail() {
        ApiClient.setToken()
        reverseTimer(120)
        otpViewModel.getOtpByEmail(mobNum!!).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {

                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })
    }

    private fun getOtp(type:String) {
        ApiClient.setToken()
        reverseTimer(120)
        otpViewModel.getOtpbyMob(mobNum!!, type).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {

                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        //startAnim()
                    }
                }
            }
        })
    }
}