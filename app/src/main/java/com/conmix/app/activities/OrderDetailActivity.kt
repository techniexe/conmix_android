package com.conmix.app.activities


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.adapter.OrderDetailLstAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.databinding.OrderDetailActivityBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.dialogs.WriteReviewDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.OrderViewModel
import com.conmix.app.viewmodels.VenderReviewViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 22,April,2021
 */
class OrderDetailActivity: BaseActivity(), OrderDetailLstAdapter.SelectOrderItemStatus,
    WriteReviewDialog.DialogToFragment {

    private lateinit var binding: OrderDetailActivityBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding
    private lateinit var orderViewModel: OrderViewModel
    private lateinit var venderReviewViewModel: VenderReviewViewModel
    private lateinit var orderAdapter: OrderDetailLstAdapter
    var orderId:String? = null
    var  resDta: OrderlstDetailObj? = null
    var orderData: OrderDetailLstObjRes? = null
    var orderItemData:ArrayList<OrderDetailItemDta>?= ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderDetailActivityBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)

        orderId = intent.getStringExtra("orderId")
        setupViewModel()
        getOrderDetaiData(orderId)

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
    }

    override fun onBackPressed() {
        binding.imgBack.performClick()
    }

    private fun setupViewModel() {


        orderViewModel = ViewModelProvider(
                this,
                OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(OrderViewModel::class.java)

        venderReviewViewModel = ViewModelProvider(
                this,
                VenderReviewViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(VenderReviewViewModel::class.java)


    }

    private fun startAnim() {
        if (binding.avLoading != null)

            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE

    }

    private fun stopAnim() {

        if (binding.avLoading != null)
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE

    }


    private fun getOrderDetaiData(orderId:String?) {


        ApiClient.setToken()

        orderViewModel.getOrderDetailLstData(orderId).observe(this, Observer{

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {


                        if (resource.data?.isSuccessful!!) {

                            setResposerData(response = resource.data.body()?.data)

                        } else {
                            stopAnim()
                            emptyView()
                            //Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }

    private fun setResposerData(response: OrderlstDetailObj?) {
        orderData = response?.orderData
        binding.tvOrderTitle.text = String.format(getString(R.string.txt_order), orderData?.display_id)
        orderItemData = response?.orderItemData?: ArrayList()
        emptyView()
        setupUI()

    }

    private fun setupUI() {
        stopAnim()
        binding.rvOrderList.layoutManager = LinearLayoutManager(this)
        orderAdapter = OrderDetailLstAdapter(this, orderItemData, supportFragmentManager)
        orderAdapter.setOrderListener(this)
        binding.rvOrderList.adapter = orderAdapter
        orderItemData?.let { setData() }
    }


    private fun setData() {
        if (orderItemData != null) {



            if (orderItemData?.size?:0 > 1) {
                binding.tvCartItemLable.text = String.format(
                        getString(R.string.txt_cart_items),
                        orderItemData?.size
                )
            } else {
                binding.tvCartItemLable.text = String.format(
                        getString(R.string.txt_cart_item),
                        orderItemData?.size
                )
            }

            binding.tvCartItem.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.selling_price_With_Margin?:0.0)
            )

            binding.txtTransistAmtValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.TM_price?:0.0)
            )

            binding.tvConcretePumValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.CP_price?:0.0)
            )

            /*binding.tvItemGST.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.gst_price)
            )*/

            if(orderData?.cgst_price != null && orderData?.cgst_price?:0.0 > 0){
                binding.cGSTLnLy.visibility = View.VISIBLE
                binding.sGSTLnLy.visibility = View.VISIBLE
                binding.iGSTLnLy.visibility = View.GONE
                binding.tvItemCGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(orderData?.cgst_price?:0.0)
                )

                binding.tvItemSGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(orderData?.sgst_price?:0.0)
                )

            }else{
                binding.cGSTLnLy.visibility = View.GONE
                binding.sGSTLnLy.visibility = View.GONE

                if(orderData?.igst_price != null && orderData?.igst_price?:0.0 > 0){
                    binding.iGSTLnLy.visibility = View.VISIBLE
                binding.tvItemIGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(orderData?.igst_price?:0.0)
                )
                }else{
                    binding.iGSTLnLy.visibility = View.GONE

                }


            }


            if(orderData?.coupon_amount != null && orderData?.coupon_amount?:0.0>0){
                binding.discountLnLy.visibility = View.VISIBLE
                binding.amountLnLy.visibility = View.VISIBLE
                binding.txtDiscountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.coupon_amount)
                )
                binding.txtAmountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.amount?:0.0)
                )

            }else{
                binding.discountLnLy.visibility = View.GONE
                binding.amountLnLy.visibility = View.GONE
            }


            binding.tvItemTotal.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(orderData?.total_amount?:0.0)
            )

            binding.tvItemPaymentType.text = orderData?.payment_mode

            binding.tvOrderStatus.text = orderData?.order_status

            if(orderData?.order_status == "CANCELLED"){

                binding.tvOrderStatus.setTextColor( ContextCompat.getColor(
                        this,
                        R.color.rejected_color
                ))

            }else if(orderData?.order_status == "PLACED"){
                binding.tvOrderStatus.setTextColor( ContextCompat.getColor(
                        this,
                        R.color.pickup_color
                ))
            }else if(orderData?.order_status == "PROCESSING"){
                binding.tvOrderStatus.setTextColor( ContextCompat.getColor(
                        this,
                        R.color.tmAssign_color
                ))
            }else if(orderData?.order_status == "DELIVERED"){
                binding.tvOrderStatus.setTextColor( ContextCompat.getColor(
                        this,
                        R.color.delivered_color
                ))
            }else if(orderData?.order_status == "LAPSED"){
                binding.tvOrderStatus.setTextColor( ContextCompat.getColor(
                        this,
                        R.color.rejected_color
                ))
            }
            else {
                binding.tvOrderStatus.setTextColor( ContextCompat.getColor(
                        this,
                        R.color.delayed_color
                ))
            }


            if (orderData?.site_name != null) {


                binding.llAddress.visibility = View.VISIBLE

                binding.tvCompanyName.text = orderData?.company_name
                binding.tvBuyerName.text = orderData?.site_name

                val strBuffer = StringBuffer()
                strBuffer.append(orderData?.address_line1)
                strBuffer.append(",\n")
                strBuffer.append(orderData?.address_line2)
                strBuffer.append("\n")
                strBuffer.append(orderData?.city_name)
                strBuffer.append(" - ")
                strBuffer.append(orderData?.pincode)
                strBuffer.append("\n")
                strBuffer.append(orderData?.state_name)
                strBuffer.append(",\n")
                strBuffer.append(orderData?.country_name)
                binding.tvAddress.setText(strBuffer.toString())
                /*binding.tvPhoneNumber.text =
                        String.format(
                                getString(R.string.order_detail_phone_str),
                                orderData?.mobile_number
                        )*/


                /*binding.tvPhoneNumber?.text =
                    String.format(
                        getString(R.string.site_phone_str),
                        orderData?.mobile_number?:""
                    )*/


                if(!orderData?.mobile_number.isNullOrEmpty()){
                    /*binding.tvPhoneNumber?.text =
                        String.format(
                            getString(R.string.site_phone_str),
                            orderData?.mobile_number?:""
                        )
                    binding.tvPhoneNumber?.setOnClickListener {

                        val number = orderData?.mobile_number
                        val i = Intent(
                            Intent.ACTION_DIAL,
                            Uri.parse("tel:+$number")
                        )
                        startActivity(i)
                    }*/


                    val str = String.format(
                        getString(R.string.site_phone_str),
                        orderData?.mobile_number?:""
                    )


                    orderData?.mobile_number?.let {
                        Utils.setSpanWithClick(
                            context = this,
                            view = binding.tvPhoneNumber,
                            fulltext = str,
                            subtext = it,
                            color = R.color.site_add_owner_txt_color,
                            phoneNumber = orderData?.mobile_number!!
                        )
                    }
                }else{
                    binding.tvPhoneNumber?.visibility = View.GONE
                }

            }

            if (orderData?.buyer_billing_address != null) {

                binding.llBillAddressMain.visibility = View.VISIBLE
              //  binding.tvBillCompanyName.text = orderData?.buyer_billing_address?.company_name


                if(orderData?.buyer_billing_address?.full_name != null &&  orderData?.buyer_billing_address?.full_name != " "){
                    binding.tvBillCompanyName.text = orderData?.buyer_billing_address?.full_name
                }else {
                    binding.tvBillCompanyName.text = orderData?.buyer_billing_address?.company_name
                }

                binding.tvBillAddress.text = orderData?.buyer_billing_address?.line1+",\n"+orderData?.buyer_billing_address?.line2+"\n"+
                        orderData?.buyer_billing_address_city_name+" - "+orderData?.buyer_billing_address?.pincode+"\n"+orderData?.buyer_billing_address_state_name+", "+
                        "India"

                //binding.tvBillGSTNumber.text = "GST No : " + orderData?.buyer_billing_address?.gst_number.toString()


                if(orderData?.buyer_billing_address?.gst_number ==  " " ){
                    binding.tvBillGSTNumber.visibility = View.GONE
                }else if(orderData?.buyer_billing_address?.gst_number != null){
                    binding.tvBillGSTNumber.visibility = View.VISIBLE
                    binding.tvBillGSTNumber.text = "GST No : " + orderData?.buyer_billing_address?.gst_number.toString()
                }else{
                    binding.tvBillGSTNumber.visibility = View.GONE
                }
            }else{
                binding.llBillAddressMain.visibility = View.GONE
            }
        }
    }




    private fun emptyView() {

        if (orderItemData?.size?:0 > 0) {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE
            binding.nScrollCart.visibility = View.VISIBLE

        } else {
            binding.nScrollCart.visibility = View.GONE
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE

        }
        stopAnim()
    }


    override fun onCancelOrdeItem(orderItemListData: OrderDetailItemDta?) {
        if (orderItemListData != null && orderId != null)
            orderItemListData._id?.let { orderDelete(it, orderId) }
    }

    override fun onReviewItem(orderItemListData: OrderDetailItemDta?) {
        var writeReviewDailog = WriteReviewDialog(orderItemListData?.vendor_id,null)
        writeReviewDailog.setListener(this)
        writeReviewDailog.show(supportFragmentManager, "writeReviewDailog")
    }

    override fun onAssignQtyItem(orderItemListData: OrderDetailItemDta?) {
        val intent =  Intent(this, OrderAssignQtyActivity::class.java)
        intent.putExtra("orderItemListData",orderItemListData)
        startActivityForResult(intent, 344)
        Bungee.slideLeft(this)
    }




    fun orderDelete(orderItemId: String, orderId: String?) {
        val fragment: BreakAlertDialog =
                BreakAlertDialog(
                        object : BreakAlertDialog.ClickListener {
                            override fun onDoneClicked() {
                                cancelMyOrderData(orderItemId, orderId)
                            }

                            override fun onCancelClicked() {

                            }
                        },
                        getString(R.string.alert_cancel_order_txt),
                        getString(R.string.txt_yes),
                        getString(R.string.txt_cancel)
                )

        fragment.show(supportFragmentManager, "alert")
        fragment.isCancelable = false

        true
    }


    private fun cancelMyOrderData(orderItemId: String, orderId: String?) {
        ApiClient.setToken()
        val pOId = PaymentTranstedIdObj(orderData?.gateway_transaction_id)
        orderViewModel.cancelOrderdta(pOId,orderItemId,orderId)
                .observe(this, Observer {

                    it?.let { resource ->
                        when (resource.status) {
                            Status.LOADING -> {
                                startAnim()
                            }
                            Status.SUCCESS -> {
                                stopAnim()
                                if (resource.data?.isSuccessful!!) {
                                    Utils.showToast(
                                            this,
                                            getString(R.string.cancel_order_sucess),
                                            Toast.LENGTH_LONG
                                    )
                                    resource.data?.let { response ->
                                        getOrderDetaiData(orderId)


                                    }
                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                            }
                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }

                    }
                })
    }

    override fun dialogDismiss() {

    }

    override fun dialogSave(review: VenderReviewData, vendorId:String?) {
        postVenderReview(review,vendorId)
    }


    private fun postVenderReview(review: VenderReviewData, vendorId:String?) {

        val venderWriteReviewObj = VenderWriteReviewObj(vendorId,review.review_text,review.rating,"unpublished")

        ApiClient.setToken()

        venderReviewViewModel.postVenderReviewData(venderWriteReviewObj)
                .observe(this, Observer {

                    it?.let { resource ->
                        when (resource.status) {
                            Status.LOADING -> {
                                startAnim()
                            }
                            Status.SUCCESS -> {
                                stopAnim()
                                if (resource.data?.isSuccessful!!) {
                                    Utils.showToast(
                                            this,
                                            getString(R.string.txt_review_added),
                                            Toast.LENGTH_LONG
                                    )
                                    resource.data?.let { response ->
                                        getOrderDetaiData(orderId)


                                    }
                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                            }
                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }

                    }
                })
    }

    /*fun showMenuPopup(v: View,orderItemListData: OrderDetailItemDta?) {
        val popup = PopupMenu(this, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.assign_order_menu, popup.menu)
        popup.show()

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {


                R.id.track_order_menu_btn -> {



                    true
                }
                R.id.assign_order_menu_btn -> {



                    true
                }

                else -> false
            }
        }
    }
*/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 344) {
            if (resultCode == Activity.RESULT_OK) {
                getOrderDetaiData(orderId)
            }
        }
    }

    override fun onAssignOrderTrack(orderItemListData: OrderDetailItemDta?) {
        val intent = Intent(this, TMCPAssignActivity::class.java)
        intent.putParcelableArrayListExtra("orderItemPartDataObj",orderItemListData?.orderItemPartData)
        intent.putExtra("displayId",orderData?.display_id)
        intent.putExtra("productid",orderItemListData?.display_item_id)
        startActivity(intent)
        Bungee.slideLeft(this)
    }

    override fun onRepeatOrderItem(dta: OrderDetailItemDta?) {

        if(dta?.design_mix != null){
            var intent =  Intent(this, ConcrateGradeListActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_GRADE_ID,dta?.concrete_grade?._id)
            intent.putExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_TITLE,dta?.concrete_grade?.name)
            intent.putExtra("qty",dta?.quantity)
            intent.putExtra("isfromReapeatOrder",true)
            startActivity(intent)
            Bungee.slideLeft(this)

        }else{
           //Custom Mix Order
            val intent = Intent(this, CustomDesignMix1Activity::class.java)
            intent.putExtra("orderitem",dta)
            intent.putExtra("isfromReapeatOrder",true)
            startActivity(intent)
            Bungee.slideLeft(this)

        }
    }

    override fun onTrackOrderItem(orderItemListData: OrderDetailItemDta?) {
        val intent = Intent(this, TrackActivity::class.java)
        intent.putExtra(
            CoreConstants.Intent.INTENT_ORDER_ID,
            orderItemListData?.order_id
        )
        intent.putExtra(
            CoreConstants.Intent.INTENT_ORDER_DIAPLAY_ID,
            orderItemListData?.display_item_id
        )
        intent.putExtra(
            CoreConstants.Intent.INTENT_ORDER_ITEM_ID,
            orderItemListData?._id
        )
        intent.putExtra(CoreConstants.Intent.INTENT_CREATED_DATE, orderData?.created_at)
        startActivity(intent)
        Bungee.slideLeft(this)
    }


}