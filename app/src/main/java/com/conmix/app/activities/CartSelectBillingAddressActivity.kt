package com.conmix.app.activities


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.adapter.SelectBillingAdapter
import com.conmix.app.adapter.SelectSiteAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.BillingAddressActivityBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.AddNewSiteAddViewModel
import com.conmix.app.viewmodels.SelectSiteAddProfViewModel

import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 28,September,2021
 */
class CartSelectBillingAddressActivity : BaseActivity(),
    SelectBillingAdapter.SelectSiteAddressInterface {
    private lateinit var binding: BillingAddressActivityBinding

    var siteAddressList = ArrayList<BillAddressObjLst>()
    var beforeTime: String? = null
    var isLoading = true
    var siteAdapter: SelectBillingAdapter? = null
    private var billId: String? = null
    private lateinit var selectSiteAddProfViewModel: SelectSiteAddProfViewModel
    private lateinit var addNewSiteViewModel: AddNewSiteAddViewModel
    var cartStateId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BillingAddressActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)

        billId = intent.getStringExtra("billId")
        cartStateId = intent.getStringExtra("cartStateId")


        setUi()
        setupViewModel()
        getBillSiteAddressData()

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.btnAddNewSite.setOnClickListener {
            val intent = Intent(this, AddNewBillingAddressActivity::class.java)
            startActivityForResult(intent, 240)
            Bungee.slideLeft(this)
        }

        binding.btnProceedPay.setOnClickListener {


            val event: BillAddressObjLst? = siteAddressList.find { it._id == billId }

            if (event != null) {
                billId = event._id


                PostBillId(billId)
            }

        }

    }

    private fun setupViewModel() {
        selectSiteAddProfViewModel =
            ViewModelProvider(
                this,
                SelectSiteAddProfViewModel.ViewModelFactory(ApiClient().apiService)
            ).get(SelectSiteAddProfViewModel::class.java)
        addNewSiteViewModel = ViewModelProvider(
            this, AddNewSiteAddViewModel.ViewModelFactory(
                ApiClient().apiService
            )
        )
            .get(AddNewSiteAddViewModel::class.java)

    }

    private fun setUi() {

        binding.tvCategoryTitle.setText(R.string.billing_add_txt)
        if (billId != null) {
            binding.btnProceedPay.isEnabled = true
            binding.btnProceedPay.alpha = 1f
        } else {
            binding.btnProceedPay.isEnabled = false
            binding.btnProceedPay.alpha = 0.5f
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {
                siteAdapter = null
                getBillSiteAddressData()
            }
        }
    }


    private fun getBillSiteAddressData() {
        ApiClient.setToken()

        selectSiteAddProfViewModel.getBillAdressdata()
            .observe(this, Observer {

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {
                            startAnim()

                        }
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    setResposerData(response.body())
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    override fun onBackPressed() {


        if (!billId.isNullOrEmpty()) {
            finish()
            Bungee.slideRight(this)
        }
    }

    private fun startAnim() {
        if (binding.avLoadingCategory != null)
            binding.avLoadingCategory.show()

    }

    private fun stopAnim() {

        if (binding.avLoadingCategory != null)
            binding.avLoadingCategory.hide()

    }

    override fun onResume() {
        super.onResume()

        siteAdapter = SelectBillingAdapter(this, siteAddressList)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvSiteAddListView?.layoutManager = mLayoutManager
        siteAdapter?.setListener(this)
        binding.rvSiteAddListView?.adapter = siteAdapter
    }


    private fun setResposerData(response: BillAddressListData?) {

        for (item in response?.data!!) {

            item.isSelected = item._id.equals(billId)

        }

        try {


            if (siteAdapter == null && response.data != null) {
                beforeTime = null

                siteAddressList.clear()
                siteAddressList.addAll(response.data)
                binding.rvSiteAddListView?.adapter = siteAdapter
                siteAdapter?.notifyDataSetChanged()


            } else if (siteAddressList != null && response?.data != null) {

                if (beforeTime != null) {

                    siteAddressList.addAll(response?.data)

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    //progressBarPost?.visibility = View.GONE

                } else if (beforeTime == null) {

                    response.data.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(0, it)
                        } else {
                            siteAddressList.add(0, it)
                        }
                    }

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    // progressBarPost?.visibility = View.GONE


                } else {


                    siteAddressList.clear()
                    response.data?.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(it)
                        } else {
                            siteAddressList.add(it)
                        }

                    }
                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }

                }

            }


            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        } catch (e: Exception) {
            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        }
    }

    /*private fun emptyView() {

        if (siteAddressList.isNotEmpty()) {
            bindingNoSite.constrainEmptySite.visibility = View.GONE
            bindingNoSite.imgNoList.visibility = View.GONE
            binding.rvSiteAddListView.visibility = View.VISIBLE
            siteAdapter?.loadDone()

        } else {
            binding.rvSiteAddListView.visibility = View.GONE
            bindingNoSite.constrainEmptySite.visibility = View.VISIBLE
            bindingNoSite.imgNoList.visibility = View.INVISIBLE
            bindingNoSite.tvNoData.text = "No Delivery Address available"
            siteAdapter?.loadDone()

        }
    }*/

    private fun emptyView() {

        if (siteAddressList.isNotEmpty()) {
            binding.tvNoData.visibility = View.GONE
            binding.rvSiteAddListView.visibility = View.VISIBLE
            siteAdapter?.loadDone()

        } else {
            binding.rvSiteAddListView.visibility = View.GONE
            binding.tvNoData.visibility = View.VISIBLE
            binding.tvNoData.text = "No Billing Address available"
            binding.btnProceedPay.isEnabled = false
            binding.btnProceedPay.alpha = 0.5f
            siteAdapter?.loadDone()

        }
    }

    override fun onAddSelected(site: BillAddressObjLst) {

/*   if(site != null){
            binding.btnProceedPay.isEnabled = true
            binding.btnProceedPay.alpha = 1f
        }

        billId = site._id
        for (item in siteAddressList) {
            if (item._id.equals(billId)) {
                item.isSelected = true
            } else {
                item.isSelected = false
            }
        }
        siteAdapter?.notifyDataSetChanged()*/

        if (site != null) {
            binding.btnProceedPay.isEnabled = true
            binding.btnProceedPay.alpha = 1f
        }

        if (cartStateId != null) {

            if (cartStateId.equals(site.state_id)) {
                binding.btnProceedPay.isEnabled = true
                binding.btnProceedPay.alpha = 1f

                billId = site._id

                for (item in siteAddressList) {

                    if (item._id.equals(billId)) {
                        item.isSelected = true

                    } else {
                        item.isSelected = false
                    }
                }

                siteAdapter?.notifyDataSetChanged()

            } else {
                billId = site._id
                loop@ for (item in siteAddressList) {

                    if (item._id.equals(billId)) {

                        if(siteAddressList.size > 1){
                            item.isSelected = false
                            break@loop
                        }else{
                            item.isSelected = true
                            break@loop
                        }


                    } else {
                        if (item.isSelected == true)
                            item.isSelected = true
                        else
                            item.isSelected = false
                    }
                }

                siteAdapter?.notifyDataSetChanged()

                val fragment =
                    BreakAlertDialog(
                        object : BreakAlertDialog.ClickListener {
                            override fun onDoneClicked() {
                                PostBillId(billId)

                            }

                            override fun onCancelClicked() {


                            }
                        },
                        getString(R.string.txt_billingnsite_diffrent),
                        getString(R.string.txt_yes),
                        getString(R.string.txt_cancel)
                    )

                fragment.show(supportFragmentManager, "alert")
                fragment.isCancelable = false

            }


        } else {
            billId = site._id

            for (item in siteAddressList) {

                if (item._id.equals(billId)) {
                    item.isSelected = true

                } else {
                    item.isSelected = false
                }
            }

            siteAdapter?.notifyDataSetChanged()
        }


    }


    private fun PostBillId(billId: String?) {

        val cartbillAddressIdObj = AddBillAddressIdCart(billId)

        ApiClient.setToken()

        addNewSiteViewModel.addBillAddressCart(cartbillAddressIdObj)
            .observe(this, androidx.lifecycle.Observer {

                it?.let { resource ->
                    when (resource.status) {

                        Status.SUCCESS -> {


                            if (resource.data?.isSuccessful!!) {

                                //redirecToMainActivity()

                                val intent = Intent()
                                setResult(RESULT_OK, intent)
                                finish()
                                Bungee.slideRight(this)

                            } else {

                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {

                        }
                        Status.ERROR -> {
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                    }
                }
            })
    }
}