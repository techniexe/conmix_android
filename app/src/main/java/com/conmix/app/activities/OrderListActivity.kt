package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.OrderListAdapter
import com.conmix.app.data.OrderLstObj
import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.databinding.OrderListActivityBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.OrderViewModel
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class OrderListActivity : BaseActivity(), OrderListAdapter.SelectMyOrderListInterface {
    override fun onClicked(orderId: String?) {
        val intent = Intent(this, OrderDetailActivity::class.java)
        intent.putExtra("orderId", orderId)
        startActivity(intent)
        Bungee.slideLeft(this)
    }

    private lateinit var binding: OrderListActivityBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding
    private lateinit var orderViewModel: OrderViewModel

    var myOrderList = ArrayList<OrderLstObj>()
    var before: String? = null
    var after: String? = null
    var user_id: String? = null
    var isLoading = true
    var orderListAdapter: OrderListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderListActivityBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)
        setupViewModel()
        setAdapter()

        /*binding.orderFilterMenu?.setOnClickListener {
            showMenuPopup(binding.orderFilterMenu)
        }*/

        binding.rvOderList?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    var visibleItemCount = binding.rvOderList?.layoutManager?.getChildCount()
                    var totalItemCount = binding.rvOderList?.layoutManager?.getItemCount()
                    var pastVisiblesItems =
                        (binding.rvOderList?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()



                    if (isLoading) {


                        if (visibleItemCount != null) {
                            if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {


                                isLoading = false
                                if (orderListAdapter != null) {
                                    //mAdapter!!.loading()

                                    orderListAdapter!!.loading()

                                    // afterTime = null
                                    val handler = Handler()
                                    handler.postDelayed(Runnable {
                                        isLoading = true
                                        try {
                                            before = myOrderList.get(myOrderList.size - 1).created_at
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }

                                        getMyOrderListData(user_id, before, null, null, null, null)


                                    }, 2000)
                                }
                            }
                        }
                    }
                }

            }
        })

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }


    }

    private fun setAdapter() {

        orderListAdapter = OrderListAdapter(this, myOrderList)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvOderList?.setLayoutManager(mLayoutManager)
        orderListAdapter?.setListener(this)
        binding.rvOderList?.setAdapter(orderListAdapter)
        getMyOrderListData(user_id, before, null, null, null, null)
    }

    override fun onBackPressed() {
        binding.imgBack.performClick()
    }

    private fun setupViewModel() {


        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }

    fun showMenuPopup(v: View) {
        val popup = PopupMenu(this, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.order_menu_filter, popup.menu)
        popup.show()

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit_profile_menu -> {



                    true
                }
                R.id.change_Password_menu -> {

                    true
                }
                R.id.logOut_menu ->{



                    true
                }
                else -> false
            }
        }
    }

    private fun getMyOrderListData(
        user_id: String?,
        before: String?,
        after: String?,
        order_status: String?,
        payment_status: String?,
        gateway_transaction_id: String?
    ) {
        ApiClient.setToken()

        orderViewModel.getOrderlstData(
            user_id,
            before,
            after,
            order_status,
            payment_status,
            gateway_transaction_id
        )
            .observe(this, Observer {

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {
                             startAnim()

                        }
                        Status.SUCCESS -> {
                            // stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->
                                    setResposerData(response.body()?.data)
                                }
                            } else {
                                stopAnim()
                                emptyView()
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                             stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }


    private fun emptyView() {

        if (myOrderList.isNotEmpty()) {
            //binding.orderFilterMenu.visibility = View.VISIBLE
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE
            binding.rvOderList.visibility = View.VISIBLE
            orderListAdapter?.loadDone()

        } else {
            //binding.orderFilterMenu.visibility = View.GONE
            binding.rvOderList.visibility = View.GONE
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE
            orderListAdapter?.loadDone()

        }
    }


    private fun setResposerData(response: ArrayList<OrderLstObj>?) {

        try {


            if (orderListAdapter == null && response != null) {
                before = null
                after = null
                myOrderList?.clear()
                myOrderList?.addAll(response)
                binding.rvOderList?.setAdapter(orderListAdapter)
                orderListAdapter?.notifyDataSetChanged()
                // recyclerView?.smoothSnapToPosition(0)
                // mAdapter?.loadDone()
                // progressBar?.visibility = View.GONE


            } else if (myOrderList != null && response != null) {

                if (before != null && after == null) {

                    myOrderList?.addAll(response)

                    try {
                        orderListAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    //progressBarPost?.visibility = View.GONE

                } else if (before == null && after != null) {

                    response?.forEach {
                        if (myOrderList.contains(it)) {
                            val index = myOrderList.indexOf(it)
                            myOrderList.removeAt(index)
                            myOrderList.add(0, it)
                        } else {
                            myOrderList.add(0, it)
                        }
                    }

                    try {
                        orderListAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    // progressBarPost?.visibility = View.GONE


                } else {


                    myOrderList?.clear()
                    response?.forEach {
                        if (myOrderList.contains(it)) {
                            val index = myOrderList.indexOf(it)
                            myOrderList.removeAt(index)
                            myOrderList.add(it)
                        } else {
                            myOrderList.add(it)
                        }

                    }
                    try {
                        orderListAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }

                }

            }

            // progressBarPost?.visibility = View.GONE
            isLoading = true

            orderListAdapter?.loadDone()
            stopAnim()
            emptyView()

        } catch (e: Exception) {
            isLoading = true

            orderListAdapter?.loadDone()
            stopAnim()
            emptyView()
            // progressBarPost?.visibility = View.GONE
        }

    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()

    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()
    }

}