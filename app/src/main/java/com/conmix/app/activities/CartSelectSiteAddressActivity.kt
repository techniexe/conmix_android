package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.SelectSiteAdapter
import com.conmix.app.data.CartAddressIdObj
import com.conmix.app.data.SiteAddressListData
import com.conmix.app.data.SiteAddressObjLst
import com.conmix.app.databinding.ActivitySelectSiteAddLoginBinding
import com.conmix.app.databinding.NoSiteBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel
import com.conmix.app.viewmodels.SelectSiteAddressViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.lang.NumberFormatException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 17,September,2021
 */
class CartSelectSiteAddressActivity: BaseActivity(),
    SelectSiteAdapter.SelectSiteAddressInterface {
    private lateinit var binding: ActivitySelectSiteAddLoginBinding
    private lateinit var bindingNoSite: NoSiteBinding

    private lateinit var selectSiteAddressViewModel: SelectSiteAddressViewModel
    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel

    var siteAddressList = ArrayList<SiteAddressObjLst>()
    var beforeTime: String? = null
    private var afterTime: String? = null
    var search: String? = null
    var isLoading = true
    private var isBackButton = false

    //private var cartId: String = ""
    var siteAdapter: SelectSiteAdapter? = null
    private var siteId: String? = null
    private var selectedDate: String? = null
    var minDate:Long = 0
    var maxDate :Long = 0
    var isQty:Boolean = false
    private var selectedEndDate:String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectSiteAddLoginBinding.inflate(layoutInflater)
        bindingNoSite = binding.noSiteLay
        setContentView(binding.root)
        //changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        siteId = intent.getStringExtra("siteId")
        /*if(siteId == null){
            siteId = SharedPrefrence.getSiteId(this)
        }*/

        setUi()
        setupViewModel()
        getSiteAddressData(null, null, null)

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.btnAddNewSite.setOnClickListener {
            val intent = Intent(this, AddNewSiteAddressActivity::class.java)
            startActivityForResult(intent, 240)
            Bungee.slideLeft(this)
        }

        binding.btnProceedPay.setOnClickListener {

            if (isBackButton) {
                // addSiteaddress()

                val event: SiteAddressObjLst? = siteAddressList.find { it._id == siteId }

                if(event != null) {
                    siteId = event._id
                    setPrefrence(
                        siteId,
                        event.state_id,
                        event.city_id,
                        event.location.coordinates?.get(1).toString(),
                        event.location.coordinates?.get(0).toString(),
                        event.site_name.toString(),
                        event.city_name.toString()
                    )
                }


                val intent = Intent()
                setResult(RESULT_OK, intent)
                finish()
                Bungee.slideRight(this)
            } else {

                if(isQty) {

                    val result  = Utils.comparenddateToStartday(selectedEndDate!!,selectedDate!!)
                    val ratio =  binding.qtytxt?.text.toString().toDouble() / 3.0
                    val rationdecimal = Math.ceil(ratio).toInt()

                    if(result > rationdecimal){
                        binding.dateInfoTxt.visibility = View.VISIBLE
                        Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)
                       // binding.dateInfoTxt.text = String.format(getString(R.string.date_range_info), rationdecimal)
                       /* Utils.showToast(this,String.format(getString(R.string.date_range_info), rationdecimal),
                            Toast.LENGTH_SHORT)*/
                        Utils.dateRangeErrorTost(this,rationdecimal,binding.dateInfoTxt)
                    }else {

                        SharedPrefrence.setSELECTEDQTY(this, binding.qtytxt.text.toString().toInt())
                        SharedPrefrence.setSelectedDate(
                            this@CartSelectSiteAddressActivity,
                            selectedDate
                        )
                        SharedPrefrence.setSelectedENDDate(
                            this@CartSelectSiteAddressActivity,
                            selectedEndDate
                        )
                        finish()
                        Bungee.slideRight(this)
                    }
                }else{
                    Utils.showToast(this,getString(R.string.qty_value_error), Toast.LENGTH_SHORT)
                }

            }
        }





        binding.rvSiteAddListView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = binding.rvSiteAddListView?.layoutManager?.childCount
                    val totalItemCount = binding.rvSiteAddListView?.layoutManager?.itemCount
                    val pastVisiblesItems =
                        (binding.rvSiteAddListView?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if (isLoading) {


                        if (visibleItemCount != null) {
                            if (visibleItemCount.plus(pastVisiblesItems) >= totalItemCount ?: 0) {


                                isLoading = false
                                if (siteAdapter != null) {
                                    //mAdapter!!.loading()

                                    siteAdapter!!.loading()

                                    // afterTime = null
                                    val handler = Handler()
                                    handler.postDelayed({


                                        isLoading = true

                                        try {
                                            beforeTime =
                                                siteAddressList[siteAddressList.size - 1].created_at
                                        } catch (e: Exception) {

                                        }

                                        getSiteAddressData(beforeTime, null, null)


                                    }, 2000)
                                }
                            }
                        }
                    }
                }

            }
        })


    }

    private fun setupViewModel() {
        selectSiteAddressViewModel =
            ViewModelProvider(
                this,
                SelectSiteAddressViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(SelectSiteAddressViewModel::class.java)
        concrateGradeListViewModel =
            ViewModelProvider(
                this,
                ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(ConcreateGradeListViewModel::class.java)
        customMixAddToCartViewModel = ViewModelProvider(
            this,
            CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(CustomMixAddToCartViewModel::class.java)

    }

    private fun setUi() {

        binding.tvCategoryTitle.setText(R.string.choose_site_add_txt)

        isBackButton = intent.getBooleanExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE, false)

        if (isBackButton) {
            binding.imgBack.visibility = View.VISIBLE
            binding.dateCalendarlnly.visibility = View.GONE
            binding.qtylnly.visibility = View.GONE
        }
        binding.btnProceedPay.isEnabled = !siteId.isNullOrEmpty()

    }

    override fun onBackPressed() {

        //var siteId = SharedPrefrence.getSiteId(this)
        if (isBackButton && !siteId.isNullOrEmpty()) {
            finish()
            Bungee.slideRight(this)
        }
    }

    private fun startAnim() {
        if (binding.avLoadingCategory != null)
            binding.avLoadingCategory.show()

    }

    private fun stopAnim() {

        if (binding.avLoadingCategory != null)
            binding.avLoadingCategory.hide()

    }

    override fun onResume() {
        super.onResume()
        /*Utils.setFirebaseAnalytics(
                this,
                getString(R.string.loginSelectSiteAddress),
                getString(R.string.loginSelectSiteAddress),
                ""
        )
        Utils.setFirebaseCrashlytics(
                this,
                getString(R.string.loginSelectSiteAddress),
                getString(R.string.method),
                ""
        )
        Utils.setFacebookAnalytics(this, getString(R.string.loginSelectSiteAddress))*/
        siteAdapter = SelectSiteAdapter(this, siteAddressList)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvSiteAddListView?.layoutManager = mLayoutManager
        siteAdapter?.setListener(this)
        binding.rvSiteAddListView?.adapter = siteAdapter
    }


    private fun getSiteAddressData(
        beforeTime: String?, afterTime: String?, search: String?
    ) {
        ApiClient.setToken()

        selectSiteAddressViewModel.getSiteAdressdata(beforeTime, afterTime, search)
            .observe(this, androidx.lifecycle.Observer {

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {
                            // startAnim()

                        }
                        Status.SUCCESS -> {
                            // stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    setResposerData(response = response.body())
                                }
                            } else {
                                emptyView()
                                Utils.setErrorData(this, resource.data.errorBody())
                            }


                        }
                        Status.ERROR -> {
                            // stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    private fun setResposerData(response: SiteAddressListData?) {

        for (item in response?.data!!) {

            item.isSelected = item._id.equals(siteId)
            if(item._id.equals(siteId)){
                getConcrateGradeListData(null,item.location.coordinates?.get(0).toString(),item.location.coordinates?.get(1).toString(),null,null,null,null,null,null)
            }
        }

        try {


            if (siteAdapter == null && response.data != null) {
                beforeTime = null
                afterTime = null
                siteAddressList.clear()
                siteAddressList.addAll(response.data)
                binding.rvSiteAddListView?.adapter = siteAdapter
                siteAdapter?.notifyDataSetChanged()


            } else if (siteAddressList != null && response?.data != null) {

                if (beforeTime != null && afterTime == null) {

                    siteAddressList.addAll(response?.data)

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    //progressBarPost?.visibility = View.GONE

                } else if (beforeTime == null && afterTime != null) {

                    response.data.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(0, it)
                        } else {
                            siteAddressList.add(0, it)
                        }
                    }

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    // progressBarPost?.visibility = View.GONE


                } else {


                    siteAddressList.clear()
                    response.data?.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(it)
                        } else {
                            siteAddressList.add(it)
                        }

                    }
                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }

                }

            }


            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        } catch (e: Exception) {
            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        }
    }

    private fun emptyView() {

        if (siteAddressList.isNotEmpty()) {
            bindingNoSite.constrainEmptySite.visibility = View.GONE
            bindingNoSite.imgNoList.visibility = View.GONE
            binding.rvSiteAddListView.visibility = View.VISIBLE
            siteAdapter?.loadDone()

        } else {
            binding.rvSiteAddListView.visibility = View.GONE
            bindingNoSite.constrainEmptySite.visibility = View.VISIBLE
            bindingNoSite.imgNoList.visibility = View.INVISIBLE
            bindingNoSite.tvNoData.text = "No Delivery Address available"
            siteAdapter?.loadDone()

        }
    }

    /*override fun onAddSelected(
        siteAddressObjLst: SiteAddressObjLst
    ) {
        *//* Utils.setFacebookAnalytics(this, getString(R.string.home_AddSite))*//*
        getConcrateGradeListData(null,siteAddressObjLst.location.coordinates?.get(0).toString(),siteAddressObjLst.location.coordinates?.get(1).toString(),null,null,null,null,null,null)
        siteId = siteAddressObjLst._id
        setPrefrence(
            siteId,
            siteAddressObjLst.state_id,
            siteAddressObjLst.city_id,
            siteAddressObjLst.location.coordinates?.get(1).toString(),
            siteAddressObjLst.location.coordinates?.get(0).toString(),
            siteAddressObjLst.site_name.toString(),
            siteAddressObjLst.city_name.toString()
        )
        binding.btnProceedPay.isEnabled = true


        for (item in siteAddressList) {

            item.isSelected = item._id.equals(siteId)
        }

        siteAdapter?.notifyDataSetChanged()


    }*/

    override fun onAddSelected(
        siteAddressObjLst: SiteAddressObjLst
    ) {


        val savedStateId = SharedPrefrence.getStateId(this)
        val saveCityId = SharedPrefrence.getCityId(this)

        if (savedStateId.equals(siteAddressObjLst.state_id) && saveCityId.equals(siteAddressObjLst.city_id)) {
            binding.btnProceedPay.isEnabled = true

            if (siteId != siteAddressObjLst._id) {
                Utils.showToast(this, getString(R.string.txt_site_change), Toast.LENGTH_LONG)
            }
            getConcrateGradeListData(null,siteAddressObjLst.location.coordinates?.get(0).toString(),siteAddressObjLst.location.coordinates?.get(1).toString(),null,null,null,null,null,null)

            siteId = siteAddressObjLst._id

            /* siteId = siteAddressObjLst._id
            setPrefrence(
                siteId,
                siteAddressObjLst.state_id,
                siteAddressObjLst.city_id,
                siteAddressObjLst.location.coordinates?.get(1).toString(),
                siteAddressObjLst.location.coordinates?.get(0).toString(),
                siteAddressObjLst.site_name.toString(),
                siteAddressObjLst.city_name.toString()
            )*/
            for (item in siteAddressList) {

                if (item._id.equals(siteId)) {
                    item.isSelected = true

                } else {
                    item.isSelected = false
                }
            }

            siteAdapter?.notifyDataSetChanged()

        } else {


            for (item in siteAddressList) {

                if (item._id.equals(siteAddressObjLst._id)) {
                    item.isSelected = false

                } else {
                    if (item.isSelected == true)
                        item.isSelected = true
                    else
                        item.isSelected = false
                }
            }

            siteAdapter?.notifyDataSetChanged()

            val fragment =
                BreakAlertDialog(
                    object : BreakAlertDialog.ClickListener {
                        override fun onDoneClicked() {
                            siteId = siteAddressObjLst._id
                            setPrefrence(
                                siteId,
                                siteAddressObjLst.state_id,
                                siteAddressObjLst.city_id,
                                siteAddressObjLst.location.coordinates?.get(1).toString(),
                                siteAddressObjLst.location.coordinates?.get(0).toString(),
                                siteAddressObjLst.site_name.toString(),
                                siteAddressObjLst.city_name.toString()
                            )
                            PostSiteId(siteId)


                        }

                        override fun onCancelClicked() {


                        }
                    },
                    getString(R.string.txt_nsite_diffrent),
                    getString(R.string.txt_yes),
                    getString(R.string.txt_cancel)
                )

            fragment.show(supportFragmentManager, "alert")
            fragment.isCancelable = false

        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {
                beforeTime = null
                afterTime = null
                search = null
                siteAdapter = null
                getSiteAddressData(null, null, null)
            }
        }
    }

    private fun redirecToMainActivity() {
        val intent = Intent(this@CartSelectSiteAddressActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
        Bungee.slideRight(this@CartSelectSiteAddressActivity)
    }

    private fun setPrefrence(
        siteId: String?,
        stateId: String?,
        cityId: String?,
        lat: String?,
        long: String?,
        siteName: String?,
        cityname: String?
    ) {
        if (!siteId.isNullOrEmpty()) {
            SharedPrefrence.setSiteId(
                this,
                siteId
            )
        }
        if (!stateId.isNullOrEmpty()) {
            SharedPrefrence.setStateId(
                this,
                stateId
            )
        }
        if (!cityId.isNullOrEmpty()) {
            SharedPrefrence.setCityId(
                this,
                cityId
            )
        }
        if (!lat.isNullOrEmpty()) {
            SharedPrefrence.setlat(
                this,
                lat
            )
        }
        if (!long.isNullOrEmpty()) {
            SharedPrefrence.setlang(
                this,
                long
            )
        }
        if (!siteName.isNullOrEmpty() && !cityname.isNullOrEmpty()) {
            val strBuffer = StringBuffer()
            strBuffer.append((siteName))
            strBuffer.append(" ")
            strBuffer.append(",")
            strBuffer.append(" ")
            strBuffer.append((cityname))
            SharedPrefrence.setSiteName(
                this,
                strBuffer.toString()
            )
        }


    }

    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }

    private fun getConcrateGradeListData(
        site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?
    ) {
        ApiClient.setToken()

        concrateGradeListViewModel.getConcrateGradeList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,null,null)
            .observe(this, androidx.lifecycle.Observer{

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {


                        }
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 200) {
                                        if (response.body()?.data?.size == 0) {
                                            binding.tvErrorLoc.visibility = View.VISIBLE
                                        } else {
                                            binding.tvErrorLoc.visibility = View.GONE
                                        }
                                    }

                                }
                            } else {

                                Utils.setErrorData(this, resource.data.errorBody())
                            }


                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    private fun PostSiteId(siteId:String?) {

        val cartAddressIdObj = CartAddressIdObj(siteId)

        ApiClient.setToken()

        customMixAddToCartViewModel.cartAddressAdd(cartAddressIdObj).observe(this, androidx.lifecycle.Observer{

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {


                        if (resource.data?.isSuccessful!!) {

                            redirecToMainActivity()

                        } else {

                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                    }
                }
            }
        })
    }

}