package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.databinding.ForgotPasswordBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import com.conmix.app.viewmodels.ForgotPasswordViewModel
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 25,February,2021
 */
class ForgotPassWordActivity: BaseActivity() {

    private lateinit var binding: ForgotPasswordBinding
    private var mobNum = ""
    private lateinit var forgotPasswordViewModel: ForgotPasswordViewModel
    var isFromProductDetail: Boolean = false
    var isEmail = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fullScreen()
        stopAnim()
        getIntentdate()
        setupViewModel()
        setFilters()

        binding.backBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        binding.etMobileNo.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                forgotpasswordProcess(v)
            }
            false
        }
        binding.btnRegister.setOnClickListener {
            forgotpasswordProcess(binding.btnRegister)
        }

    }

    private fun getIntentdate() {
        isFromProductDetail = intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)

    }

    private fun setFilters() {
        binding.etMobileNo.filters = arrayOf(ignoreFirstWhiteSpace())
    }

    private fun forgotpasswordProcess(v: View) {
        Utils.hideKeyboard(v)
        val phoneUtil = PhoneNumberUtil.getInstance()
        mobNum = binding.etMobileNo.text.toString().trim()
        if (mobNum.isNotEmpty()) {
            try {
                val numberProto = phoneUtil.parse(mobNum, binding.ccpLG.selectedCountryNameCode)
               val  mobNum1 = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)
                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                                numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE || phoneUtil.isPossibleNumber(
                                numberProto
                        )
                ) {

                    forgotPassword(mobNum1.toString())
                } else {
                    stopAnim()
                    Utils.showToast(
                            this,
                            getString(R.string.err_invalid_mobile),
                            Toast.LENGTH_SHORT
                    )
                }
            } catch (e: NumberParseException) {
                stopAnim()


                if (mobNum.isNotEmpty()) {

                    val emval = Utils.isValidEmail(mobNum)
                    if (emval) {
                        isEmail = true
                        forgotPassword(mobNum)
                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_ragister_mobileemail_number),
                            Toast.LENGTH_SHORT)
                    }
                }
            }
        } else {
            stopAnim()
            Utils.showToast(
                    this,
                    getString(R.string.err_ragister_mobileemail_number),
                    Toast.LENGTH_SHORT
            )
        }
    }
    private fun forgotPassword(identifier: String) {
        ApiClient.setToken()

        forgotPasswordViewModel.forgotPassword(identifier,"sms").observe(this, Observer{
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {

                            val otpIntent = Intent(this@ForgotPassWordActivity,ForgotPassWordOTPActivity::class.java)
                            otpIntent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER, identifier)
                            otpIntent.putExtra("isEmail",isEmail)
                            startActivity(otpIntent)
                            finish()
                            Bungee.slideLeft(this)
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }
                }
            }
        })

    }

    private fun setupViewModel() {
        forgotPasswordViewModel = ViewModelProvider(this,ForgotPasswordViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(ForgotPasswordViewModel::class.java)
    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()
    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)
    }
}