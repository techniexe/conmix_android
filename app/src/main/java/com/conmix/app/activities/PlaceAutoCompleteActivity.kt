package com.conmix.app.activities

import `in`.madapps.placesautocomplete.PlaceAPI
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.data.Citydetails
import com.conmix.app.data.ProductCategoryWiseListPickupLocation
import com.conmix.app.databinding.DialogAutocompleteBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.AddNewSiteAddViewModel
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.sucho.placesautocomplete.adapter.PlacesAutoCompleteAdapter
import com.sucho.placesautocomplete.listener.OnPlacesDetailsListener
import com.sucho.placesautocomplete.model.Place
import com.sucho.placesautocomplete.model.PlaceDetails
import java.text.SimpleDateFormat
import java.util.*
import android.os.Parcel

import android.os.Parcelable
import android.os.Parcelable.Creator
import android.util.TypedValue
import com.conmix.app.activities.PlaceAutoCompleteActivity.RangeValidator
import gun0912.tedimagepicker.util.ToastUtil.context
import java.lang.NumberFormatException



import androidx.core.content.res.ResourcesCompat
import com.conmix.app.BuildConfig
import com.conmix.app.ConmixApp
import com.conmix.app.data.SessionRequest
import com.conmix.app.viewmodels.SessionViewModel


/**
 * Created by Hitesh Patel on 10,March,2021
 */
class PlaceAutoCompleteActivity: BaseActivity(), TextWatcher,PlacesAutoCompleteAdapter.LocationListner {
    private lateinit var binding: DialogAutocompleteBinding
    private lateinit var placesAutoCompleteAdapter: PlacesAutoCompleteAdapter
    lateinit var autoComplete: EditText
    lateinit var rvListPlace: RecyclerView
    lateinit var btnCancel: AppCompatButton
    private var selectedCityLatLng = ProductCategoryWiseListPickupLocation()
    private lateinit var addNewSiteViewModel: AddNewSiteAddViewModel
    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel
    private var selectedDate: String? = null
    private var selectedEndDate:String?= null
    private var placeDetailsNew: PlaceDetails? = null
    var fullAddress:String = ""
    var minDate:Long = 0
    var maxDate :Long = 0
    var isQty:Boolean = false
    private lateinit var sessionViewModel: SessionViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogAutocompleteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViewModel()
        var isLogging = SharedPrefrence.getLogin(this)
        if (!isLogging) {
            //registerFCMToken()
            getSessionToken()
        }else{
            // registerFCMToken()
        }

        val builder = MaterialDatePicker.Builder.dateRangePicker()
        builder.setTitleText("Select Date")
        val constraintBuilder = CalendarConstraints.Builder()
        val c = Calendar.getInstance()
        val ce = Calendar.getInstance()
        c.add(Calendar.DATE,1)
        ce.add(Calendar.DATE,32)

        val calendarStart = c
        val calendarEnd = ce

         minDate = calendarStart.timeInMillis
         maxDate = calendarEnd.timeInMillis


        val selectedDateAvl = SharedPrefrence.getSelectedDate(this)
        if(selectedDateAvl == null){
            val  calendar = Calendar.getInstance()
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val date = simpleDateFormat.format(calendar.time)
            selectedDate = Utils.getConvertDateWithoutTime(date)
            SharedPrefrence.setSelectedDate(this@PlaceAutoCompleteActivity,selectedDate)
            selectedEndDate = Utils.getConvertDateWithoutTime(date)
            SharedPrefrence.setSelectedENDDate(this@PlaceAutoCompleteActivity,selectedEndDate)
            binding.datetxt.setText("$selectedDate - $selectedEndDate")

        }else{
            selectedDate = SharedPrefrence.getSelectedDate(this)
            selectedEndDate = SharedPrefrence.getSelectedENDDate(this)
            binding.datetxt.setText("$selectedDate - $selectedEndDate")
        }

        val qtys = SharedPrefrence.getSELECTEDQTY(this)
        binding.qtytxt.setText(qtys.toString())
        isQty = true

        binding.qtytxt?.addTextChangedListener(object : TextWatcher {


            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int,before: Int, count: Int) {

                runOnUiThread {
                    if(s.length == 0 ){

                        binding.qtyErrorTxt?.visibility = View.VISIBLE
                        binding.qtyErrorTxt.text =  getString(R.string.qty_empty_erro)
                        binding.dateInfoTxt.visibility = View.GONE
                        isQty = false

                    }else{
                        if(s.length > 0){

                            try{
                                if(s.toString().toInt() >=3 && s.toString().toInt() <=100){
                                    isQty = true
                                    binding.qtyErrorTxt?.visibility = View.GONE
                                    val ratio = s.toString().toDouble() / 3.0
                                    val rationdecimal = Math.ceil(ratio).toInt()
                                    binding.dateInfoTxt.visibility = View.VISIBLE
                                   // binding.dateInfoTxt.text = String.format(getString(R.string.date_range_info), rationdecimal)
                                    Utils.dateRangeError(this@PlaceAutoCompleteActivity,rationdecimal,binding.dateInfoTxt)
                                    // binding.waterLtrValue.setText(s.toString())
                                    binding.dateInfoTxt.setTextColor(Color.parseColor("#19A42C"))
                                }else{
                                    isQty = false
                                    binding.qtyErrorTxt?.visibility = View.VISIBLE
                                    binding.qtyErrorTxt.text =  getString(R.string.qty_value_error)
                                    binding.dateInfoTxt.visibility = View.GONE
                                }
                            }catch (e:NumberFormatException){
                                isQty = false
                                binding.qtyErrorTxt?.visibility = View.VISIBLE
                                binding.qtyErrorTxt.text =  getString(R.string.qty_value_error)
                                binding.dateInfoTxt.visibility = View.GONE
                            }


                        }else{
                            isQty = false
                            binding.qtyErrorTxt?.visibility = View.VISIBLE
                            binding.qtyErrorTxt.text =  getString(R.string.qty_empty_erro)
                            binding.dateInfoTxt.visibility = View.GONE
                        }
                    }
                }
            }
        })



        val placesApi = PlaceAPI.Builder().apiKey("AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o").build(this)
        autoComplete = binding.searchView
        btnCancel = binding.btnCancel
        binding.calendarlnly.setOnClickListener {

            /*val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(this, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                  //  selectedDate = year.toString() + "-" + getNumString(month + 1) + "-" + getNumString(dayOfMonth)
                   // binding.datetxt.setText(selectedDate)
                    val   datelng = Utils.getDateTime(year, month, dayOfMonth)

                    selectedDate = Utils.getIsoDate(datelng)
                    binding.datetxt.setText(selectedDate)

                    //binding.datetxt.setText(Utils.getConvertDateWithoutTime(selectedDate!!))

                }

            }, newDate.get(Calendar.YEAR), newDate.get(Calendar.MONTH), newDate.get(Calendar.DAY_OF_MONTH))
            val c = Calendar.getInstance()
            datePicker.datePicker.minDate = c.timeInMillis
            datePicker.show()*/
            constraintBuilder.setStart(minDate)
            constraintBuilder.setEnd(maxDate)
            constraintBuilder.setValidator(RangeValidator(minDate, maxDate))
            builder.setSelection(androidx.core.util.Pair(Utils.getTimeInLong(selectedDate!!),Utils.getTimeInLong(selectedEndDate!!)))
            builder.setCalendarConstraints(constraintBuilder.build())

            val picker = builder.build()
            picker.show(this@PlaceAutoCompleteActivity.supportFragmentManager, "date_picker_tag")

            picker.addOnPositiveButtonClickListener {
                val formater = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val awal = formater.format(Date(it.first ?: 0))
                val akhit = formater.format(Date(it.second ?: 0))
                //Toast.makeText(this, "$awal - $akhit", Toast.LENGTH_SHORT).show()
                selectedDate = awal
                selectedEndDate = akhit
                binding.datetxt.setText("$awal - $akhit")

                if(isQty) {

                    val result = Utils.comparenddateToStartday(selectedEndDate!!, selectedDate!!)
                    val ratio = binding.qtytxt?.text.toString().toDouble() / 3.0
                    val rationdecimal = Math.ceil(ratio).toInt()

                    if (result > rationdecimal) {
                        binding.dateInfoTxt.visibility = View.VISIBLE
                        /*binding.dateInfoTxt.text =
                            String.format(getString(R.string.date_range_info_error), rationdecimal)*/
                        Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)
                        binding.dateInfoTxt.setTextColor(Color.parseColor("#B94A48"))
                    } else {
                        binding.dateInfoTxt.visibility = View.VISIBLE
                        /*binding.dateInfoTxt.text =
                            String.format(getString(R.string.date_range_info), rationdecimal)*/
                        Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)
                        binding.dateInfoTxt.setTextColor(Color.parseColor("#19A42C"))
                    }
                }else{
                    binding.dateInfoTxt.visibility = View.GONE
                }



            }

            picker.addOnNegativeButtonClickListener {
                picker.dismiss()
            }
        }

        autoComplete.addTextChangedListener(this)
        autoComplete.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.getRawX() >= autoComplete.getRight() - autoComplete.getTotalPaddingRight()) {
                    autoComplete.setText("")
                    return true

                }
                return false
            }
        })
        rvListPlace = binding.rvList
        showSearchBar(placesApi)
        binding.btnCancel?.setOnClickListener {
            var stateId = SharedPrefrence.getStateId(this)
            if (!stateId.isNullOrEmpty()) {
                finish()
                overridePendingTransition(R.anim.nothing, R.anim.bottom_down)
            } else {
                finishAffinity()
            }
        }

        binding.btnSave.setOnClickListener {


            if(isQty) {

                val result  = Utils.comparenddateToStartday(selectedEndDate!!,selectedDate!!)
                val ratio =  binding.qtytxt?.text.toString().toDouble() / 3.0
                val rationdecimal = Math.ceil(ratio).toInt()

                if(result > rationdecimal){
                    binding.dateInfoTxt.visibility = View.VISIBLE
                    /*binding.dateInfoTxt.text = String.format(getString(R.string.date_range_info), rationdecimal)
                    Utils.showToast(this,String.format(getString(R.string.date_range_info), rationdecimal),Toast.LENGTH_SHORT)*/

                    Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)
                    Utils.dateRangeErrorTost(this,rationdecimal,binding.dateInfoTxt)
                }else{
                    SharedPrefrence.setSELECTEDQTY(this,binding.qtytxt.text.toString().toInt())
                    SharedPrefrence.setSelectedDate(this@PlaceAutoCompleteActivity, selectedDate)
                    SharedPrefrence.setSelectedENDDate(this@PlaceAutoCompleteActivity, selectedEndDate)
                    if (placeDetailsNew != null) {
                        val address = placeDetailsNew!!.address
                        var city = ""
                        var state = ""
                        (0 until address.size).forEach { i ->
                            when {
                                address[i].type.contains("locality") -> city += address[i].shortName
                                address[i].type.contains("administrative_area_level_1") -> state += address[i].longName
                            }
                        }
                        addStateCity(placeDetailsNew!!.lat, placeDetailsNew!!.lng, city, state)
                    } else {
                        finish()
                        //Bungee.slideDown(this)
                        overridePendingTransition(R.anim.nothing, R.anim.bottom_down)
                    }
                }

            }else{
                Utils.showToast(this, getString(R.string.qty_value_error),Toast.LENGTH_SHORT)
            }

        }



        var stateName = SharedPrefrence.getStateName(this)
        var cityName = SharedPrefrence.getCityname(this)

        fullAddress = SharedPrefrence.getFullAdd(this)?:""

        if (!stateName.isNullOrEmpty() && !cityName.isNullOrEmpty()) {
            val strBuffer = StringBuffer()
            //strBuffer.append("Guest user - ")
            strBuffer.append((fullAddress))


            if (!strBuffer.isEmpty()) {
                binding.btnSave?.isEnabled = true
                binding.btnSave?.isClickable = true
                btnCancel.visibility = View.VISIBLE
                binding.dateCalendarlnly?.visibility = View.VISIBLE
                binding.donebtnlnly?.visibility = View.VISIBLE
                binding.qtylnly?.visibility = View.VISIBLE
                binding.locIcn?.visibility = View.VISIBLE
                binding.tvUserDesciption.setText(strBuffer.toString())
                binding.tvUserDesciption.setTextColor(Color.parseColor("#0E5377"))
                binding.tvUserDesciption.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
                binding.tvUserDesciption.typeface = ResourcesCompat.getFont(context!!, R.font.roboto_bold)
                //textView.setTypeface(typeface)

            }

        } else {
            binding.btnSave?.isEnabled = false
            binding.btnSave?.isClickable = false
            btnCancel.visibility = View.GONE

        }

    }

    internal class RangeValidator : CalendarConstraints.DateValidator {
        var minDate: Long
        var maxDate: Long

        constructor(minDate: Long, maxDate: Long) {
            this.minDate = minDate
            this.maxDate = maxDate
        }

        constructor(parcel: Parcel) {
            minDate = parcel.readLong()
            maxDate = parcel.readLong()
        }

        override fun isValid(date: Long): Boolean {
            return !(minDate >= date || maxDate <= date)
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeLong(minDate)
            dest.writeLong(maxDate)
        }

        companion object {
            @SuppressLint("ParcelCreator")
            val CREATOR: Creator<RangeValidator> = object : Creator<RangeValidator> {
                override fun createFromParcel(parcel: Parcel): RangeValidator? {
                    return RangeValidator(parcel)
                }

                override fun newArray(size: Int): Array<RangeValidator?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        binding.btnCancel.performClick()
    }

    private fun setUpViewModel() {

        sessionViewModel =
            ViewModelProvider(
                this,
                SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
            ).get(SessionViewModel::class.java)
        addNewSiteViewModel = ViewModelProvider(
                this, AddNewSiteAddViewModel.ViewModelFactory(ApiClient().apiService))
                .get(AddNewSiteAddViewModel::class.java)
        concrateGradeListViewModel =
            ViewModelProvider(
                this,
                ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(ConcreateGradeListViewModel::class.java)
    }
    private fun showSearchBar(placesApi: PlaceAPI) {

        placesAutoCompleteAdapter = PlacesAutoCompleteAdapter(
                this,
                placesApi!!
        )
       placesAutoCompleteAdapter?.setListner(this)
       rvListPlace?.adapter = placesAutoCompleteAdapter


    }


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        isClearIconVisible(s)
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        isClearIconVisible(s)
    }

    override fun afterTextChanged(s: Editable) {
        isClearIconVisible(s)
        if (::placesAutoCompleteAdapter.isInitialized) {
            placesAutoCompleteAdapter.filter?.filter(s);
        }
    }

    private fun isClearIconVisible(s: CharSequence) {
        if (s.isNotEmpty()) {
            binding.qtylnly?.visibility = View.GONE
            binding.dateCalendarlnly?.visibility = View.GONE
            binding.donebtnlnly?.visibility = View.GONE


            autoComplete.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_search_24,
                    0,
                    R.drawable.ic_cancel_round,
                    0
            );
        } else {
            binding.dateCalendarlnly?.visibility = View.VISIBLE
            binding.donebtnlnly?.visibility = View.VISIBLE
            binding.qtylnly?.visibility = View.VISIBLE
            autoComplete.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search_24, 0, 0, 0);
        }
    }


    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }

    override fun dialogDismiss() {
       // TODO("Not yet implemented")
    }

    override fun dialogSave(pApi: PlaceAPI, place1: Place) {

       getPlaceDetails(pApi, place1)
    }

    private fun getPlaceDetails(placeApi: PlaceAPI, place: Place) {
        placeApi.fetchPlaceDetails(place.id, object :
                OnPlacesDetailsListener {
            override fun onError(errorMessage: String) {

            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onPlaceDetailsFetched(placeDetails: PlaceDetails) {
                setupUI(placeDetails,place)
            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupUI(placeDetails: PlaceDetails,place:Place) {
        val address = placeDetails.address



        var city = ""
        var state = ""
        var lat = 0.0
        var long = 0.0




        (0 until address.size).forEach { i ->
            when {
                address[i].type.contains("locality") -> city += address[i].shortName
                address[i].type.contains("administrative_area_level_1") -> state += address[i].longName

            }
        }



        runOnUiThread {
            autoComplete?.text.clear()
            binding.btnSave?.isEnabled = true
            binding.btnSave?.isClickable = true


            if (!placeDetails.lat.isNaN() && !placeDetails.lng.isNaN() && city.isNotEmpty() && state.isNotEmpty()) {

                val strBuffer = StringBuffer()
               // strBuffer.append("Guest user - ")
                strBuffer.append(place.description)
               /* strBuffer.append(",")
                strBuffer.append(" ")
                strBuffer.append((city))
                strBuffer.append(",")
                strBuffer.append(" ")
                strBuffer.append((state))qtytxt
*/

                fullAddress = place.description
                lat = placeDetails.lat
                long = placeDetails.lng

                getConcrateGradeListData(null,long.toString(),lat.toString(),null,null,null,null,null,null)

                val autocompleteTxt = StringBuffer()
                autocompleteTxt.append((city))
                autocompleteTxt.append(",")
                autocompleteTxt.append(" ")
                autocompleteTxt.append((state))


                if (!strBuffer.isEmpty()) {
                    /*_binding?.llLocation?.visibility = View.VISIBLE
                    _binding?.tvLocation?.text = strBuffer.toString()*/
                    binding.dateCalendarlnly?.visibility = View.VISIBLE
                    binding.donebtnlnly?.visibility = View.VISIBLE
                    binding.qtylnly?.visibility = View.VISIBLE
                    binding.locIcn?.visibility = View.VISIBLE
                    binding.tvUserDesciption.setText(strBuffer.toString())
                    binding.tvUserDesciption.setTextColor(Color.parseColor("#0E5377"))
                    binding.tvUserDesciption.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
                    binding.tvUserDesciption.typeface = ResourcesCompat.getFont(context!!, R.font.roboto_bold)
                    closeKeyboard()
                }
                placeDetailsNew = placeDetails
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun closeKeyboard() {
        // this will give us the view
        // which is currently focus
        // in this layout
        val view = this.currentFocus

        // if nothing is currently
        // focus then this will protect
        // the app from crash
        if (view != null) {

            // now assign the system
            // service to InputMethodManager
            val manager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun addStateCity(lat: Double, lng: Double, city: String, state: String) {

        val coordinates = ArrayList<Double>()
        coordinates.add(lng)
        coordinates.add(lat)

        var location = Citydetails()
        location.city_name = city
        location.state_name = state
        selectedCityLatLng.coordinates = coordinates
        location.location = selectedCityLatLng

        ApiClient.setToken()
        addNewSiteViewModel.addStateCity(location).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->

                                setPrefrence(response.body()!!.data, lat, lng)


                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }

                }

            }
        })

    }
    private fun setPrefrence(

        data: Citydetails,
        lat: Double,
        lng: Double

            ) {

        if (!data.state_id.isNullOrEmpty()) {
            SharedPrefrence.setStateId(
                    this,
                    data.state_id
            )
        }
        if (!data._id.isNullOrEmpty()) {
            SharedPrefrence.setCityId(
                    this,
                    data._id
            )
        }
        if (!lat.isNaN()) {
            SharedPrefrence.setlat(
                    this,
                    lat.toString()
            )
        }
        if (!lng.isNaN()) {
            SharedPrefrence.setlang(
                    this,
                    lng.toString()
            )
        }
        if (!data.city_name.isNullOrBlank()) {
            SharedPrefrence.setCityName(
                    this,
                    data.city_name!!
            )
        }
        if (!data.state_name.isNullOrBlank()) {
            SharedPrefrence.setStateName(
                    this,
                    data.state_name!!
            )
        }
        var cityName = SharedPrefrence.getCityname(this)
        var sateName = SharedPrefrence.getStateName(this)

        SharedPrefrence.setFullAdd(this,fullAddress?:"")

        finish()
        //Bungee.slideDown(this)
        overridePendingTransition(R.anim.nothing, R.anim.bottom_down)

    }

    private fun getConcrateGradeListData(
        site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?
    ) {
        ApiClient.setToken()



        concrateGradeListViewModel.getConcrateGradeList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,null,null)
            .observe(this, Observer{

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {


                        }
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 200) {
                                        if (response.body()?.data?.size == 0) {
                                            binding.tvErrorLoc.visibility = View.VISIBLE
                                        } else {
                                            binding.tvErrorLoc.visibility = View.GONE
                                        }
                                    }

                                }
                            } else {

                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    private fun getSessionToken() {
        ApiClient.setToken()
        val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

        sessionViewModel.getSession(sessionRequest)
            .observe(this, androidx.lifecycle.Observer { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        if (response.data?.isSuccessful!!) {
                            response.data?.let { response ->
                                if (response.body()?.data?.token != null) {
                                    SharedPrefrence.setSessionToken(
                                        this,
                                        response.body()?.data?.token
                                    )

                                }
                            }
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            })

    }
}