package com.conmix.app.activities

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.databinding.OrderAssignQtyActivityBinding
import com.conmix.app.utils.Utils
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList
import com.conmix.app.R
import com.conmix.app.adapter.SelectRegistrationAdapter
import com.conmix.app.data.*
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.viewmodels.OrderViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.NumberFormatException
import java.text.SimpleDateFormat


/**
 * Created by Hitesh Patel on 17,August,2021
 */
class OrderAssignQtyActivity : BaseActivity() {
    private lateinit var binding: OrderAssignQtyActivityBinding
    var orderItemListData: OrderDetailItemDta? = null

    var satrtTimeTypes = arrayOf(
        "Start Time", "00:00",
        "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00",
        "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00",
        "21:00", "22:00", "23:00"
    )


    var satrtTimeTypeArray: ArrayList<String> = ArrayList<String>()
    var accountTypeSelectTxt: String? = null
    var startDate: Long? = null
    var endDate: Long? = null
    var finalStartDate: String? = null
    var isQty: Boolean = false
    var isCheckTm: Boolean = false
    var rationdecimal = 3
    var finalStartDateTime: String? = null
    var finalEndDateTime: String? = null
    var partQty: Int = 0
    private lateinit var orderViewModel: OrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderAssignQtyActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupViewModel()

        orderItemListData = intent.getParcelableExtra("orderItemListData")

        partQty = orderItemListData?.part_quantity?.toString()?.toInt() ?: 0

        binding.delivertQtyTxt.setText(3.toString())
        isQty = true

        satrtTimeTypeArray.addAll(satrtTimeTypes)
        setAccountResposerData(satrtTimeTypeArray, binding.startTimeSpinner)

        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val Cdate = simpleDateFormat.format(calendar.time)
        val startDateN = Utils.comparetwoDates(orderItemListData?.delivery_date!!, Cdate)
        val endDateN = Utils.comparetwoDates(orderItemListData?.end_date!!, Cdate)

        if (Utils.comparedateTodayNew(orderItemListData?.delivery_date!!, Cdate)) {

           /* val newDeliverDate = Utils.getAddDaysafter(orderItemListData?.delivery_date!!)
            startDate = Utils.getDateTimeMilLi(newDeliverDate!!)
            endDate = Utils.getDateTimeMilLi(orderItemListData?.end_date!!)
            binding.chooseDeliveryDateTxt?.text = Utils.getDateShangeQtyAssign(newDeliverDate)
            finalStartDate =  Utils.getParsedSendServerFormate(binding.chooseDeliveryDateTxt?.text.toString())*/

           // val newDeliverDate = Utils.getAddDaysafter(orderItemListData?.delivery_date!!)
            startDate = Utils.getDateTimeMilLi(orderItemListData?.delivery_date!!)
            endDate = Utils.getDateTimeMilLi(orderItemListData?.end_date!!)
            binding.chooseDeliveryDateTxt?.text = Utils.getDateShangeQtyAssign(orderItemListData?.delivery_date!!)
            finalStartDate =  Utils.getParsedSendServerFormate(binding.chooseDeliveryDateTxt?.text.toString())


        } else if (startDateN < 0) {
            /*val newDeliverDate = Utils.getAddDaysafter(Cdate)
            startDate = Utils.getDateTimeMilLi(newDeliverDate!!)
            endDate = Utils.getDateTimeMilLi(orderItemListData?.end_date!!)
            binding.chooseDeliveryDateTxt?.text = Utils.getDateShangeQtyAssign(newDeliverDate)
            finalStartDate = Utils.getParsedSendServerFormate(binding.chooseDeliveryDateTxt?.text.toString())*/

          //  val newDeliverDate = Utils.getAddDaysafter(Cdate)
            startDate = Utils.getDateTimeMilLi(Cdate)
            if(endDateN < 0 ){
                endDate = Utils.getDateTimeMilLi(Cdate)
            }else{
                endDate = Utils.getDateTimeMilLi(orderItemListData?.end_date!!)
            }

            binding.chooseDeliveryDateTxt?.text = Utils.getDateShangeQtyAssign(Cdate)
            finalStartDate = Utils.getParsedSendServerFormate(binding.chooseDeliveryDateTxt?.text.toString())


        } else {
            startDate = Utils.getDateTimeMilLi(orderItemListData?.delivery_date!!)
            endDate = Utils.getDateTimeMilLi(orderItemListData?.end_date!!)
            binding.chooseDeliveryDateTxt?.text =  Utils.getDateShangeQtyAssign(orderItemListData?.delivery_date!!)
            finalStartDate = Utils.getParsedSendServerFormate(binding.chooseDeliveryDateTxt?.text.toString())
        }

        binding.chooseDeliveryDateTxt?.setOnClickListener {
            val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(
                this,
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(
                        view: DatePicker?,
                        year: Int,
                        month: Int,
                        dayOfMonth: Int
                    ) {
                        binding.startTimeSpinner.setSelection(0)
                        binding.checkTmErrorTxt?.visibility = View.GONE
                        binding.qtyTmErrorTxt?.visibility = View.GONE
                        val datelng = Utils.getDateTime(year, month, dayOfMonth)
                        val selectedDate = Utils.getIsoDate(datelng)
                        binding.chooseDeliveryDateTxt.setText(selectedDate)
                        finalStartDate = Utils.getParsedSendServerFormate(selectedDate)

                    }

                },
                newDate.get(Calendar.YEAR),
                newDate.get(Calendar.MONTH),
                newDate.get(Calendar.DAY_OF_MONTH)
            )
            val c = Calendar.getInstance()
            datePicker.datePicker.minDate = startDate as Long
            datePicker.datePicker.maxDate = endDate as Long
            datePicker.show()
        }

        binding.backOrderQtyAssign?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.submitBtn.setOnClickListener {
            if (validateFields()) {
                UpdateTmAssign()
            }
        }

        binding.delivertQtyTxt?.addTextChangedListener(object : TextWatcher {


            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                runOnUiThread {

                    binding.startTimeSpinner.setSelection(0)
                    binding.startTimeSpinner.isEnabled = false
                    binding.checkTmErrorTxt?.visibility = View.GONE
                    binding.qtyTmErrorTxt?.visibility = View.GONE
                    if (s.length == 0) {
                        binding.qtyErrorTxt?.visibility = View.VISIBLE
                        binding.qtyErrorTxt.text = getString(R.string.qty_empty_erro)
                        isQty = false

                    } else {
                        if (s.length > 0) {

                            try {
                                if (s.toString().toInt() >= 3 && s.toString().toInt() <= partQty) {

                                    val repqty = partQty - (s.toString().toInt())

                                    if (repqty in 1..2) {
                                        binding.startTimeSpinner.isEnabled = true
                                        isQty = true
                                        binding.qtyErrorTxt?.visibility = View.GONE
                                        val ratio = s.toString().toDouble() / 5.0
                                        rationdecimal = (Math.ceil(ratio).toInt()) * 3
                                        binding.qtyTmErrorTxt?.visibility = View.VISIBLE
                                        binding.qtyTmErrorTxt.text = String.format(
                                            getString(R.string.qty_value_err_grater),
                                            partQty
                                        )

                                    } else {

                                        binding.startTimeSpinner.isEnabled = true
                                        isQty = true
                                        binding.qtyErrorTxt?.visibility = View.GONE
                                        val ratio = (s.toString().toDouble() / 5.0) * 3
                                        rationdecimal = Math.ceil(ratio).toInt()
                                    }

                                } else {
                                    if (s.toString().toInt() < 3) {
                                        isQty = false
                                        binding.qtyErrorTxt?.visibility = View.VISIBLE
                                        binding.qtyErrorTxt.text =
                                            String.format(getString(R.string.qty_value_below_error_new))
                                    } else if (s.toString().toInt() > partQty) {
                                        isQty = false
                                        binding.qtyErrorTxt?.visibility = View.VISIBLE
                                        binding.qtyErrorTxt.text = String.format(
                                            getString(R.string.qty_value_up_error_new),
                                            partQty
                                        )
                                    }
                                }
                            } catch (e: NumberFormatException) {

                                isQty = false
                                binding.qtyErrorTxt?.visibility = View.VISIBLE
                                binding.qtyErrorTxt.text =
                                    String.format(getString(R.string.qty_value_error_new), partQty)

                            }


                        } else {
                            isQty = false
                            binding.qtyErrorTxt?.visibility = View.VISIBLE
                            binding.qtyErrorTxt.text =
                                String.format(getString(R.string.qty_value_error_new), partQty)

                        }
                    }
                }
            }
        })
    }

    private fun setupViewModel() {


        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)
    }


    private fun setAccountResposerData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectRegistrationAdapter =
            SelectRegistrationAdapter(view.context, data)
        binding.startTimeSpinner.adapter = spinnerAdapter

        binding.startTimeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

                if (parent.selectedItem.equals("Start Time")) {
                    isCheckTm = false
                    // nothing
                    accountTypeSelectTxt = null
                    finalStartDateTime = null
                    finalEndDateTime = null
                    binding.checkTmErrorTxt?.visibility = View.GONE
                } else {
                    accountTypeSelectTxt = parent.selectedItem as String
                    finalStartDateTime = finalStartDate + "T" + accountTypeSelectTxt + ":00.000Z"
                    finalEndDateTime = Utils.getAddHoursafter(finalStartDateTime!!, rationdecimal)
                    checkTMAvilable()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    fun checkTMAvilable() {

        val chckObj = checkTmAvlObj(
            orderItemListData?.vendor_id,
            finalStartDate + "T00:00:00.000Z",
            finalStartDateTime,
            finalEndDateTime
        )
        ApiClient.setToken()
        orderViewModel.checkTmAvalibity(chckObj).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                isCheckTm = false
                                try {

                                    if (response.code() == 400) {
                                        /*try{
                                            val gson = Gson()
                                            val type = object : TypeToken<ErrorRes>() {}.type
                                            //Please call errorBody().string() once only as the second try will return an empty string see: https://github.com/square/retrofit/issues/1321#issuecomment-251160231
                                            val err: ErrorRes? = gson.fromJson(response?.errorBody()?.string(), type)
                                            if (err != null) {
                                                if (err.error.message?.isNotEmpty() == true) {

                                                    Utils.showToast(
                                                        this@OrderAssignQtyActivity,
                                                        err.error.message,
                                                        Toast.LENGTH_SHORT
                                                    )

                                                } else {
                                                    Utils.showToast(
                                                        this@OrderAssignQtyActivity,
                                                        "Invalid Request",
                                                        Toast.LENGTH_SHORT
                                                    )
                                                }
                                            }
                                        }catch (e:Exception){
                                            e.printStackTrace()
                                        }*/

                                        binding.checkTmErrorTxt?.visibility = View.VISIBLE
                                        binding.checkTmErrorTxt.text =
                                            getString(R.string.checkTM_empty_erro)
                                        binding.checkTmErrorTxt.setTextColor(Color.parseColor("#B94A48"))

                                        val calendar = Calendar.getInstance()
                                        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                                        val Cdate = simpleDateFormat.format(calendar.time)
                                        val checkEndDate = Utils.comparetwoDates(orderItemListData?.end_date!!, Cdate)
                                        if(checkEndDate == 0){
                                            val oldEndDate = Utils.getAdd30Daysafter(orderItemListData?.end_date!!)

                                            endDate = oldEndDate?.let { it1 ->
                                                Utils.getDateTimeMilLi(
                                                    it1
                                                )
                                            }
                                        }

                                    } else if (response.code() == 200) {
                                        isCheckTm = true
                                        binding.checkTmErrorTxt?.visibility = View.VISIBLE
                                        binding.checkTmErrorTxt.text =
                                            getString(R.string.checkTM_success_erro)
                                        binding.checkTmErrorTxt.setTextColor(Color.parseColor("#19A42C"))
                                    }

                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        } else {
                            isCheckTm = false
                            if (resource.data.code() == 400) {

                                /*val res =resource.data.errorBody()?.string().toString()
                                 val gson = Gson()
                                 val type = object : TypeToken<ErrorRes>() {}.type
                                 val err = gson.fromJson<ErrorRes>(res, type)
                                 if (err?.error?.message != null && err.error.message.isNotEmpty()){
                                 // Utils.showToast(this, err.error.message, Toast.LENGTH_SHORT)
                                 val  messageError = err.error.message.toString()

                             }else {

                                 Utils.setErrorData(this, resource.data.errorBody())
                             }*/


                                binding.checkTmErrorTxt?.visibility = View.VISIBLE
                                binding.checkTmErrorTxt.text =
                                    getString(R.string.checkTM_empty_erro)
                                binding.checkTmErrorTxt.setTextColor(Color.parseColor("#B94A48"))

                                val calendar = Calendar.getInstance()
                                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                                val Cdate = simpleDateFormat.format(calendar.time)
                                val checkEndDate = Utils.comparetwoDates(orderItemListData?.end_date!!, Cdate)
                                if(checkEndDate == 0){
                                    val oldEndDate = Utils.getAdd30Daysafter(orderItemListData?.end_date!!)

                                    endDate = oldEndDate?.let { it1 ->
                                        Utils.getDateTimeMilLi(
                                            it1
                                        )
                                    }
                                }



                            }
                        }
                    }
                    Status.ERROR -> {
                        isCheckTm = false
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }

        })

    }


    fun UpdateTmAssign() {

        val chckObj = AssignTMOBJ(
            finalStartDate + "T00:00:00.000Z",
            binding.delivertQtyTxt?.text?.toString()?.toInt(),
            finalStartDateTime,
            finalEndDateTime
        )
        ApiClient.setToken()
        orderViewModel.UpdateAssignTm(orderItemListData?.order_id, orderItemListData?._id, chckObj)
            .observe(this, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            //  if (resource.data?.isSuccessful!!) {
                            // resource.data.let { response ->

                            try {

                                if (resource.data?.code() == 201) {

                                    Utils.showToast(
                                        this@OrderAssignQtyActivity,
                                        "RMC qty assigned successfully",
                                        Toast.LENGTH_SHORT
                                    )
                                    val intent = Intent()
                                    setResult(RESULT_OK, intent)
                                    finish()
                                    Bungee.slideRight(this)
                                } else if (resource.data?.code() == 400) {
                                    try {
                                        val gson = Gson()
                                        val type = object : TypeToken<ErrorRes>() {}.type
                                        //Please call errorBody().string() once only as the second try will return an empty string see: https://github.com/square/retrofit/issues/1321#issuecomment-251160231
                                        val err: ErrorRes? = gson.fromJson(
                                            resource.data?.errorBody()?.string(),
                                            type
                                        )
                                        if (err != null) {
                                            if (err.error.message?.isNotEmpty() == true) {

                                                Utils.showToast(
                                                    this@OrderAssignQtyActivity,
                                                    err.error.message,
                                                    Toast.LENGTH_SHORT
                                                )

                                            } else {
                                                Utils.showToast(
                                                    this@OrderAssignQtyActivity,
                                                    "Invalid Request",
                                                    Toast.LENGTH_SHORT
                                                )
                                            }
                                        }
                                    } catch (e: java.lang.Exception) {

                                    }


                                } else {
                                    Utils.showToast(this, "Invalid Request", Toast.LENGTH_LONG)
                                }


                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            //}
                            // }
                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {

                        }
                    }
                }

            })

    }

    private fun validateFields(): Boolean {
        if (binding.delivertQtyTxt.text.isNullOrEmpty()) {
            Utils.showToast(
                this,
                "Please enter assign quantity",
                Toast.LENGTH_SHORT
            )
            return false
        }
        if (!isQty) {
            Utils.showToast(
                this,
                String.format(getString(R.string.qty_value_error_new), partQty),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if (accountTypeSelectTxt == null) {
            Utils.showToast(
                this,
                "Please select delivery Start Time",
                Toast.LENGTH_SHORT
            )
            return false
        }


        if (!isCheckTm) {

            Utils.showToast(
                this,
                getString(R.string.checkTM_empty_erro),
                Toast.LENGTH_SHORT
            )
            return false
        }
        return true
    }
}