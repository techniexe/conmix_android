package com.conmix.app.activities

import android.os.Bundle
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat
import com.conmix.app.R
import com.conmix.app.databinding.AboutUsWebviewActivityBinding

import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class AboutUsWebView : BaseActivity(), View.OnClickListener{

    private lateinit var binding: AboutUsWebviewActivityBinding
    var url: String? = null
    var title: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AboutUsWebviewActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        getIntentData()
        binding.imgBack.setOnClickListener(this)
    }


    private fun getIntentData() {
        url = intent.getStringExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL)
        title = intent.getStringExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE)
        binding.tvCategoryTitle.text = title
        startAnim()
        startWebView(url)
    }

    private fun startWebView(url: String?) {

        binding.webView.webViewClient = object : WebViewClient() {

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                stopAnim()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                try {
                    stopAnim()
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        }
        // Javascript inabled on webview
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.builtInZoomControls = false
        binding.webView.settings.loadsImagesAutomatically = true
        binding.webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        binding.webView.loadUrl(url?:"")
    }

    override fun onBackPressed() {
        // Let the system handle the back button
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)
    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.visibility = View.VISIBLE
            binding.avLoading.show()
    }

    private fun stopAnim() {
        binding.avLoading.visibility = View.GONE
        binding.avLoading.hide()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }
}