package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.data.ChnagePasswordProfileObj
import com.conmix.app.data.LoginResponse
import com.conmix.app.data.UpdateUserPasswordProfileObj
import com.conmix.app.data.UserProfileObj
import com.conmix.app.databinding.ActivityChangePasswordBinding

import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import com.conmix.app.viewmodels.ChangePasswordViewModel
import com.conmix.app.viewmodels.SignUpViewModel
import com.conmix.app.viewmodels.UserProfileFrgViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 15,March,2021
 */
class ChangePasswordActivity: BaseActivity(), View.OnClickListener {


    private lateinit var changePasswrdViewModel: ChangePasswordViewModel
    private  var oldPassWordEdt: String? = null
    private var newPassWordEdt: String? = null
    private var confirmPasswordEdt: String? = null
    private lateinit var binding: ActivityChangePasswordBinding
    lateinit var userProfileFrgViewModel: UserProfileFrgViewModel
    var userProfileObj: UserProfileObj? = null
    private lateinit var signupViewModel: SignUpViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        setupViewModel()
        getUserPrfdataApiCall()
        setFilters()
        binding.imgBack.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.btnUpdate.setOnClickListener(this)
    }
    private fun setFilters() {
        binding.etOldPasswrd.filters =
            arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(20))
        binding.etNewPasswrd.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(20))
        binding.etConfirmPasswrd.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(20))
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)

    }



    private fun getUserPrfdataApiCall() {

        if (Utils.isNetworkAvailable(this)) {

            ApiClient.setToken()
            userProfileFrgViewModel.getUserPrfData().observe(this, Observer {

                it?.let { resource ->
                    when (resource.status) {

                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {


                                userProfileObj = resource.data.body()?.data
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
        }
    }

    private fun setData(response: LoginResponse?) {
        SharedPrefrence.setSessionToken(this, response?.customToken)
        binding.imgBack?.performClick()
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }

    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.INVISIBLE
        }

    }
    private fun setupViewModel(){
        changePasswrdViewModel =
                ViewModelProvider(this, ChangePasswordViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(ChangePasswordViewModel::class.java)

        userProfileFrgViewModel =
            ViewModelProvider(
                this,
                UserProfileFrgViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(UserProfileFrgViewModel::class.java)

        signupViewModel =
            ViewModelProvider(this, SignUpViewModel.ViewModelFactory(ApiClient().apiService))
                .get(SignUpViewModel::class.java)

    }




    private fun validUserData(): Boolean {
        oldPassWordEdt = binding.etOldPasswrd.text.toString().trim()
        newPassWordEdt = binding.etNewPasswrd.text.toString().trim()
        confirmPasswordEdt = binding.etConfirmPasswrd.text.toString().trim()

        if (oldPassWordEdt.isNullOrEmpty()) {

            Utils.showToast(this, getString(R.string.txt_change_OLD_password), Toast.LENGTH_SHORT)

            return false
        }
        if (oldPassWordEdt?.length ?: 0 < 6) {

            Utils.showToast(
                this,
                getString(R.string.err_password_length),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if (newPassWordEdt.isNullOrEmpty()) {

            Utils.showToast(this, getString(R.string.txt_changepaswword_old), Toast.LENGTH_SHORT)

            return false
        }

        if (newPassWordEdt?.length ?: 0 < 6) {

            Utils.showToast(
                    this,
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }


        if (confirmPasswordEdt.isNullOrEmpty()) {

            Utils.showToast(
                    this,
                    getString(R.string.txt_confirm_changepaswword),
                    Toast.LENGTH_SHORT
            )

            return false
        }

        if (confirmPasswordEdt?.length!! < 6) {

            Utils.showToast(
                    this,
                    getString(R.string.txt_confirm_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }




        if (!newPassWordEdt.equals(confirmPasswordEdt)) {
            Utils.showToast(
                    this,
                    getString(R.string.txt_txt_password_match),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.imgBack -> {
                finish()
                Bungee.slideRight(this)
            }
            R.id.btnCancel -> {
                binding.imgBack?.performClick()
            }
            R.id.btnUpdate -> {
                if (validUserData()) {
                   /* val obj = UpdateUserPasswordProfileObj(newPassWordEdt)
                    updateProfilefun(obj)*/

                   /* val otpIntent = Intent(this@ChangePasswordActivity, OTPActivity::class.java)
                    otpIntent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER, userProfileObj?.mobile_number)
                    otpIntent.putExtra(CoreConstants.Intent.INTENT_CHANGE_PASSWORD,true)
                    startActivityForResult(otpIntent,242)
                    Bungee.slideLeft(this)*/
                    getOtp()
                }
            }
        }
    }

    private fun getOtp() {

        ApiClient.setToken()

        changePasswrdViewModel.getUserOtp().observe(this, Observer{
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        when {
                            resource.data?.isSuccessful!! -> {

                                val otpIntent = Intent(this@ChangePasswordActivity, ChangePWOTPActivity::class.java)
                                otpIntent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER, userProfileObj?.mobile_number)
                                otpIntent.putExtra("oldPassword",oldPassWordEdt)
                                otpIntent.putExtra("newPassword",newPassWordEdt)
                                startActivity(otpIntent)
                                Bungee.slideLeft(this)
                                finish()



                            }
                            resource.data.code() == 400 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 422 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 500 -> {
                                Utils.showToast(
                                    this@ChangePasswordActivity,
                                    getString(R.string.err_internal_server),
                                    Toast.LENGTH_SHORT
                                )
                            }
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }

                }
            }
        })

    }



}