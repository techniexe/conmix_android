package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.SelectSiteProfileAdapter
import com.conmix.app.data.SiteAddressObjLst
import com.conmix.app.databinding.ActivitySelectSiteAddressProfileBinding
import com.conmix.app.databinding.NoSiteBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.SelectSiteAddProfViewModel
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class SelectSiteAddressProfileActivity : BaseActivity(), SelectSiteProfileAdapter.SelectSiteProfileAddressInterface, View.OnClickListener{

    private lateinit var selectSiteAddProfViewModel: SelectSiteAddProfViewModel
    var siteAddressList = ArrayList<SiteAddressObjLst>()
    var beforeTime: String? = null
    private var afterTime: String? = null
    var search: String? = null
    var isLoading = true
    var siteAdapter: SelectSiteProfileAdapter? = null
    private var siteAddressObjLst: SiteAddressObjLst? = null
    private lateinit var binding :ActivitySelectSiteAddressProfileBinding
    private lateinit var bindingNoSite: NoSiteBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectSiteAddressProfileBinding.inflate(layoutInflater)
        bindingNoSite = binding?.noSiteLayProfile
        setContentView(binding.root)

        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        binding.tvCategoryTitle.setText(R.string.select_site_add_txt)
        stopAnim()
        setUpViewModel()

        getSiteAddressData(null, null, null)
        binding.btnAddNewSite.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)


        binding.rvSiteAddListView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = binding.rvSiteAddListView?.layoutManager?.childCount
                    val totalItemCount = binding.rvSiteAddListView?.layoutManager?.itemCount
                    val pastVisiblesItems =
                        (binding.rvSiteAddListView?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()



                    if (isLoading) {


                        if (visibleItemCount != null) {
                            if (visibleItemCount.plus(pastVisiblesItems) >= totalItemCount ?: 0) {


                                isLoading = false
                                if (siteAdapter != null) {
                                    //mAdapter!!.loading()

                                    siteAdapter!!.loading()

                                    // afterTime = null
                                    val handler = Handler()
                                    handler.postDelayed({

                                        isLoading = true

                                        try {
                                            beforeTime =
                                                siteAddressList[siteAddressList.size - 1].created_at
                                        } catch (e: Exception) {

                                        }

                                        getSiteAddressData(beforeTime, null, null)

                                    }, 2000)
                                }
                            }
                        }
                    }
                }

            }
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)
    }

    private fun startAnim() {
        binding.avLoadingCategory.show()

    }

    private fun stopAnim() {
        binding.avLoadingCategory.hide()

    }


    private fun setUpViewModel() {
        selectSiteAddProfViewModel =
            ViewModelProvider(
                this,
                SelectSiteAddProfViewModel.ViewModelFactory(ApiClient().apiService)
            ).get(SelectSiteAddProfViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()

        siteAdapter = SelectSiteProfileAdapter(supportFragmentManager, this, siteAddressList)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvSiteAddListView?.layoutManager = mLayoutManager
        siteAdapter?.setListener(this)
        binding.rvSiteAddListView?.adapter = siteAdapter
    }

    private fun getSiteAddressData(
        beforeTime: String?, afterTime: String?, search: String?
    ) {
        ApiClient.setToken()

        selectSiteAddProfViewModel.getSiteAdressdata(beforeTime, afterTime, search)
            .observe(this, Observer {

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {
                            startAnim()

                        }
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    setResposerData(response.body()?.data)
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    private fun setResposerData(response: ArrayList<SiteAddressObjLst>?) {

        try {


            if (siteAdapter == null && response != null) {
                beforeTime = null
                afterTime = null
                siteAddressList.clear()
                siteAddressList.addAll(response)
                binding?.rvSiteAddListView?.adapter = siteAdapter
                siteAdapter?.notifyDataSetChanged()


            } else if (siteAddressList != null && response != null) {

                if (beforeTime != null && afterTime == null) {

                    siteAddressList.addAll(response)

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }


                } else if (beforeTime == null && afterTime != null) {

                    response.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(0, it)
                        } else {
                            siteAddressList.add(0, it)
                        }
                    }

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }


                } else {


                    siteAddressList.clear()
                    response.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(it)
                        } else {
                            siteAddressList.add(it)
                        }

                    }
                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }

                }

            }

            // progressBarPost?.visibility = View.GONE
            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        } catch (e: Exception) {
            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        }
    }

    private fun emptyView() {
        if (siteAddressList.isNotEmpty()) {
            bindingNoSite.constrainEmptySite.visibility = View.GONE
            stopAnim()
            siteAdapter?.loadDone()

        } else {
            bindingNoSite.constrainEmptySite.visibility = View.VISIBLE
            stopAnim()
            siteAdapter?.loadDone()

        }
    }


    private fun deleteSiteAddress(
        siteId: String?
    ) {
        ApiClient.setToken()

        selectSiteAddProfViewModel.deleteSiteAddress(siteId)
            .observe(this, Observer{

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {
                            // startAnim()

                        }
                        Status.SUCCESS -> {
                            // stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                Utils.showToast(
                                    this,
                                    getString(R.string.txt_delete_site),
                                    Toast.LENGTH_LONG
                                )

                                resource.data.let { response ->
                                    val cithObj = siteAddressList?.find { it._id == siteId }

                                    if (cithObj != null) {
                                        val index = siteAddressList?.indexOf(cithObj)

                                        siteAddressList?.removeAt(index)

                                        siteAdapter?.notifyDataSetChanged()
                                        checkDeliveryNameDeleteOrEdit(cithObj, false)
                                        emptyView()

                                    }

                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            // stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    private fun checkDeliveryNameDeleteOrEdit(
        siteAddressObjLst: SiteAddressObjLst?,
        isEdit: Boolean
    ) {
        val savedSiteId = SharedPrefrence.getSiteId(this)

        if (siteAddressObjLst != null && siteAddressObjLst._id == savedSiteId) {

            if (isEdit) {
                if (!siteAddressObjLst?.site_name.isNullOrEmpty() && !siteAddressObjLst?.city_name.isNullOrEmpty()) {
                    val strBuffer = StringBuffer()
                    strBuffer.append((siteAddressObjLst?.site_name))
                    strBuffer.append(" ")
                    strBuffer.append(",")
                    strBuffer.append(" ")
                    strBuffer.append((siteAddressObjLst?.city_name))
                    SharedPrefrence.setSiteName(
                        this,
                        strBuffer.toString()
                    )
                }

                if (!siteAddressObjLst.city_id.isNullOrEmpty()) {
                    SharedPrefrence.setCityId(
                        this,
                        siteAddressObjLst.city_id.toString()
                    )
                }


                if (!siteAddressObjLst.state_id.isNullOrEmpty()) {
                    SharedPrefrence.setStateId(
                        this,
                        siteAddressObjLst.state_id.toString()
                    )
                }

                if (!siteAddressObjLst.location.coordinates.isNullOrEmpty()) {
                    SharedPrefrence.setlang(
                        this,
                        siteAddressObjLst.location.coordinates?.get(0).toString()
                    )
                    SharedPrefrence.setlat(
                        this,
                        siteAddressObjLst.location.coordinates?.get(1).toString()
                    )
                }
                finish()
                Bungee.slideRight(this)

            } else {
                SharedPrefrence.setSiteId(this, null)
                SharedPrefrence.setSiteName(this, null)
                SharedPrefrence.setlang(this, null)
                SharedPrefrence.setlat(this, null)
                SharedPrefrence.setCityId(this, null)
                SharedPrefrence.setStateId(this, null)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {
                beforeTime = null
                afterTime = null
                search = null
                siteAdapter = null
                getSiteAddressData(null, null, null)
            }
        }else if(requestCode == 408){
            if (resultCode == Activity.RESULT_OK) {
                siteAddressObjLst = data?.getParcelableExtra("updateData")?:null
                if(siteAddressObjLst != null){
                    val id = siteAddressObjLst?._id

                    val cithObj = siteAddressList.find { it._id == id }

                    if(cithObj != null){
                        val index = siteAddressList.indexOf(cithObj)

                        siteAddressList[index].user_id = siteAddressObjLst?.user_id!!

                        siteAddressList[index]._id = siteAddressObjLst?._id!!
                        siteAddressList[index].company_name = siteAddressObjLst?.company_name
                        siteAddressList[index].site_name = siteAddressObjLst?.site_name
                        siteAddressList[index].address_line1 = siteAddressObjLst?.address_line1
                        siteAddressList[index].address_line2 = siteAddressObjLst?.address_line2
                        siteAddressList[index].country_id = siteAddressObjLst?.country_id
                        siteAddressList[index].state_id = siteAddressObjLst?.state_id
                        siteAddressList[index].city_id = siteAddressObjLst?.city_id
                       // siteAddressList[index].sub_city_id = siteAddressObjLst?.sub_city_id
                        siteAddressList[index].city_name = siteAddressObjLst?.city_name
                        siteAddressList[index].country_code = siteAddressObjLst?.country_code
                        siteAddressList[index].country_name = siteAddressObjLst?.country_name
                        siteAddressList[index].state_name = siteAddressObjLst?.state_name
                        siteAddressList[index].pincode = siteAddressObjLst?.pincode
                        siteAddressList[index].location = siteAddressObjLst?.location!!
                        siteAddressList[index].person_name = siteAddressObjLst?.person_name
                        siteAddressList[index].title = siteAddressObjLst?.title

                        siteAddressList[index].email =siteAddressObjLst?.email
                        siteAddressList[index].email_verified = siteAddressObjLst?.email_verified
                        siteAddressList[index].mobile_number = siteAddressObjLst?.mobile_number

                        siteAddressList[index].mobile_number_verified = siteAddressObjLst?.mobile_number_verified

                        siteAddressList[index].alt_mobile_number =siteAddressObjLst?.alt_mobile_number
                        siteAddressList[index].alt_mobile_number_verified = siteAddressObjLst?.alt_mobile_number_verified

                        siteAddressList[index].whatsapp_number = siteAddressObjLst?.whatsapp_number

                        siteAddressList[index].whatsapp_number_verified =
                            siteAddressObjLst?.whatsapp_number_verified

                        siteAddressList[index].landline_number = siteAddressObjLst?.landline_number

                        siteAddressList[index].profile_pic = siteAddressObjLst?.profile_pic

                        siteAddressList[index].created_at = siteAddressObjLst?.created_at
                        siteAddressList[index].isSelected = false

                        checkDeliveryNameDeleteOrEdit(siteAddressObjLst, true)

                        siteAdapter?.notifyDataSetChanged()

                    }
                }

            }
        }
    }

    override fun onDeleteAdd(siteAddressId: String) {


        val fragment = BreakAlertDialog(
            object : BreakAlertDialog.ClickListener {
                override fun onDoneClicked() {
                    deleteSiteAddress(siteAddressId)
                }

                override fun onCancelClicked() {


                }
            },
            getString(R.string.are_you_sure_you_want_to_cancel),
            getString(R.string.txt_yes),
            getString(R.string.txt_cancel)
        )

        fragment.show(supportFragmentManager, "alert")
        fragment.isCancelable = false


    }

    override fun onEditAdd(siteAddressobj: SiteAddressObjLst) {

        val intent = Intent(this, EditSiteAddressActivity::class.java)
        intent.putExtra("addressObj", siteAddressobj)
        startActivityForResult(intent, 408)
        Bungee.slideLeft(this)


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnAddNewSite -> {
                val intent = Intent(this, AddNewSiteAddressActivity::class.java)
                startActivityForResult(intent, 240)
                Bungee.slideLeft(this)
            }

            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }


}