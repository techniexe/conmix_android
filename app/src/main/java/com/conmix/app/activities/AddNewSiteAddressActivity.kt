package com.conmix.app.activities

import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.data.*
import com.conmix.app.databinding.ActivityAddNewSiteAddressesBinding

import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import com.conmix.app.viewmodels.AddNewSiteAddViewModel
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.sucho.placepicker.AddressData
import com.sucho.placepicker.Constants
import com.sucho.placepicker.MapType
import com.sucho.placepicker.PlacePicker
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class AddNewSiteAddressActivity: BaseActivity(), View.OnClickListener {

    private lateinit var binding: ActivityAddNewSiteAddressesBinding

    private lateinit var addNewSiteViewModel: AddNewSiteAddViewModel
    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel
    private var stateDataList = ArrayList<StateModel>()
    private var cityDataList = ArrayList<CityModel>()

    private lateinit var stateName: String
    private lateinit var cityName: String
    private lateinit var companyNameVal: String
    private lateinit var siteAddNameVal: String
    private lateinit var contactPersonNameVal: String
    private lateinit var mobileNoEdtVal: String
    private lateinit var siteadd1TxtEdtVal: String
    private lateinit var siteadd2TxtEdtVal: String
    private lateinit var pincodeVal: String
    private lateinit var countryEdtVal:String
    private lateinit var emailEdtVal: String
    var countryCodeVal: String = ""
    var selectedStateId: String = ""
    var selectedCityId: String = ""
    private var selectedCityLatLng = ProductCategoryWiseListPickupLocation()
    var lat: Double = 0.0
    var lang: Double = 0.0
    var address1: String = ""
    var address2: String = ""
    var inputString: String? = null
    private var timer: Timer? = null
    var messageError :String?= null
    var isSiteAvl = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddNewSiteAddressesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        setUI()
        setUpViewModel()

        binding.imgBack.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.etLatlong.setOnClickListener(this)


        binding.etSiteName?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

                inputString = if (p0.toString().isEmpty()) null else p0.toString()

                    timer = Timer()
                    timer?.schedule(object : TimerTask() {
                        override fun run() {
                            // do your actual work here
                            val  handler =  Handler(Looper.getMainLooper());
                            handler.post {
                                checkSiteNameAvilable(inputString)
                            }
                        }
                    }, 1000)


            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (timer != null) {
                    timer?.cancel();
                }
            }
        })

    }

    private fun setUI() {
        binding.tvCategoryTitle.text = getString(R.string.add_new_site_title_txt)

        // etCompanyName.filters = arrayOf(ignoreFirstWhiteSpace())
        //  etSiteName.filters = arrayOf(ignoreFirstWhiteSpace())
        binding.etContactPersonName.filters =
            arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(64))
        binding.etMobileNo.filters =
            arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(13))
        binding.etEmail.filters = arrayOf(ignoreFirstWhiteSpace())
        // etSiteadd1.filters = arrayOf(ignoreFirstWhiteSpace())
        // etSiteadd2.filters = arrayOf(ignoreFirstWhiteSpace())
        binding.etPinCode.filters =
            arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(6))
    }

    private fun setUpViewModel() {
        addNewSiteViewModel = ViewModelProvider(
            this, AddNewSiteAddViewModel.ViewModelFactory(
                ApiClient().apiService
            )
        )
            .get(AddNewSiteAddViewModel::class.java)
        concrateGradeListViewModel =
            ViewModelProvider(
                this,
                ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(ConcreateGradeListViewModel::class.java)
    }

    private fun validUserData(): Boolean {
        stateName = binding.etState.text.toString().trim()
        cityName = binding.etCity.text.toString().trim()

        companyNameVal = binding.etCompanyName?.text.toString().trim()
        siteAddNameVal = binding.etSiteName?.text.toString().trim()
        contactPersonNameVal = binding.etContactPersonName?.text.toString().trim()
        mobileNoEdtVal = binding.etMobileNo?.text.toString().trim()
        emailEdtVal = binding.etEmail?.text.toString().trim()
        siteadd1TxtEdtVal = binding.etSiteadd1?.text.toString().trim()
        siteadd2TxtEdtVal = binding.etSiteadd2?.text.toString().trim()
        countryEdtVal = binding.etContry?.text.toString().trim()

        pincodeVal = binding.etPinCode?.text.toString().trim()
        val phoneUtil = PhoneNumberUtil.getInstance()
        if (companyNameVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_company), Toast.LENGTH_SHORT)
            return false
        }

        if (siteAddNameVal.isEmpty()) {

            Utils.showToast(this, getString(R.string.error_empty_site), Toast.LENGTH_SHORT)

            return false
        }

        if (contactPersonNameVal.isEmpty()) {

            Utils.showToast(this, getString(R.string.error_empty_person_name), Toast.LENGTH_SHORT)

            return false
        }
        if (contactPersonNameVal.length < 2) {

            Utils.showToast(
                this,
                getString(R.string.txt_name_length),
                Toast.LENGTH_SHORT
            )
            return false
        }



        if (mobileNoEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_mobile), Toast.LENGTH_SHORT)
            return false
        }

        if (mobileNoEdtVal.isNotEmpty()) {
            try {

                val numberProto = phoneUtil.parse(mobileNoEdtVal, binding.ccp.selectedCountryNameCode)
                val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                        numberProto
                    ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                ) {

                } else {
                    Utils.showToast(
                        this,
                        getString(R.string.err_invalid_number),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            } catch (e: NumberParseException) {
                Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                return false

            }

        }

        if (emailEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT)
            return false
        }

        if (emailEdtVal.isNotEmpty()) {

            val emval = Utils.isValidEmail(emailEdtVal)
            if (emval) {

            } else {
                Utils.showToast(
                    this,
                    getString(R.string.error_empty_valid_email),
                    Toast.LENGTH_SHORT
                )
                return false
            }
        }
        /*  var coordinates = ArrayList<Double>()
              coordinates.add(70.70)
              coordinates.add(22.22)
              selectedCityLatLng.coordinates = coordinates*/
        if (selectedCityLatLng.coordinates == null) {
            Utils.showToast(this, getString(R.string.error_lat_long), Toast.LENGTH_SHORT)
            return false
        }
        if (siteadd1TxtEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_address1), Toast.LENGTH_SHORT)
            return false
        }

        if (siteadd2TxtEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_address2), Toast.LENGTH_SHORT)
            return false
        }

        if (cityName.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_city), Toast.LENGTH_SHORT)
            return false
        }
        if (stateName.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_state), Toast.LENGTH_SHORT)
            return false
        }

        if (countryEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_country), Toast.LENGTH_SHORT)
            return false
        }
        if (pincodeVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_pincode), Toast.LENGTH_SHORT)
            return false
        }
        if (pincodeVal.length < 6) {
            Utils.showToast(this, getString(R.string.error_pincode_valid), Toast.LENGTH_SHORT)
            return false
        }


        return true
    }

    private fun validStateData(): Boolean {
         validUserData()
        if (countryCodeVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_country), Toast.LENGTH_SHORT)
            return false
        }

        if (selectedStateId.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_state), Toast.LENGTH_SHORT)
            return false
        }

        if (selectedCityId.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_city), Toast.LENGTH_SHORT)
            return false
        }

        return true
    }



    private fun addStateCity() {

        var location = Citydetails()
        location.city_name = cityName
        location.state_name = stateName
        location.location = selectedCityLatLng

        ApiClient.setToken()
        addNewSiteViewModel.addStateCity(location).observe(this, Observer{
                response ->
            when(response.status){
                    Status.SUCCESS -> {
                        if (response.data?.isSuccessful!!) {
                            response.data.let { response ->
                                if (response.body() != null) {

                                    if (response.body()!!.data._id != null)
                                        selectedCityId = response.body()!!.data._id!!

                                    if (response.body()!!.data.state_id != null)
                                        selectedStateId = response.body()!!.data.state_id!!

                                    if (response.body()!!.data.country_id != null)
                                        countryCodeVal = response.body()!!.data.country_id!!

                                    if (validStateData()) {
                                        postAddData()
                                    }
                                }

                            }
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }



            }
        })

    }

    private fun postAddData() {

        val siteNewAddObj = AddSiteAddPostObj(
            companyNameVal,
            siteAddNameVal,
            siteadd1TxtEdtVal,
            siteadd2TxtEdtVal,
            countryCodeVal.toInt(),
            selectedStateId,
            selectedCityId,
            null,
            pincodeVal.toInt(),
            contactPersonNameVal,
            null,
            emailEdtVal,
            mobileNoEdtVal,
            null,
            null,
            null,
            null,
            selectedCityLatLng
        )

        val siteMainObj = AddSiteAddPostData(siteNewAddObj)

        ApiClient.setToken()
        addNewSiteViewModel.postAddNewAddress(siteMainObj).observe(this,Observer{
                response ->
            when(response.status){
                    Status.SUCCESS -> {
                        stopAnim()
                        response.data?.let { res ->
                            if (res.isSuccessful) {
                                Utils.showToast(
                                    this,
                                    getString(R.string.txt_site_added),
                                    Toast.LENGTH_LONG
                                )
                                val newSiteId = res.body()?.data?.id
                                val intent = Intent()
                                setResult(RESULT_OK, intent)
                                finish()
                                binding.imgBack?.performClick()
                            } else {
                                Utils.setErrorData(this, response.data.errorBody())
                            }
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }

            }
        })
    }

    private fun getCountryData() {
        ApiClient.setToken()
        addNewSiteViewModel.getCountryData().observe(this,Observer{
                response ->
            when(response.status){
                    Status.SUCCESS -> {
                        if (response.data?.isSuccessful!!) {
                            response.data.let { response ->
                                setCountryResponseData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }


            }
        })
    }


    private fun setCountryResponseData(data: ArrayList<CountryModel>?) {

        if (data != null) {
            countryCodeVal = data[0].country_id.toString()
            getStateDatafun(countryCodeVal)
        }
    }

    private fun getStateDatafun(countryId: String) {
        ApiClient.setToken()
        addNewSiteViewModel.getStateData(countryId).observe(this, Observer{
                response ->
            when(response.status){
                    Status.SUCCESS -> {
                        if (response.data?.isSuccessful!!) {
                            response.data.let { response ->
                                setStateResponseData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
            }
        })
    }

    private fun setStateResponseData(data: ArrayList<StateModel>?) {
        stateDataList.clear()
        if (data != null) {
            stateDataList.addAll(data)
        }

        val savedStateId = SharedPrefrence.getStateId(this)


        for ((index, value) in stateDataList.withIndex()) {
            if (value._id.equals(savedStateId)) {
                binding.etState.setText(value.state_name)
                value.isSelected = true
                selectedStateId = value._id.toString()
            }
        }
        getCityDatafun(selectedStateId)


    }

    private fun getCityDatafun(stateId: String) {
        ApiClient.setToken()
        addNewSiteViewModel.getCityData(stateId).observe(this,Observer{
                response ->
            when(response.status){
                    Status.SUCCESS -> {
                        if (response.data?.isSuccessful!!) {
                            response.data.let { response ->
                                setCityResponseData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }


            }
        })
    }

    private fun setCityResponseData(data: ArrayList<CityModel>?) {
        cityDataList.clear()
        if (data != null) {
            cityDataList = data
        }


        val savedCityId = SharedPrefrence.getCityId(this)


        for ((index, value) in cityDataList.withIndex()) {
            if (value._id.equals(savedCityId)) {
                binding.etCity.setText(value.city_name)
                value.isSelected = true
                selectedCityId = value._id.toString()
                break
            }
        }
        if (binding.etCity.text.isEmpty() && cityDataList.isNotEmpty()) {
            binding.etCity.setText(cityDataList.get(0).city_name)
            cityDataList.get(0).isSelected = true
            selectedCityId = cityDataList.get(0)._id.toString()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }

    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.INVISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.etLatlong -> {
                showPlacePicker()
            }
            /* R.id.etState -> {
                 val intent = Intent(this, StateActivity::class.java)
                 intent.putParcelableArrayListExtra(CoreConstants.Intent.INTENT_STATE, stateDataList)
                 startActivityForResult(intent, Constants.STATE_PICKET)
                 Bungee.slideUp(this)
             }
             R.id.etCity -> {
                 val intent = Intent(this, CityActivity::class.java)
                 intent.putParcelableArrayListExtra(CoreConstants.Intent.INTENT_CITY, cityDataList)
                 startActivityForResult(intent, Constants.CITY_PICKET)
                 Bungee.slideUp(this)
             }*/
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.btnSave -> {
                if (validUserData()) {

                    if(isSiteAvl){
                        addStateCity()
                    }else{
                        if(!messageError.isNullOrEmpty())
                        Utils.showToast(this, messageError?:"", Toast.LENGTH_SHORT)
                    }


                    // postAddData()

                }
            }
            R.id.btnCancel -> {
                binding.imgBack?.performClick()
            }
        }
    }

    private fun showPlacePicker() {

        val intent = PlacePicker.IntentBuilder()
            //.setLatLong(23.0225, 72.5714)
            .showLatLong(true)

            //.setMapRawResourceStyle(R.raw.map_style)
            .setMapType(MapType.NORMAL)
            .setLatLong(
                lat, lang
            )
            .setMapZoom(17f)
            .setPlaceSearchBar(true, getString(R.string.key_maps))
            .build(this)
        startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if ((requestCode == Constants.PLACE_PICKER_REQUEST) && (resultCode == RESULT_OK)) {

            val addressData = data?.getParcelableExtra<AddressData>(Constants.ADDRESS_INTENT)


            if (addressData != null
            ) {
                getConcrateGradeListData(null,addressData.addressList?.get(0)?.longitude.toString(),addressData.addressList?.get(0)?.latitude.toString(),null,null,null,null,null,null)

                addressData.addressList?.get(0)?.let { setValue(it) }

            }
        } else if ((requestCode == Constants.STATE_PICKET) && (resultCode == RESULT_OK)) {
            val stateData = data?.getParcelableExtra<StateModel>(Constants.STATE_INTENT)
            binding.etState.setText(stateData?.state_name.toString())
            selectedStateId = stateData?._id.toString()
            for (row in stateDataList) {
                row.isSelected = stateData?._id.equals(row._id)
            }
            binding.etCity.setText("")
            getCityDatafun(selectedStateId)
        } else if ((requestCode == Constants.CITY_PICKET) && (resultCode == RESULT_OK)) {
            val cityModel = data?.getParcelableExtra<CityModel>(Constants.CITY_INTENT)
            binding.etCity.setText(cityModel?.city_name.toString())
            selectedCityId = cityModel?._id.toString()
            for (row in cityDataList) {
                row.isSelected = cityModel?._id.equals(row._id)
            }
        }
    }

    private fun generateFinalAddress(
        address: String
    ) {
        val s = address.split(",")
        address1 = ""
        address2 = ""

        var subString = s.subList(0, s.size - 3)
        if (subString.size == 1) {
            address1 = subString[0]
            address2 = subString[0]
        }

        if (subString.size == 2) {
            address1 = subString[0]
            address2 = subString[1]
        }

        if (subString.size == 3) {

            address1 = subString[0] + subString[1]
            address2 = subString[2]
        }
        if (subString.size > 3) {

            for ((index, value) in subString.withIndex()) {
                if (index < 2) {
                    address1 += value
                } else {
                    address2 += value
                }
            }
        }


    }


    private fun setValue(addressData: Address) {

        //setContentView(R.layout.activity_add_new_site_address);

        //  bindIds()

        generateFinalAddress(address = addressData.getAddressLine(0))

        if (address1.isNotEmpty()) {
            binding.etSiteadd1.setText(address1)

        }

        if (address2.isNotEmpty()) {
            binding.etSiteadd2.setText(address2)

        }

        if (!addressData.adminArea.isNullOrEmpty()) {
            binding.etState.setText(addressData.adminArea)
            binding.etState.isEnabled = false

        }

        if (!addressData.locality.isNullOrEmpty()) {
            binding.etCity.setText(addressData.locality)
            binding.etCity.isEnabled = false

        }

        if (!addressData.countryName.isNullOrEmpty()) {
            binding.etContry.setText(addressData.countryName)
            binding.etContry.isEnabled = false

        }
        if (!addressData.postalCode.isNullOrEmpty()) {
            binding.etPinCode.setText(addressData.postalCode)
            binding.etPinCode.isEnabled = false

        } else {
            binding.etPinCode.isEnabled = true
        }


        val buffer = StringBuffer()
        buffer.append(addressData.latitude)
        buffer.append(",")
        buffer.append(addressData.longitude)
        binding.etLatlong.setText(buffer.toString())

        val coordinates = ArrayList<Double>()
        coordinates.add(addressData.longitude)
        coordinates.add(addressData.latitude)
        selectedCityLatLng.coordinates = coordinates
        lat = addressData.latitude
        lang = addressData.longitude

    }

    private fun getConcrateGradeListData(
        site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?
    ) {
        ApiClient.setToken()

        concrateGradeListViewModel.getConcrateGradeList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,null,null)
            .observe(this, Observer{

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {


                        }
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 200) {
                                        if (response.body()?.data?.size == 0) {
                                            binding.tvErrorLoc.visibility = View.VISIBLE
                                        } else {
                                            binding.tvErrorLoc.visibility = View.GONE
                                        }
                                    }

                                }
                            } else {

                                Utils.setErrorData(this, resource.data.errorBody())
                            }


                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    fun checkSiteNameAvilable(inputStr:String?){

        val chckObj = CheckSiteNameObj(inputStr.toString().trim())
        ApiClient.setToken()
        addNewSiteViewModel.checkSiteAdd(chckObj).observe(this,Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    if (response.data?.isSuccessful!!) {
                        response.data.let { response ->
                            try {
                                if(response.code() == 400 ){
                                    try{
                                        val gson = Gson()
                                        val type = object : TypeToken<ErrorRes>() {}.type
                                        //Please call errorBody().string() once only as the second try will return an empty string see: https://github.com/square/retrofit/issues/1321#issuecomment-251160231
                                        val err: ErrorRes? = gson.fromJson(response?.errorBody()?.string(), type)
                                        if (err != null) {
                                            if (err.error.message?.isNotEmpty() == true) {
                                                binding.tvErrorSite.visibility = View.VISIBLE
                                                Utils.showToast(
                                                    this@AddNewSiteAddressActivity,
                                                    err.error.message,
                                                    Toast.LENGTH_SHORT
                                                )
                                                binding.tvErrorSite.text = err.error.message.toString()
                                            } else {
                                                Utils.showToast(
                                                    this@AddNewSiteAddressActivity,
                                                    "Invalid Request",
                                                    Toast.LENGTH_SHORT
                                                )
                                            }
                                        }
                                    }catch (e:Exception){
                                        e.printStackTrace()
                                    }
                                }else{
                                    isSiteAvl = true
                                    binding.tvErrorSite.visibility = View.GONE
                                }

                            }catch (e:Exception){

                            }
                        }
                    } else {

                        if(response.data.code() == 400 ){
                            binding.tvErrorSite.visibility = View.VISIBLE
                            val res =response.data.errorBody()?.string().toString()
                            val gson = Gson()
                            val type = object : TypeToken<ErrorRes>() {}.type
                            val err = gson.fromJson<ErrorRes>(res, type)
                            if (err?.error?.message != null && err.error.message.isNotEmpty())
                               // Utils.showToast(this, err.error.message, Toast.LENGTH_SHORT)


                            binding.tvErrorSite.text = err.error.message.toString()

                            isSiteAvl = false
                            messageError = err.error.message.toString()

                        }else {
                            isSiteAvl = true
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                }
                Status.ERROR -> {
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
                Status.LOADING -> {

                }
            }
        })

    }
}