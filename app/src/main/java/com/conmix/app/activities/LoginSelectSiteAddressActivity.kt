package com.conmix.app.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders


import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.SelectSiteAdapter
import com.conmix.app.data.SiteAddressListData
import com.conmix.app.data.SiteAddressObjLst
import com.conmix.app.databinding.ActivitySelectSiteAddLoginBinding

import com.conmix.app.databinding.ActivitySelectSiteAddressBinding
import com.conmix.app.databinding.NoSiteBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.conmix.app.viewmodels.NotificationViewModel

import com.conmix.app.viewmodels.SelectSiteAddressViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.lang.NumberFormatException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class LoginSelectSiteAddressActivity: BaseActivity(),
        SelectSiteAdapter.SelectSiteAddressInterface {
    private lateinit var binding: ActivitySelectSiteAddLoginBinding
    private lateinit var bindingNoSite:NoSiteBinding

    private lateinit var selectSiteAddressViewModel: SelectSiteAddressViewModel
    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel

    var siteAddressList = ArrayList<SiteAddressObjLst>()
    var beforeTime: String? = null
    private var afterTime: String? = null
    var search: String? = null
    var isLoading = true
    private var isBackButton = false

    //private var cartId: String = ""
    var siteAdapter: SelectSiteAdapter? = null
    private var siteId: String? = null
    private var selectedDate: String? = null
    var minDate:Long = 0
    var maxDate :Long = 0
    var isQty:Boolean = false
    private var selectedEndDate:String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectSiteAddLoginBinding.inflate(layoutInflater)
        bindingNoSite = binding.noSiteLay
        setContentView(binding.root)
        //changeStatusBarColor(ContextCompat.getColor(this, R.color.white))

        val builder = MaterialDatePicker.Builder.dateRangePicker()
        builder.setTitleText("Select Date")
        val constraintBuilder = CalendarConstraints.Builder()
        val c = Calendar.getInstance()
        val ce = Calendar.getInstance()
        c.add(Calendar.DATE,1)
        ce.add(Calendar.DATE,32)

        val calendarStart = c
        val calendarEnd = ce

        minDate = calendarStart.timeInMillis
        maxDate = calendarEnd.timeInMillis
        setUi()
        setupViewModel()
        getSiteAddressData(null, null, null)

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.btnAddNewSite.setOnClickListener {
            val intent = Intent(this, AddNewSiteAddressActivity::class.java)
            startActivityForResult(intent, 240)
            Bungee.slideLeft(this)
        }

        binding.btnProceedPay.setOnClickListener {

            if (isBackButton) {
               // addSiteaddress()
                val intent = Intent()
                setResult(RESULT_OK, intent)
                finish()
                Bungee.slideRight(this)
            } else {

                if(isQty) {

                    val result  = Utils.comparenddateToStartday(selectedEndDate!!,selectedDate!!)
                    val ratio =  binding.qtytxt?.text.toString().toDouble() / 3.0
                    val rationdecimal = Math.ceil(ratio).toInt()

                    if(result > rationdecimal){
                        binding.dateInfoTxt.visibility = View.VISIBLE
                        Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)
                        Utils.dateRangeErrorTost(this,rationdecimal,binding.dateInfoTxt)
                        //binding.dateInfoTxt.text = String.format(getString(R.string.date_range_info), rationdecimal)
                        //Utils.showToast(this,String.format(getString(R.string.date_range_info), rationdecimal),Toast.LENGTH_SHORT)

                    }else {

                        SharedPrefrence.setSELECTEDQTY(this, binding.qtytxt.text.toString().toInt())
                        SharedPrefrence.setSelectedDate(
                            this@LoginSelectSiteAddressActivity,
                            selectedDate
                        )
                        SharedPrefrence.setSelectedENDDate(
                            this@LoginSelectSiteAddressActivity,
                            selectedEndDate
                        )
                        finish()
                        Bungee.slideRight(this)
                    }
                }else{
                    Utils.showToast(this,getString(R.string.qty_value_error),Toast.LENGTH_SHORT)
                }

            }
        }

        binding.calendarlnly.setOnClickListener {

            constraintBuilder.setStart(minDate)
            constraintBuilder.setEnd(maxDate)
            constraintBuilder.setValidator(
                PlaceAutoCompleteActivity.RangeValidator(
                    minDate,
                    maxDate
                )
            )
            builder.setSelection(androidx.core.util.Pair(Utils.getTimeInLong(selectedDate!!),Utils.getTimeInLong(selectedEndDate!!)))
            builder.setCalendarConstraints(constraintBuilder.build())

            val picker = builder.build()
            picker.show(this@LoginSelectSiteAddressActivity.supportFragmentManager, "date_picker_tag")

            picker.addOnPositiveButtonClickListener {
                val formater = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val awal = formater.format(Date(it.first ?: 0))
                val akhit = formater.format(Date(it.second ?: 0))
                selectedDate = awal
                selectedEndDate = akhit
                binding.datetxt.setText("$awal - $akhit")


                if(isQty) {

                    val result = Utils.comparenddateToStartday(selectedEndDate!!, selectedDate!!)
                    val ratio = binding.qtytxt?.text.toString().toDouble() / 3.0
                    val rationdecimal = Math.ceil(ratio).toInt()

                    if (result > rationdecimal) {
                        binding.dateInfoTxt.visibility = View.VISIBLE
                        Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)

                        binding.dateInfoTxt.setTextColor(Color.parseColor("#B94A48"))
                    } else {
                        binding.dateInfoTxt.visibility = View.VISIBLE
                        Utils.dateRangeError(this,rationdecimal,binding.dateInfoTxt)
                        binding.dateInfoTxt.setTextColor(Color.parseColor("#19A42C"))
                    }
                }else{
                    //Utils.showToast(this,getString(R.string.qty_value_error),Toast.LENGTH_SHORT)
                    binding.dateInfoTxt.visibility = View.GONE
                }

            }

            picker.addOnNegativeButtonClickListener {
                picker.dismiss()
            }
        }



        binding.rvSiteAddListView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = binding.rvSiteAddListView?.layoutManager?.childCount
                    val totalItemCount = binding.rvSiteAddListView?.layoutManager?.itemCount
                    val pastVisiblesItems =
                            (binding.rvSiteAddListView?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if (isLoading) {


                        if (visibleItemCount != null) {
                            if (visibleItemCount.plus(pastVisiblesItems) >= totalItemCount ?: 0) {


                                isLoading = false
                                if (siteAdapter != null) {
                                    //mAdapter!!.loading()

                                    siteAdapter!!.loading()

                                    // afterTime = null
                                    val handler = Handler()
                                    handler.postDelayed({


                                        isLoading = true

                                        try {
                                            beforeTime =
                                                    siteAddressList[siteAddressList.size - 1].created_at
                                        } catch (e: Exception) {

                                        }

                                        getSiteAddressData(beforeTime, null, null)


                                    }, 2000)
                                }
                            }
                        }
                    }
                }

            }
        })


    }

    private fun setupViewModel() {
        selectSiteAddressViewModel =
                ViewModelProvider(
                        this,
                        SelectSiteAddressViewModel.ViewModelFactory(ApiClient().apiService)
                )
                        .get(SelectSiteAddressViewModel::class.java)
        concrateGradeListViewModel =
            ViewModelProvider(
                this,
                ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(ConcreateGradeListViewModel::class.java)

    }

    private fun setUi() {

        binding.tvCategoryTitle.setText(R.string.choose_site_add_txt)
        siteId = SharedPrefrence.getSiteId(this)
        isBackButton = intent.getBooleanExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE, false)

        if (isBackButton) {
            binding.imgBack.visibility = View.VISIBLE
            binding.dateCalendarlnly.visibility = View.GONE
            binding.qtylnly.visibility = View.GONE
        } else {

            val qtys = SharedPrefrence.getSELECTEDQTY(this)
            binding.qtytxt.setText(qtys.toString())
            isQty = true
            binding.dateCalendarlnly.visibility = View.VISIBLE
            binding.qtylnly.visibility = View.VISIBLE
            val selectedDateAvl = SharedPrefrence.getSelectedDate(this)
            if(selectedDateAvl == null){
                val  calendar = Calendar.getInstance()
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val date = simpleDateFormat.format(calendar.time)
                selectedDate = Utils.getConvertDateWithoutTime(date)
                SharedPrefrence.setSelectedDate(this@LoginSelectSiteAddressActivity,selectedDate)
                selectedEndDate = Utils.getConvertDateWithoutTime(date)
                SharedPrefrence.setSelectedENDDate(this@LoginSelectSiteAddressActivity,selectedEndDate)
                binding.datetxt.setText("$selectedDate - $selectedEndDate")
            }else{
                selectedDate = SharedPrefrence.getSelectedDate(this)
                selectedEndDate = SharedPrefrence.getSelectedENDDate(this)
                binding.datetxt.setText("$selectedDate - $selectedEndDate")
            }

            binding.imgBack.visibility = View.GONE


            binding.qtytxt?.addTextChangedListener(object : TextWatcher {


                override fun afterTextChanged(s: Editable) {

                }

                override fun beforeTextChanged(s: CharSequence, start: Int,
                                               count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int,before: Int, count: Int) {

                    runOnUiThread {
                        if(s.length == 0 ){

                            binding.qtyErrorTxt?.visibility = View.VISIBLE
                            binding.qtyErrorTxt.text =  getString(R.string.qty_empty_erro)
                            binding.dateInfoTxt.visibility = View.GONE
                            isQty = false

                        }else{
                            if(s.length > 0){

                                try{
                                    if(s.toString().toInt() >=3 && s.toString().toInt() <=100){
                                        isQty = true
                                        binding.qtyErrorTxt?.visibility = View.GONE
                                        val ratio = s.toString().toDouble() / 3.0
                                        val rationdecimal = Math.ceil(ratio).toInt()
                                        binding.dateInfoTxt.visibility = View.VISIBLE
                                        Utils.dateRangeError(this@LoginSelectSiteAddressActivity,rationdecimal,binding.dateInfoTxt)

                                        // binding.waterLtrValue.setText(s.toString())
                                        binding.dateInfoTxt.setTextColor(Color.parseColor("#19A42C"))
                                    }else{
                                        isQty = false
                                        binding.qtyErrorTxt?.visibility = View.VISIBLE
                                        binding.qtyErrorTxt.text =  getString(R.string.qty_value_error)
                                        binding.dateInfoTxt.visibility = View.GONE
                                    }
                                }catch (e: NumberFormatException){
                                    isQty = false
                                    binding.qtyErrorTxt?.visibility = View.VISIBLE
                                    binding.qtyErrorTxt.text =  getString(R.string.qty_value_error)
                                    binding.dateInfoTxt.visibility = View.GONE
                                }


                            }else{
                                isQty = false
                                binding.qtyErrorTxt?.visibility = View.VISIBLE
                                binding.qtyErrorTxt.text =  getString(R.string.qty_empty_erro)
                                binding.dateInfoTxt.visibility = View.GONE
                            }
                        }
                    }
                }
            })

        }
        binding.btnProceedPay.isEnabled = !siteId.isNullOrEmpty()

    }

    override fun onBackPressed() {

        var siteId = SharedPrefrence.getSiteId(this)
        if (isBackButton && !siteId.isNullOrEmpty()) {
            finish()
            Bungee.slideRight(this)
        } else {
            finishAffinity()
            /*moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);*/
        }

    }

    private fun startAnim() {
        if (binding.avLoadingCategory != null)
            binding.avLoadingCategory.show()

    }

    private fun stopAnim() {

        if (binding.avLoadingCategory != null)
            binding.avLoadingCategory.hide()

    }

    override fun onResume() {
        super.onResume()
        /*Utils.setFirebaseAnalytics(
                this,
                getString(R.string.loginSelectSiteAddress),
                getString(R.string.loginSelectSiteAddress),
                ""
        )
        Utils.setFirebaseCrashlytics(
                this,
                getString(R.string.loginSelectSiteAddress),
                getString(R.string.method),
                ""
        )
        Utils.setFacebookAnalytics(this, getString(R.string.loginSelectSiteAddress))*/
        siteAdapter = SelectSiteAdapter(this, siteAddressList)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvSiteAddListView?.layoutManager = mLayoutManager
        siteAdapter?.setListener(this)
        binding.rvSiteAddListView?.adapter = siteAdapter
    }


    private fun getSiteAddressData(
            beforeTime: String?, afterTime: String?, search: String?
    ) {
        ApiClient.setToken()

        selectSiteAddressViewModel.getSiteAdressdata(beforeTime, afterTime, search)
                .observe(this,Observer {

                    it?.let {

                        resource ->
                        when (resource.status) {
                            Status.LOADING -> {
                                // startAnim()

                            }
                            Status.SUCCESS -> {
                                // stopAnim()
                                if (resource.data?.isSuccessful!!) {
                                    resource.data.let { response ->
                                        setResposerData(response = response.body())
                                    }
                                } else {
                                    emptyView()
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }


                            }
                            Status.ERROR -> {
                                // stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }

                    }
                })
    }

    private fun setResposerData(response: SiteAddressListData?) {

        for (item in response?.data!!) {

            item.isSelected = item._id.equals(siteId)
            if(item._id.equals(siteId)){
                getConcrateGradeListData(null,item.location.coordinates?.get(0).toString(),item.location.coordinates?.get(1).toString(),null,null,null,null,null,null)
            }
        }

        try {


            if (siteAdapter == null && response.data != null) {
                beforeTime = null
                afterTime = null
                siteAddressList.clear()
                siteAddressList.addAll(response.data)
                binding.rvSiteAddListView?.adapter = siteAdapter
                siteAdapter?.notifyDataSetChanged()


            } else if (siteAddressList != null && response?.data != null) {

                if (beforeTime != null && afterTime == null) {

                    siteAddressList.addAll(response?.data)

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    //progressBarPost?.visibility = View.GONE

                } else if (beforeTime == null && afterTime != null) {

                    response.data.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(0, it)
                        } else {
                            siteAddressList.add(0, it)
                        }
                    }

                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    // progressBarPost?.visibility = View.GONE


                } else {


                    siteAddressList.clear()
                    response.data?.forEach {
                        if (siteAddressList.contains(it)) {
                            val index = siteAddressList.indexOf(it)
                            siteAddressList.removeAt(index)
                            siteAddressList.add(it)
                        } else {
                            siteAddressList.add(it)
                        }

                    }
                    try {
                        siteAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }

                }

            }


            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        } catch (e: Exception) {
            isLoading = true

            siteAdapter?.loadDone()

            emptyView()

        }
    }

    private fun emptyView() {

        if (siteAddressList.isNotEmpty()) {
            bindingNoSite.constrainEmptySite.visibility = View.GONE
            bindingNoSite.imgNoList.visibility = View.GONE
            binding.rvSiteAddListView.visibility = View.VISIBLE
            siteAdapter?.loadDone()

        } else {
            binding.rvSiteAddListView.visibility = View.GONE
            bindingNoSite.constrainEmptySite.visibility = View.VISIBLE
            bindingNoSite.imgNoList.visibility = View.INVISIBLE
            siteAdapter?.loadDone()

        }
    }

    override fun onAddSelected(
            siteAddressObjLst: SiteAddressObjLst
    ) {
       /* Utils.setFacebookAnalytics(this, getString(R.string.home_AddSite))*/
        getConcrateGradeListData(null,siteAddressObjLst.location.coordinates?.get(0).toString(),siteAddressObjLst.location.coordinates?.get(1).toString(),null,null,null,null,null,null)
        siteId = siteAddressObjLst._id
        setPrefrence(
                siteId,
                siteAddressObjLst.state_id,
                siteAddressObjLst.city_id,
                siteAddressObjLst.location.coordinates?.get(1).toString(),
                siteAddressObjLst.location.coordinates?.get(0).toString(),
                siteAddressObjLst.site_name.toString(),
                siteAddressObjLst.city_name.toString()
        )
        binding.btnProceedPay.isEnabled = true


        for (item in siteAddressList) {

            item.isSelected = item._id.equals(siteId)
        }

        siteAdapter?.notifyDataSetChanged()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {
                beforeTime = null
                afterTime = null
                search = null
                siteAdapter = null
                getSiteAddressData(null, null, null)
            }
        }
    }

    private fun setPrefrence(
            siteId: String?,
            stateId: String?,
            cityId: String?,
            lat: String?,
            long: String?,
            siteName: String?,
            cityname: String?
    ) {
        if (!siteId.isNullOrEmpty()) {
            SharedPrefrence.setSiteId(
                    this,
                    siteId
            )
        }
        if (!stateId.isNullOrEmpty()) {
            SharedPrefrence.setStateId(
                    this,
                    stateId
            )
        }
        if (!cityId.isNullOrEmpty()) {
            SharedPrefrence.setCityId(
                    this,
                    cityId
            )
        }
        if (!lat.isNullOrEmpty()) {
            SharedPrefrence.setlat(
                    this,
                    lat
            )
        }
        if (!long.isNullOrEmpty()) {
            SharedPrefrence.setlang(
                    this,
                    long
            )
        }
        if (!siteName.isNullOrEmpty() && !cityname.isNullOrEmpty()) {
            val strBuffer = StringBuffer()
            strBuffer.append((siteName))
            strBuffer.append(" ")
            strBuffer.append(",")
            strBuffer.append(" ")
            strBuffer.append((cityname))
            SharedPrefrence.setSiteName(
                    this,
                    strBuffer.toString()
            )
        }


    }

    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }

    private fun getConcrateGradeListData(
        site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?
    ) {
        ApiClient.setToken()

        concrateGradeListViewModel.getConcrateGradeList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,null,null)
            .observe(this, Observer{

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {


                        }
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 200) {
                                        if (response.body()?.data?.size == 0) {
                                            binding.tvErrorLoc.visibility = View.VISIBLE
                                        } else {
                                            binding.tvErrorLoc.visibility = View.GONE
                                        }
                                    }

                                }
                            } else {

                                Utils.setErrorData(this, resource.data.errorBody())
                            }


                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }
}