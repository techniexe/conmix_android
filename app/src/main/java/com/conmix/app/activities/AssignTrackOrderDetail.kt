package com.conmix.app.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.R
import com.conmix.app.adapter.TrackCPOrderDetailAdapter
import com.conmix.app.adapter.TrackOrderDetailAdapter
import com.conmix.app.data.AssignTrackDlObj
import com.conmix.app.data.CPTrackObj
import com.conmix.app.databinding.ActivityAssignTrackOrderDetailBinding
import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.TrackOrderViewModel

import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 20,August,2021
 */
class AssignTrackOrderDetail: AppCompatActivity() {
    private lateinit var binding: ActivityAssignTrackOrderDetailBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding
    private lateinit var trackOrderViewModel: TrackOrderViewModel
    private var cpTrackObj: CPTrackObj?= null
    private var cpTrackID: String?= null
    var assignTrackDlObj: AssignTrackDlObj?= null
    var displayId: String? = null
    private lateinit var trackCPOrderDetailAdapter: TrackCPOrderDetailAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAssignTrackOrderDetailBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)
        setupViewModel()
        getIntentData()
        getAssignTrackOrderList(cpTrackID)

        binding.imgBackTrack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
    }

    override fun onBackPressed() {
        binding.imgBackTrack.performClick()
    }

    private fun getAssignTrackOrderList(id:String?) {
        ApiClient.setToken()
        trackOrderViewModel.getAssignTrackOrderDtlData(id).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                        startAnim()

                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                setResposerData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }

        })
    }

    private fun setupViewModel() {


        trackOrderViewModel = ViewModelProvider(
            this,
            TrackOrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(TrackOrderViewModel::class.java)


    }
    private fun getIntentData() {
        if (intent != null) {
            cpTrackObj = intent.getParcelableExtra("assignObj")
            cpTrackID = intent.getStringExtra("trackId")
            displayId = intent.getStringExtra("displayId")

        }
    }

    private fun setResposerData(response: AssignTrackDlObj?) {
        if (response != null) {
            assignTrackDlObj = response
        }
        emptyView()
        setupUI()

    }

    private fun emptyView() {
        if (assignTrackDlObj != null) {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE
            binding.rvAssignOrderDetailLnLy.visibility = View.VISIBLE
            binding.tvOrderID.visibility = View.VISIBLE
            binding.tvLeft.visibility = View.VISIBLE
            binding.vwLine.visibility = View.VISIBLE

        } else {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE
            binding.rvAssignOrderDetailLnLy.visibility = View.GONE
            binding.tvOrderID.visibility = View.GONE
            binding.tvLeft.visibility = View.GONE
            binding.vwLine.visibility = View.GONE
        }
    }


    private fun setupUI() {
        binding.tvTitle.text = getString(R.string.txt_assign_tracker_order)
        binding.tvOrderID.text = String.format(getString(R.string.txt_order), displayId)
        binding.tvLeft.text = "Order Date : " + Utils.getConvertDate(assignTrackDlObj?.created_at?:"")

        trackCPOrderDetailAdapter = TrackCPOrderDetailAdapter(this, assignTrackDlObj)
        binding.rvAssignOrderDetailLnLy.adapter = trackCPOrderDetailAdapter


    }
    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()
    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()
    }
}