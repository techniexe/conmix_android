package com.conmix.app.activities


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.conmix.app.R
import com.conmix.app.adapter.CustomMixlstDetailAdapter
import com.conmix.app.data.AddToCartCustomMixObj
import com.conmix.app.data.CustomMixPostObj
import com.conmix.app.data.CustomMixResObjData
import com.conmix.app.databinding.CustomMixLstDetailActivityBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.dialogs.ChooseTimeDialog
import com.conmix.app.dialogs.LoginDialog
import com.conmix.app.dialogs.TMCPBreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.CustomConmixViewModel
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.google.gson.Gson
import org.json.JSONObject
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 02,April,2021
 */
class CustomMixlstDeatilActivity : BaseActivity(), CustomMixlstDetailAdapter.SelectCustomMixInterface,
    ChooseTimeDialog.DialogToFragment {
    private lateinit var  binding: CustomMixLstDetailActivityBinding
    private val imageList = ArrayList<SlideModel>()
    private lateinit var customConmixViewModel: CustomConmixViewModel
    private lateinit var customMixAddToCartViewModel : CustomMixAddToCartViewModel
    var customMixReslst:ArrayList<CustomMixResObjData> = ArrayList()
    var customMixAdapter: CustomMixlstDetailAdapter? = null
    var customMixPostObjRequest: JSONObject? = null
    var customMixPostObjRequestNew: CustomMixPostObj? = null
    var vendorId:String?= null
    var addressId:String?= null
    var withTm:Boolean = true
    var withCp:Boolean = true
    var deliveryDate:String? = null
    var isFirstTime:Boolean = true
    var isFirstTime1:Boolean = true
    var isOpenDialog:Boolean = false
    var openCheckOut:Boolean = true
    var customMixResObjData: CustomMixResObjData? = null
    var endDate:String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CustomMixLstDetailActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        vendorId = intent.getStringExtra("vendorId")
        addressId = intent.getStringExtra("addressId")



       // deliveryDate = SharedPrefrence.getSelectedDate(this)

        val delivery_date_old = SharedPrefrence.getSelectedDate(this)
        deliveryDate =  delivery_date_old?.let { Utils.sendDeliveryDate(it) }

        val endDate_old = SharedPrefrence.getSelectedENDDate(this)
        endDate = endDate_old?.let { Utils.sendDeliveryDate(it) }

        SharedPrefrence.getCustomMixObj(this).also { customMixPostObjRequest = it }
        val gson = Gson()

        var testModel = gson.fromJson(customMixPostObjRequest.toString(), CustomMixPostObj::class.java)

        withTm = testModel.with_TM!!
        withCp = testModel.with_CP!!

        customMixPostObjRequestNew = CustomMixPostObj(testModel.site_id, testModel.long, testModel.lat, testModel.concrete_grade_id,
                testModel.cement_grade_id, testModel.cement_brand_id, testModel.cement_quantity, testModel.sand_source_id, testModel.sand_quantity, testModel.aggregate1_sub_cat_id,
                testModel.aggregate2_sub_cat_id, testModel.aggregate_source_id, testModel.aggregate1_quantity, testModel.aggregate2_quantity, testModel.fly_ash_source_id, testModel.fly_ash_quantity,
                testModel.admix_brand_id, testModel.admix_category_id, testModel.admix_quantity, testModel.water_quantity, withCp, withTm, deliveryDate, addressId,testModel.admix_code_id)


        if(withTm == false && withCp == false){
            binding.checkbox1.setClickable(false)
            binding.checkbox2.setClickable(false)
        }

        setupViewModel()

        getCustomMixDetail(vendorId)

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

       /* binding.checkButtonLy.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            startActivity(intent)
            Bungee.slideLeft(this)
        }*/



        /*binding.btnCustomMix.setOnClickListener {
            val intent = Intent(this, CustomDesignMix1Activity::class.java)
            startActivity(intent)
            Bungee.slideLeft(this)
            finish()
        }*/



    }

    override fun onBackPressed() {
        binding.imgBack.performClick()
    }

    fun setupViewModel(){
        customConmixViewModel = ViewModelProvider(
                this,
                CustomConmixViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(CustomConmixViewModel::class.java)


        customMixAddToCartViewModel = ViewModelProvider(this,
                CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService))
                .get(CustomMixAddToCartViewModel::class.java)

    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
        //loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE
        }
        //loaderFrame.visibility = View.GONE
    }

    fun setUpUi(designListObj: ArrayList<CustomMixResObjData>?){

        stopAnim()
        imageList?.clear()
        designListObj?.get(0)?.vendor_media?.forEach {

            if(it.media_type == "image") {

                imageList.add(SlideModel(it.media_url ?: "","", ScaleTypes.CENTER_CROP))
            }

        }

        binding.imageSlider.setImageList(imageList)

        /*binding.imageSlider.setSliderAdapter(SliderAdapterExample(this, imageList))
        binding.imageSlider.startAutoCycle()
        binding.imageSlider.indicatorUnselectedColor = ContextCompat.getColor(this, R.color.white);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.SLIDE)
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        binding.imageSlider.scrollTimeInSec = 2*/


        customMixAdapter = CustomMixlstDetailAdapter(this, customMixReslst, "M10", supportFragmentManager)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvCustomMixListVw.layoutManager = mLayoutManager
        customMixAdapter?.setListener(this)
        (binding.rvCustomMixListVw.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        binding.rvCustomMixListVw.adapter = customMixAdapter
        binding.checkbox1.isChecked = designListObj?.get(0)?.with_TM?:false
        binding.checkbox2.isChecked = designListObj?.get(0)?.with_CP?:false


        if(designListObj?.get(0)?.with_TM == false && designListObj?.get(0)?.with_CP == false){
            binding.checkbox1.setClickable(false)
            binding.checkbox2.setClickable(false)
        }




        binding.tmPopUpmenu.setOnClickListener {

            val fragment = TMCPBreakAlertDialog(object : TMCPBreakAlertDialog.ClickListener {
                override fun onDoneClicked() {

                }

                override fun onCancelClicked() {


                }
            }, String.format(getString(R.string.tmPricePopUpTxt),Utils.setPrecesionFormate(designListObj?.get(0)?.TM_price)), "Ok")


            fragment.show(this.supportFragmentManager, "alert")
            fragment.isCancelable = false


        }

        binding.cpPopUpmenu.setOnClickListener {
            val fragment = TMCPBreakAlertDialog(object : TMCPBreakAlertDialog.ClickListener {
                override fun onDoneClicked() {

                }

                override fun onCancelClicked() {


                }
            }, String.format(getString(R.string.cpPricePopUpTxt),Utils.setPrecesionFormate(designListObj?.get(0)?.CP_price)), "Ok")


            fragment.show(this.supportFragmentManager, "alert")
            fragment.isCancelable = false
        }




        binding.checkbox2.setOnCheckedChangeListener{ buttonView, isChecked ->

           /* if(isFirstTime1){
                isFirstTime1 = false
            }else {



            }*/


            withCp = isChecked

           /* if (isChecked) {
                withTm = isChecked

                if (binding.checkbox1.isChecked ) {

                   *//* if(!isOpenDialog){
                        var chooseTimeSlotDialog = ChooseTimeDialog(designListObj?.get(0)?.TM_price_for_unit,designListObj?.get(0)?.CP_price)
                        chooseTimeSlotDialog.setListener(this)
                        chooseTimeSlotDialog.show(supportFragmentManager, "chooseTimeSlotDialog")
                    }*//*


                } else {
                    binding.checkbox1.isChecked = true
                }
            }else{
                isOpenDialog = false
            }*/

        }

    }


    private fun getCustomMixDetail(vendorId: String?) {
        ApiClient.setToken()
        customConmixViewModel.getCustomMixDetailData(vendorId, customMixPostObjRequestNew).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->


                                if (response.body()?.data != null) {

                                    customMixReslst = response.body()?.data!!.response
                                    setUpUi(customMixReslst)
                                }


                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })

    }

    override fun onClickDesignListAddtoCart(site: CustomMixResObjData, pos: Int, qty:Int) {

        AddToCart(site, pos,qty)

    }

    override fun onProductAddedCart(dta:CustomMixResObjData,position: Int, qty: Int) {
        customMixReslst.get(position).quantity  = qty
        val cust = customMixReslst
        customMixAdapter?.notifyDataSetChanged()

    }

    override fun onProductRemovedFromCart(dta:CustomMixResObjData,position: Int, qty: Int) {
        customMixReslst.get(position).quantity  = qty
        val cust = customMixReslst
        customMixAdapter?.notifyDataSetChanged()
    }

    override fun onQtyDialog(site: CustomMixResObjData, position: Int, qty: Int) {
        customMixReslst.get(position).quantity  = qty
        val cust = customMixReslst
        customMixAdapter?.notifyDataSetChanged()
    }


    private fun AddToCart(site: CustomMixResObjData, pos: Int, qty:Int){
        val isLogin = SharedPrefrence.getLogin(this)
        if(!isLogin){
            val siteId = SharedPrefrence.getSiteId(this)
            val long = SharedPrefrence.getlang(this)
            val lat = SharedPrefrence.getlat(this)
            val objpost = AddToCartCustomMixObj(null,site,qty,SharedPrefrence.getStateId(this),null,null,null,siteId,long?.toDouble(),lat?.toDouble(),withCp,withTm,deliveryDate,null,null,null,endDate)
            var loginDialog = LoginDialog(true,objpost)
            loginDialog.show(supportFragmentManager, "loginDialog")
            Bungee.slideUp(this)
        }else{

            val siteId = SharedPrefrence.getSiteId(this)
            val long = SharedPrefrence.getlang(this)
            val lat = SharedPrefrence.getlat(this)
            /*var customMixToCartObj = CustomMixResObjData(site.concrete_grade_id,site.final_admix_brand_id,site.final_admix_cat_id,
                    site.admix_quantity,site.admix_per_kg_rate,site.final_fly_ash_source_id,site.fly_ash_per_kg_rate,site.fly_ash_quantity,
                    site.final_aggregate2_sub_cat_id,site.aggregate2_quantity,site.aggregate2_per_kg_rate,site.final_agg_source_id,site.final_aggregate1_sub_cat_id,
                    site.aggregate1_quantity,site.aggregate1_per_kg_rate,site.final_sand_source_id,site.sand_quantity,site.sand_per_kg_rate,site.final_cement_brand_id,site.final_cement_grade_id,site.cement_per_kg_rate,site.cement_quantity,site.water_quantity,site.water_per_ltr_rate,site.vendor_id,site.address_id)
*/

            if(withTm){
                val objpost = AddToCartCustomMixObj(null,site,qty,SharedPrefrence.getStateId(this),null,null,null,siteId,long?.toDouble(),lat?.toDouble(),withCp,withTm,deliveryDate,null,null,null,endDate)
                postCustomMixData(objpost)
            }else{


               /* val buyerInFo = SharedPrefrence.getTRANSISTNo(this)
                if(buyerInFo != null){

                    val objpost = AddToCartCustomMixObj(null,site,qty,SharedPrefrence.getStateId(this),null,null,null,siteId,long?.toDouble(),lat?.toDouble(),withCp,withTm,deliveryDate,SharedPrefrence.getTRANSISTNo(this),SharedPrefrence.getDriverName(this),SharedPrefrence.getDriverMobile(this))
                    postCustomMixData(objpost)


                }else{
                    val intent = Intent(this, BuyerInfoActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.bottom_up, R.anim.nothing)
                }*/

                val objpost = AddToCartCustomMixObj(null,site,qty,SharedPrefrence.getStateId(this),null,null,null,siteId,long?.toDouble(),lat?.toDouble(),withCp,withTm,deliveryDate,null,null,null,endDate)
                postCustomMixData(objpost)
            }
        }
    }

    override fun dialogChangeDateSave(date: String,endt:String) {


        deliveryDate = date
        isOpenDialog = true
        withTm = true


        SharedPrefrence.getCustomMixObj(this).also { customMixPostObjRequest = it }
        val gson = Gson()

        var testModel = gson.fromJson(customMixPostObjRequest.toString(), CustomMixPostObj::class.java)

        customMixPostObjRequestNew = CustomMixPostObj(testModel.site_id, testModel.long, testModel.lat, testModel.concrete_grade_id,
                testModel.cement_grade_id, testModel.cement_brand_id, testModel.cement_quantity, testModel.sand_source_id, testModel.sand_quantity, testModel.aggregate1_sub_cat_id,
                testModel.aggregate2_sub_cat_id, testModel.aggregate_source_id, testModel.aggregate1_quantity, testModel.aggregate2_quantity, testModel.fly_ash_source_id, testModel.fly_ash_quantity,
                testModel.admix_brand_id, testModel.admix_category_id, testModel.admix_quantity, testModel.water_quantity, withCp, withTm, deliveryDate, addressId,testModel.admix_code_id)

        getCustomMixDetail(vendorId)

    }


    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.body()?.data != null) {

                                    if(openCheckOut){
                                        openCheckOut = false
                                        //binding.addtoCartItemLy.visibility = View.GONE
                                        binding.addtoCartItemSpace.visibility = View.VISIBLE
                                    }

                                    SharedPrefrence.setTRANSISTNo(this,null)
                                    SharedPrefrence.setDriverName(this,null)
                                    SharedPrefrence.setDriverMobile(this,null)
                                    SharedPrefrence.setOPRETORName(this,null)
                                    SharedPrefrence.setOPRETORMobile(this,null)


                                    Utils.showToast(this,getString(R.string.cart_added_successfully),Toast.LENGTH_SHORT)
                                    val intent = Intent(this, CartActivity::class.java)
                                    startActivity(intent)
                                    Bungee.slideLeft(this)

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        binding.avLoading.visibility = View.GONE
                        binding.avLoading.hide()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 202) {
            if (resultCode == Activity.RESULT_OK) {
                isOpenDialog = false
                withTm = false
                binding.checkbox1.isChecked = false
                binding.checkbox2.isChecked = false
                withCp = false

                SharedPrefrence.getCustomMixObj(this).also { customMixPostObjRequest = it }
                val gson = Gson()

                var testModel = gson.fromJson(customMixPostObjRequest.toString(), CustomMixPostObj::class.java)

                customMixPostObjRequestNew = CustomMixPostObj(testModel.site_id, testModel.long, testModel.lat, testModel.concrete_grade_id,
                    testModel.cement_grade_id, testModel.cement_brand_id, testModel.cement_quantity, testModel.sand_source_id, testModel.sand_quantity, testModel.aggregate1_sub_cat_id,
                    testModel.aggregate2_sub_cat_id, testModel.aggregate_source_id, testModel.aggregate1_quantity, testModel.aggregate2_quantity, testModel.fly_ash_source_id, testModel.fly_ash_quantity,
                    testModel.admix_brand_id, testModel.admix_category_id, testModel.admix_quantity, testModel.water_quantity, withCp, withTm, deliveryDate, addressId,testModel.admix_code_id)

                getCustomMixDetail(vendorId)
            }
        }
    }
}