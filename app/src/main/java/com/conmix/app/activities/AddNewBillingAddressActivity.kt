package com.conmix.app.activities

import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.R
import com.conmix.app.adapter.SelectCityBillAdapter
import com.conmix.app.adapter.SelectConcreteGradeAdapter
import com.conmix.app.adapter.SelectRegistrationAdapter
import com.conmix.app.adapter.SelectStateBillAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.ActivityAddNewBillingAddressesBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.AddNewSiteAddViewModel
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.conmix.app.viewmodels.UserProfileFrgViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.sucho.placepicker.AddressData
import com.sucho.placepicker.Constants
import com.sucho.placepicker.MapType
import com.sucho.placepicker.PlacePicker
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 28,September,2021
 */
class AddNewBillingAddressActivity: BaseActivity(), View.OnClickListener {

    private lateinit var binding: ActivityAddNewBillingAddressesBinding
    private lateinit var addNewSiteViewModel: AddNewSiteAddViewModel
    private lateinit var userProfileFrgViewModel: UserProfileFrgViewModel
    var userProfileObj: UserProfileObj? = null
    private  var fullNameEdt:String = " "
    private  var companyNameVal: String = " "
    private lateinit var siteadd1TxtEdtVal: String
    private lateinit var siteadd2TxtEdtVal: String
    private  var cityName: String? = null
    private  var cityID: String? = null
    private  var stateName: String?= null
    private  var stateID: String?= null
    private lateinit var pincodeVal: String
    private  var gstNum:String = " "
    var isEditAdd :Boolean = false
    var isEditFirstCity :Boolean = false
    var isEditFirstState :Boolean = false
    private var stateDataList = ArrayList<StateModel>()
    private var cityDataList = ArrayList<CityModel>()
    var billAddressobj: BillAddressObjLst?= null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddNewBillingAddressesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))

        isEditAdd = intent.getBooleanExtra("isEdit",false)
        billAddressobj = intent.getParcelableExtra("billObj")

        isEditFirstState = isEditAdd



        setUpViewModel()
        getUserPrfdataApiCall()
        getStateDatafun()


        binding.imgBack.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)


    }

    private fun init(){
        binding.etAddressLine1.filters =arrayOf(Utils.ignoreFirstWhiteSpace())
        binding.etAddressLine2.filters =arrayOf(Utils.ignoreFirstWhiteSpace())
        binding.contactNameEdt.filters = arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(64))
        binding.etCompanyName.filters =arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(64))
        /*binding.etCity.filters = arrayOf(Utils.ignoreFirstWhiteSpace())
        binding.etState.filters = arrayOf(Utils.ignoreFirstWhiteSpace())*/
        binding.etPinCode.filters = arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(6))
        binding.etGST.filters = arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(16),InputFilter.AllCaps())
    }



    private fun getUserPrfdataApiCall() {


            ApiClient.setToken()
            userProfileFrgViewModel.getUserPrfData().observe(this, androidx.lifecycle.Observer {

                it?.let { resource ->
                    when (resource.status) {

                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                setResposerData(response = resource.data.body())
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
        }

    private fun setResposerData(response: UserProfileData?) {
        this.userProfileObj = response!!.data
        setUI()

    }

    private fun setUI() {

        if(userProfileObj != null) {

            setCityGradeData(cityDataList,binding.citySpinner)
            if (isEditAdd) {
                binding.tvCategoryTitle.text = getString(R.string.edit_new_billing_title_txt)

               /* if (billAddressobj?.buyerDetails?.account_type == "Individual") {
                    binding.companyNameTIL.visibility = View.GONE
                    binding.fullNameTIL.visibility = View.VISIBLE
                    binding.contactNameEdt.setText(billAddressobj?.full_name?:"".trim())
                    binding.etGST.setText(billAddressobj?.gst_number?:"".trim())

                } else {
                    binding.companyNameTIL.visibility = View.VISIBLE
                    binding.fullNameTIL.visibility = View.GONE
                    binding.etCompanyName.setText(billAddressobj?.company_name?:"".trim())
                    binding.etGST.setText(billAddressobj?.gst_number?:"".trim())
                }*/


                if(billAddressobj?.full_name != null &&  billAddressobj?.full_name != " "){
                    binding.companyNameTIL.visibility = View.GONE
                    binding.fullNameTIL.visibility = View.VISIBLE
                    binding.contactNameEdt.setText(billAddressobj?.full_name?:"".trim())
                    binding.etGST.setText(billAddressobj?.gst_number?:"".trim())
                }else {
                    binding.companyNameTIL.visibility = View.VISIBLE
                    binding.fullNameTIL.visibility = View.GONE
                    binding.etCompanyName.setText(billAddressobj?.company_name?:"".trim())
                    binding.etGST.setText(billAddressobj?.gst_number?:"".trim())
                }



                binding.etPinCode.setText(billAddressobj?.pincode?.toString())
                binding.etAddressLine1.setText(billAddressobj?.line1)
                binding.etAddressLine2.setText(billAddressobj?.line2)
                binding.btnSave.setText("UPDATE")


            } else {

                if(userProfileObj?.account_type  == "Individual"){
                    binding.companyNameTIL.visibility = View.GONE
                    binding.fullNameTIL.visibility = View.VISIBLE
                }else{
                    binding.companyNameTIL.visibility = View.VISIBLE
                    binding.fullNameTIL.visibility = View.GONE
                }

                binding.tvCategoryTitle.text = getString(R.string.add_new_billing_title_txt)
                binding.btnSave.setText("SAVE")
            }

            init()
        }

    }

    private fun setUpViewModel() {
        addNewSiteViewModel = ViewModelProvider(
            this, AddNewSiteAddViewModel.ViewModelFactory(
                ApiClient().apiService
            )
        )
            .get(AddNewSiteAddViewModel::class.java)

        userProfileFrgViewModel =
            ViewModelProvider(
                this,
                UserProfileFrgViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(UserProfileFrgViewModel::class.java)

    }

    private fun validUserData(): Boolean {

        fullNameEdt = binding.contactNameEdt.text.toString()
        companyNameVal = binding.etCompanyName?.text.toString()
        siteadd1TxtEdtVal = binding.etAddressLine1?.text.toString().trim()
        siteadd2TxtEdtVal = binding.etAddressLine2?.text.toString().trim()

        pincodeVal = binding.etPinCode.text.toString().trim()
        gstNum = binding.etGST.text.toString()


        if(isEditAdd){

            if(binding.fullNameTIL.visibility == View.VISIBLE){
                if (fullNameEdt.isEmpty()) {
                    Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
                    return false
                }

                if (fullNameEdt.length ?:0 < 3) {
                    Utils.showToast(
                        this,
                        getString(R.string.txt_name_length),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }

                if(!Utils.isValidUsername(fullNameEdt)){
                    Utils.showToast(
                        this,
                        getString(R.string.txt_name_regex_error),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            }else{
                if (companyNameVal.isEmpty()) {
                    Utils.showToast(this, getString(R.string.error_empty_company), Toast.LENGTH_SHORT)
                    return false
                }
            }



            if (siteadd1TxtEdtVal.isEmpty()) {
                Utils.showToast(this, getString(R.string.error_empty_address1), Toast.LENGTH_SHORT)
                return false
            }

            if (siteadd2TxtEdtVal.isEmpty()) {
                Utils.showToast(this, getString(R.string.error_empty_address2), Toast.LENGTH_SHORT)
                return false
            }

            if (stateName.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.error_bill_state), Toast.LENGTH_SHORT)
                return false
            }

            if (cityName.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.error_bill_city), Toast.LENGTH_SHORT)
                return false
            }



            if (pincodeVal.isEmpty()) {
                Utils.showToast(this, getString(R.string.error_empty_pincode), Toast.LENGTH_SHORT)
                return false
            }
            if (pincodeVal.length < 6) {
                Utils.showToast(this, getString(R.string.error_pincode_valid), Toast.LENGTH_SHORT)
                return false
            }

            if(binding.fullNameTIL.visibility == View.VISIBLE){

                if (gstNum.trim().isNotEmpty()) {
                    val gst = Utils.isValidGST(gstNum)
                    if (!gst) {
                        Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                        return false
                    }

                    if (gstNum?.length?:0 < 15) {
                        Utils.showToast(
                            this,
                            getString(R.string.txt_gstno_length),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                }
            }else{
                if (gstNum.isEmpty()) {
                    Utils.showToast(this, getString(R.string.txt_gstno), Toast.LENGTH_SHORT)
                    return false
                } else {

                    val gst = Utils.isValidGST(gstNum)
                    if (!gst) {
                        Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                        return false
                    }
                }

                if (gstNum.length ?:0 < 15) {
                    Utils.showToast(
                        this,
                        getString(R.string.txt_gstno_length),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            }
            return true


        }else{
            if(userProfileObj?.account_type == "Individual"){
                if (fullNameEdt.isEmpty()) {
                    Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
                    return false
                }

                if (fullNameEdt.length ?:0 < 3) {
                    Utils.showToast(
                        this,
                        getString(R.string.txt_name_length),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }

                if(!Utils.isValidUsername(fullNameEdt)){
                    Utils.showToast(
                        this,
                        getString(R.string.txt_name_regex_error),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }

            }else{
                if (companyNameVal.isEmpty()) {
                    Utils.showToast(this, getString(R.string.error_empty_company), Toast.LENGTH_SHORT)
                    return false
                }
            }


            if (siteadd1TxtEdtVal.isEmpty()) {
                Utils.showToast(this, getString(R.string.error_empty_address1), Toast.LENGTH_SHORT)
                return false
            }

            if (siteadd2TxtEdtVal.isEmpty()) {
                Utils.showToast(this, getString(R.string.error_empty_address2), Toast.LENGTH_SHORT)
                return false
            }

            if (stateName.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.error_bill_state), Toast.LENGTH_SHORT)
                return false
            }

            if (cityName.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.error_bill_city), Toast.LENGTH_SHORT)
                return false
            }



            if (pincodeVal.isEmpty()) {
                Utils.showToast(this, getString(R.string.error_empty_pincode), Toast.LENGTH_SHORT)
                return false
            }
            if (pincodeVal.length < 6) {
                Utils.showToast(this, getString(R.string.error_pincode_valid), Toast.LENGTH_SHORT)
                return false
            }

            if(userProfileObj?.account_type == "Individual"){

                if (gstNum.trim().isNotEmpty()) {
                    val gst = Utils.isValidGST(gstNum)
                    if (!gst) {
                        Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                        return false
                    }

                    if (gstNum?.length?:0 < 15) {
                        Utils.showToast(
                            this,
                            getString(R.string.txt_gstno_length),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                }
            }else{
                if (gstNum.isEmpty()) {
                    Utils.showToast(this, getString(R.string.txt_gstno), Toast.LENGTH_SHORT)
                    return false
                } else {

                    val gst = Utils.isValidGST(gstNum)
                    if (!gst) {
                        Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                        return false
                    }
                }

                if (gstNum.length ?:0 < 15) {
                    Utils.showToast(
                        this,
                        getString(R.string.txt_gstno_length),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            }
            return true
        }

        return true

    }



    private fun postAddData() {
        var billNewAddObj :AddBillAddPostObj? = null

        if(binding.fullNameTIL.visibility == View.VISIBLE){


         if(gstNum.isEmpty()){
             billNewAddObj = AddBillAddPostObj(
                 " ",
                 siteadd1TxtEdtVal,
                 siteadd2TxtEdtVal,
                 stateID,
                 cityID,
                 pincodeVal,
                 " ",
                 fullNameEdt

             )
         }  else{
             billNewAddObj = AddBillAddPostObj(
                 " ",
                 siteadd1TxtEdtVal,
                 siteadd2TxtEdtVal,
                 stateID,
                 cityID,
                 pincodeVal,
                 gstNum,
                 fullNameEdt

             )
         }
        }else{
            billNewAddObj = AddBillAddPostObj(
                companyNameVal,
                siteadd1TxtEdtVal,
                siteadd2TxtEdtVal,
                stateID,
                cityID,
                pincodeVal,
                gstNum,
                " "

            )
        }

        ApiClient.setToken()
        addNewSiteViewModel.postAddNewBillAddress(billNewAddObj).observe(this,androidx.lifecycle.Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    stopAnim()
                    response.data?.let { res ->
                        if (res.isSuccessful) {
                            Utils.showToast(
                                this,
                                getString(R.string.txt_billAddress_added),
                                Toast.LENGTH_LONG
                            )
                            //val newSiteId = res.body()?.data?._id
                            val intent = Intent()
                            setResult(RESULT_OK, intent)
                            finish()
                            binding.imgBack?.performClick()
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                }
                Status.ERROR -> {
                    stopAnim()
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
                Status.LOADING -> {
                    startAnim()
                }

            }
        })
    }

    private fun updateBillAddData() {

      /*  val billNewAddObj = AddBillAddPostObj(
            companyNameVal,
            siteadd1TxtEdtVal,
            siteadd2TxtEdtVal,
            stateID,
            cityID,
            pincodeVal,
            gstNum,
            fullNameEdt

        )*/


        var billNewAddObj :AddBillAddPostObj? = null

        if(binding.fullNameTIL.visibility == View.VISIBLE){


            if(gstNum.isEmpty()){
                billNewAddObj = AddBillAddPostObj(
                    " ",
                    siteadd1TxtEdtVal,
                    siteadd2TxtEdtVal,
                    stateID,
                    cityID,
                    pincodeVal,
                    " ",
                    fullNameEdt

                )
            }  else{
                billNewAddObj = AddBillAddPostObj(
                    " ",
                    siteadd1TxtEdtVal,
                    siteadd2TxtEdtVal,
                    stateID,
                    cityID,
                    pincodeVal,
                    gstNum,
                    fullNameEdt

                )
            }
        }else{
            billNewAddObj = AddBillAddPostObj(
                companyNameVal,
                siteadd1TxtEdtVal,
                siteadd2TxtEdtVal,
                stateID,
                cityID,
                pincodeVal,
                gstNum,
                " "

            )
        }



        ApiClient.setToken()
        addNewSiteViewModel.updateBillAddNewAdd(billAddressobj?._id,billNewAddObj).observe(this,androidx.lifecycle.Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    stopAnim()
                    response.data?.let { res ->
                        if (res.isSuccessful) {
                            Utils.showToast(
                                this,
                                getString(R.string.txt_billAddress_updated),
                                Toast.LENGTH_LONG
                            )
                            //val newSiteId = res.body()?.data?._id
                            val intent = Intent()
                            setResult(RESULT_OK, intent)
                            finish()
                            binding.imgBack?.performClick()
                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                }
                Status.ERROR -> {
                    stopAnim()
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
                Status.LOADING -> {
                    startAnim()
                }

            }
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }

    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.INVISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.btnSave -> {
                if (validUserData()) {
                    if (isEditAdd){
                        updateBillAddData()
                    }else{
                        postAddData()
                    }

                }
            }
            R.id.btnCancel -> {
                binding.imgBack?.performClick()
            }
        }
    }

    private fun getStateDatafun() {

        ApiClient.setToken()
        addNewSiteViewModel.getEditStateData().observe(this, androidx.lifecycle.Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    if (response.data?.isSuccessful!!) {
                        response.data.let { response ->
                            stateDataList.clear()
                            if (response?.body()?.data != null) {
                                stateDataList.addAll(response?.body()?.data!!)

                                if(stateDataList.size?:0 > 0){
                                    setStateGradeData(stateDataList, binding.stateSpinner)
                                }else{
                                    stateName = null
                                    stateID = null
                                    cityName = null
                                    cityID = null
                                }
                            }
                        }
                    } else {
                        Utils.setErrorData(this, response.data.errorBody())
                    }
                }
                Status.ERROR -> {
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
                Status.LOADING -> {

                }
            }
        })
    }

    private fun getCityDatafun(stateId:String?) {
        ApiClient.setToken()
        addNewSiteViewModel.getCityData(stateId).observe(this, androidx.lifecycle.Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    if (response.data?.isSuccessful!!) {
                        response.data.let { response ->
                            cityDataList.clear()
                            if (response?.body()?.data != null) {
                                cityDataList.addAll(response?.body()?.data!!)
                                setCityGradeData(cityDataList,binding.citySpinner)
                            }
                        }
                    } else {
                        Utils.setErrorData(this, response.data.errorBody())
                    }
                }
                Status.ERROR -> {
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
                Status.LOADING -> {

                }
            }
        })
    }

    private fun setStateGradeData(data: ArrayList<StateModel>, view: View) {

        val firstObj =  StateModel("first", "Select your state", null,
            null,null, null,
            null)
        data.add(0,firstObj)

        val spinnerAdapter: SelectStateBillAdapter = SelectStateBillAdapter(view.context, data)
        binding.stateSpinner.setAdapter(spinnerAdapter)

        binding.stateSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if(position == 0){

                    stateName = null
                    stateID = null
                    cityName = null
                    cityID = null

                }else if(position != 0){
                    stateName = data.get(position).state_name.toString()
                    stateID = data.get(position)._id.toString()
                    cityName = null
                    cityID = null
                    getCityDatafun(stateID)
                }
                if(isEditFirstState) {
                    isEditFirstState = false
                    if (data?.size > 0) {
                        for ((index, value) in data.withIndex()) {
                            if (value._id.equals(billAddressobj?.state_id)) {
                                binding.stateSpinner?.setSelection(index)
                                isEditFirstCity = true

                                Handler(Looper.getMainLooper()).postDelayed({
                                    //Do something after 100ms
                                    isEditState(isEditFirstCity)
                                }, 1000)
                            }
                        }
                    }
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }}

    }

    private fun setCityGradeData(data: ArrayList<CityModel>, view: View) {

        val firstObj =  CityModel("first",null, "Select your city", null,
            null,null, 0,
            null,null,0,false)
        data.add(0,firstObj)

        val spinnerAdapter: SelectCityBillAdapter = SelectCityBillAdapter(view.context, data)
        binding.citySpinner.setAdapter(spinnerAdapter)

        binding.citySpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {


                if(position == 0){
                    cityName = null
                    cityID = null

                }else if(position != 0){
                    cityName = data.get(position).city_name.toString()
                    cityID = data.get(position)._id.toString()


                }



            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                cityName = null
                cityID = null
            }}
    }

    private  fun  isEditState(isEdd:Boolean){

        if(isEdd){
            isEditFirstCity  = false

            if(cityDataList?.size > 0){
                for ((index, value) in cityDataList.withIndex()) {
                    if (value._id.equals(billAddressobj?.city_id)) {
                        binding.citySpinner?.setSelection(index)

                    }
                }
            }
        }
    }
}