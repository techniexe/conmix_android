package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.data.*
import com.conmix.app.databinding.ActivityAddNewSiteAddressesBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import com.conmix.app.viewmodels.AddNewSiteAddViewModel
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.conmix.app.viewmodels.EditSiteAddViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.sucho.placepicker.AddressData
import com.sucho.placepicker.Constants
import com.sucho.placepicker.MapType
import com.sucho.placepicker.PlacePicker
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 12,March,2021
 */
class EditSiteAddressActivity: BaseActivity(), View.OnClickListener {

    private lateinit var editSiteAddViewModel: EditSiteAddViewModel
    private lateinit var addNewSiteViewModel: AddNewSiteAddViewModel
    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel
    private var stateDataList = ArrayList<StateModel>()
    private var cityDataList = ArrayList<CityModel>()
    private lateinit var companyNameVal: String
    private lateinit var siteAddNameVal: String
    private lateinit var contactPersonNameVal: String
    private lateinit var mobileNoEdtVal: String
    private lateinit var siteadd1TxtEdtVal: String
    private lateinit var siteadd2TxtEdtVal: String
    private lateinit var pincodeVal: String
    private lateinit var emailEdtVal: String
    lateinit var countryCodeVal: String
    lateinit var selectedStateId: String
    private var selectedCountryId: Int? = 0
    lateinit var selectedCityId: String
    lateinit var countryName: String
    lateinit var stateName: String
    lateinit var cityName: String
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    var selectedCityLatLng: ProductCategoryWiseListPickupLocation? = null

    var siteAddressObjLst: SiteAddressObjLst? = null
    var address1: String = ""
    var address2: String = ""
    var lat: Double = 0.0
    var lang: Double = 0.0
    var inputString: String? = null
    private var timer: Timer? = null
    var messageError :String?= null
    var isSiteAvl = true

    private lateinit var binding: ActivityAddNewSiteAddressesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddNewSiteAddressesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        binding.tvCategoryTitle?.text = "Edit Delivery Address"
        binding.btnSave?.text = "Update"
        setUpViewModel()
        siteAddressObjLst = intent.getParcelableExtra("addressObj")
        setUpUI()

        // getCountryData()

        binding.etLatlong.setOnClickListener(this)
        binding.etState.setOnClickListener(this)
        binding.etCity.setOnClickListener(this)
        binding.imgBack?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        binding.btnCancel?.setOnClickListener {
            binding.imgBack?.performClick()
        }


        binding.btnSave?.setOnClickListener {
            if (validUserData()) {


                //postAddData()
                if(isSiteAvl){
                    addStateCity()
                }else{
                    if(!messageError.isNullOrEmpty())
                        Utils.showToast(this, messageError?:"", Toast.LENGTH_SHORT)
                }


            }

        }

        binding.etSiteName?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

                inputString = if (p0.toString().isEmpty()) null else p0.toString()

                timer = Timer()
                timer?.schedule(object : TimerTask() {
                    override fun run() {
                        // do your actual work here
                        val  handler =  Handler(Looper.getMainLooper());
                        handler.post {
                            checkSiteNameAvilable(inputString)
                        }
                    }
                }, 1000)


            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (timer != null) {
                    timer?.cancel();
                }
            }
        })


    }

    private fun setUI() {
        // etSiteName.filters = arrayOf(ignoreFirstWhiteSpace())
        //etContactPersonName.filters =
        //     arrayOf(InputFilter.LengthFilter(64))
        binding.etMobileNo.filters =
            arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(13))
        binding.etEmail.filters = arrayOf(ignoreFirstWhiteSpace())
        //  etSiteadd1.filters = arrayOf(ignoreFirstWhiteSpace())
        //   etSiteadd2.filters = arrayOf(ignoreFirstWhiteSpace())
        binding.etPinCode.filters =
            arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(6))
        // getCountryData()

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.etLatlong -> {
                showPlacePicker()
            }
            /* R.id.etState -> {
                 val intent = Intent(this, StateActivity::class.java)
                 intent.putParcelableArrayListExtra(CoreConstants.Intent.INTENT_STATE, stateDataList)
                 startActivityForResult(intent, Constants.STATE_PICKET)
                 Bungee.slideUp(this)
             }
             R.id.etCity -> {
                 val intent = Intent(this, CityActivity::class.java)
                 intent.putParcelableArrayListExtra(CoreConstants.Intent.INTENT_CITY, cityDataList)
                 startActivityForResult(intent, Constants.CITY_PICKET)
                 Bungee.slideUp(this)
             }*/
        }
    }

    private fun showPlacePicker() {

        if (siteAddressObjLst != null && siteAddressObjLst?.location != null) {
            latitude = siteAddressObjLst!!.location.coordinates?.get(1)!!
            longitude = siteAddressObjLst?.location!!.coordinates?.get(0)!!
        }


        val intent = PlacePicker.IntentBuilder()

            .setLatLong(latitude, longitude)
            .showLatLong(true)
            //.setMapRawResourceStyle(R.raw.map_style)
            .setMapType(MapType.NORMAL)
            .setMapZoom(17f)
            .setLatLong(
                lat, lang
            )
            .setPlaceSearchBar(true, getString(R.string.key_maps))
            .build(this)
        startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST)
        //recreate()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if ((requestCode == Constants.PLACE_PICKER_REQUEST) && (resultCode == Activity.RESULT_OK)) {

            val addressData = data?.getParcelableExtra<AddressData>(Constants.ADDRESS_INTENT)


            if (addressData != null
            ) {
                getConcrateGradeListData(null,addressData.addressList?.get(0)?.longitude.toString(),addressData.addressList?.get(0)?.latitude.toString(),null,null,null,null,null,null)
                addressData.addressList?.get(0)?.let { setValue(it) }

            }
        } else if ((requestCode == Constants.STATE_PICKET) && (resultCode == Activity.RESULT_OK)) {
            val stateData = data?.getParcelableExtra<StateModel>(Constants.STATE_INTENT)
            binding.etState.setText(stateData?.state_name.toString())
            selectedStateId = stateData?._id.toString()
            stateName = stateData?.state_name.toString()
            for (row in stateDataList) {
                row.isSelected = stateData?._id.equals(row._id)
            }
            binding.etState.setSelection(binding.etState.length())
            binding.etCity.setText("")
            getCityDatafun(selectedStateId)
        } else if ((requestCode == Constants.CITY_PICKET) && (resultCode == Activity.RESULT_OK)) {
            val cityModel = data?.getParcelableExtra<CityModel>(Constants.CITY_INTENT)
            binding.etCity.setText(cityModel?.city_name.toString())
            selectedCityId = cityModel?._id.toString()
            cityName = cityModel?.city_name.toString()
            for (row in cityDataList) {
                row.isSelected = cityModel?._id.equals(row._id)
            }
        }
    }

    private fun postAddData() {

        val siteNewAddObj = AddSiteAddPostObj(
            companyNameVal,
            siteAddNameVal,
            siteadd1TxtEdtVal,
            siteadd2TxtEdtVal,
            siteAddressObjLst?.country_id,
            selectedStateId,
            selectedCityId,
            null,
            pincodeVal.toInt(),
            contactPersonNameVal,
            null,
            emailEdtVal,
            mobileNoEdtVal,
            null,
            null,
            null,
            null,
            selectedCityLatLng
        )

        val siteMainObj = AddSiteAddPostData(siteNewAddObj)

        ApiClient.setToken()
        editSiteAddViewModel.updateSiteAddress(siteAddressObjLst?._id!!, siteMainObj)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    Utils.showToast(
                                        this,
                                        getString(R.string.txt_site_updated),
                                        Toast.LENGTH_LONG
                                    )
                                    val nsiteAddressObjLst = SiteAddressObjLst(
                                        siteAddressObjLst?.user_id!!,
                                        siteAddressObjLst?._id!!,
                                        companyNameVal,
                                        siteAddNameVal,
                                        siteadd1TxtEdtVal,
                                        siteadd2TxtEdtVal,
                                        siteAddressObjLst?.country_id,
                                        selectedStateId,
                                        selectedCityId,
                                        cityName,
                                        siteAddressObjLst?.country_code,
                                        countryName,
                                        stateName,
                                        pincodeVal.toInt(),
                                        selectedCityLatLng!!,
                                        contactPersonNameVal,
                                        siteAddressObjLst?.title,
                                        emailEdtVal,
                                        siteAddressObjLst?.email_verified,
                                        mobileNoEdtVal,
                                        siteAddressObjLst?.mobile_number_verified,
                                        siteAddressObjLst?.alt_mobile_number,
                                        siteAddressObjLst?.alt_mobile_number_verified,
                                        siteAddressObjLst?.whatsapp_number,
                                        siteAddressObjLst?.whatsapp_number_verified,
                                        siteAddressObjLst?.landline_number,
                                        siteAddressObjLst?.profile_pic,
                                        siteAddressObjLst?.created_at,
                                        false
                                    )
                                    val intent = Intent()
                                    intent.putExtra("updateData", nsiteAddressObjLst)
                                    setResult(Activity.RESULT_OK, intent)
                                    finish()
                                    binding.imgBack?.performClick()

                                }
                            } else {
                                Utils.setErrorData(this, resource.data?.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                    }
                }
            })
    }


    private fun setUpUI() {
        companyNameVal = siteAddressObjLst?.company_name.toString()
        siteAddNameVal = siteAddressObjLst?.site_name.toString()
        contactPersonNameVal = siteAddressObjLst?.person_name.toString()
        mobileNoEdtVal = siteAddressObjLst?.mobile_number.toString()
        siteadd1TxtEdtVal = siteAddressObjLst?.address_line1.toString()
        siteadd2TxtEdtVal = siteAddressObjLst?.address_line2.toString()
        pincodeVal = siteAddressObjLst?.pincode.toString()
        emailEdtVal = siteAddressObjLst?.email.toString()

        countryCodeVal = siteAddressObjLst?.country_code.toString()
        selectedStateId = siteAddressObjLst?.state_id.toString()
        selectedCityId = siteAddressObjLst?.city_id.toString()
        selectedCountryId = siteAddressObjLst?.country_id?.toInt()

        selectedCityLatLng = siteAddressObjLst?.location!!

        binding.etCompanyName?.setText(companyNameVal)
        binding.etSiteName?.setText(siteAddNameVal)
        binding.etContactPersonName?.setText(contactPersonNameVal)
        binding.etMobileNo?.setText(mobileNoEdtVal)
        binding.etEmail?.setText(emailEdtVal)
        binding.etSiteadd1?.setText(siteadd1TxtEdtVal)
        binding.etSiteadd2?.setText(siteadd2TxtEdtVal)
        binding.etPinCode?.setText(pincodeVal)
        if (siteAddressObjLst?.location != null) {
            val strBuffer = StringBuffer()
            strBuffer.append(siteAddressObjLst?.location!!.coordinates!![1])
            strBuffer.append(",")
            strBuffer.append(siteAddressObjLst?.location!!.coordinates!![0])
            binding.etLatlong.setText(strBuffer.toString())


        }
        binding.etState.setText(siteAddressObjLst?.state_name)
        binding.etCity.setText(siteAddressObjLst?.city_name)
        binding.etSiteName.setSelection(binding.etSiteName.text.length)
        binding.etContactPersonName.setSelection(binding.etContactPersonName.text.length)
        binding.etMobileNo.setSelection(binding.etMobileNo.text.length)
        binding.etEmail.setSelection(binding.etEmail.text.length)
        binding.etSiteadd1.setSelection(binding.etSiteadd1.text.length)
        binding.etSiteadd2.setSelection(binding.etSiteadd2.text.length)
        binding.etPinCode.setSelection(binding.etPinCode.text.length)
        binding.etLatlong.setSelection(binding.etLatlong.text.length)
        stateName = siteAddressObjLst?.state_name.toString()
        cityName = siteAddressObjLst?.city_name.toString()
        countryName = siteAddressObjLst?.country_name.toString()
        binding.etContry?.setText(countryName)


        lat = siteAddressObjLst?.location?.coordinates!![1]
        lang = siteAddressObjLst?.location?.coordinates!![0]
        getConcrateGradeListData(null,lang.toString(),lat.toString(),null,null,null,null,null,null)

        setUI()
    }


    private fun setUpViewModel() {
        editSiteAddViewModel = ViewModelProvider(
            this, EditSiteAddViewModel.ViewModelFactory(
                ApiClient().apiService
            )
        )
            .get(EditSiteAddViewModel::class.java)

        concrateGradeListViewModel =
            ViewModelProvider(
                this,
                ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(ConcreateGradeListViewModel::class.java)
    }

    private fun getCountryData() {
        ApiClient.setToken()
        editSiteAddViewModel.getCountryData().observe(this, Observer{
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                setCountryResponseData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, resource.data?.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }

                }

            }
        })
    }


    private fun setCountryResponseData(data: ArrayList<CountryModel>?) {
        if (data != null) {
            countryCodeVal = data[0].country_id.toString()
            //countryName = data[0]?.country_name.toString()
            getStateDatafun(countryCodeVal)
        }


    }


    private fun getStateDatafun(countryId: String) {
        ApiClient.setToken()
        editSiteAddViewModel.getStateData(countryId).observe(this,Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                setStateResponseData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, resource.data?.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }

                }

            }
        })
    }

    private fun validUserData(): Boolean {
        stateName = binding.etState.text.toString().trim()
        cityName = binding.etCity.text.toString().trim()

        companyNameVal = binding.etCompanyName?.text.toString().trim()
        siteAddNameVal = binding.etSiteName?.text.toString().trim()
        contactPersonNameVal = binding.etContactPersonName?.text.toString().trim()
        mobileNoEdtVal = binding.etMobileNo?.text.toString().trim()
        emailEdtVal = binding.etEmail?.text.toString().trim()
        siteadd1TxtEdtVal = binding.etSiteadd1?.text.toString().trim()
        siteadd2TxtEdtVal = binding.etSiteadd2?.text.toString().trim()
        pincodeVal = binding.etPinCode?.text.toString().trim()
        countryName = binding.etContry?.text.toString().trim()
        val phoneUtil = PhoneNumberUtil.getInstance()

        if (companyNameVal.isEmpty()) {

            Utils.showToast(this, getString(R.string.error_empty_company), Toast.LENGTH_SHORT)

            return false
        }
        if (siteAddNameVal.isEmpty()) {

            Utils.showToast(this, getString(R.string.error_empty_site), Toast.LENGTH_SHORT)

            return false
        }

        if (contactPersonNameVal.isEmpty()) {

            Utils.showToast(this, getString(R.string.error_empty_person_name), Toast.LENGTH_SHORT)

            return false
        }
        if (contactPersonNameVal.length < 2) {

            Utils.showToast(
                this,
                getString(R.string.txt_name_length),
                Toast.LENGTH_SHORT
            )
            return false
        }



        if (mobileNoEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_mobile), Toast.LENGTH_SHORT)
            return false
        }

        if (mobileNoEdtVal.isNotEmpty()) {
            try {

                val numberProto = phoneUtil.parse(mobileNoEdtVal, binding.ccp.selectedCountryNameCode)
                val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                        numberProto
                    ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                ) {

                } else {
                    Utils.showToast(
                        this,
                        getString(R.string.err_invalid_number),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            } catch (e: NumberParseException) {
                Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                return false

            }

        }

        if (emailEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT)
            return false
        }

        if (emailEdtVal.isNotEmpty()) {

            val emval = Utils.isValidEmail(emailEdtVal)
            if (emval) {

            } else {
                Utils.showToast(
                    this,
                    getString(R.string.error_empty_valid_email),
                    Toast.LENGTH_SHORT
                )
                return false
            }
        }
        /*  var coordinates = ArrayList<Double>()
                   coordinates.add(70.70)
                   coordinates.add(22.22)
                   selectedCityLatLng.coordinates = coordinates*/
        if (selectedCityLatLng?.coordinates == null) {
            Utils.showToast(this, getString(R.string.error_lat_long), Toast.LENGTH_SHORT)
            return false
        }
        if (siteadd1TxtEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_address1), Toast.LENGTH_SHORT)
            return false
        }

        if (siteadd2TxtEdtVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_address2), Toast.LENGTH_SHORT)
            return false
        }
        if (cityName.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_city), Toast.LENGTH_SHORT)
            return false
        }
        if (stateName.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_state), Toast.LENGTH_SHORT)
            return false
        }

        if (countryName.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_country), Toast.LENGTH_SHORT)
            return false
        }

        if (pincodeVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_empty_pincode), Toast.LENGTH_SHORT)
            return false
        }
        if (pincodeVal.length < 6) {
            Utils.showToast(this, getString(R.string.error_pincode_valid), Toast.LENGTH_SHORT)
            return false
        }

        return true
    }


    private fun setStateResponseData(data: ArrayList<StateModel>?) {
        stateDataList.clear()
        if (data != null) {
            stateDataList.addAll(data)
        }
        for (state in stateDataList) {
            state.isSelected = selectedStateId == state._id
        }

        getCityDatafun(selectedStateId)

    }

    private fun getCityDatafun(stateId: String) {
        ApiClient.setToken()
        editSiteAddViewModel.getCityData(stateId).observe(this,Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                setCityResponseData(response.body()?.data)
                            }
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }

                }

            }
        })
    }

    private fun setCityResponseData(data: ArrayList<CityModel>?) {
        cityDataList.clear()
        if (data != null) {
            cityDataList = data
        }


        if (binding.etCity.text.isEmpty() && cityDataList.isNotEmpty()) {
            binding.etCity.setText(cityDataList.get(0).city_name)
            cityDataList.get(0).isSelected = true
            selectedCityId = cityDataList.get(0)._id.toString()
            cityName = cityDataList.get(0).city_name.toString()
        } else {
            for (city in cityDataList) {
                city.isSelected = selectedCityId == city._id
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.imgBack?.performClick()
    }

    private fun startAnim() {
        binding.avLoading.show()
        binding.avLoading.visibility = View.VISIBLE

    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.INVISIBLE
        }

    }

    private fun setValue(addressData: Address) {

         //setContentView(R.layout.activity_add_new_site_address);
        //  bindIds()

        generateFinalAddress(address = addressData.getAddressLine(0))

        if (address1.isNotEmpty()) {

            binding.etSiteadd1.setText(address1)

        }

        if (address2.isNotEmpty()) {

            binding.etSiteadd2.setText(address2)

        }

        if (!addressData.adminArea.isNullOrEmpty()) {
            binding.etState.setText(addressData.adminArea)
            binding.etState.isEnabled = false

        }

        if (!addressData.locality.isNullOrEmpty()) {
            binding.etCity.setText(addressData.locality)
            binding.etCity.isEnabled = false

        }
        if (!addressData.countryName.isNullOrEmpty()) {
            binding.etContry.setText(addressData.countryName)
            binding.etContry.isEnabled = false

        }
        if (!addressData.postalCode.isNullOrEmpty()) {
            binding.etPinCode.setText(addressData.postalCode)
            binding.etPinCode.isEnabled = false

        } else {
            binding.etPinCode.isEnabled = true
        }

        val buffer = StringBuffer()
        buffer.append(addressData.latitude)
        buffer.append(",")
        buffer.append(addressData.longitude)
        binding.etLatlong.setText(buffer.toString())

        val coordinates = ArrayList<Double>()
        coordinates.add(addressData.longitude)
        coordinates.add(addressData.latitude)
        selectedCityLatLng?.coordinates = coordinates
        countryName = addressData.countryName
        lat = addressData.latitude
        lang = addressData.longitude

    }

    private fun generateFinalAddress(
        address: String
    ) {
        val s = address.split(",")


        var subString = s.subList(0, s.size - 3)
        if (subString.size == 1) {
            address1 = subString[0]
            address2 = subString[0]
        }

        if (subString.size == 2) {
            address1 = subString[0]
            address2 = subString[1]
        }

        if (subString.size == 3) {

            address1 = subString[0] + subString[1]
            address2 = subString[2]
        }
        if (subString.size > 3) {

            for ((index, value) in subString.withIndex()) {
                if (index < 2) {
                    address1 += value
                } else {
                    address2 += value
                }
            }
        }


    }


    private fun addStateCity() {

        var location = Citydetails()
        location.city_name = cityName
        location.state_name = stateName
        location.location = selectedCityLatLng

        ApiClient.setToken()
        editSiteAddViewModel.addStateCity(location).observe(this,Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response.body() != null) {

                                    if (response.body()!!.data._id != null)
                                        selectedCityId = response.body()!!.data._id!!

                                    if (response.body()!!.data.state_id != null)
                                        selectedStateId = response.body()!!.data.state_id!!

                                    if (response.body()!!.data.country_id != null)
                                        countryCodeVal = response.body()!!.data.country_id!!

                                    if (validStateData()) {
                                        postAddData()
                                    }
                                }

                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }

    private fun validStateData(): Boolean {
        validUserData()
        if (countryCodeVal.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_country), Toast.LENGTH_SHORT)
            return false
        }

        if (selectedStateId.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_state), Toast.LENGTH_SHORT)
            return false
        }

        if (selectedCityId.isEmpty()) {
            Utils.showToast(this, getString(R.string.error_city), Toast.LENGTH_SHORT)
            return false
        }

        return true
    }

    private fun getConcrateGradeListData(
        site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?
    ) {
        ApiClient.setToken()

        concrateGradeListViewModel.getConcrateGradeList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,null,null)
            .observe(this, Observer{

                it?.let {

                        resource ->
                    when (resource.status) {
                        Status.LOADING -> {


                        }
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 200) {
                                        if (response.body()?.data?.size == 0) {
                                            binding.tvErrorLoc.visibility = View.VISIBLE
                                        } else {
                                            binding.tvErrorLoc.visibility = View.GONE
                                        }
                                    }

                                }
                            } else {

                                Utils.setErrorData(this, resource.data.errorBody())
                            }


                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }

                }
            })
    }

    fun checkSiteNameAvilable(inputStr:String?){

        val chckObj = CheckSiteNameObj(inputStr.toString().trim())
        ApiClient.setToken()
        addNewSiteViewModel.checkSiteAdd(chckObj).observe(this,Observer{
                response ->
            when(response.status){
                Status.SUCCESS -> {
                    if (response.data?.isSuccessful!!) {
                        response.data.let { response ->
                            try {
                                if(response.code() == 400 ){
                                    try{
                                        val gson = Gson()
                                        val type = object : TypeToken<ErrorRes>() {}.type
                                        //Please call errorBody().string() once only as the second try will return an empty string see: https://github.com/square/retrofit/issues/1321#issuecomment-251160231
                                        val err: ErrorRes? = gson.fromJson(response?.errorBody()?.string(), type)
                                        if (err != null) {
                                            if (err.error.message?.isNotEmpty() == true) {
                                                binding.tvErrorSite.visibility = View.VISIBLE
                                                Utils.showToast(
                                                    this@EditSiteAddressActivity,
                                                    err.error.message,
                                                    Toast.LENGTH_SHORT
                                                )
                                                binding.tvErrorSite.text = err.error.message.toString()
                                            } else {
                                                Utils.showToast(
                                                    this@EditSiteAddressActivity,
                                                    "Invalid Request",
                                                    Toast.LENGTH_SHORT
                                                )
                                            }
                                        }
                                    }catch (e:Exception){
                                        e.printStackTrace()
                                    }
                                }else{
                                    isSiteAvl = true
                                    binding.tvErrorSite.visibility = View.GONE
                                }

                            }catch (e:Exception){

                            }
                        }
                    } else {

                        if(response.data.code() == 400 ){
                            binding.tvErrorSite.visibility = View.VISIBLE
                            val res =response.data.errorBody()?.string().toString()
                            val gson = Gson()
                            val type = object : TypeToken<ErrorRes>() {}.type
                            val err = gson.fromJson<ErrorRes>(res, type)
                            if (err?.error?.message != null && err.error.message.isNotEmpty())
                            // Utils.showToast(this, err.error.message, Toast.LENGTH_SHORT)


                                binding.tvErrorSite.text = err.error.message.toString()

                            isSiteAvl = false
                            messageError = err.error.message.toString()

                        }else {
                            isSiteAvl = true
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                }
                Status.ERROR -> {
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
                Status.LOADING -> {

                }
            }
        })

    }

}