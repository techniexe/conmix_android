package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.DesignMixListAdapter
import com.conmix.app.data.DesignListData

import com.conmix.app.data.DesignListObj
import com.conmix.app.databinding.ConcreateGradeActivityBinding
import com.conmix.app.databinding.EmptyConcreteGradeBinding


import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ConcreateGradeListViewModel

import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee

import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 17,March,2021
 */
class ConcrateGradeListActivity: BaseActivity(), DesignMixListAdapter.SelectDesignMixInterface{


    private lateinit var binding: ConcreateGradeActivityBinding
    private lateinit var bindingNoList: EmptyConcreteGradeBinding

    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel
    var conGradeList = ArrayList<DesignListObj>()
    var beforeTime: String? = null
    private var afterTime: String? = null
    var isLoading = true

    var conGradeAdapter: DesignMixListAdapter? = null
    private var siteId: String? = null
    private var gradeId:String? = null
    var lat:String? =null
    var long:String?= null
    var site_id:String? = null
    var page_Num:Int = 1
    var titleToolbar:String? = null
    var delivery_date:String?= null
    var endDate:String? = null
    var qty:Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ConcreateGradeActivityBinding.inflate(layoutInflater)
        bindingNoList = binding.noConcrateGrade
        setContentView(binding.root)
        setupViewModel()
        val delivery_date_old = SharedPrefrence.getSelectedDate(this)

        delivery_date =  delivery_date_old?.let { Utils.sendDeliveryDate(it) }
        val endDate_old = SharedPrefrence.getSelectedENDDate(this)
        endDate = endDate_old?.let {  Utils.sendDeliveryDate(it) }
        qty = SharedPrefrence.getSELECTEDQTY(this)

        setUi()
        binding.backConCreteGradeList.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

       /* binding.btnCustomMix.setOnClickListener {
            val intent = Intent(this, CustomDesignMix1Activity::class.java)
            startActivity(intent)
            Bungee.slideLeft(this)
        }*/


        binding.rvConcrateGradeListVw?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = binding.rvConcrateGradeListVw?.layoutManager?.childCount
                    val totalItemCount = binding.rvConcrateGradeListVw?.layoutManager?.itemCount
                    val pastVisiblesItems =
                            (binding.rvConcrateGradeListVw?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if (isLoading) {


                        if (visibleItemCount != null) {
                            if (visibleItemCount.plus(pastVisiblesItems) >= totalItemCount ?: 0) {


                                isLoading = false
                                if (conGradeAdapter != null) {
                                    //mAdapter!!.loading()

                                    conGradeAdapter!!.loading()

                                    // afterTime = null
                                    val handler = Handler()
                                    handler.postDelayed({

                                        page_Num++
                                        isLoading = true

                                        /*try {
                                            beforeTime =
                                                    siteAddressList[siteAddressList.size - 1].created_at
                                        } catch (e: Exception) {

                                        }*/

                                        getConcrateGradeListData(null,long,lat,gradeId,null,null,null,page_Num.toString(),delivery_date,endDate,qty)


                                    }, 2000)
                                }
                            }
                        }
                    }
                }

            }
        })

    }

    private fun setUi() {

        gradeId = intent.getStringExtra(CoreConstants.Intent.INTENT_GRADE_ID)
        titleToolbar = intent.getStringExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_TITLE)
        val isFromReapetOrder = intent.getBooleanExtra("isfromReapeatOrder",false)
        if(isFromReapetOrder){
            qty = intent.getIntExtra("qty",3)
        }

        lat = SharedPrefrence.getlat(this)
        long = SharedPrefrence.getlang(this)
        site_id = SharedPrefrence.getSiteId(this)
        binding.tvTitle.setText(titleToolbar)

        conGradeAdapter = DesignMixListAdapter(supportFragmentManager,this, conGradeList,titleToolbar)
        val mLayoutManager = LinearLayoutManager(this)
        binding.rvConcrateGradeListVw?.layoutManager = mLayoutManager
        conGradeAdapter?.setListener(this)
        binding.rvConcrateGradeListVw?.adapter = conGradeAdapter

        startAnim()
        getConcrateGradeListData(null,long,lat,gradeId,null,null,null,"1",delivery_date,endDate,qty)

    }
    private fun setupViewModel() {
        concrateGradeListViewModel =
                ViewModelProvider(
                        this,
                        ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
                )
                        .get(ConcreateGradeListViewModel::class.java)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        binding.backConCreteGradeList?.performClick()

    }
    private fun startAnim() {
        if (binding.avLoading != null)

            binding.avLoading.show()
            binding.avLoading.visibility =View.VISIBLE

    }

    private fun stopAnim() {

        if (binding.avLoading != null)

            binding.avLoading.hide()
            binding.avLoading.visibility =View.GONE

    }

    private fun getConcrateGradeListData(
            site_id:String?,long:String?,lat:String?,garde_id:String?,before:String?,after:String?,order_by:String?,page_num:String?,delivery_date: String?,endDate:String?,qty:Int?
    ) {
        ApiClient.setToken()

        concrateGradeListViewModel.getConcrateGradeList(site_id,long,lat,garde_id,before,after,order_by,page_num,delivery_date,endDate,qty)
                .observe(this, Observer{

                    it?.let {

                        resource ->
                        when (resource.status) {
                            Status.LOADING -> {


                            }
                            Status.SUCCESS -> {
                                 stopAnim()
                                if (resource.data?.isSuccessful!!) {
                                    resource.data.let { response ->
                                        setResposerData(response = response.body())
                                    }
                                } else {
                                    emptyView()
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }


                            }
                            Status.ERROR -> {
                                 stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }

                    }
                })
    }


    private fun setResposerData(response: DesignListData?) {

        try {
            if (conGradeAdapter == null && response?.data != null) {
                beforeTime = null
                afterTime = null
                conGradeList.clear()
                conGradeList.addAll(response.data)
                binding.rvConcrateGradeListVw?.adapter = conGradeAdapter
                conGradeAdapter?.notifyDataSetChanged()


            } else if (conGradeList != null && response?.data != null) {


                response.data?.forEach {
                    if (conGradeList.contains(it)) {
                        val index = conGradeList.indexOf(it)
                        conGradeList.removeAt(index)
                        conGradeList.add(it)
                    } else {
                        conGradeList.add(it)
                    }
                }
                try {
                    conGradeAdapter?.notifyDataSetChanged()
                } catch (e: Exception) {
                }



                /* if (beforeTime != null && afterTime == null) {

                     conGradeList.addAll(response?.data)

                     try {
                         conGradeAdapter?.notifyDataSetChanged()
                     } catch (e: Exception) {

                     }
                     //progressBarPost?.visibility = View.GONE

                 } else if (beforeTime == null && afterTime != null) {

                     response.data.forEach {
                         if (conGradeList.contains(it)) {
                             val index = conGradeList.indexOf(it)
                             conGradeList.removeAt(index)
                             conGradeList.add(0, it)
                         } else {
                             conGradeList.add(0, it)
                         }
                     }

                     try {
                         conGradeAdapter?.notifyDataSetChanged()
                     } catch (e: Exception) {

                     }
                     // progressBarPost?.visibility = View.GONE


                 } else {


                    // conGradeList.clear()

                 }*/

            }


            isLoading = true
            conGradeAdapter?.loadDone()
            emptyView()

        } catch (e: Exception) {
            isLoading = true
            conGradeAdapter?.loadDone()
            emptyView()
        }
    }

    private fun emptyView() {

        if (conGradeList.isNotEmpty()) {
            bindingNoList.constrainEmptySite.visibility = View.GONE
            conGradeAdapter?.loadDone()

        } else {
            bindingNoList.constrainEmptySite.visibility = View.VISIBLE
            bindingNoList.tvNoData.text = getString(R.string.grade_list_error)
            conGradeAdapter?.loadDone()

        }
    }

    override fun onClickDesignList(dlo: DesignListObj) {

        val intent = Intent(this, ConcrateGradeDetailActivity::class.java)
        intent.putExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_TITLE,titleToolbar)
        intent.putExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_OBJ,dlo)
        intent.putExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_ID,dlo._id)
        startActivity(intent)
        Bungee.slideLeft(this)

    }


}