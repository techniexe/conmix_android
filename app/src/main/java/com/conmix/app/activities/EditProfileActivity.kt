package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.adapter.SelectPaymentTypeAdapter
import com.conmix.app.adapter.SelectRegistrationAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.EditProfileBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.OTPViewModel
import com.conmix.app.viewmodels.SignUpRegisterViewModel

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * Created by Hitesh Patel on 15,March,2021
 */
class EditProfileActivity : BaseActivity() {

    var userProfileObj: UserProfileObj? = null
    private lateinit var binding: EditProfileBinding
    private lateinit var signUpRegisterViewModel: SignUpRegisterViewModel
    private lateinit var otpViewModel: OTPViewModel
    var accountTypeArray: ArrayList<String> = ArrayList<String>()
    var accountTypes = arrayOf("Builder", "Contractor","Individual")
    var companyTypeArray: ArrayList<String> = ArrayList<String>()
    var companyTypes = arrayOf("Select Company Type","Proprietor","One Person Company","Partnership", "LLP.",  "PVT.LTD.","LTD.")
    /*var indivualComTypeArray:ArrayList<String> = ArrayList<String>()
    var indivualComTypes = arrayOf("Select Company Type","Proprietor")*/


    var paymentTypesArray:ArrayList<PaymentTypesObj>? = null

    var accountTypeSelectTxt: String? = null
    var companyTypeSelectTxt: String? = null
    var paymentTypeSelectTxt:String?= null
    private var mobNum: String? = null
    private  var companyNameEdt: String? = null
    private  var fullNameEdt: String? = null
    private  var mobileNumEdt: String? = null
    private  var emailAddEdt: String? = null
    private  var passwordValEdt: String? = null
    private  var panNumEdt: String? = null
   // private  var gstNoValEdt: String? = null
    var isEmail = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        binding.tvTitle.visibility = View.VISIBLE
        binding.passwordTxtInput.visibility = View.GONE
        binding.tvTitle.setText(R.string.txt_update_profile_title)
        userProfileObj = intent.getParcelableExtra("userObj")


        accountTypeArray.addAll(accountTypes)
        setAccountResposerData(accountTypeArray, binding.accountTypeSpinner)

        companyTypeArray.addAll(companyTypes)
        setCompanyResposerData(companyTypeArray, binding.companyTypeSpinner)

        setupViewModel()

        //getPaymentTypes()
        setupUI()

        binding.btnSave.setOnClickListener {
            if (validUserData()) {
                // paymentTypeSelectTxt
                val obj = UpdateUserProfileObj(
                        accountTypeSelectTxt, companyTypeSelectTxt,
                        companyNameEdt, fullNameEdt,
                        emailAddEdt, mobileNumEdt,
                    null, panNumEdt, mobileNumEdt,null,null,null,null
                )
                updateProfilefun(obj)

            }
        }

        binding.updateEmailTxt.setOnClickListener {
            isEmail = true
            val obj = UpdateProfileOtpObj(null,null,binding.emailEdt.text.toString().trim(),null)
            getUpdateEmailOtp(obj)
        }

        binding.updateMobTxt.setOnClickListener {
            isEmail = false
            val obj = UpdateProfileOtpObj(binding.etMobileNo.text.toString().trim(),null,null,null)
            getUpdateEmailOtp(obj)
        }

        binding.btnCancel.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.backRegistration.setOnClickListener {
            binding.btnCancel.performClick()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.btnCancel.performClick()
    }

    fun setupViewModel() {
        signUpRegisterViewModel = ViewModelProvider(this,SignUpRegisterViewModel.ViewModelFactory(ApiClient().apiService)).get(SignUpRegisterViewModel::class.java)
        otpViewModel =
            ViewModelProvider(this, OTPViewModel.ViewModelFactory(ApiClient().apiService))
                .get(OTPViewModel::class.java)
    }

    private fun updateProfilefun(obj: UpdateUserProfileObj) {
        ApiClient.setToken()
        signUpRegisterViewModel.updateUserProfileData(obj)
                .observe(this, Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                stopAnim()
                                if (resource.data?.isSuccessful!!) {
                                    Utils.showToast(
                                            this,
                                            getString(R.string.edit_profile_sucess),
                                            Toast.LENGTH_LONG
                                    )
                                    resource.data.let { response -> setData(response.body()?.data) }
                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                            }
                            Status.LOADING -> {
                                startAnim()
                            }
                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }
                    }
                })
    }


    private fun getUpdateEmailOtp(updateProfileOtpObj: UpdateProfileOtpObj) {
        ApiClient.setToken()
        otpViewModel.getOTPByUpdateEmail(updateProfileOtpObj)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            when {
                                resource.data?.isSuccessful!! -> {

                                    Utils.showToast(
                                        this@EditProfileActivity,
                                        getString(R.string.err_send),
                                        Toast.LENGTH_SHORT
                                    )
                                    if(isEmail){
                                        val intent = Intent(this@EditProfileActivity, EmailOTPActivity::class.java)
                                        intent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, isEmail)
                                        intent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER,binding.emailEdt.text.toString().trim())
                                        startActivity(intent)
                                        Bungee.slideLeft(this)
                                    }else{
                                        val intent = Intent(this@EditProfileActivity, EmailOTPActivity::class.java)
                                        intent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, isEmail)
                                        intent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER,binding.etMobileNo.text.toString().trim())
                                        startActivity(intent)
                                        Bungee.slideLeft(this)
                                    }


                                }
                                resource.data.code() == 400 -> {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                                resource.data.code() == 422 -> {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                                resource.data.code() == 500 -> {
                                    Utils.showToast(
                                        this@EditProfileActivity,
                                        getString(R.string.err_internal_server),
                                        Toast.LENGTH_SHORT
                                    )
                                }
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
    }


    private fun setData(response: LoginResponse?) {
        SharedPrefrence.setSessionToken(this, response?.customToken)
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        binding.backRegistration.performClick()
    }

    private fun validUserData(): Boolean {
        companyNameEdt = binding.etCompanyName.text.toString().trim()
        fullNameEdt = binding.contactNameEdt.text.toString().trim()
        mobileNumEdt = binding.etMobileNo.text.toString().trim()
        emailAddEdt = binding.emailEdt.text.toString().trim()
        panNumEdt = binding.etPanNo.text.toString().trim()
       // gstNoValEdt = binding.etGST.text.toString().trim()
        val phoneUtil = PhoneNumberUtil.getInstance()

        if (accountTypeSelectTxt.isNullOrEmpty()) {
            Utils.showToast(
                    this,
                    getString(R.string.txt_Select_account_type),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        if(accountTypeSelectTxt == "Individual"){
           // gstNoValEdt = null


            if(companyTypeSelectTxt.isNullOrEmpty()){
                companyTypeSelectTxt = ""
            }

            if(companyNameEdt.isNullOrEmpty()){
                companyNameEdt = ""
            }


            if (fullNameEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
                return false
            }

            if (fullNameEdt?.length?:0 < 3) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if(!Utils.isValidUsername(fullNameEdt)){
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_regex_error),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if (emailAddEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_email), Toast.LENGTH_SHORT)
                return false
            }

            if (!emailAddEdt.isNullOrEmpty()) {

                val emval = Utils.isValidEmail(emailAddEdt)
                if (!emval) {
                    Utils.showToast(this, getString(R.string.txt_email_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }
            if (mobileNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_mobile), Toast.LENGTH_SHORT)
                return false
            }


            if (mobileNumEdt?.isNotEmpty() == true) {
                try {

                    val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccp.selectedCountryNameCode)
                    val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                            numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {

                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                } catch (e: NumberParseException) {
                    Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                    return false

                }

            }

           /* if (gstNoValEdt?.length?:0 >= 1) {
                val gst = Utils.isValidGST(gstNoValEdt)
                if (!gst) {
                    Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }
*/

            if (panNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_pan), Toast.LENGTH_SHORT)
                return false
            } else {
                val pan = Utils.isValidPan(panNumEdt)
                if (pan) {

                } else {
                    Utils.showToast(this, getString(R.string.txt_pan_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }

            if (panNumEdt?.length?:0 != 10) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_pan_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }




        }else{

            if (companyTypeSelectTxt.isNullOrEmpty()) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_Select_company_type),
                    Toast.LENGTH_SHORT
                )
                return false
            }
            if (companyNameEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_Select_company_name), Toast.LENGTH_SHORT)
                return false
            }

            if (fullNameEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
                return false
            }

            if (fullNameEdt?.length?:0 < 3) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if(!Utils.isValidUsername(fullNameEdt)){
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_regex_error),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if (emailAddEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_email), Toast.LENGTH_SHORT)
                return false
            }

            if (emailAddEdt?.isNotEmpty() == true) {

                val emval = Utils.isValidEmail(emailAddEdt)
                if (!emval) {
                    Utils.showToast(this, getString(R.string.txt_email_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }
            if (mobileNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_mobile), Toast.LENGTH_SHORT)
                return false
            }


            if (mobileNumEdt?.isNotEmpty() == true) {
                try {

                    val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccp.selectedCountryNameCode)
                    val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                            numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {

                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                } catch (e: NumberParseException) {
                    Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                    return false

                }

            }

            /*if (gstNoValEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_gstno), Toast.LENGTH_SHORT)
                return false
            } else {

                val gst = Utils.isValidGST(gstNoValEdt)
                if (!gst) {
                    Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }

            if (gstNoValEdt?.length?:0 < 15) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_gstno_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }*/

            if (panNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_pan), Toast.LENGTH_SHORT)
                return false
            } else {
                val pan = Utils.isValidPan(panNumEdt)
                if (pan) {

                } else {
                    Utils.showToast(this, getString(R.string.txt_pan_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }

            if (panNumEdt?.length?:0 != 10) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_pan_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }
        }
        return true
    }

    private fun setAccountResposerData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectRegistrationAdapter = SelectRegistrationAdapter(view.context, data)
        binding.accountTypeSpinner.adapter = spinnerAdapter

        binding.accountTypeSpinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
            ) {
                accountTypeSelectTxt = parent.selectedItem as String
                /*if(accountTypeSelectTxt == "Individual"){
                    indivualComTypeArray.clear()
                    companyTypeArray.clear()
                   *//* indivualComTypeArray.addAll(indivualComTypes)
                    setCompanyResposerData(indivualComTypeArray, binding.companyTypeSpinner)*//*
                    companyTypeArray.addAll(companyTypes)
                    setCompanyResposerData(companyTypeArray, binding.companyTypeSpinner)
                }else{
                    indivualComTypeArray.clear()
                    companyTypeArray.clear()
                    companyTypeArray.addAll(companyTypes)
                    setCompanyResposerData(companyTypeArray, binding.companyTypeSpinner)
                }*/

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    private fun setPaymentResposerData(data: ArrayList<PaymentTypesObj>?, view: View) {

        val spinnerAdapter: SelectPaymentTypeAdapter = SelectPaymentTypeAdapter(view.context, data)
        binding.paymentTypeSpinner.adapter = spinnerAdapter

        binding.paymentTypeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                //paymentTypeSelectTxt = parent.selectedItem as String
                paymentTypeSelectTxt = data?.get(position)?._id
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setCompanyResposerData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectRegistrationAdapter = SelectRegistrationAdapter(view.context, data)
        binding.companyTypeSpinner.adapter = spinnerAdapter

        binding.companyTypeSpinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
            ) {
                //companyTypeSelectTxt = parent.selectedItem as String
                if(parent.selectedItem.equals("Select Company Type")){

                    companyTypeSelectTxt = null

                }else{
                    companyTypeSelectTxt = parent.selectedItem as String

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun setupUI() {

        binding.termsLnLy.visibility = View.GONE

        if (userProfileObj != null) {

            if (userProfileObj?.account_type == "Builder") {
                accountTypeSelectTxt = "Builder"
                binding.accountTypeSpinner.setSelection(0)
            } else if (userProfileObj?.account_type == "Contractor") {
                accountTypeSelectTxt = "Contractor"
                binding.accountTypeSpinner.setSelection(1)
            }else if (userProfileObj?.account_type == "Individual") {
                accountTypeSelectTxt = "Individual"
                binding.accountTypeSpinner.setSelection(2)
            }

           /* if (userProfileObj?.company_type == "Partnership") {
                companyTypeSelectTxt = "Partnership"
                binding.companyTypeSpinner.setSelection(0)
            } else if (userProfileObj?.company_type == "Proprietor") {
                companyTypeSelectTxt = "Proprietor"
                binding.companyTypeSpinner.setSelection(1)
            }else if (userProfileObj?.company_type == "LLP") {
                companyTypeSelectTxt = "LLP"
                binding.companyTypeSpinner.setSelection(2)
            }else if (userProfileObj?.company_type == "PVT.LTD.") {
                companyTypeSelectTxt = "PVT.LTD."
                binding.companyTypeSpinner.setSelection(3)
            }else if (userProfileObj?.company_type == "LTD.") {
                companyTypeSelectTxt = "LTD."
                binding.companyTypeSpinner.setSelection(4)
            }
*/

          /*  if (userProfileObj?.account_type == "Builder") {
                if (userProfileObj?.company_type == "Select Company Type") {
                    companyTypeSelectTxt = "Select Company Type"
                    binding.companyTypeSpinner.setSelection(0)
                }else if (userProfileObj?.company_type == "Proprietor") {
                    companyTypeSelectTxt = "Proprietor"
                    binding.companyTypeSpinner.setSelection(1)
                } else if (userProfileObj?.company_type == "One Person Company") {
                    companyTypeSelectTxt = "One Person Company"
                    binding.companyTypeSpinner.setSelection(2)
                }else if (userProfileObj?.company_type == "Partnership") {
                    companyTypeSelectTxt = "Partnership"
                    binding.companyTypeSpinner.setSelection(3)
                }else if (userProfileObj?.company_type == "LLP.") {
                    companyTypeSelectTxt = "LLP."
                    binding.companyTypeSpinner.setSelection(4)
                }else if (userProfileObj?.company_type == "PVT.LTD.") {
                    companyTypeSelectTxt = "PVT.LTD."
                    binding.companyTypeSpinner.setSelection(5)
                }else if (userProfileObj?.company_type == "LTD.") {
                    companyTypeSelectTxt = "LTD."
                    binding.companyTypeSpinner.setSelection(6)
                }
            } else if (userProfileObj?.account_type == "Contractor") {
                if (userProfileObj?.company_type == "Select Company Type") {
                    companyTypeSelectTxt = "Select Company Type"
                    binding.companyTypeSpinner.setSelection(0)
                }else if (userProfileObj?.company_type == "Proprietor") {
                    companyTypeSelectTxt = "Proprietor"
                    binding.companyTypeSpinner.setSelection(1)
                } else if (userProfileObj?.company_type == "One Person Company") {
                    companyTypeSelectTxt = "One Person Company"
                    binding.companyTypeSpinner.setSelection(2)
                }else if (userProfileObj?.company_type == "Partnership") {
                    companyTypeSelectTxt = "Partnership"
                    binding.companyTypeSpinner.setSelection(3)
                }else if (userProfileObj?.company_type == "LLP.") {
                    companyTypeSelectTxt = "LLP."
                    binding.companyTypeSpinner.setSelection(4)
                }else if (userProfileObj?.company_type == "PVT.LTD.") {
                    companyTypeSelectTxt = "PVT.LTD."
                    binding.companyTypeSpinner.setSelection(5)
                }else if (userProfileObj?.company_type == "LTD.") {
                    companyTypeSelectTxt = "LTD."
                    binding.companyTypeSpinner.setSelection(6)
                }
            }else if (userProfileObj?.account_type == "Individual") {
                if (userProfileObj?.company_type == "Select Company Type") {
                    companyTypeSelectTxt = "Select Company Type"
                    binding.companyTypeSpinner.setSelection(0)
                }else if (userProfileObj?.company_type == "Proprietor") {
                    companyTypeSelectTxt = "Proprietor"
                    binding.companyTypeSpinner.setSelection(1)
                }
            }*/


            if (userProfileObj?.company_type == "Select Company Type") {
                companyTypeSelectTxt = "Select Company Type"
                binding.companyTypeSpinner.setSelection(0)
            }else if (userProfileObj?.company_type == "Proprietor") {
                companyTypeSelectTxt = "Proprietor"
                binding.companyTypeSpinner.setSelection(1)
            } else if (userProfileObj?.company_type == "One Person Company") {
                companyTypeSelectTxt = "One Person Company"
                binding.companyTypeSpinner.setSelection(2)
            }else if (userProfileObj?.company_type == "Partnership") {
                companyTypeSelectTxt = "Partnership"
                binding.companyTypeSpinner.setSelection(3)
            }else if (userProfileObj?.company_type == "LLP.") {
                companyTypeSelectTxt = "LLP."
                binding.companyTypeSpinner.setSelection(4)
            }else if (userProfileObj?.company_type == "PVT.LTD.") {
                companyTypeSelectTxt = "PVT.LTD."
                binding.companyTypeSpinner.setSelection(5)
            }else if (userProfileObj?.company_type == "LTD.") {
                companyTypeSelectTxt = "LTD."
                binding.companyTypeSpinner.setSelection(6)
            }

           /* if(userProfileObj?.pay_method_id != null){

                paymentTypesArray?.forEach {
                    if(userProfileObj?.pay_method_id?.equals(it._id) == true){
                        val index = paymentTypesArray?.indexOf(it)
                        binding.paymentTypeSpinner.setSelection(index!!)
                    }
                }
            }*/


           /* if (userProfileObj?.signup_type == "mobile") {
                binding.etMobileNo.setText(mobNum)
                binding.etMobileNo.isEnabled = false
                binding.etMobileNo.setTextColor(Color.parseColor("#66000000"))
            }else if(userProfileObj?.signup_type == "email"){
                binding.emailEdt.setText(mobNum)
                binding.emailEdt.isEnabled = false
                binding.emailEdt.setTextColor(Color.parseColor("#66000000"))
            }*/

            binding.etMobileNo.setTextColor(Color.parseColor("#66000000"))
            binding.emailEdt.setTextColor(Color.parseColor("#66000000"))

            binding.etCompanyName.setText(userProfileObj?.company_name)
            binding.contactNameEdt.setText(userProfileObj?.full_name)
            binding.emailEdt.setText(userProfileObj?.email)
            binding.emailEdt.isEnabled = false
            binding.etMobileNo.setText(userProfileObj?.mobile_number)
            binding.etMobileNo.isEnabled = false
            //binding.etGST.setText(userProfileObj?.gst_number)
            binding.etPanNo.setText(userProfileObj?.pan_number)


            binding.contactNameEdt.filters = arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(64))
            binding.etMobileNo.filters =
                    arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(13))
            binding.emailEdt.filters = arrayOf(Utils.ignoreFirstWhiteSpace())
            binding.etPanNo.filters =
                    arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(10), InputFilter.AllCaps())
            /*binding.etGST.filters = arrayOf(Utils.ignoreFirstWhiteSpace(), InputFilter.LengthFilter(15), InputFilter.AllCaps())*/

        }
    }

    private fun startAnim() {
        binding.avLoading.show()
    }

    private fun stopAnim() {
        binding.avLoading.hide()
    }

    fun getPaymentTypes(){
        ApiClient.setToken()

        signUpRegisterViewModel.getPaymnetType(null,null,null)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                 val  resDta = response.body()?.data
                                    if(resDta != null){
                                        paymentTypesArray = resDta
                                    }
                                    setPaymentResposerData(paymentTypesArray,binding.paymentTypeSpinner)
                                    setupUI()
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {
                        }

                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
    }
}