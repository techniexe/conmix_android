package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.data.LoginResponse
import com.conmix.app.data.UpdateUserPasswordProfileObj
import com.conmix.app.databinding.ActivityForgotConfirmBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ChangePasswordViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class ActivityForgotConfirm : BaseActivity(),View.OnClickListener {
    private lateinit var binding: ActivityForgotConfirmBinding
    private lateinit var changePasswrdViewModel: ChangePasswordViewModel
    private var newPassWordEdt: String? = null
    private var confirmPasswordEdt: String? = null
    var isFromProductDetail: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotConfirmBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fullScreen()
        setupViewModel()
        setFilters()
        getIntentdate()
        binding.btnSignIn.setOnClickListener(this)
        binding.backBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
    }

    private fun getIntentdate() {
        isFromProductDetail = intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)
    }

    private fun setupViewModel() {
        changePasswrdViewModel =
                ViewModelProvider(
                        this,
                        ChangePasswordViewModel.ViewModelFactory(ApiClient().apiService)
                )
                        .get(ChangePasswordViewModel::class.java)
    }

    private fun setFilters() {
        binding.passwordFirstEdt1.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(20))
        binding.passwordconfirmEdt1.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(20))
    }

    // ignore enter First space on edittext
    fun ignoreFirstWhiteSpace(): InputFilter {
        return InputFilter { source, start, end, dest, dstart, dend ->
            for (i in start until end) {
                if (Character.isWhitespace(source[i])) {
                    if (dstart == 0) return@InputFilter ""
                }
            }
            null
        }
    }
    override fun onBackPressed() {
       // super.onBackPressed()
        binding.backBtn.performClick()

    }

    private fun updateProfilefun(obj: UpdateUserPasswordProfileObj) {
        ApiClient.setToken()
        changePasswrdViewModel.updatePasswordData(obj)
            .observe(this, Observer{
                response ->
            when(response.status){
                            Status.SUCCESS -> {
                                stopAnim()
                                if (response.data?.isSuccessful!!) {
                                    Utils.showToast(
                                            this,
                                            getString(R.string.txt_password_sucess),
                                            Toast.LENGTH_LONG
                                    )
                                    response.let { response -> setData(response.data?.body()?.data) }
                                } else {
                                    Utils.setErrorData(this, response.data.errorBody())
                                }
                            }
                            Status.LOADING -> {
                                startAnim()
                            }

                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                            }
                    }
                })
    }

    private fun setData(response: LoginResponse?) {
        SharedPrefrence.setSessionToken(this, response?.customToken)
        //SharedPrefrence.setLogin(this, true)
        navigateToMainActivity()
    }

    private fun navigateToMainActivity() {

        if (isFromProductDetail) {
            finish()
            Bungee.slideRight(this)

        } else {

            var intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
            Bungee.slideRight(this)
        }
    }


    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.INVISIBLE
        }
    }

    private fun validUserData(): Boolean {

        newPassWordEdt = binding.passwordFirstEdt1.text.toString().trim()
        confirmPasswordEdt = binding.passwordconfirmEdt1.text.toString().trim()

        if (newPassWordEdt.isNullOrEmpty()) {

            Utils.showToast(this, getString(R.string.txt_changepaswword), Toast.LENGTH_SHORT)
            return false
        }

        if (newPassWordEdt?.length ?: 0 < 6) {

            Utils.showToast(
                    this,
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }


        if (confirmPasswordEdt.isNullOrEmpty()) {

            Utils.showToast(
                    this,
                    getString(R.string.txt_confirm_changepaswword),
                    Toast.LENGTH_SHORT
            )

            return false
        }

        if (confirmPasswordEdt?.length!! < 6) {

            Utils.showToast(
                    this,
                    getString(R.string.txt_confirm_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }

        if (!newPassWordEdt.equals(confirmPasswordEdt)) {
            Utils.showToast(
                    this,
                    getString(R.string.txt_txt_password_match),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.btnSignIn -> {
                if (validUserData()) {
                    val obj = UpdateUserPasswordProfileObj(newPassWordEdt)
                    updateProfilefun(obj)
                }
            }
        }
    }
}