package com.conmix.app.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import com.conmix.app.databinding.ReportFullScreenActivityBinding
import com.conmix.app.utils.Utils
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 03,September,2021
 */
class TMReportActivity: AppCompatActivity() {
    private lateinit var binding: ReportFullScreenActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ReportFullScreenActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val imgPath = intent.getStringExtra("image");
        Utils.setImageUsingGlide(this, imgPath, binding.profilePic)

        binding.imgBackTrack?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.imgBackTrack?.performClick()
    }
}