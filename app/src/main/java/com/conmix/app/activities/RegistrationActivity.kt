package com.conmix.app.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.adapter.SelectPaymentTypeAdapter
import com.conmix.app.adapter.SelectRegistrationAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.RegistrationSignupBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel
import com.conmix.app.viewmodels.NotificationViewModel
import com.conmix.app.viewmodels.SignUpRegisterViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 02,March,2021
 */
class RegistrationActivity: BaseActivity() {

    private lateinit var binding: RegistrationSignupBinding
    private lateinit var signUpRegisterViewModel: SignUpRegisterViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel
    var accountTypeArray:ArrayList<String> = ArrayList<String>()
    var accountTypes = arrayOf("Builder", "Contractor","Individual")
    var companyTypeArray:ArrayList<String> = ArrayList<String>()
    var companyTypes = arrayOf("Select Company Type","Proprietor","One Person Company","Partnership", "LLP.",  "PVT.LTD.","LTD.")
    var paymentTypesArray:ArrayList<PaymentTypesObj>? = null
    var accountTypeSelectTxt: String? = null
    var companyTypeSelectTxt: String? = null
    var paymentTypeSelectTxt:String?= null
    private var mobNum:String?= null
    private  var companyNameEdt: String?= null
    private  var fullNameEdt: String?= null
    private  var mobileNumEdt: String?= null
    private  var emailAddEdt: String?= null
    private  var passwordValEdt: String?= null
    private  var panNumEdt: String?= null
   // private  var gstNoValEdt: String?= null
    var isFromProductDetail: Boolean = false
    var objCart: AddToCartCustomMixObj? = null
    var isEmail = false
    var isCheckTerms = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = RegistrationSignupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewModel()
        setupUI()
        getIntentdate()

        mobNum = intent.getStringExtra(CoreConstants.Intent.INTENT_MOB_NUMBER)
        isEmail = intent.getBooleanExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS,false)

        if (!isEmail) {
            binding.etMobileNo.setText(mobNum)
            binding.etMobileNo.isEnabled = false
            binding.etMobileNo.setTextColor(Color.parseColor("#66000000"))
        }else{
            binding.emailEdt.setText(mobNum)
            binding.emailEdt.isEnabled = false
            binding.emailEdt.setTextColor(Color.parseColor("#66000000"))
        }

       // getPaymentTypes()

        accountTypeArray.addAll(accountTypes)
        setAccountResposerData(accountTypeArray,binding.accountTypeSpinner)

        companyTypeArray.addAll(companyTypes)
        setCompanyResposerData(companyTypeArray,binding.companyTypeSpinner)


        binding.termsCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->

            isCheckTerms = isChecked


        }

        /*val signupLinkClickSpan = object : ClickableSpan() {
            override fun onClick(view: View) {


                val intent = Intent(this@RegistrationActivity, SignUpActivity::class.java)
                startActivity(intent)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val clickableText = arrayOf(getString(R.string.termsConditionTxtRegister))
        val clickableSpan: Array<ClickableSpan> = arrayOf(signupLinkClickSpan)
        Utils.setStringSpanable(binding.termsTextvw, clickableText, clickableSpan)*/


        binding.termsTextvw.makeLinks(
            Pair("Terms of Usage", View.OnClickListener {
                val intent = Intent(this@RegistrationActivity, AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, getString(R.string.terms_condition_txt))
                startActivity(intent)
                Bungee.slideLeft(this@RegistrationActivity)
            }),
            Pair("Privacy Policy", View.OnClickListener {
                val intent = Intent(this@RegistrationActivity, AboutUsWebView::class.java)
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, "http://www.google.com")
                intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, getString(R.string.txt_privacy_policy))
                startActivity(intent)
                Bungee.slideLeft(this@RegistrationActivity)
            }))
        binding.btnSave.setOnClickListener {
            if (validUserData()) {
                updateUserData()
            }
        }
        binding.btnCancel.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.backRegistration.setOnClickListener {
            binding.btnCancel.performClick()
        }
    }

    private fun getIntentdate() {
        isFromProductDetail = intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)
        objCart = intent.getParcelableExtra("cartObj")


    }

    fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        var startIndexOfLink = -1
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                    textPaint.color = textPaint.linkColor
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = false
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            startIndexOfLink = this.text.toString().indexOf(link.first, startIndexOfLink + 1)
//      if(startIndexOfLink == -1) continue // todo if you want to verify your texts contains links text
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        binding.btnCancel.performClick()
    }


    private fun setAccountResposerData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectRegistrationAdapter = SelectRegistrationAdapter(view.context, data)
        binding.accountTypeSpinner.adapter = spinnerAdapter

        binding.accountTypeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                accountTypeSelectTxt = parent.selectedItem as String

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setCompanyResposerData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectRegistrationAdapter = SelectRegistrationAdapter(view.context, data)
        binding.companyTypeSpinner.adapter = spinnerAdapter

        binding.companyTypeSpinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
            ) {
                //companyTypeSelectTxt = parent.selectedItem as String

                if(parent.selectedItem.equals("Select Company Type")){

                    companyTypeSelectTxt = null

                }else{
                    companyTypeSelectTxt = parent.selectedItem as String

                }

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }
    private fun setPaymentResposerData(data: ArrayList<PaymentTypesObj>?, view: View) {

        val spinnerAdapter: SelectPaymentTypeAdapter = SelectPaymentTypeAdapter(view.context, data)
        binding.paymentTypeSpinner.adapter = spinnerAdapter

        binding.paymentTypeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                //paymentTypeSelectTxt = parent.selectedItem as String
                paymentTypeSelectTxt = data?.get(position)?._id


            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun setupViewModel() {

        signUpRegisterViewModel = ViewModelProvider(
                this,
                SignUpRegisterViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(SignUpRegisterViewModel::class.java)


        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)

        customMixAddToCartViewModel = ViewModelProvider(
            this,
            CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(CustomMixAddToCartViewModel::class.java)

    }

    private fun setupUI() {
        binding.btnSave.text = getString(R.string.signup_btn_txt)
        binding.contactNameEdt.filters = arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(64))
        binding.etMobileNo.filters = arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(13))
        binding.emailEdt.filters = arrayOf(ignoreFirstWhiteSpace())
        binding.passwordEdt.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(20))
        binding.etPanNo.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(10), InputFilter.AllCaps())
        /*binding.etGST.filters =
                arrayOf(ignoreFirstWhiteSpace(), InputFilter.LengthFilter(15), InputFilter.AllCaps())*/
    }

    private fun validUserData(): Boolean {

        fullNameEdt = binding.contactNameEdt.text.toString().trim()?:null
        mobileNumEdt = binding.etMobileNo.text.toString().trim()?:null
        emailAddEdt = binding.emailEdt.text.toString().trim()?:null
        passwordValEdt = binding.passwordEdt.text.toString().trim()?:null
        panNumEdt = binding.etPanNo.text.toString().trim()?:null

        val phoneUtil = PhoneNumberUtil.getInstance()

        if(accountTypeSelectTxt.isNullOrEmpty()){
            Utils.showToast(
                    this,
                    getString(R.string.txt_Select_account_type),
                    Toast.LENGTH_SHORT
            )
            return false
        }



        if(accountTypeSelectTxt == "Individual"){
            //gstNoValEdt = null

            if(companyTypeSelectTxt.isNullOrEmpty()){
                companyTypeSelectTxt = ""
            }

            if(companyNameEdt.isNullOrEmpty()){
                companyNameEdt = ""
            }


            if (fullNameEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
                return false
            }

            if (fullNameEdt?.length?:0 < 3) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if(!Utils.isValidUsername(fullNameEdt)){
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_regex_error),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if (emailAddEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_email), Toast.LENGTH_SHORT)
                return false
            }

            if (!emailAddEdt.isNullOrEmpty()) {

                val emval = Utils.isValidEmail(emailAddEdt)
                if (!emval) {
                    Utils.showToast(this, getString(R.string.txt_email_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }
            if (mobileNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_mobile), Toast.LENGTH_SHORT)
                return false
            }


            if (mobileNumEdt?.isNotEmpty() == true) {
                try {

                    val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccp.selectedCountryNameCode)
                    val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                            numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {

                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                } catch (e: NumberParseException) {
                    Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                    return false

                }

            }
            if (passwordValEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_password), Toast.LENGTH_SHORT)
                return false
            }

            if (passwordValEdt?.length?:0 < 6) {

                Utils.showToast(
                    this,
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }



            /*if (gstNoValEdt?.length?:0 >= 1) {
                val gst = Utils.isValidGST(gstNoValEdt)
                if (!gst) {
                    Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }*/

            if (panNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_pan), Toast.LENGTH_SHORT)
                return false
            } else {
                val pan = Utils.isValidPan(panNumEdt)
                if (pan) {

                } else {
                    Utils.showToast(this, getString(R.string.txt_pan_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }

            if (panNumEdt?.length?:0 != 10) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_pan_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }




        }else{

            companyNameEdt = binding.etCompanyName.text.toString().trim()?:null
            //gstNoValEdt = binding.etGST.text.toString().trim()?:null

            if (companyTypeSelectTxt.isNullOrEmpty()) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_Select_company_type),
                    Toast.LENGTH_SHORT
                )
                return false
            }
            if (companyNameEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_Select_company_name), Toast.LENGTH_SHORT)
                return false
            }

            if (fullNameEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
                return false
            }

            if (fullNameEdt?.length?:0 < 3) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if(!Utils.isValidUsername(fullNameEdt)){
                Utils.showToast(
                    this,
                    getString(R.string.txt_name_regex_error),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if (emailAddEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_email), Toast.LENGTH_SHORT)
                return false
            }

            if (emailAddEdt?.isNotEmpty() == true) {

                val emval = Utils.isValidEmail(emailAddEdt)
                if (!emval) {
                    Utils.showToast(this, getString(R.string.txt_email_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }
            if (mobileNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_mobile), Toast.LENGTH_SHORT)
                return false
            }


            if (mobileNumEdt?.isNotEmpty() == true) {
                try {

                    val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccp.selectedCountryNameCode)
                    val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                            numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {

                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                } catch (e: NumberParseException) {
                    Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                    return false

                }

            }
            if (passwordValEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_password), Toast.LENGTH_SHORT)
                return false
            }

            if (passwordValEdt?.length?:0 < 6) {

                Utils.showToast(
                    this,
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            /*if (gstNoValEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_gstno), Toast.LENGTH_SHORT)
                return false
            } else {

                val gst = Utils.isValidGST(gstNoValEdt)
                if (!gst) {
                    Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }

            if (gstNoValEdt?.length?:0 < 15) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_gstno_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }
*/
            if (panNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_pan), Toast.LENGTH_SHORT)
                return false
            } else {
                val pan = Utils.isValidPan(panNumEdt)
                if (pan) {

                } else {
                    Utils.showToast(this, getString(R.string.txt_pan_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }

            if (panNumEdt?.length?:0 != 10) {
                Utils.showToast(
                    this,
                    getString(R.string.txt_pan_length),
                    Toast.LENGTH_SHORT
                )
                return false
            }
        }

        if(isCheckTerms == false){
            Utils.showToast(
                this,
                getString(R.string.termsConditionTxtError),
                Toast.LENGTH_SHORT
            )
            return false
        }


        /*if(companyTypeSelectTxt.isNullOrEmpty()){
            Utils.showToast(
                    this,
                    getString(R.string.txt_Select_company_type),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        if (companyNameEdt.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_Select_company_name), Toast.LENGTH_SHORT)
            return false
        }

        if (fullNameEdt.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_name), Toast.LENGTH_SHORT)
            return false
        }

        if (fullNameEdt.length < 3) {
            Utils.showToast(
                    this,
                    getString(R.string.txt_name_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }

        if(!Utils.isValidUsername(fullNameEdt)){
            Utils.showToast(
                this,
                getString(R.string.txt_name_regex_error),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if (emailAddEdt.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_email), Toast.LENGTH_SHORT)
            return false
        }

        if (emailAddEdt.isNotEmpty()) {

            val emval = Utils.isValidEmail(emailAddEdt)
            if (emval) {

            } else {
                Utils.showToast(this, getString(R.string.txt_email_valid), Toast.LENGTH_SHORT)
                return false
            }
        }


        if (mobileNumEdt.isNullOrEmpty()) {
            Utils.showToast(this, getString(R.string.txt_mobile), Toast.LENGTH_SHORT)
            return false
        }


        if (mobileNumEdt.isNotEmpty()) {
            try {

                val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccp.selectedCountryNameCode)
                val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                                numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                ) {

                } else {
                    Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                    )
                    return false
                }
            } catch (e: NumberParseException) {
                Utils.showToast(this, getString(R.string.err_invalid_number), Toast.LENGTH_SHORT)
                return false

            }

        }


        if (passwordValEdt.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_password), Toast.LENGTH_SHORT)
            return false
        }

        if (passwordValEdt.length < 6) {

            Utils.showToast(
                    this,
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }

        if (gstNoValEdt.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_gstno), Toast.LENGTH_SHORT)
            return false
        } else {

            val gst = Utils.isValidGST(gstNoValEdt)
            if (gst) {

            } else {
                Utils.showToast(this, getString(R.string.txt_gst_valid), Toast.LENGTH_SHORT)
                return false
            }
        }

        if (gstNoValEdt.length < 15) {
            Utils.showToast(
                    this,
                    getString(R.string.txt_gstno_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }

        if (panNumEdt.isEmpty()) {
            Utils.showToast(this, getString(R.string.txt_pan), Toast.LENGTH_SHORT)
            return false
        } else {
            val pan = Utils.isValidPan(panNumEdt)
            if (pan) {

            } else {
                Utils.showToast(this, getString(R.string.txt_pan_valid), Toast.LENGTH_SHORT)
                return false
            }
        }

        if (panNumEdt.length != 10) {
            Utils.showToast(
                    this,
                    getString(R.string.txt_pan_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }


*/
        return true
    }


    private fun updateUserData() {
        val mobileSignUp = SharedPrefrence.getIsMobileSignUp(this)

        val mobileSignUpKey = if (mobileSignUp) "mobile" else "email"
        //,paymentTypeSelectTxt
        /*val signUpRegisterRequest = SignUpRegisterModel(accountTypeSelectTxt,companyTypeSelectTxt,companyNameEdt,
                fullNameEdt,emailAddEdt,mobileNumEdt,passwordValEdt,gstNoValEdt,panNumEdt,mobileNumEdt)*/

        val signUpRegisterRequest = SignUpRegisterModel(accountTypeSelectTxt,companyTypeSelectTxt,companyNameEdt,
            fullNameEdt,emailAddEdt,mobileNumEdt,passwordValEdt,null,panNumEdt,mobileNumEdt)

        ApiClient.setToken()

        signUpRegisterViewModel.signUpRegisterUser(mobileSignUpKey, signUpRegisterRequest)
                .observe(this, Observer{
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                stopAnim()
                                if (resource.data?.isSuccessful!!) {

                                    Utils.showToast(
                                            this,
                                            getString(R.string.txt_sucess),
                                            Toast.LENGTH_LONG
                                    )
                                    resource.data.let { response -> registerFCMToken(response.body()?.data) }

                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                            }
                            Status.LOADING -> {
                                startAnim()
                            }

                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                        }
                    }
                })
    }


    private fun setData(response: LoginResponse?) {
        SharedPrefrence.setSessionToken(this, response?.customToken)
        SharedPrefrence.setLogin(this, true)

        if (isFromProductDetail) {

            postCustomMixData(objCart)
        } else {
            val intent =Intent(this@RegistrationActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)
        }


    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()


    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()


    }

    private fun registerFCMToken(response: LoginResponse?) {

        Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                //Log.w(MainActivity.TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            val secureId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
            SharedPrefrence.setDeviceID(this, secureId)
            SharedPrefrence.setFCMtokem(this, token)
            registerFcmTokenToServer(token, secureId ?: "",response)
        })
    }

    private fun registerFcmTokenToServer(token: String, deviceId: String,response: LoginResponse?) {
        ApiClient.setToken()
        var fcmData = FcmData(token, deviceId, "android")
        notificationViewModel.ragisterToken(fcmData).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        if (resource.data?.isSuccessful!!) {

                            setData(response)

                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }
        })
    }

    companion object {

        private const val TAG = "Registration"
    }

    fun getPaymentTypes(){
        ApiClient.setToken()

        signUpRegisterViewModel.getPaymnetType(null,null,null)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {


                                resource.data.let { response ->

                                    val  resDta = response.body()?.data

                                    if(resDta != null){
                                        paymentTypesArray = resDta
                                    }


                                    setPaymentResposerData(paymentTypesArray,binding.paymentTypeSpinner)

                                }

                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {

                        }

                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
    }

    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                if (response.body()?.data != null) {

                                    SharedPrefrence.setTRANSISTNo(this,null)
                                    SharedPrefrence.setDriverName(this,null)
                                    SharedPrefrence.setDriverMobile(this,null)
                                    SharedPrefrence.setOPRETORName(this,null)
                                    SharedPrefrence.setOPRETORMobile(this,null)

                                    Utils.showToast(this,getString(R.string.cart_added_successfully),Toast.LENGTH_SHORT)
                                    finish()
                                    val intent = Intent(this, CartActivity::class.java)
                                    startActivity(intent)
                                    Bungee.slideLeft(this)

                                }
                            }
                        } else {

                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        /*binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()*/
                    }
                }
            }
        })
    }

}