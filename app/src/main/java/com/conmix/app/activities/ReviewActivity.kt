package com.conmix.app.activities

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.ReviewdapterWithPaging
import com.conmix.app.data.VenderReviewData
import com.conmix.app.databinding.NoReviewBinding
import com.conmix.app.databinding.ReviewActivityBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.VenderReviewViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 13,April,2021
 */
class ReviewActivity : BaseActivity(),View.OnClickListener{
    private lateinit var binding: ReviewActivityBinding
    private lateinit var bindingEmptyReview: NoReviewBinding
    private lateinit var venderReviewViewModel: VenderReviewViewModel
    var reviewList = ArrayList<VenderReviewData>()
    var beforeTime: String? = null
    var isLoading = true
    private var reviewdapter: ReviewdapterWithPaging? = null
    var venderId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ReviewActivityBinding.inflate(layoutInflater)
        bindingEmptyReview = binding.noReviewLy
        setContentView(binding.root)
        getIntentData()

        setupViewModel()

        binding.imgBack.setOnClickListener(this@ReviewActivity)

        binding.rvViewAll?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = binding.rvViewAll?.layoutManager?.childCount
                    val totalItemCount = binding.rvViewAll?.layoutManager?.itemCount
                    val pastVisiblesItems =
                            (binding.rvViewAll?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                    if (isLoading) {

                        if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {
                            isLoading = false
                            if (reviewdapter != null) {

                                reviewdapter!!.loading()
                                beforeTime = reviewList[reviewList.size - 1].created_at

                                val handler = Handler()
                                handler.postDelayed(Runnable {

                                    getVenderReviewData(venderId,beforeTime,null)

                                }, 500)


                            }
                        }
                    }
                }

            }
        })

    }

    private fun setupViewModel(){

        venderReviewViewModel = ViewModelProvider(
                this,
                VenderReviewViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(VenderReviewViewModel::class.java)
    }

    private fun getIntentData() {

        reviewList  = intent.getParcelableArrayListExtra(CoreConstants.Intent.INTENT_REVIEW_LIST)?:ArrayList()
        venderId =  intent.getStringExtra(CoreConstants.Intent.INTENT_VENDER_ID)?:null

        setUpReviewUI(reviewList)

    }

    fun getVenderReviewData(venderid:String?,before:String?,after:String?){
        ApiClient.setToken()
        venderReviewViewModel.getVenderReviewData(venderid,before,after).observe(this, Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            setUpReviewUI(resource.data.body()?.data?.reviews)
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }

        }
    }

    private fun setUpReviewUI(rvList: ArrayList<VenderReviewData>?) {
        if (reviewdapter == null) {
            if (rvList != null) {
                reviewList.addAll(rvList)
            }
            reviewdapter = ReviewdapterWithPaging(this, reviewList)
            binding.rvViewAll?.adapter = reviewdapter
        } else {
            if (rvList != null) {
                reviewList.addAll(rvList)
            }
            if (reviewList.size > 0) {
                if (beforeTime != null) {
                    reviewdapter?.addLoadMoreLikes(reviewList)
                }
            }
            isLoading = true
            reviewdapter?.loadDone()
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
    }

}