package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.adapter.CartAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.CartListActivityBinding
import com.conmix.app.databinding.NoCartitemBinding
import com.conmix.app.dialogs.BreakAlertDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.*

import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import comCoreConstants.conmix.utils.CoreConstants
import gun0912.tedimagepicker.util.ToastUtil.context
import org.json.JSONException
import org.json.JSONObject
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 13,April,2021
 */
class CartActivity : BaseActivity(), CartAdapter.ProductsAdapterInterface,
    PaymentResultWithDataListener {

    private lateinit var binding: CartListActivityBinding
    private lateinit var bindingEmptyCart: NoCartitemBinding
    private lateinit var userProfileFrgViewModel: UserProfileFrgViewModel
    var userProfileObj: UserProfileObj? = null
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel
    private lateinit var cartAdapter: CartAdapter
    var stateId:String?= null
    var siteId:String? = null
    var lat:Double?= null
    var long:Double?= null
    var deliveryDate:String?= null
    var cartItemsSchema: ArrayList<CartItemLst> = ArrayList<CartItemLst>()
    var cartObjDta: CartObjDta?= null
    private lateinit var orderViewModel: OrderViewModel
    private lateinit var applyCouponViewModel: ApplyCouponViewModel
    val TAG:String = CartActivity::class.toString()
    var nwamnt:String? = null
    private lateinit var paymentCaptureViewModel: PaymentCaptureViewModel
    var sucessMsg:String = "Product added to cart successfully"
    var endDate:String?= null

    private val startForResultByerInfo = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {

            val data = result.data
            val objItems = data?.getParcelableExtra("cartItems")?:null
            val tmNo = data?.getStringExtra("tmNo")
            val driverName = data?.getStringExtra("driverName")
            val driverMoNo = data?.getStringExtra("driverMoNo")
           // val opretorName = data?.getStringExtra("opretorName")
           //val opretorMoNo = data?.getStringExtra("opretorMoNo")
            onProductBuyerAddedCart(objItems,tmNo,driverName,driverMoNo)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CartListActivityBinding.inflate(layoutInflater)
        bindingEmptyCart = binding.emptyCartLy
        setContentView(binding.root)
        Checkout.preload(this.applicationContext)
        setupViewModel()
        getUserPrfdataApiCall()
        binding.cartToolbar.visibility = View.VISIBLE

        stateId = SharedPrefrence.getStateId(this)
        lat = SharedPrefrence.getlat(this)?.toDouble()
        long = SharedPrefrence.getlang(this)?.toDouble()
        deliveryDate = SharedPrefrence.getSelectedDate(this)
        endDate = SharedPrefrence.getSelectedENDDate(this)

        startAnim()
        getCartdata()

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                PostSiteId()
            }
        }
        binding.btnAddNewSite.setOnClickListener {
           /* val intent = Intent(this, LoginSelectSiteAddressActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE,true)
            startActivityForResult(intent,240)
            Bungee.slideLeft(this)*/

            startForResult.launch(Intent(this, CartSelectSiteAddressActivity::class.java).apply {
                putExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE,true)
                putExtra("siteId",cartObjDta?.site_id)
            })
            Bungee.slideLeft(this)
        }


        binding.btnAddNewBiilingAddress.setOnClickListener {

            val intent = Intent(this, CartSelectBillingAddressActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_DELIVERY_BUTTON_INVISIBLE,true)
            intent.putExtra("billId",cartObjDta?.billing_address_id)
            intent.putExtra("cartStateId",cartObjDta?.siteInfo?.state_id)
            startActivityForResult(intent,480)
            Bungee.slideLeft(this)
        }


        val startForResultApplyCopon = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }

        binding.applyCoupanTxt.setOnClickListener {
            val intent = Intent(this, ApplyCouponActivity::class.java)
            intent.putExtra("couponCode",cartObjDta?.couponInfo?.code)
            startActivityForResult(intent,420)
            Bungee.slideLeft(this)

            /*startForResultApplyCopon.launch(Intent(this, ApplyCouponActivity::class.java).apply {
                putExtra("couponCode",cartObjDta?.couponInfo?.code)
            })
            Bungee.slideLeft(this)*/
        }

        binding.btnCheckOut.setOnClickListener {
            var siteId = binding.tvAddress.text
            if (siteId.isNullOrEmpty() ) {
               // processApi()

                Utils.showToast(this, getString(R.string.txt_delivery), Toast.LENGTH_SHORT)

            } else if(cartObjDta?.billing_address_id == null) {

                Utils.showToast(this, getString(R.string.txt_bii_delivery), Toast.LENGTH_SHORT)
            }else{
                startAnim()
                StartPayment()
            }
        }

        binding.cancelCoupon.setOnClickListener {
            postCouponCode()
        }

    }

    private fun StartPayment(){
        // Set key id
        val activity:Activity = this@CartActivity
        val co = Checkout()
        co.setKeyID("rzp_test_MDFf9gouEAhcyB")

        //set Image
        co.setImage(R.mipmap.ic_launcher)
        try {
            val options = JSONObject()
            options.put("name","Conmix")
            options.put("description","")
            options.put("theme.color", "#136E9D")
            options.put("currency","INR");
            //options.put("order_id", "order_DBJOWzybf0sJbb");
           /* val amtn = cartObjDta?.total_amount?:0.0 * 100
            val amnt = Math.round(amtn)
            options.put("amount",amnt)//pass amount in currency subunits*/
            val amtn1 = (cartObjDta?.total_amount?:0.0) * 100
            val amnt = Math.round(amtn1)
            nwamnt = amnt.toString()
            options.put("amount",amnt.toString())//pass amount in currency subunits
            options.put("timeout",200)
            val retryObj =  JSONObject()
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            val prefill = JSONObject()
            prefill.put("name",userProfileObj?.full_name)
            prefill.put("email",userProfileObj?.email)
            prefill.put("contact",userProfileObj?.mobile_number)

            options.put("prefill",prefill)
            val notesObj = JSONObject()

            var strBuffer = StringBuffer()
            strBuffer.append(cartObjDta?.siteInfo?.address_line1)
            strBuffer.append(",")
            strBuffer.append(cartObjDta?.siteInfo?.address_line2)
            strBuffer.append(",")
            strBuffer.append(cartObjDta?.siteInfo?.city_name)
            strBuffer.append(" - ")
            strBuffer.append(cartObjDta?.siteInfo?.pincode)
            strBuffer.append(",")
            strBuffer.append(cartObjDta?.siteInfo?.state_name)
            strBuffer.append(",")
            strBuffer.append(cartObjDta?.siteInfo?.country_name)
            notesObj.put("address",strBuffer)
            notesObj.put("merchant_order_id",cartObjDta?.unique_id)

            options.put("notes",notesObj)
            options.put("send_sms_hash",true)

            co.open(activity,options)
        }catch (e: Exception){
            Toast.makeText(activity,"Error in payment: "+ e.message,Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {
                PostSiteId()
            }
        }else if(requestCode == 420){
            if (resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }
        else if(requestCode == 480){
            if (resultCode == Activity.RESULT_OK) {
                getCartdata()
            }
        }
        else if(requestCode == 244){
            if (resultCode == Activity.RESULT_OK) {
                val objItems = data?.getParcelableExtra("cartItems")?:null
                val tmNo = data?.getStringExtra("tmNo")
                val driverName = data?.getStringExtra("driverName")
                val driverMoNo = data?.getStringExtra("driverMoNo")
                val opretorName = data?.getStringExtra("opretorName")
                val opretorMoNo = data?.getStringExtra("opretorMoNo")
               // onProductBuyerAddedCart(objItems,tmNo,driverName,driverMoNo,opretorName,opretorMoNo)

            }
        }
    }

    override fun onBackPressed() {
        binding.imgBack.performClick()
    }

    private fun setupViewModel() {

        customMixAddToCartViewModel = ViewModelProvider(
                this,
                CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(CustomMixAddToCartViewModel::class.java)

        orderViewModel = ViewModelProvider(
                this,
                OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(OrderViewModel::class.java)

        applyCouponViewModel = ViewModelProvider(
                this,
                ApplyCouponViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(ApplyCouponViewModel::class.java)
        userProfileFrgViewModel =
            ViewModelProvider(
                this,
                UserProfileFrgViewModel.ViewModelFactory(ApiClient().apiService)
            ).get(UserProfileFrgViewModel::class.java)

        paymentCaptureViewModel = ViewModelProvider(
            this,
            PaymentCaptureViewModel.ViewModelFactory(ApiClient().apiPaymentCapture)
        )
            .get(PaymentCaptureViewModel::class.java)
    }
    private fun startAnim() {
        if (binding.avLoading != null)

            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE

    }

    private fun stopAnim() {

        if (binding.avLoading != null)

            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE

    }

    override fun onResume() {
        super.onResume()

    }

    private fun setResposerData(response: CartObjDta) {
        cartObjDta = response
        cartItemsSchema = response.items?: ArrayList()
        emptyView()
        setupUI()

    }

    private fun setupUI() {

        binding.rvCartList.layoutManager = LinearLayoutManager(this)
        cartAdapter = CartAdapter(this, cartObjDta, supportFragmentManager)
        cartAdapter.setProductsAdapterListener(this)
        binding.rvCartList.adapter = cartAdapter
        cartItemsSchema?.let { setData() }
    }


    private fun processApi(razorpayPaymentId: String?) {
        ApiClient.setToken()
        val obj = GatewayTranstedIdObj(razorpayPaymentId)
        orderViewModel.postOrderData(obj).observe(this, Observer{
                response ->
            when(response.status){
                        Status.LOADING -> {
                           // startAnim()
                            startAnim()
                        }
                        Status.SUCCESS -> {
                            stopAnim()
                            if (response.data?.code() == 200) {

                                redirectToDialog(response.data.body()?.data!!)
                            } else {
                                Utils.setErrorData(this, response.data?.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                        }
                }
            })

    }

    private fun redirecToMainActivity() {
        stopAnim()
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        //finish()
        Bungee.slideRight(this)
    }

    private fun redirectToDialog(dOrder: OrderResObj) {
        val fragment = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {
               // redirecToMainActivity()

                processApiNew(dOrder)
            }

            override fun onCancelClicked() {


            }
        }, String.format(getString(R.string.alert_order_place), dOrder.display_id), "Ok", "")
        fragment.show(this.supportFragmentManager, "alert")
        fragment.isCancelable = false
    }


    private fun processApiNew(order: OrderResObj) {
        ApiClient.setToken()



        orderViewModel.processorder(order._id!!).observe(this, Observer{
                response ->
            when(response.status){
                Status.LOADING -> {
                    startAnim()

                }
                Status.SUCCESS -> {
                    stopAnim()
                    if (response.data?.isSuccessful!!) {
                        response.data.let { it1 ->
                            // redirectToDialog(order.display_id)
                            redirecToMainActivity()
                        }
                    } else {
                        Utils.setErrorData(this, response.data.errorBody())
                    }
                }
                Status.ERROR -> {
                    stopAnim()
                    Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                }
            }
        })

    }



    private fun PostSiteId() {

        val cartAddressIdObj = CartAddressIdObj(SharedPrefrence.getSiteId(this))

        ApiClient.setToken()

        customMixAddToCartViewModel.cartAddressAdd(cartAddressIdObj).observe(this, Observer{
                response ->
            when(response.status){

                    Status.SUCCESS -> {
                        stopAnim()

                        if (response.data?.isSuccessful!!) {

                            getCartdata()

                        } else {
                            Utils.setErrorData(this, response.data.errorBody())
                            //Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
            }
        })
    }


    private fun setData() {
        if (cartItemsSchema != null) {

            if (cartObjDta?.totalNoOfItems?:0 > 1) {
                binding.tvCartItemLable.text = String.format(
                        getString(R.string.txt_cart_items),
                        cartItemsSchema.size
                )
            } else {
                binding.tvCartItemLable.text = String.format(
                        getString(R.string.txt_cart_item),
                        cartItemsSchema.size
                )
            }

            binding.tvCartItem.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.selling_price_With_Margin?:0.0)
            )

            binding.txtTransistAmtValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.TM_price?:0.0)
            )

            binding.tvConcretePumValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.CP_price?:0.0)
            )

           /* binding.tvItemGST.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.gst_price?:0.0)
            )*/

            if(cartObjDta?.cgst_price != null && cartObjDta?.cgst_price?:0.0 > 0){
                binding.cGSTLnLy.visibility = View.VISIBLE
                binding.sGSTLnLy.visibility = View.VISIBLE
                binding.iGSTLnLy.visibility = View.GONE
                binding.tvItemCGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.cgst_price?:0.0)
                )

                binding.tvItemSGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.sgst_price?:0.0)
                )



            }else{
                binding.cGSTLnLy.visibility = View.GONE
                binding.sGSTLnLy.visibility = View.GONE
                /*binding.iGSTLnLy.visibility = View.VISIBLE
                binding.tvItemIGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.igst_price)
                )*/

                if(cartObjDta?.igst_price != null && cartObjDta?.igst_price?:0.0 > 0){
                    binding.iGSTLnLy.visibility = View.VISIBLE

                    binding.tvItemIGST.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.igst_price?:0.0)
                    )
                }else{
                    binding.iGSTLnLy.visibility = View.GONE

                }
            }

            if(cartObjDta?.coupon_amount != null && cartObjDta?.coupon_amount?:0.0 > 0) {

                binding.discountLnLy.visibility = View.VISIBLE
                binding.amountLnLy.visibility = View.VISIBLE
                binding.txtDiscountValue.text = String.format(
                        getString(R.string.txt_unit_price),
                        Utils.setPrecesionFormate(cartObjDta?.coupon_amount?:0.0)
                )
                binding.txtAmountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.amount?:0.0)
                )
            }else{
                binding.discountLnLy.visibility = View.GONE
                binding.amountLnLy.visibility = View.GONE
            }

           /* binding.discountLnLy.visibility = View.VISIBLE
            binding.txtDiscountValue.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.coupon_amount)
            )*/


            if(cartObjDta?.couponInfo != null){
                binding.applyCoupanTxt.text = cartObjDta?.couponInfo?.code
                binding.arrowleft.visibility = View.GONE
                binding.cancelCoupon.visibility = View.VISIBLE
                binding.applyCoupanTxt.setTextColor(ContextCompat.getColor(this,R.color.avalibity_color))
            }else{
                binding.applyCoupanTxt.text = "APPLY PROMOCODE"
                binding.arrowleft.visibility = View.VISIBLE
                binding.cancelCoupon.visibility = View.GONE
                binding.applyCoupanTxt.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary))
            }



            binding.tvItemTotal.text = String.format(
                    getString(R.string.txt_unit_price),
                    Utils.setPrecesionFormate(cartObjDta?.total_amount?:0.0)
            )



            if (cartObjDta?.siteInfo != null) {

               /* setPrefrence(
                        cartObjDta?.siteInfo?._id,
                        cartObjDta?.siteInfo?.state_id,
                        cartObjDta?.siteInfo?.city_id,
                        cartObjDta?.siteInfo?.location?.coordinates?.get(1)?.toString(),
                        cartObjDta?.siteInfo?.location?.coordinates?.get(0)?.toString(),
                        cartObjDta?.siteInfo?.site_name,
                        cartObjDta?.siteInfo?.city_name
                )*/

                binding.btnAddNewSite.text = getString(R.string.change_new_site)
                binding.llAddress.visibility = View.VISIBLE

                binding.tvCompanyName?.text = cartObjDta?.siteInfo?.company_name
                binding.tvBuyerName?.text = cartObjDta?.siteInfo?.site_name

                var strBuffer = StringBuffer()
                strBuffer.append(cartObjDta?.siteInfo?.address_line1)
                strBuffer.append("\n")
                strBuffer.append(cartObjDta?.siteInfo?.address_line2)
                strBuffer.append("\n")
                strBuffer.append(cartObjDta?.siteInfo?.city_name)
                strBuffer.append(" - ")
                strBuffer.append(cartObjDta?.siteInfo?.pincode)
                strBuffer.append("\n")
                strBuffer.append(cartObjDta?.siteInfo?.state_name)
                strBuffer.append(",\n")
                strBuffer.append(cartObjDta?.siteInfo?.country_name)
                binding.tvAddress.setText(strBuffer.toString())

                binding.tvsitePersonName?.text = String.format(
                    getString(R.string.sitepersonName_str),
                    cartObjDta?.siteInfo?.person_name?:""
                )



                if(!cartObjDta?.siteInfo?.mobile_number.isNullOrEmpty()){
                    val str = String.format(
                        getString(R.string.site_phone_str),
                        cartObjDta?.siteInfo?.mobile_number?:""
                    )


                    cartObjDta?.siteInfo?.mobile_number?.let {
                        Utils.setSpanWithClick(
                            context = this,
                            view = binding.tvPhoneNumber,
                            fulltext = str,
                            subtext = it,
                            color = R.color.site_add_owner_txt_color,
                            phoneNumber = cartObjDta?.siteInfo?.mobile_number!!
                        )
                    }
                }else{
                    binding.tvPhoneNumber?.visibility = View.GONE
                }




            } else {
                binding.btnAddNewSite.text = getString(R.string.add_new_site)
                binding.llAddress.visibility = View.GONE
            }

            if (cartObjDta?.billingAddressInfo != null) {
                binding.btnAddNewBiilingAddress.text = getString(R.string.cart_change_billing_add_btn)
                binding.llbillAddress.visibility = View.VISIBLE
               // binding.tvBillingCompanyName.text = cartObjDta?.billingAddressInfo?.company_name
                if(cartObjDta?.billingAddressInfo?.full_name != null &&  cartObjDta?.billingAddressInfo?.full_name != " "){
                    binding.tvBillingCompanyName.text = cartObjDta?.billingAddressInfo?.full_name
                }else {
                    binding.tvBillingCompanyName.text = cartObjDta?.billingAddressInfo?.company_name
                }

                binding.tvbillingAddress.text = cartObjDta?.billingAddressInfo?.line1+",\n"+cartObjDta?.billingAddressInfo?.line2+"\n"+
                        cartObjDta?.billingAddressInfo?.city_name+" - "+cartObjDta?.billingAddressInfo?.pincode+"\n"+cartObjDta?.billingAddressInfo?.state_name+", "+
                        cartObjDta?.billingAddressInfo?.country_name
                //holder.personNameTxt.text = dta.person_name.toString()


                if(cartObjDta?.billingAddressInfo?.gst_number ==  " " ){
                    binding.tvGstBilling.visibility = View.GONE
                }else if(cartObjDta?.billingAddressInfo?.gst_number != null){
                    binding.tvGstBilling.visibility = View.VISIBLE
                    binding.tvGstBilling.text = "GST No : " + cartObjDta?.billingAddressInfo?.gst_number.toString()
                }else{
                    binding.tvGstBilling.visibility = View.GONE
                }



            }else{
                binding.btnAddNewBiilingAddress.text = getString(R.string.add_new_billing_btn)
                binding.llbillAddress.visibility = View.GONE
            }

        }
    }

    private fun getCartdata() {

        siteId = SharedPrefrence.getSiteId(this)
        stateId = SharedPrefrence.getStateId(this)
        lat = SharedPrefrence.getlat(this)?.toDouble()
        long = SharedPrefrence.getlang(this)?.toDouble()

        ApiClient.setToken()

        customMixAddToCartViewModel.getCartData(stateId,long, lat, null,null,null).observe(this, Observer{
                response ->
            when(response.status){

                        Status.SUCCESS -> {
                            stopAnim()

                            if (response.data?.isSuccessful!!) {

                                setResposerData(response = response.data.body()?.data!!)

                            } else {
                                emptyView()
                                //Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.LOADING -> {

                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                        }

                }
            })
    }


    private fun emptyView() {

        if (cartItemsSchema.size >0) {
            bindingEmptyCart.constrainEmptyCart.visibility = View.GONE
            binding.llCart.visibility = View.VISIBLE
            binding.llbutton.visibility = View.VISIBLE


        } else {
            binding.llCart.visibility = View.GONE
            bindingEmptyCart.constrainEmptyCart.visibility = View.VISIBLE
            binding.llbutton.visibility = View.GONE



        }
    }

    override fun onProductAddedCart(cartItems: CartItemLst?, cartItemsSchema: CartObjDta?) {
         stateId  = SharedPrefrence.getStateId(this)
         siteId = SharedPrefrence.getSiteId(this)
         long = SharedPrefrence.getlang(this)?.toDouble()
         lat = SharedPrefrence.getlat(this)?.toDouble()


        val objpost = AddToCartCustomMixObj(
                cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartItemsSchema?._id,
                cartItems?._id,
                cartItemsSchema?.user_id,
                siteId,
                long,
                lat,
                cartItems?.with_CP,cartItems?.with_TM,
                cartItems?.delivery_date!!,cartItems?.TM_no,
                cartItems?.driver_name,cartItems?.driver_mobile,Utils.sendDeliveryDate(endDate!!))
        sucessMsg = ""
        postCustomMixData(objpost)

    }


    fun onProductBuyerAddedCart(cartItems: CartItemLst?, tmNo:String?, driverName:String?, driverMoNo:String?) {
        stateId  = SharedPrefrence.getStateId(this)
        siteId = SharedPrefrence.getSiteId(this)
        long = SharedPrefrence.getlang(this)?.toDouble()
        lat = SharedPrefrence.getlat(this)?.toDouble()


        val objpost = AddToCartCustomMixObj(
                cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartObjDta?._id,
                cartItems?._id,
                cartObjDta?.user_id,
                siteId,
                long,
                lat,
                cartItems?.with_CP,cartItems?.with_TM,
                cartItems?.delivery_date!!,tmNo,
                driverName,driverMoNo,Utils.sendDeliveryDate(endDate!!))
        sucessMsg = ""
        postCustomMixData(objpost)
    }

    override fun onProductRemovedFromCart(cartItems: CartItemLst?, cartItemsSchema: CartObjDta?) {

        stateId  = SharedPrefrence.getStateId(this)
        siteId = SharedPrefrence.getSiteId(this)
        long = SharedPrefrence.getlang(this)?.toDouble()
        lat = SharedPrefrence.getlat(this)?.toDouble()


        val objpost = AddToCartCustomMixObj(cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartItemsSchema?._id,
                cartItems?._id,
                cartItemsSchema?.user_id,
                siteId,
                long,
                lat,
                cartItems?.with_CP,cartItems?.with_TM,
                cartItems?.delivery_date!!,cartItems?.TM_no,
                cartItems?.driver_name,cartItems?.driver_mobile,Utils.sendDeliveryDate(endDate!!))
        sucessMsg = ""
        postCustomMixData(objpost)

    }

    override fun onProductDelete(index: Int, itemId: String?,cart_Id:String?) {

        val fragment = BreakAlertDialog(
                object : BreakAlertDialog.ClickListener {
                    override fun onDoneClicked() {
                        deleteCartItem(index,itemId,cart_Id)
                    }

                    override fun onCancelClicked() {


                    }
                },
                getString(R.string.alert_msg_cart_delete),
                getString(R.string.txt_yes),
                getString(R.string.txt_cancel)
        )

        fragment.show(supportFragmentManager, "alert")
        fragment.isCancelable = false


    }

    override fun onDateChange(date: String?, cartItems: CartItemLst?, cartItemsSchema: CartObjDta?,endt:String) {

        stateId  = SharedPrefrence.getStateId(this)
        siteId = SharedPrefrence.getSiteId(this)
        long = SharedPrefrence.getlang(this)?.toDouble()
        lat = SharedPrefrence.getlat(this)?.toDouble()


        val objpost = AddToCartCustomMixObj(cartItems?.design_mix_id,
                cartItems?.custom_mix,
                cartItems?.quantity,
                stateId,
                cartItemsSchema?._id,
                cartItems?._id,
                cartItemsSchema?.user_id,
                siteId,
                long,
                lat,
                true,true,
                Utils.sendDeliveryDate(date!!),cartItems?.TM_no,
                cartItems?.driver_name,cartItems?.driver_mobile,Utils.sendDeliveryDate(endt!!))
        sucessMsg = "Date Updated Successfully"
        postCustomMixData(objpost)

    }


    private fun deleteCartItem(index: Int, itemId: String?,cart_Id:String?) {
        ApiClient.setToken()

        val obj = DeleteCartObj(itemId,cart_Id)

        customMixAddToCartViewModel.deleteCartItem(obj)
            .observe(this,Observer{
                    response ->
                when(response.status){

                            Status.SUCCESS -> {
                                stopAnim()
                                if (response.data?.isSuccessful!!) {
                                    deleteItem(index)
                                } else {
                                    Utils.setErrorData(this, response.data.errorBody())
                                }
                            }
                            Status.LOADING -> {
                                startAnim()
                            }
                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                            }

                    }
                })
    }


    private fun deleteItem(position: Int) {
        if (!cartObjDta?.items.isNullOrEmpty()) {
            cartObjDta!!.items!!.removeAt(position)
            cartAdapter.notifyDataSetChanged()
            getCartdata()
        }
    }

    override fun onBuyerInfoAdd(cartItems: CartItemLst?) {
       /* val intent = Intent(this, BuyerInfoActivity::class.java)
        intent.putExtra("isFromCart",true)
        intent.putExtra("cartItems",cartItems)
        startActivityForResult(intent,244)
        overridePendingTransition(R.anim.bottom_up, R.anim.nothing)*/


        startForResultByerInfo.launch(Intent(this, BuyerInfoActivity::class.java).apply {
            putExtra("isFromCart",true)
            putExtra("cartItems",cartItems)
        })
        overridePendingTransition(R.anim.bottom_up, R.anim.nothing)
    }

    private fun setPrefrence(


            siteId: String?,
            stateId: String?,
            cityId: String?,
            lat: String?,
            long: String?,
            siteName: String?,
            cityname: String?
    ) {
        if (!siteId.isNullOrEmpty()) {
            SharedPrefrence.setSiteId(
                    this,
                    siteId
            )
        }
        if (!stateId.isNullOrEmpty()) {
            SharedPrefrence.setStateId(
                    this,
                    stateId
            )
        }
        if (!cityId.isNullOrEmpty()) {
            SharedPrefrence.setCityId(
                    this,
                    cityId
            )
        }
        if (!lat.isNullOrEmpty()) {
            SharedPrefrence.setlat(
                    this,
                    lat
            )
        }
        if (!long.isNullOrEmpty()) {
            SharedPrefrence.setlang(
                    this,
                    long
            )
        }
        if (!siteName.isNullOrEmpty() && !cityname.isNullOrEmpty()) {
            val strBuffer = StringBuffer()
            strBuffer.append((siteName))
            strBuffer.append(" ")
            strBuffer.append(",")
            strBuffer.append(" ")
            strBuffer.append((cityname))
            SharedPrefrence.setSiteName(
                    this,
                    strBuffer.toString()
            )
        }
    }



    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.body()?.data != null) {

                                    if(!sucessMsg.isNullOrEmpty()) {
                                        Utils.showToast(
                                            this@CartActivity,
                                            sucessMsg,
                                            Toast.LENGTH_SHORT
                                        )
                                    }
                                    getCartdata()

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        binding.avLoading.visibility = View.GONE
                        binding.avLoading.hide()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                       /* binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()*/
                    }
                }
            }
        })
    }


    fun postCouponCode() {

        val cpObj = CouponObj("")

        ApiClient.setToken()
        applyCouponViewModel.postCouponCode(cpObj).observe(this,Observer{
                response ->
            when(response.status){

                    Status.SUCCESS -> {
                        stopAnim()

                        if (response.data?.isSuccessful!!) {
                            response.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.code() == 201) {

                                    Utils.showToast(this,"Coupon code removed successfully",Toast.LENGTH_SHORT)

                                    getCartdata()

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }

            }
        })
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?, paymentData: PaymentData?) {
        startAnim()
        CallCapture(razorpayPaymentId)

    }

    fun CallCapture(razorpayPaymentId: String?){
        val headerStr = String.format(getString(R.string.header_capture_str),"rzp_test_MDFf9gouEAhcyB","R82pv8IwsTCBeqOSeCftceQX")

        val data: ByteArray = headerStr.toByteArray(charset("UTF-8"))
        val base64 = Base64.encodeToString(data, Base64.NO_WRAP)

        val encodedString = "Basic "+ base64

        val paymentCapObj = PaymentCaptureObj(nwamnt!!,"INR")

        getPaymentCapture(encodedString,razorpayPaymentId!!,paymentCapObj,razorpayPaymentId)
    }

    fun getPaymentCapture(authheader:String, payment_id:String, paymentCaptureObj: PaymentCaptureObj, razorpayPaymentId: String?) {

        paymentCaptureViewModel.paymentCapture(authheader,payment_id,paymentCaptureObj).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        //stopAnim()
                        resource.data?.let {

                            if(resource.data?.code() == 200){
                                processApi(razorpayPaymentId)
                            }else{
                                retryCapturetError(razorpayPaymentId)
                            }

                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }

        })
    }

    private fun retryCapturetError(razorpayPaymentId: String?) {

        stopAnim()
        val dialogFragment: BreakAlertDialog = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {
                startAnim()
                CallCapture(razorpayPaymentId)
            }

            override fun onCancelClicked() {


            }
        }, "Please try again", "Ok", "")
        val fragmentManager: FragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(dialogFragment, "BreakAlertDialog.FRAGMENT_TAG").commitAllowingStateLoss()
        dialogFragment.show(this@CartActivity.supportFragmentManager, "Alert")
        dialogFragment.isCancelable = false

    }


    override fun onPaymentError(errorCode: Int, response: String?,paymentData: PaymentData?) {
        stopAnim()
        try {
            val jsonObject = JSONObject(response.toString())
            val errormsg = jsonObject.getJSONObject("error")
            val  discr = errormsg.getString("description")
            try {
                redirectToDialogPaymentError(discr)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } catch (err: JSONException) {
            Log.d("Error", err.toString())
        }

    }

    private fun redirectToDialogPaymentError(error: String?) {
        /*val fragment = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {

            }

            override fun onCancelClicked() {


            }
        }, String.format(error), "Ok", "")


        fragment.show(this@CartActivity.supportFragmentManager, "Alert")
        fragment.isCancelable = false*/


        val dialogFragment: BreakAlertDialog = BreakAlertDialog(object : BreakAlertDialog.ClickListener {
            override fun onDoneClicked() {

            }

            override fun onCancelClicked() {


            }
        }, String.format(error?:""), "Ok", "")
        val fragmentManager: FragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(dialogFragment, "BreakAlertDialog.FRAGMENT_TAG").commitAllowingStateLoss()
        dialogFragment.show(this@CartActivity.supportFragmentManager, "Alert")
        dialogFragment.isCancelable = false

    }

    private fun getUserPrfdataApiCall() {

        ApiClient.setToken()
        userProfileFrgViewModel.getUserPrfData().observe(this, Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            userProfileObj = resource.data.body()?.data
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }


}