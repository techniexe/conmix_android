package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.R
import com.conmix.app.data.ChnagePasswordProfileObj
import com.conmix.app.databinding.OtpActivityBinding
import com.conmix.app.services.SMSBroadcast.SmsVerifyCatcher
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ChangePasswordViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.regex.Pattern


/**
 * Created by Hitesh Patel on 22,July,2021
 */
class ChangePWOTPActivity : BaseActivity(), View.OnClickListener, TextView.OnEditorActionListener {
    private lateinit var binding: OtpActivityBinding
    private var mobNum:String? = null
    private var oldPass:String? = null
    private var newPass:String? = null
    private lateinit var changePasswrdViewModel: ChangePasswordViewModel

    private var otp: String? = ""
    private var cTimer: CountDownTimer? = null
    private var smsVerifyCatcher: SmsVerifyCatcher? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OtpActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fullScreen()
        setupViewModel()

        mobNum = intent.getStringExtra(CoreConstants.Intent.INTENT_MOB_NUMBER)
        oldPass  = intent.getStringExtra("oldPassword")
        newPass = intent.getStringExtra("newPassword")
        binding.btnVerify.isEnabled = false
        binding.btnVerify.alpha = 0.5f
        if (!mobNum.isNullOrEmpty()) {
            binding.tvDescription.visibility = View.VISIBLE

            binding.tvDescription.text = Utils.getMaskMobileNumber(mobNum,false,this)
        } else
            binding.tvDescription.visibility = View.GONE

        binding.backBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        smsVerifyCatcher?.setPhoneNumberFilter("VM-DSHSMS")
        //init SmsVerifyCatcher
        smsVerifyCatcher = SmsVerifyCatcher(
            this
        ) { message ->
            val code: String? = parseCode(message)

            //Parse verification code
            binding.codeEdtpin.setText(code) //set code in edit text
            //then you can send verification code to server
        }

        //smsVerifyCatcher.setFilter("regexp");
        reverseTimer(120)
        binding.btnVerify.setOnClickListener(this)
        binding.resendTxt.setOnClickListener(this)
        binding.callWithCodeText.setOnClickListener(this)

        binding.codeEdtpin.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                stopAnim()
                otp = binding.codeEdtpin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    val objpass = ChnagePasswordProfileObj(oldPass,newPass,otp)
                    updatePassWordfun(objpass)
                } else {
                    stopAnim()
                }
            }
            false
        }
        binding.codeEdtpin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //binding.btnVerify.isEnabled = s?.length == 6
                if(s?.length == 6){
                    binding.btnVerify.isEnabled = true
                    binding.btnVerify.alpha = 1.0f
                }else{
                    binding.btnVerify.isEnabled = false
                    binding.btnVerify.alpha = 0.5f
                }
            }

        })
    }


    private fun setupViewModel() {


        changePasswrdViewModel =
            ViewModelProvider(this, ChangePasswordViewModel.ViewModelFactory(ApiClient().apiService))
                .get(ChangePasswordViewModel::class.java)
    }
    private fun parseCode(message: String): String? {
        val p = Pattern.compile("\\b\\d{6}\\b")
        val m = p.matcher(message)
        var code: String? = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    override fun onStart() {
        super.onStart()
        smsVerifyCatcher!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        smsVerifyCatcher!!.onStop()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!)
        smsVerifyCatcher!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return false
    }


    private fun reverseTimer(Seconds: Int) {
        // tvResendCode.visibility = View.GONE

        binding.resendTxt.visibility = View.GONE

        binding.callWithCodeText.visibility = View.GONE
        binding.faltuLine.visibility = View.GONE
        binding.otpTimer.visibility = View.VISIBLE
        binding.otpTimer.text = "00:00"
        cTimer?.cancel()
        cTimer = object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val minutes = seconds / 60
                seconds %= 60
                binding.otpTimer.text = String.format("%02d", minutes) + ":" + String.format("%02d", seconds)

            }

            override fun onFinish() {

                binding.resendTxt.visibility = View.VISIBLE
                binding.callWithCodeText.visibility = View.GONE
                binding.faltuLine.visibility = View.GONE
                binding.otpTimer.visibility = View.GONE
                binding.resendTxt.text = getString(R.string.txt_resend_code)
            }
        }.start()
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
        //loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE
        }
        //loaderFrame.visibility = View.GONE
    }





    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnVerify -> {
                stopAnim()
                otp = binding.codeEdtpin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    val objpass = ChnagePasswordProfileObj(oldPass,newPass,otp)
                    updatePassWordfun(objpass)
                } else {
                    stopAnim()
                }
            }
            R.id.resend_txt -> {

                getOtp()

            }
            R.id.callWithCodeText->{

                getOtp()
            }
        }
    }

    private fun getOtp() {

        ApiClient.setToken()
        reverseTimer(120)
        changePasswrdViewModel.getUserOtp().observe(this, Observer{
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        when {
                            resource.data?.isSuccessful!! -> {

                            }
                            resource.data.code() == 400 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 422 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 500 -> {
                                Utils.showToast(
                                    this@ChangePWOTPActivity,
                                    getString(R.string.err_internal_server),
                                    Toast.LENGTH_SHORT
                                )
                            }
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        //startAnim()

                    }

                }
            }
        })

    }


    private fun updatePassWordfun(obj: ChnagePasswordProfileObj){

        ApiClient.setToken()

        changePasswrdViewModel.changePasswordData(obj)
            .observe(this, Observer{
                    response ->
                when(response.status){
                    Status.SUCCESS -> {
                        stopAnim()
                        if (response.data?.isSuccessful!!) {

                            if(response.data?.code() == 201){
                                Utils.showToast(
                                    this,
                                    "Password updated successfully",
                                    Toast.LENGTH_LONG
                                )
                                finish()
                                Bungee.slideRight(this)
                            }else if(response.data?.code() == 400){
                                cTimer?.cancel()
                                binding.resendTxt.visibility = View.VISIBLE
                                binding.callWithCodeText.visibility = View.GONE
                                binding.otpTimer.visibility = View.GONE
                                Utils.setErrorData(this, response.data.errorBody())
                            }else if(response.data?.code() == 422){
                                cTimer?.cancel()
                                binding.resendTxt.visibility = View.VISIBLE
                                binding.otpTimer.visibility = View.GONE
                                binding.callWithCodeText.visibility = View.GONE
                                Utils.setErrorData(this, response.data.errorBody())
                            }else if(response.data?.code() == 500){
                                cTimer?.cancel()
                                binding.resendTxt.visibility = View.VISIBLE
                                binding.otpTimer.visibility = View.GONE
                                binding.callWithCodeText.visibility = View.GONE
                                Utils.showToast(
                                    this@ChangePWOTPActivity,
                                    getString(R.string.err_internal_server),
                                    Toast.LENGTH_SHORT
                                )
                            }

                            //response.let { response -> setData(response.data?.body()?.data) }
                        } else {
                            cTimer?.cancel()
                            binding.resendTxt.visibility = View.VISIBLE
                            binding.otpTimer.visibility = View.GONE
                            binding.callWithCodeText.visibility = View.GONE
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }

                    Status.ERROR -> {
                        stopAnim()
                        cTimer?.cancel()
                        binding.resendTxt.visibility = View.VISIBLE
                        binding.otpTimer.visibility = View.GONE
                        binding.callWithCodeText.visibility = View.GONE
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }

                }
            })
    }
}