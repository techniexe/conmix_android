package com.conmix.app.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.adapter.TrackOrderAdapter
import com.conmix.app.data.TrackOrderLstobj
import com.conmix.app.databinding.ActivityTrackOrderListBinding
import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.TrackOrderViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 17,May,2021
 */
class TrackActivity : AppCompatActivity() {
    var orderId: String? = null
    var orderItemId: String? = null
    var displayId: String? = null
    var orderDate: String? = null

    private lateinit var binding: ActivityTrackOrderListBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding
    private lateinit var trackOrderViewModel: TrackOrderViewModel
    var myTrackOrderList = ArrayList<TrackOrderLstobj>()
    var orderListAdapter: TrackOrderAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackOrderListBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)
        setupViewModel()
        getIntentData()
        getTrackOrderList()

        binding.imgBackTrack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

    }

    private fun getIntentData() {
        orderId = intent.getStringExtra(CoreConstants.Intent.INTENT_ORDER_ID)
        orderDate = intent.getStringExtra(CoreConstants.Intent.INTENT_CREATED_DATE)
        orderItemId = intent.getStringExtra(CoreConstants.Intent.INTENT_ORDER_ITEM_ID)
        displayId = intent.getStringExtra(CoreConstants.Intent.INTENT_ORDER_DIAPLAY_ID)

    }


    private fun getTrackOrderList() {
        ApiClient.setToken()
        trackOrderViewModel.getTrackOrderLstData(orderItemId).observe(this, Observer {

            it?.let {

                    resource ->
                when (resource.status) {
                    Status.LOADING -> {
                        startAnim()

                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                setResposerData(response.body()?.data)
                            }
                        } else {
                            emptyView()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }

        })
    }


    override fun onBackPressed() {
        binding.imgBackTrack.performClick()
    }

    private fun setupViewModel() {


        trackOrderViewModel = ViewModelProvider(
            this,
            TrackOrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(TrackOrderViewModel::class.java)


    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()

    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()
    }

    private fun setResposerData(response: ArrayList<TrackOrderLstobj>?) {
        if (response != null) {
            this.myTrackOrderList = response
        }
        emptyView()
        setupUI()

    }

    private fun setupUI() {
        binding.tvTitle.text = getString(R.string.txt_delivery_title)
        binding.tvOrderID.text = String.format(getString(R.string.txt_order_item), displayId)

        binding.tvLeft.text = "Order Date : " + Utils.getConvertDate(orderDate!!)


        orderListAdapter = TrackOrderAdapter(this, myTrackOrderList)
        binding.rvOrderlist.addItemDecoration(
            DividerItemDecoration(
                binding.rvOrderlist.context,
                (binding.rvOrderlist.layoutManager as LinearLayoutManager).orientation
            )
        )
        binding.rvOrderlist.adapter = orderListAdapter

    }

    private fun emptyView() {
        if (myTrackOrderList.isNotEmpty()) {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE
            binding.rvOrderlist.visibility = View.VISIBLE
            binding.tvLeft.visibility = View.VISIBLE
            binding.vwLine.visibility = View.VISIBLE

        } else {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE
            bindingEmptyOrder.tvNoData.text = getString(R.string.tm_assign_lst_err)
            binding.rvOrderlist.visibility = View.GONE
            binding.tvLeft.visibility = View.GONE
            binding.vwLine.visibility = View.GONE
        }
    }
}