package com.conmix.app.activities


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.R
import com.conmix.app.data.UpdateProfileOtpObj
import com.conmix.app.databinding.EmailOtpActivityBinding
import com.conmix.app.databinding.NewEmailAddActivityBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.OTPViewModel
import com.conmix.app.viewmodels.SignUpRegisterViewModel
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 01,December,2021
 */
class NewEmailAddActivity: BaseActivity() {
    private lateinit var binding: NewEmailAddActivityBinding
    private lateinit var signUpRegisterViewModel: SignUpRegisterViewModel
    private lateinit var otpViewModel: OTPViewModel
    private  var mobileNumEdt: String? = null
    private  var emailAddEdt: String? = null
    var isEmail = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = NewEmailAddActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        changeStatusBarColor(ContextCompat.getColor(this, R.color.white))
        setupViewModel()
        getIntentdate()

        if(isEmail){
            binding.otpTiltleTxt.text = "Add New Email Address"
            binding.textInputMobileno.visibility = View.GONE
            binding.textInputEmailId.visibility = View.VISIBLE
        }else{
            binding.otpTiltleTxt.text = "Add New Mobile No."
            binding.textInputMobileno.visibility = View.VISIBLE
            binding.textInputEmailId.visibility = View.GONE
        }


        binding.backBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        binding.btnVerify.setOnClickListener {


            if(isEmail){
                if(validUserData()){
                    val obj = UpdateProfileOtpObj(null,null,null,binding.etEmailEdt.text.toString().trim())
                    getUpdateEmailOtp(obj)
                }
            }else{
                Utils.hideKeyboard(binding.btnVerify)
                mobileNumEdt = binding.etMobileEdt.text.toString().trim()

               /* val phoneUtil = PhoneNumberUtil.getInstance()
                mobileNumEdt = binding.etMobileEdt.text.toString().trim()
                if (mobileNumEdt?.isNotEmpty() == true) {

                    try {

                        val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccpL.selectedCountryNameCode)
                        mobileNumEdt = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)
                        if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType( numberProto) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE) {


                        } else {
                            stopAnim()
                            Utils.showToast(this,getString(R.string.err_invalid_mobile), Toast.LENGTH_SHORT)
                        }
                    } catch (e: NumberParseException) {
                        stopAnim()

                        Utils.showToast(this,getString(R.string.err_invalid_mobile), Toast.LENGTH_SHORT)


                    }
                } else {
                    stopAnim()
                    Utils.showToast(this,getString(R.string.err_invalid_mobile), Toast.LENGTH_SHORT)
                }*/

                if(validUserData()){
                val obj = UpdateProfileOtpObj(null,mobileNumEdt,null,null)
                getUpdateEmailOtp(obj)
                }
            }


        }
    }
    private fun getIntentdate() {

        isEmail = intent.getBooleanExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS,false)

    }
    fun setupViewModel() {
        signUpRegisterViewModel = ViewModelProvider(this,SignUpRegisterViewModel.ViewModelFactory(
            ApiClient().apiService)).get(SignUpRegisterViewModel::class.java)
        otpViewModel =
            ViewModelProvider(this, OTPViewModel.ViewModelFactory(ApiClient().apiService))
                .get(OTPViewModel::class.java)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.backBtn.performClick()
    }

    private fun validUserData(): Boolean {

       // mobileNumEdt = binding.etMobileNo.text.toString().trim()
        emailAddEdt = binding.etEmailEdt.text.toString().trim()

        val phoneUtil = PhoneNumberUtil.getInstance()


        if(isEmail){
            if (emailAddEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_email), Toast.LENGTH_SHORT)
                return false
            }

            if (!emailAddEdt.isNullOrEmpty()) {

                val emval = Utils.isValidEmail(emailAddEdt)
                if (!emval) {
                    Utils.showToast(this, getString(R.string.txt_email_valid), Toast.LENGTH_SHORT)
                    return false
                }
            }
        }else {

            if (mobileNumEdt.isNullOrEmpty()) {
                Utils.showToast(this, getString(R.string.txt_mobile), Toast.LENGTH_SHORT)
                return false
            }


            if (mobileNumEdt?.isNotEmpty() == true) {
                try {

                    val numberProto = phoneUtil.parse(mobileNumEdt, binding.ccpL.selectedCountryNameCode)
                    mobileNumEdt = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                            numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {
                        return true
                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_mobile),
                            Toast.LENGTH_SHORT
                        )
                        return false
                    }
                } catch (e: NumberParseException) {
                    Utils.showToast(
                        this,
                        getString(R.string.err_invalid_number),
                        Toast.LENGTH_SHORT
                    )
                    return false

                }

            }
        }
        return true
    }


    private fun getUpdateEmailOtp(updateProfileOtpObj: UpdateProfileOtpObj) {
        ApiClient.setToken()
        otpViewModel.getOTPByUpdateEmail(updateProfileOtpObj)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            stopAnim()
                            when {
                                resource.data?.isSuccessful!! -> {

                                    Utils.showToast(
                                        this@NewEmailAddActivity,
                                        getString(R.string.err_send),
                                        Toast.LENGTH_SHORT
                                    )

                                    if(isEmail){
                                        val intent = Intent(this@NewEmailAddActivity, NewEmailOTPActivity::class.java)
                                        intent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, true)
                                        intent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER,binding.etEmailEdt.text.toString().trim())
                                        startActivity(intent)
                                        Bungee.slideLeft(this)
                                    }else{
                                        val intent = Intent(this@NewEmailAddActivity, NewEmailOTPActivity::class.java)
                                        intent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, false)
                                        intent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER,mobileNumEdt)
                                        startActivity(intent)
                                        Bungee.slideLeft(this)
                                    }


                                }
                                resource.data.code() == 400 -> {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                                resource.data.code() == 422 -> {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }
                                resource.data.code() == 500 -> {
                                    Utils.showToast(
                                        this@NewEmailAddActivity,
                                        getString(R.string.err_internal_server),
                                        Toast.LENGTH_SHORT
                                    )
                                }
                            }
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            })
    }
    private fun startAnim() {
        binding.avLoading.show()
    }

    private fun stopAnim() {
        binding.avLoading.hide()
    }
}