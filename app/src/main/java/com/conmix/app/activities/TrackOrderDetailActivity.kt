package com.conmix.app.activities

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.adapter.TrackOrderDetailAdapter
import com.conmix.app.bottomsheet.OrderStatusBottomsheet
import com.conmix.app.bottomsheet.TMStatusBottomsheet
import com.conmix.app.data.OrderDetailItemDta
import com.conmix.app.data.TrackOrderLstobj
import com.conmix.app.databinding.ActivityTrackOrderDetailBinding

import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.TrackOrderViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 17,May,2021
 */
class TrackOrderDetailActivity:AppCompatActivity() {

    private lateinit var binding: ActivityTrackOrderDetailBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding
    private lateinit var trackOrderViewModel: TrackOrderViewModel
    var orderDate: String? = null
    var displayId: String? = null
    private lateinit var trackOrderDetailAdapter: TrackOrderDetailAdapter
    var orderItemId: String? = null
    var trackId: String? = null
    private lateinit var orderTrackDetails: TrackOrderLstobj
    var isShowMenu:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackOrderDetailBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)
        setupViewModel()
        getIntentData()
        getTrackOrderList()

        binding.imgBackTrack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
    }

    override fun onBackPressed() {
        binding.imgBackTrack.performClick()
    }

    private fun setupViewModel() {


        trackOrderViewModel = ViewModelProvider(
            this,
            TrackOrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(TrackOrderViewModel::class.java)


    }

    private fun getTrackOrderList() {
        ApiClient.setToken()
        trackOrderViewModel.getTrackOrderDtlData(orderItemId, trackId).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                        startAnim()

                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                setResposerData(response.body()?.data)
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }

        })
    }

    private fun emptyView() {
        if (orderTrackDetails != null) {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE
            binding.rvOrderDetaillist.visibility = View.VISIBLE
            binding.tvOrderID.visibility = View.VISIBLE
            binding.tvLeft.visibility = View.VISIBLE
            binding.vwLine.visibility = View.VISIBLE

        } else {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE
            binding.rvOrderDetaillist.visibility = View.GONE
            binding.tvOrderID.visibility = View.GONE
            binding.tvLeft.visibility = View.GONE
            binding.vwLine.visibility = View.GONE
        }
    }

    private fun setResposerData(response: TrackOrderLstobj?) {
        if (response != null) {
            orderTrackDetails = response
        }
        emptyView()
        setupUI()

    }

    private fun setupUI() {
        binding.tvTitle.text = getString(R.string.txt_truck_title)
        binding.tvOrderID.text = String.format(getString(R.string.txt_order), displayId)
        binding.tvLeft.text = "Order Date : " + Utils.getConvertDate(orderDate!!)


        var strBuffer = StringBuffer()
        strBuffer.append(setCategoryName())

        binding.tvItemTitle.text = strBuffer.toString()
        binding.tvSubTitle.text = String.format(
            getString(R.string.txt_track_pickup),
            orderTrackDetails?.pickup_quantity?.toInt()
        )
       /* Utils.setImageUsingGlide(
            this,
            orderTrackDetails?.sub_category_image_url,
            binding.imgTrackOrder
        )
        if (orderTrackDetails?.source_name != null) {
            binding.tvTrackPlace.text =
                orderTrackDetails.source_name
        } else {
            binding.tvTrackPlace.text = ""
        }*/

       /* if (orderTrackDetails.royality_quantity != null) {
            if (orderTrackDetails.royality_quantity?.toInt()!! > 0) {
                binding.tvRoyaltipass.text = String.format(
                    getString(R.string.txt_royalti),
                    orderTrackDetails.royality_quantity?.toInt()
                )
            }
        } else {
            binding.tvRoyaltipass.text = String.format(getString(R.string.txt_royalti), "0")
        }*/

        if(!orderTrackDetails?.qube_test_report_7days.isNullOrEmpty()){
            isShowMenu = true
        }

        if(!orderTrackDetails?.qube_test_report_28days.isNullOrEmpty()){
            isShowMenu = true
        }
        if(!orderTrackDetails?.invoice_url.isNullOrEmpty()){
            isShowMenu = true
        }


        if(isShowMenu){
            binding.imgMenuBtn.visibility = View.VISIBLE
        }else{
            binding.imgMenuBtn.visibility = View.GONE
        }

        binding.imgMenuBtn?.setOnClickListener {
            popupMenu(this,orderTrackDetails)
        }

        trackOrderDetailAdapter = TrackOrderDetailAdapter(this, orderTrackDetails)
        binding.rvOrderDetaillist.adapter = trackOrderDetailAdapter


    }

    private fun popupMenu(mcontext: Context, dta: TrackOrderLstobj?) {

        var orderOption = TMStatusBottomsheet(mcontext,dta)
        orderOption.show(supportFragmentManager,"BottomDialogFragment")

    }

    private fun setCategoryName(): String? {
        //return orderTrackDetails.concrete_grade_name + " - " + orderTrackDetails.designMixDetails?.product_name

        return if(orderTrackDetails?.designMixDetails != null){
            orderTrackDetails.concrete_grade_name + " - " + orderTrackDetails.designMixDetails?.product_name
        }else{
            orderTrackDetails.concrete_grade_name+" - Custom Mix"
        }
    }


    private fun getIntentData() {
        if (intent != null) {
            orderItemId = intent.getStringExtra(CoreConstants.Intent.INTENT_ORDER_ITEM_ID)
            trackId = intent.getStringExtra(CoreConstants.Intent.INTENT_TRACK_ID)
            orderDate = intent.getStringExtra(CoreConstants.Intent.INTENT_CREATED_DATE)
            displayId = intent.getStringExtra(CoreConstants.Intent.INTENT_ORDER_DIAPLAY_ID)
        }
    }


    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()

    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()
    }
}