package com.conmix.app.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.R
import com.conmix.app.adapter.TMCPAssignAdapter
import com.conmix.app.data.orderItemPartDataObj
import com.conmix.app.databinding.NoOrderBinding
import com.conmix.app.databinding.TmcpassignlistactivityBinding
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 19,August,2021
 */
class TMCPAssignActivity : AppCompatActivity() {
    private lateinit var binding: TmcpassignlistactivityBinding
    private lateinit var bindingEmptyOrder: NoOrderBinding

    var myTrackOrderList = ArrayList<orderItemPartDataObj>()
    var orderListAdapter: TMCPAssignAdapter? = null
    var displayId: String? = null
    var displayItemId:String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TmcpassignlistactivityBinding.inflate(layoutInflater)
        bindingEmptyOrder = binding.emptyOrderList
        setContentView(binding.root)

        getIntentData()


        binding.imgBackTrack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

    }

    private fun getIntentData() {
        myTrackOrderList = intent.getParcelableArrayListExtra<orderItemPartDataObj>("orderItemPartDataObj")?: ArrayList()
        displayId = intent.getStringExtra("displayId")
        displayItemId = intent.getStringExtra("productid")

        setResposerData()
    }


    override fun onBackPressed() {
        binding.imgBackTrack.performClick()
    }


    private fun setResposerData() {


        setupUI()

    }

    private fun setupUI() {
        binding.tvOrderID.text = String.format(getString(R.string.cp_assign_txt_order), displayItemId)
        if(myTrackOrderList.size > 0 ){
            orderListAdapter = TMCPAssignAdapter(this, myTrackOrderList,displayId)
            binding.rvOrderlist.addItemDecoration(
                DividerItemDecoration(
                    binding.rvOrderlist.context,
                    (binding.rvOrderlist.layoutManager as LinearLayoutManager).orientation
                )
            )
            binding.rvOrderlist.adapter = orderListAdapter
        }else{
            emptyView()
        }
    }

    private fun emptyView() {
        if (myTrackOrderList.isNotEmpty()) {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.GONE
            binding.rvOrderlist.visibility = View.VISIBLE
        } else {
            bindingEmptyOrder.constrainEmptyOrder.visibility = View.VISIBLE
            bindingEmptyOrder.tvNoData.text = getString(R.string.cp_assign_lst_err)
            binding.rvOrderlist.visibility = View.GONE

        }
    }
}