package com.conmix.app.activities


import android.content.Intent
import android.os.Bundle
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.BuildConfig
import com.conmix.app.R
import com.conmix.app.data.AddToCartCustomMixObj
import com.conmix.app.data.SessionRequest
import com.conmix.app.data.SessionResponse
import com.conmix.app.databinding.SignupActivityBinding
import com.conmix.app.dialogs.LoginDialog
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.SessionViewModel
import com.conmix.app.viewmodels.SignUpViewModel
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 25,February,2021
 */
class SignUpActivity: BaseActivity() {
    private lateinit var binding: SignupActivityBinding
    private lateinit var signupViewModel: SignUpViewModel
    private lateinit var sessionViewModel: SessionViewModel
    var mobNum = ""
    var isFromProductDetail: Boolean = false
    var objCart: AddToCartCustomMixObj? = null
    var isEmail = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SignupActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fullScreen()
        setupUI()
        setupViewModel()
        getIntentdate()
        binding.backBtn.setOnClickListener {
            finish()

        }

        /*binding.btnSignUp.setOnClickListener {
            val intent = Intent(this, OTPActivity::class.java)
            startActivity(intent)
        }*/

        binding.etMobileNo.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signUp(binding.etMobileNo)
            }
            false
        }
        binding.btnSignUp.setOnClickListener { v ->

            signUp(v)

        }

    }
    private fun getIntentdate() {
        isFromProductDetail = intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)
        objCart = intent.getParcelableExtra("cartObj")

    }

    private fun setupUI() {
        binding.etMobileNo.filters = arrayOf(
            Utils.ignoreFirstWhiteSpace())


        val signupLinkClickSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
               /* val intent = Intent(this@SignUpActivity, LoginActivity::class.java)
                intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, true)
                intent.putExtra("cartObj",objCart)
                startActivity(intent)
                finish()
                Bungee.slideLeft(this@SignUpActivity)*/

                /*if(isFromProductDetail){

                    var loginDialog = LoginDialog(true,objCart)
                    loginDialog.show(supportFragmentManager, "loginDialog")
                    Bungee.slideUp(this@SignUpActivity)
                    finish()
                }else{
                    val intent = Intent(this@SignUpActivity, LoginActivity::class.java)
                    intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, true)
                    intent.putExtra("cartObj",objCart)
                    startActivity(intent)
                    finish()
                    Bungee.slideLeft(this@SignUpActivity)
                }*/


                val intent = Intent(this@SignUpActivity, LoginActivity::class.java)
                intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, true)
                intent.putExtra("cartObj",objCart)
                startActivity(intent)
                finish()
                Bungee.slideLeft(this@SignUpActivity)

            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val clickableText = arrayOf(getString(R.string.txt_account_login_sub_msg))
        val clickableSpan: Array<ClickableSpan> = arrayOf(signupLinkClickSpan)
        Utils.setStringSpanable(binding.loginTxt, clickableText, clickableSpan)

    }

    private fun setupViewModel() {

        sessionViewModel =
                ViewModelProvider(this, SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth))
                        .get(SessionViewModel::class.java)



        signupViewModel =
                ViewModelProvider(this, SignUpViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(SignUpViewModel::class.java)
    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()

        // loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()

        //loaderFrame.visibility = View.GONE
    }

    private fun signUp(v: View) {
        ApiClient.setToken()
        Utils.hideKeyboard(v)
        startAnim()
        val phoneUtil = PhoneNumberUtil.getInstance()
        mobNum = binding.etMobileNo.text.toString().trim()
        if (mobNum.isNotEmpty()) {

            try {

                val numberProto = phoneUtil.parse(mobNum, binding.ccp.selectedCountryNameCode)
                mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)
                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType( numberProto) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE) {

                    SharedPrefrence.setIsMobileSignUp(this, true)
                    getSessionToken(true)
                } else {
                    stopAnim()
                    Utils.showToast(this,getString(R.string.err_ragister_mobileemail_number), Toast.LENGTH_SHORT)
                }
            } catch (e: NumberParseException) {
                stopAnim()

                /*Utils.showToast(
                        this,
                        getString(R.string.err_require_mobile_number),
                        Toast.LENGTH_SHORT
                )*/

                if (mobNum.isNotEmpty()) {

                    val emval = Utils.isValidEmail(mobNum)
                    if (emval) {
                        isEmail = true
                        getSessionToken(true)
                    } else {
                        Utils.showToast(
                                this,
                                getString(R.string.err_ragister_mobileemail_number),
                                Toast.LENGTH_SHORT
                        )

                    }
                }
            }
        } else {
            stopAnim()
            Utils.showToast(
                    this,
                    getString(R.string.err_require_mobile_number),
                    Toast.LENGTH_SHORT
            )
        }

    }

    private fun getSessionToken(otpCall: Boolean) {
        ApiClient.setToken()
        if (otpCall) {
            stopAnim()
        }

        val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

        sessionViewModel.getSession(sessionRequest = sessionRequest)
            .observe(this, Observer{
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {

                                resource.data.let { response ->
                                    setData(response.body()?.data)
                                    if (otpCall) {

                                        if(!isEmail){
                                            getOtp()
                                        }else{
                                            getOtpEmail()
                                        }
                                    }
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {


                        }

                    }
                }
            })
    }



    private fun setData(response: SessionResponse?) {
        SharedPrefrence.setSessionToken(this, response?.token)
    }

    private fun getOtp() {

        ApiClient.setToken()

        signupViewModel.getOtpbyMob(mobNum, "sms").observe(this, Observer{
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        when {
                            resource.data?.isSuccessful!! -> {

                                Utils.showToast(
                                    this@SignUpActivity,
                                    getString(R.string.err_send),
                                    Toast.LENGTH_SHORT
                                )

                                val otpIntent = Intent(this@SignUpActivity, OTPActivity::class.java)
                                otpIntent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER, mobNum)
                                otpIntent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, isEmail)
                                otpIntent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL,isFromProductDetail)
                                otpIntent.putExtra("cartObj",objCart)
                                startActivity(otpIntent)
                                if (isFromProductDetail)
                                    finish()
                                Bungee.slideLeft(this)
                            }
                            resource.data.code() == 400 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 422 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 500 -> {
                                Utils.showToast(
                                    this@SignUpActivity,
                                    getString(R.string.err_internal_server),
                                    Toast.LENGTH_SHORT
                                )
                            }
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }

                }
            }
        })

    }

    private fun getOtpEmail() {

        ApiClient.setToken()

        signupViewModel.getOtpByEmail(mobNum).observe(this, Observer{
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        stopAnim()
                        when {
                            resource.data?.isSuccessful!! -> {

                                Utils.showToast(
                                    this@SignUpActivity,
                                    getString(R.string.err_send),
                                    Toast.LENGTH_SHORT
                                )

                                val otpIntent = Intent(this@SignUpActivity, OTPActivity::class.java)
                                otpIntent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER, mobNum)
                                otpIntent.putExtra(CoreConstants.Intent.INTENT_EMAIL_ADDRESS, isEmail)
                                otpIntent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL,isFromProductDetail)
                                otpIntent.putExtra("cartObj",objCart)
                                startActivity(otpIntent)
                                if (isFromProductDetail)
                                    finish()
                                Bungee.slideLeft(this)
                            }
                            resource.data.code() == 400 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 422 -> {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                            resource.data.code() == 500 -> {
                                Utils.showToast(
                                    this@SignUpActivity,
                                    getString(R.string.err_internal_server),
                                    Toast.LENGTH_SHORT
                                )
                            }
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }

                }
            }
        })

    }

}