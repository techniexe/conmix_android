package com.conmix.app.activities


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.InputFilter
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.conmix.app.BuildConfig
import com.conmix.app.R
import com.conmix.app.data.*
import com.conmix.app.databinding.LoginActivityBinding
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel
import com.conmix.app.viewmodels.LoginViewModel
import com.conmix.app.viewmodels.NotificationViewModel
import com.conmix.app.viewmodels.SessionViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 24,February,2021
 */
class LoginActivity: BaseActivity() {

    private lateinit var binding: LoginActivityBinding
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    var productId: String? = null
    var subCategoryId: String? = null
    var isFromProductDetail: Boolean = false
    var objCart: AddToCartCustomMixObj? = null
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)


        fullScreen()
        setupViewModel()
        setupUI()
        getIntentdate()

        /*binding.signUpTxt.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, isFromProductDetail)
            intent.putExtra("cartObj",objCart)
            startActivity(intent)
        }*/
        binding.forgotpassword.setOnClickListener {
            val intent = Intent(this, ForgotPassWordActivity::class.java)
            intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, isFromProductDetail)
            intent.putExtra("cartObj",objCart)
            startActivity(intent)
        }

        val isAuthErr = intent.getBooleanExtra(CoreConstants.Intent.INTENT_AUTH_ERROR, false)
        if (isAuthErr) {
            // removeFCMToken()
            Utils.showToast(this, getString(R.string.login_again), Toast.LENGTH_SHORT)
        }
        getSessionToken(null, null)

        binding.etPassword.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signIn(binding.etPassword)
            }
            false
        }
        binding.btnSignIn.setOnClickListener { v ->

            signIn(v)

        }

        binding.forgotpassword.setOnClickListener {

            val intent = Intent(this, ForgotPassWordActivity::class.java)
            intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, isFromProductDetail)
            intent.putExtra("cartObj",objCart)
            startActivity(intent)
            Bungee.slideLeft(this)
        }

    }

    private fun getSessionToken(userName: String?, password: String?) {
        ApiClient.setToken()
        val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

        sessionViewModel.getSession(sessionRequest = sessionRequest)
                .observe(this,androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                if (resource.data?.isSuccessful!!) {
                                    resource.data?.let { response ->
                                        setData(
                                                response.body()?.data,
                                                userName,
                                                password
                                        )
                                    }
                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }

                            }
                            Status.ERROR -> {
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                            Status.LOADING -> {

                            }

                        }
                    }
                })
    }

    private fun setData(response: SessionResponse?, userName: String?, password: String?) {
        if (response != null) {
            SharedPrefrence.setSessionToken(this, response.token)
        }

        if (userName != null && password != null) {
            login(userName, password)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        //Bungee.slideRight(this)

        if(isFromProductDetail){
            finish()
        }else{
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }



    }

    private fun setupUI() {
        binding.etMobileNo.filters = arrayOf(
                ignoreFirstWhiteSpace()

                )
        binding.etPassword.filters = arrayOf(
                ignoreFirstWhiteSpace(),
                InputFilter.LengthFilter(20)

        )

        val signupLinkClickSpan = object : ClickableSpan() {
            override fun onClick(view: View) {


                val intent = Intent(this@LoginActivity, SignUpActivity::class.java)
                intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, isFromProductDetail)
                intent.putExtra("cartObj",objCart)
                startActivity(intent)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val clickableText = arrayOf(getString(R.string.txt_account_sub_msg))
        val clickableSpan: Array<ClickableSpan> = arrayOf(signupLinkClickSpan)
        Utils.setStringSpanable(binding.signUpTxt, clickableText, clickableSpan)

    }

    private fun setupViewModel() {

        sessionViewModel =
                ViewModelProvider(
                        this,
                        SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
                ).get(SessionViewModel::class.java)

        loginViewModel =
                ViewModelProvider(this, LoginViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(LoginViewModel::class.java)

        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)

        customMixAddToCartViewModel = ViewModelProvider(
            this,
            CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(CustomMixAddToCartViewModel::class.java)

    }

    private fun startAnim() {
        if (binding.avLoading != null)
            binding.avLoading.show()

    }

    private fun stopAnim() {
        if (binding.avLoading != null)
            binding.avLoading.hide()

    }


    private fun getIntentdate() {
        isFromProductDetail = intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)
        productId = intent.getStringExtra(CoreConstants.Intent.INTENT_PRODUCT_ID)
        subCategoryId = intent.getStringExtra(CoreConstants.Intent.INTENT_SUB_CATEGORY_ID)
        objCart = intent.getParcelableExtra("cartObj")

    }

    private fun validateFields(identifier: String, password: String): Boolean {
        if (identifier.isEmpty()) {
            Utils.showToast(
                    this,
                    getString(R.string.err_ragister_mobile_number),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        if (password.isEmpty()) {
            Utils.showToast(this, getString(R.string.err_password), Toast.LENGTH_SHORT)
            return false
        }
        if (password.length < 6) {

            Utils.showToast(
                    this,
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        return true
    }

    private fun login(userName: String, password: String) {
        ApiClient.setToken()
        loginViewModel.loginWithIdentifier(
                userName = userName,
                loginReqModel = LoginReqModel(password)
        ).observe(this, Observer{

            it?.let {

                resource ->

                when (resource.status) {

                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {


                            setResponseFromLogin(resource.data.body()!!.data)
                        } else {
                            stopAnim()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })
    }

    private fun signIn(v: View) {
        Utils.hideKeyboard(v)
        if (Utils.isNetworkAvailable(this)) {
            val userName = binding.etMobileNo.text.toString().trim()
            val password = binding.etPassword.text.toString().trim()
            val phoneUtil = PhoneNumberUtil.getInstance()

            if (validateFields(userName, password)) {
                try {

                    val numberProto = phoneUtil.parse(userName, binding.ccpL.selectedCountryNameCode)
                    val mobNum =
                            phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                                    numberProto
                            ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {
                        getSessionToken(mobNum, password)
                    } else {

                        Utils.showToast(
                                this,
                                getString(R.string.err_ragister_mobileemail_number),
                                Toast.LENGTH_SHORT
                        )
                    }
                } catch (e: NumberParseException) {

                    if (userName.isNotEmpty()) {

                        val emval = Utils.isValidEmail(userName)
                        if (emval) {
                            getSessionToken(userName, password)
                        } else {
                            Utils.showToast(
                                    this,
                                    getString(R.string.err_ragister_mobileemail_number),
                                    Toast.LENGTH_SHORT
                            )

                        }
                    }


                }
            }
        } else {
            Utils.showToast(this, getString(R.string.no_internet_msg), Toast.LENGTH_SHORT)
        }
    }


    private fun setResponseFromLogin(loginResponse: LoginResponse) {
        val customToken = loginResponse.customToken
        SharedPrefrence.setSessionToken(this, customToken)
        SharedPrefrence.setLogin(this, true)
        registerFCMToken()
    }
    private fun navigateToMainActivity() {

        if (isFromProductDetail) {
           /* val intent = Intent(this, ProductDetailActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_PRODUCT_ID, productId)
            intent.putExtra(CoreConstants.Intent.INTENT_SUB_CATEGORY_ID, subCategoryId)
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)*/
            postCustomMixData(objCart)
        } else {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)
        }
    }

    private fun registerFCMToken() {

        /*Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                //Log.w(MainActivity.TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            val secureId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
            SharedPrefrence.setDeviceID(this, secureId)
            SharedPrefrence.setFCMtokem(this, token)
            registerFcmTokenToServer(token, secureId ?: "")
        })*/

        Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            val secureId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
            SharedPrefrence.setDeviceID(this, secureId)
            SharedPrefrence.setFCMtokem(this, token)
            registerFcmTokenToServer(token, secureId ?: "")
        })


    }

    private fun registerFcmTokenToServer(token: String, deviceId: String) {
        ApiClient.setToken()
        var fcmData = FcmData(token, deviceId, "android")
        notificationViewModel.ragisterToken(fcmData).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            Utils.showToast(
                                this,
                                getString(R.string.sucess_login),
                                Toast.LENGTH_SHORT
                            )
                            navigateToMainActivity()

                        } else {

                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }
        })
    }

    companion object {

        private const val TAG = "LoginActivity"
    }

    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                if (response.body()?.data != null) {

                                    SharedPrefrence.setTRANSISTNo(this,null)
                                    SharedPrefrence.setDriverName(this,null)
                                    SharedPrefrence.setDriverMobile(this,null)
                                    SharedPrefrence.setOPRETORName(this,null)
                                    SharedPrefrence.setOPRETORMobile(this,null)

                                    Utils.showToast(this,getString(R.string.cart_added_successfully),Toast.LENGTH_SHORT)
                                    val intent = Intent(this, CartActivity::class.java)
                                    startActivity(intent)
                                    Bungee.slideLeft(this)
                                    finish()

                                }
                            }
                        } else {

                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        /*binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()*/
                    }
                }
            }
        })
    }

}