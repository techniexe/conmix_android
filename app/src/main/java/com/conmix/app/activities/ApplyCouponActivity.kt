package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.adapter.PromoCodeAdapter
import com.conmix.app.data.CouponDataModel
import com.conmix.app.data.CouponObj
import com.conmix.app.databinding.ApplyCouponListBinding

import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ApplyCouponViewModel
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 20,April,2021
 */
class ApplyCouponActivity: BaseActivity(), PromoCodeAdapter.PromocodeInterface {
    private lateinit var binding: ApplyCouponListBinding
    private lateinit var applyCouponViewModel: ApplyCouponViewModel
    var couponCode:String?= null
    var cartId:String?=null
    var counsLst:ArrayList<CouponDataModel> = ArrayList()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ApplyCouponListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        couponCode = intent.getStringExtra("couponCode")
        cartId = intent.getStringExtra("cartId")

        setupViewModel()

        getCoupons()

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.promocodeApplyBtn.setOnClickListener {

            if(!binding.promocodeEdtText.text.isNullOrEmpty()){

                postCouponCode(binding.promocodeEdtText.text.toString())

            }else{
                Utils.showToast(this, "Please enter coupon code", Toast.LENGTH_SHORT)
            }

        }
    }

    override fun onBackPressed() {
        binding.imgBack.performClick()
    }

    private fun setupViewModel() {

        applyCouponViewModel = ViewModelProvider(
                this,
                ApplyCouponViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(ApplyCouponViewModel::class.java)
    }
    private fun startAnim() {
        if (binding.avLoading != null)

            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE

    }

    private fun stopAnim() {

        if (binding.avLoading != null)

            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE

    }


    fun getCoupons() {
        ApiClient.setToken()
        applyCouponViewModel.getCouponsData().observe(this, Observer{
                response ->
            when(response.status){

                    Status.SUCCESS -> {
                        stopAnim()

                        if (response.data?.isSuccessful!!) {

                            counsLst = response.data.body()?.data?: ArrayList()
                            if (!counsLst.isNullOrEmpty()) {
                                setAdapter(counsLst)
                            }else{
                                emptyView()
                            }

                        } else {
                             emptyView()
                            //Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }

            }
        })
    }

    private fun emptyView() {

        if ( counsLst.size >0) {

            binding.constrainEmptyCoupons.visibility = View.GONE
            binding.prmoCodeRv.visibility = View.VISIBLE


        } else {
            binding.constrainEmptyCoupons.visibility = View.VISIBLE
            binding.prmoCodeRv.visibility = View.GONE


        }
    }


    fun setAdapter(couponList: ArrayList<CouponDataModel>) {
        val adapter = PromoCodeAdapter(this, couponList, this)
        binding.prmoCodeRv.adapter = adapter
        adapter.couponCode = couponCode
        adapter.notifyDataSetChanged()

    }

    override fun applyPromoCode(coupon: String) {
        postCouponCode(coupon)
    }



    fun postCouponCode(coupon:String?) {

        val cpObj = CouponObj(coupon)

        ApiClient.setToken()
        applyCouponViewModel.postCouponCode(cpObj).observe(this,Observer{
                response ->
            when(response.status){

                    Status.SUCCESS -> {
                        stopAnim()

                        if (response.data?.isSuccessful!!) {
                            response.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.code() == 201) {

                                    Utils.showToast(this,"Coupon code applied",Toast.LENGTH_SHORT)
                                    val intent = Intent()
                                    intent.putExtra("coupon", coupon)
                                    setResult(Activity.RESULT_OK, intent)
                                    finish()

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(this, response.data.errorBody())
                        }
                    }
                    Status.LOADING -> {
                       // startAnim()
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, response.message.toString(), Toast.LENGTH_LONG)
                    }
                }

        })
    }
}