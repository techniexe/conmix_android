package com.conmix.app.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import com.conmix.app.R
import com.conmix.app.data.CartItemLst
import com.conmix.app.databinding.BuyerInfoFormBinding
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import java.util.*


/**
 * Created by Hitesh Patel on 09,April,2021
 */
class BuyerInfoActivity: BaseActivity() {

    private lateinit var binding: BuyerInfoFormBinding
    var isFromCart:Boolean = false
    var cartItems: CartItemLst? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BuyerInfoFormBinding.inflate(layoutInflater)
        setContentView(binding.root)

        isFromCart = intent.getBooleanExtra("isFromCart",false)
        cartItems = intent.getParcelableExtra("cartItems")

        if(!isFromCart){
            binding.dateTitle.visibility = View.VISIBLE
            binding.datelnly.visibility = View.VISIBLE
            binding.dateV.visibility = View.VISIBLE
            binding.dateV2.visibility = View.VISIBLE

            setUpUi()
        }else{
            binding.dateTitle.visibility = View.GONE
            binding.datelnly.visibility = View.GONE
            binding.dateV.visibility = View.GONE
            binding.dateV2.visibility = View.GONE
        }





        binding.imgClose.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.nothing, R.anim.bottom_down)
        }

        binding.btnSave.setOnClickListener {
             if(validApplyData()){

                 if(!isFromCart){
                     SharedPrefrence.setSelectedDate(this,binding.selectDeliverydate.text.toString())
                     SharedPrefrence.setTRANSISTNo(this,binding.transitMixerValue.text.toString())
                     SharedPrefrence.setDriverName(this,binding.driverNameValue.text.toString())
                     SharedPrefrence.setDriverMobile(this,binding.driverMobileValue.text.toString())
                    // SharedPrefrence.setOPRETORName(this,binding.operatorNameValue.text.toString())
                    // SharedPrefrence.setOPRETORMobile(this,binding.operatorMobileValue.text.toString())
                     setResult(Activity.RESULT_OK, intent)
                     binding.imgClose.performClick()
                 }else{

                     val intent = Intent()
                     intent.putExtra("cartItems", cartItems)
                     intent.putExtra("tmNo",binding.transitMixerValue.text.toString())
                     intent.putExtra("driverName",binding.driverNameValue.text.toString())
                     intent.putExtra("driverMoNo",binding.driverMobileValue.text.toString())
                   //  intent.putExtra("opretorName",binding.operatorNameValue.text.toString())
                   //  intent.putExtra("opretorMoNo",binding.operatorMobileValue.text.toString())
                     setResult(Activity.RESULT_OK, intent)
                     binding.imgClose.performClick()


                 }
             }
        }
    }


    private fun validApplyData():Boolean{

        val phoneUtil = PhoneNumberUtil.getInstance()
        if(binding.transitMixerValue.text.toString().trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.transit_mixer_numer_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

        if(binding.driverNameValue.text.toString().trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.driver_name_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

        if(binding.driverMobileValue.text.toString().trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.driver_mobile_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

        if (binding.driverMobileValue.text.toString().isNotEmpty()) {
            try {

                val numberProto = phoneUtil.parse(binding.driverMobileValue.text.toString().trim(), binding.ccpL.selectedCountryNameCode)
                val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                        numberProto
                    ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                ) {

                } else {
                    Utils.showToast(
                        this,
                        getString(R.string.driver_invalid_number),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            } catch (e: NumberParseException) {
                Utils.showToast(this, getString(R.string.driver_invalid_number), Toast.LENGTH_SHORT)
                return false
            }
        }

/*        if(binding.operatorNameValue.text.toString().trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.operator_name_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if(binding.operatorMobileValue.text.toString().trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.operator_mobile_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

        if (binding.operatorMobileValue.text.toString().isNotEmpty()) {
            try {

                val numberProto = phoneUtil.parse(binding.operatorMobileValue.text.toString().trim(), binding.ccpL1.selectedCountryNameCode)
                val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                        numberProto
                    ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                ) {

                } else {
                    Utils.showToast(
                        this,
                        getString(R.string.operator_invalid_number),
                        Toast.LENGTH_SHORT
                    )
                    return false
                }
            } catch (e: NumberParseException) {
                Utils.showToast(this, getString(R.string.operator_invalid_number), Toast.LENGTH_SHORT)
                return false

            }

        }*/

        return true
    }

    private fun setUpUi(){

        binding.selectDeliverydate.text = SharedPrefrence.getSelectedDate(this)

        val transistNo = SharedPrefrence.getTRANSISTNo(this)
        val driverName = SharedPrefrence.getDriverName(this)
        val driverMobile = SharedPrefrence.getDriverMobile(this)
       // val opretorName = SharedPrefrence.getOPRETORName(this)
      //  val opretorMobile = SharedPrefrence.getOPRETORMobile(this)

        if(transistNo != null){
            binding.transitMixerValue.setText(transistNo)
        }


        if(driverName != null){
            binding.driverNameValue.setText(driverName)
        }
        if(driverMobile != null){
            binding.driverMobileValue.setText(driverMobile)
        }


       /* if(opretorName != null){
            binding.operatorNameValue.setText(opretorName)
        }
        if(opretorMobile != null){
            binding.operatorMobileValue.setText(opretorMobile)
        }*/

        binding.transitMixerValue.filters = arrayOf(
            Utils.ignoreFirstWhiteSpace())
        binding.driverNameValue.filters = arrayOf(
            Utils.ignoreFirstWhiteSpace())
        binding.driverMobileValue.filters = arrayOf(
            Utils.ignoreFirstWhiteSpace())
        /*binding.operatorNameValue.filters = arrayOf(
            Utils.ignoreFirstWhiteSpace())
        binding.operatorMobileValue.filters = arrayOf(
            Utils.ignoreFirstWhiteSpace())*/

        binding.datelnly.setOnClickListener {

            val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(this, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                  //  val selectedDate = year.toString() + "-" + getNumString(month + 1) + "-" + getNumString(dayOfMonth)
                  //  binding.selectDeliverydate.text = selectedDate

                    val   datelng = Utils.getDateTime(year, month, dayOfMonth)

                    val  selectedDate = Utils.getIsoDate(datelng)


                    binding.selectDeliverydate.setText(selectedDate)
                }

            }, newDate.get(Calendar.YEAR), newDate.get(Calendar.MONTH), newDate.get(Calendar.DAY_OF_MONTH))
            val c = Calendar.getInstance()
            datePicker.datePicker.minDate = c.timeInMillis
            datePicker.show()
        }


    }

    override fun onBackPressed() {
        binding.imgClose.performClick()

    }

    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }

}