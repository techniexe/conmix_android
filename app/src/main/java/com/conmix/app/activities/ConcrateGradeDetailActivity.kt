package com.conmix.app.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.adapter.ProductReviewdapter
import com.conmix.app.adapter.VenderReviewAdapter
import com.conmix.app.data.*
import com.conmix.app.databinding.*
import com.conmix.app.dialogs.*
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ConcreateGradeListViewModel
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel
import com.conmix.app.viewmodels.VenderReviewViewModel
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.math.RoundingMode
import java.util.ArrayList


/**
 * Created by Hitesh Patel on 18,March,2021
 */
class ConcrateGradeDetailActivity: BaseActivity(), UpdateQtyDialog.DialogToFragment,
    ChooseTimeDialog.DialogToFragment {

    private lateinit var binding: ConcrateGradeDetailBinding
    private lateinit var bindingBanner: GradeDetailBannerBinding
    private lateinit var bindingInfo: GradeDetailInfoBinding
    private lateinit var bindingEmptyReview:NoReviewBinding
    private lateinit var bindingEmptyDesignMix: MixDetailPageEmptyBinding
    private var title:String? = null
    private var designListObj: DesignListObj? = null
    private val imageList = ArrayList<SlideModel>()
    lateinit var pickupLocation: ProductCategoryWiseListPickupLocation
    private lateinit var concrateGradeListViewModel: ConcreateGradeListViewModel
  //  private lateinit var productReviewViewModel: ProductReviewViewModel
    private lateinit var venderReviewViewModel: VenderReviewViewModel
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel
    private var designMixId:String?= null
    private var long:String? = null
    private var lat:String? = null
    private var deliveryDate:String? = null
    private var siteId:String? = null

    var withTm:Boolean = true
    var withCp:Boolean = true
    var isFirstTime:Boolean = true
    var isFirstTime1:Boolean = true
    var isOpenDialog:Boolean = false

    var productReviewsLst:ArrayList<ProductReciewLstDta> = ArrayList()
    var vendorReviewlst: VenderReviewResData? = null
    private lateinit var reviewdapter: ProductReviewdapter
    private lateinit var venderReviewAdapter: VenderReviewAdapter

    var venderID:String? = null
    var stateID:String? = null
    var startDate:String? = null
    var endDate:String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ConcrateGradeDetailBinding.inflate(layoutInflater)
        bindingBanner = binding.gradeDetailBannerlyt
        bindingInfo = binding.gradeDetailinfolyt
        bindingEmptyReview = binding.noReviewLy
        bindingEmptyDesignMix = binding.gradeDetailMixemptylyt
        setContentView(binding.root)
        setupViewModel()
        //fullScreen()

        title = intent.getStringExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_TITLE)
        //designListObj = intent.getParcelableExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_OBJ)
        designMixId = intent.getStringExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_ID)
        long = SharedPrefrence.getlang(this)
        lat = SharedPrefrence.getlat(this)
        deliveryDate = SharedPrefrence.getSelectedDate(this)
        siteId = SharedPrefrence.getSiteId(this)
        stateID = SharedPrefrence.getStateId(this)
        bindingInfo.tvProductCount.text = SharedPrefrence.getSELECTEDQTY(this)?.toString()
        startDate = SharedPrefrence.getSelectedDate(this)
        endDate = SharedPrefrence.getSelectedENDDate(this)

        startAnim()
        if(SharedPrefrence.getLogin(this)){
            getConcrateGradeObjData(designMixId,
                SharedPrefrence.getSiteId(this),
                SharedPrefrence.getlang(this),
                SharedPrefrence.getlat(this),deliveryDate,stateID,null,null)
        }else{
            getConcrateGradeObjData(designMixId,null,
                SharedPrefrence.getlang(this),
                SharedPrefrence.getlat(this),deliveryDate,stateID,null,null)
        }


        //setUpUi()
        bindingBanner.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        bindingEmptyDesignMix.imgBackError.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        bindingInfo?.tvKm.setOnClickListener {
            pickupLocation = designListObj?.address?.location!!
            if (::pickupLocation.isInitialized)
                Utils.redirectToMap(this, pickupLocation)
        }


        /*binding.checkbox1.setOnCheckedChangeListener { buttonView, isChecked ->


            if(isFirstTime){
                isFirstTime = false
            }else{
                if(!isChecked){


                    val fragment = BreakAlertDialog(
                        object : BreakAlertDialog.ClickListener {
                            override fun onDoneClicked() {
                                 isOpenDialog = false
                                 withTm = false
                                 //binding.checkbox2.isChecked = false
                                 //withCp = false

                                val intent = Intent(this@ConcrateGradeDetailActivity, BuyerInfoActivity::class.java)
                                startActivityForResult(intent,202)
                                overridePendingTransition(R.anim.bottom_up, R.anim.nothing)

                            }

                            override fun onCancelClicked() {



                            }
                        },
                        getString(R.string.alert_buyer_info_log_out),
                        getString(R.string.txt_yes),
                        getString(R.string.txt_no)
                    )

                    fragment.show(supportFragmentManager, "alert")
                    fragment.isCancelable = false

                }else{

                    isOpenDialog = true
                    withTm = true
                    *//*if( binding.checkbox2.isChecked){
                        binding.checkbox2.isChecked = false
                    }

                    withCp = true*//*

                    var chooseTimeSlotDialog = ChooseTimeDialog(designListObj?.TM_price,designListObj?.CP_price)
                    chooseTimeSlotDialog.setListener(this)
                    chooseTimeSlotDialog.show(supportFragmentManager, "chooseTimeSlotDialog")
                }

            }
        }*/

        /*binding.transitMixerLnly.setOnClickListener {
            if(binding.checkbox1.isChecked){
                val fragment = BreakAlertDialog(
                    object : BreakAlertDialog.ClickListener {
                        override fun onDoneClicked() {
                            // isOpenDialog = false
                            // withTm = false
                            //binding.checkbox2.isChecked = false
                            //withCp = false

                            val intent = Intent(this@ConcrateGradeDetailActivity, BuyerInfoActivity::class.java)
                            startActivityForResult(intent,202)
                            overridePendingTransition(R.anim.bottom_up, R.anim.nothing)

                        }

                        override fun onCancelClicked() {



                        }
                    },
                    getString(R.string.alert_buyer_info_log_out),
                    getString(R.string.txt_yes),
                    getString(R.string.txt_no)
                )

                fragment.show(supportFragmentManager, "alert")
                fragment.isCancelable = false

            }else {


                *//* if (binding.checkbox2.isChecked) {
                     binding.checkbox2.isChecked = false
                 }

                 withCp = true*//*

                var chooseTimeSlotDialog = ChooseTimeDialog(designListObj?.TM_price, designListObj?.CP_price)
                chooseTimeSlotDialog.setListener(this)
                chooseTimeSlotDialog.show(supportFragmentManager, "chooseTimeSlotDialog")
            }
        }*/

       /* binding.checkbox1.setOnClickListener {
            if(!binding.checkbox1.isChecked){


                val fragment = BreakAlertDialog(
                    object : BreakAlertDialog.ClickListener {
                        override fun onDoneClicked() {
                           // isOpenDialog = false
                           // withTm = false
                            //binding.checkbox2.isChecked = false
                            //withCp = false

                            val intent = Intent(this@ConcrateGradeDetailActivity, BuyerInfoActivity::class.java)
                            startActivityForResult(intent,202)
                            overridePendingTransition(R.anim.bottom_up, R.anim.nothing)

                        }

                        override fun onCancelClicked() {



                        }
                    },
                    getString(R.string.alert_buyer_info_log_out),
                    getString(R.string.txt_yes),
                    getString(R.string.txt_no)
                )

                fragment.show(supportFragmentManager, "alert")
                fragment.isCancelable = false

            }else {

                isOpenDialog = true
                withTm = true
               *//* if (binding.checkbox2.isChecked) {
                    binding.checkbox2.isChecked = false
                }

                withCp = true*//*

                var chooseTimeSlotDialog =
                    ChooseTimeDialog(designListObj?.TM_price, designListObj?.CP_price)
                chooseTimeSlotDialog.setListener(this)
                chooseTimeSlotDialog.show(supportFragmentManager, "chooseTimeSlotDialog")
            }
        }*/




        binding.checkbox2.setOnCheckedChangeListener{ buttonView, isChecked ->


            withCp = isChecked

             /*if(isFirstTime1){
                isFirstTime1 = false
            }else {
                 withCp = isChecked

                 if (isChecked) {
                     withTm = isChecked

                     if (binding.checkbox1.isChecked ) {

                        *//* if(!isOpenDialog){
                             var chooseTimeSlotDialog = ChooseTimeDialog(designListObj?.TM_price, designListObj?.CP_price)
                             chooseTimeSlotDialog.setListener(this)
                             chooseTimeSlotDialog.show(supportFragmentManager, "chooseTimeSlotDialog")
                         }*//*


                     } else {
                         binding.checkbox1.isChecked = true
                     }
                 }else{
                     isOpenDialog = false
                 }
             }*/
        }

        bindingInfo.btnAddToCart.setOnClickListener {

            AddToCart()
        }
    }


    private fun AddToCart(){
        val isLogin = SharedPrefrence.getLogin(this)
        if(!isLogin){

            val objpost = AddToCartCustomMixObj(designMixId,
                null,
                bindingInfo.tvProductCount.text.toString().toInt(),
                SharedPrefrence.getStateId(this),
                null,
                null,
                null,
                siteId,long?.toDouble()
                ,lat?.toDouble()
                ,withCp,
                withTm,
                Utils.sendDeliveryDate(deliveryDate!!),
                null,null,null,Utils.sendDeliveryDate(endDate!!)
            )

            var loginDialog = LoginDialog(true,objpost)
            loginDialog.show(supportFragmentManager, "loginDialog")
            Bungee.slideUp(this)
        }else{

            if(withTm){

                val objpost = AddToCartCustomMixObj(designMixId,null,bindingInfo.tvProductCount.text.toString().toInt(),SharedPrefrence.getStateId(this),null,null,null,siteId,long?.toDouble(),lat?.toDouble(),withCp,withTm,Utils.sendDeliveryDate(deliveryDate!!),null,null,null,Utils.sendDeliveryDate(endDate!!))
                postCustomMixData(objpost)
            }else{


               /* val buyerInFo = SharedPrefrence.getTRANSISTNo(this)
                if(buyerInFo != null){
                    val objpost = AddToCartCustomMixObj(designMixId,null,bindingInfo.tvProductCount.text.toString().toInt(),SharedPrefrence.getStateId(this),null,null,null,siteId,long?.toDouble(),lat?.toDouble(),withCp,withTm,Utils.sendDeliveryDate(deliveryDate!!),SharedPrefrence.getTRANSISTNo(this),SharedPrefrence.getDriverName(this),SharedPrefrence.getDriverMobile(this))
                    postCustomMixData(objpost)


                }else{
                    val intent = Intent(this, BuyerInfoActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.bottom_up, R.anim.nothing)
                }*/


                val objpost = AddToCartCustomMixObj(designMixId,
                    null,
                    bindingInfo.tvProductCount.text.toString().toInt(),
                    SharedPrefrence.getStateId(this),
                    null,
                    null,
                    null,
                    siteId,long?.toDouble()
                    ,lat?.toDouble()
                    ,withCp,
                    withTm,
                    Utils.sendDeliveryDate(deliveryDate!!),
                    null,null,null,Utils.sendDeliveryDate(endDate!!)
                   )
                postCustomMixData(objpost)

            }
        }
    }


    private fun setupViewModel() {
        concrateGradeListViewModel =
                ViewModelProvider(
                        this,
                        ConcreateGradeListViewModel.ViewModelFactory(ApiClient().apiService)
                )
                        .get(ConcreateGradeListViewModel::class.java)

       /* productReviewViewModel =  ViewModelProvider(
                this,
                ProductReviewViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(ProductReviewViewModel::class.java)*/

        venderReviewViewModel = ViewModelProvider(
                this,
                VenderReviewViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(VenderReviewViewModel::class.java)

        customMixAddToCartViewModel = ViewModelProvider(
                this,
                CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
                .get(CustomMixAddToCartViewModel::class.java)
    }
    private fun startAnim() {
        if (binding.avLoading != null)

            binding.avLoading.show()
            binding.avLoading.visibility =View.VISIBLE

    }

    private fun stopAnim() {

        if (binding.avLoading != null)

            binding.avLoading.hide()
            binding.avLoading.visibility =View.GONE

    }

    fun getConcrateGradeObjData(designMixId:String?,siteId:String?,long:String?,lat:String?,deliverDate:String?,stateId:String?,with_TM:Boolean?,with_CP:Boolean?){


        val newDate = deliverDate?.let { Utils.sendDeliveryDate(it) }

        ApiClient.setToken()
        concrateGradeListViewModel.getConcrateGradeObj(designMixId,siteId,long,lat,newDate,stateID,with_TM,with_CP).observe(this, Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {

                            setResposerData(response = resource.data.body()?.data)

                        } else {
                            stopAnim()
                            Utils.setErrorData(this, resource.data.errorBody())
                            binding.deisgnmixdeatilLnly.visibility = View.GONE
                            bindingEmptyDesignMix.llNodata.visibility = View.VISIBLE
                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                    Status.ERROR -> {
                        binding.deisgnmixdeatilLnly.visibility = View.GONE
                        bindingEmptyDesignMix.llNodata.visibility = View.VISIBLE
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }

/*
    fun getProductReviewData(productid:String?,userId:String?,before:String?,after:String?){
        ApiClient.setToken()
        productReviewViewModel.getProductReviewData(productid,userId,before,after).observe(this, Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {

                            productReviewsLst = resource.data.body()?.data!!
                            setUpUi(designListObj)


                        } else {
                            setUpUi(designListObj)
                            stopAnim()

                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                    Status.ERROR -> {
                        setUpUi(designListObj)
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }*/


    fun getVenderReviewData(venderid:String?,before:String?,after:String?){
        ApiClient.setToken()
        venderReviewViewModel.getVenderReviewData(venderid,before,after).observe(this, Observer {

            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {

                            vendorReviewlst = resource.data.body()?.data!!
                            setUpUi(designListObj)


                        } else {
                            setUpUi(designListObj)
                            stopAnim()

                        }
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                    Status.ERROR -> {
                        setUpUi(designListObj)
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }



    override fun onBackPressed() {
        super.onBackPressed()
        bindingBanner.imgBack.performClick()
    }

    private fun setResposerData(response: DesignListObj?) {
        this.designListObj = response


        withCp = designListObj?.with_CP?:false
       // withTm = designListObj?.with_TM?:false
        withTm = true
        binding.checkbox1.isChecked = withTm
        binding.checkbox2.isChecked = withCp

        if(withCp == false && withTm == false) {
            binding.checkbox1.setClickable(false)
            binding.checkbox2.setClickable(false)
        }

        //setUpUi(designListObj)

        venderID = designListObj?.vendor_id
        //getProductReviewData(designListObj?._id,null,null,null)
        getVenderReviewData(venderID,null,null)
    }

    fun setUpUi(designListObj: DesignListObj?){
        bindingEmptyDesignMix.llNodata.visibility = View.GONE
        binding.deisgnmixdeatilLnly.visibility = View.VISIBLE
        stopAnim()
        imageList?.clear()
        designListObj?.vendor_media?.forEach {

            if(it.media_type == "image") {

                imageList.add(SlideModel(it.media_url ?: "","", ScaleTypes.CENTER_CROP))
            }

        }
       /* bindingBanner.imageSlider.setSliderAdapter(SliderAdapterExample(this, imageList))
        bindingBanner.imageSlider.startAutoCycle()
        bindingBanner.imageSlider.indicatorUnselectedColor = ContextCompat.getColor(this, R.color.white);
        bindingBanner.imageSlider.setIndicatorAnimation(IndicatorAnimationType.SLIDE)
        bindingBanner.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        bindingBanner.imageSlider.scrollTimeInSec = 2*/

        if(imageList?.size?:0 > 1){
            bindingBanner.imageSingleSilder.visibility = View.GONE
            bindingBanner.imageSlider.visibility = View.VISIBLE
            bindingBanner.imageSlider.setImageList(imageList)
        }else{
            bindingBanner.imageSingleSilder.visibility = View.VISIBLE
            bindingBanner.imageSlider.visibility = View.GONE
            if(imageList?.size == 1){
                Utils.setImageUsingGradeGlide(this,imageList.get(0)?.imageUrl,bindingBanner.imageSingleSilder)
            }else{
                Utils.setImageUsingGradeGlide(this,null,bindingBanner.imageSingleSilder)
            }

        }


        bindingInfo.itemGradeByGroupName.text =  designListObj?.vendor?.company_name

        var strBuffer1 = StringBuffer()

        if(designListObj?.address?.line1?.isEmpty() == true) {
            strBuffer1.append("")
        }else{
            strBuffer1.append(designListObj?.address?.line1?:"")
        }
        if(designListObj?.address?.city_name?.isEmpty() == true) {
            strBuffer1.append("")
        }else{
            strBuffer1.append(", ")
            strBuffer1.append(designListObj?.address?.city_name?:"")
        }
        if(designListObj?.address?.state_name?.isEmpty() == true) {
            strBuffer1.append("")
        }else{
            strBuffer1.append(", ")
            strBuffer1.append(designListObj?.address?.state_name?:"")
        }

        bindingInfo.itemLocation.text = strBuffer1.toString()
        //bindingInfo.itemGradeByGroupName.text = designListObj?.vendor?.company_name
        bindingInfo.tvKm.text = String.format(
            getString(R.string.txt_km),
            Utils.setPrecesionFormate(designListObj?.distance)
        )

        /*if(designListObj?.is_available == true){
            bindingInfo.stockvalue.text = "In Stock"
        }else{
            bindingInfo.stockvalue.text = "Out Stock"
        }*/
        val sellingPrice = Utils.setPrecesionFormate(designListObj?.selling_price_with_margin?:0.0)
        val spanText = SpannableStringBuilder("Price : ₹ "+sellingPrice+" / Cu.Mtr ")
        spanText.setSpan( StyleSpan(Typeface.BOLD), 8, (spanText.length-9), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanText.setSpan(AbsoluteSizeSpan(14,true),8,(spanText.length-9),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        bindingInfo.itemSalesPrice.setText(spanText, TextView.BufferType.SPANNABLE)

       // bindingInfo.itemSalesPrice.text = "Price : ₹"+sellingPrice+" / Cu.Mtr"
        //bindingInfo.outRatingtxt.text = Utils.setPrecesionFormate(designListObj?.vendor?.rating?:0.0)
        if(designListObj?.vendor?.rating != null){
            bindingInfo.outRatingtxt.text = designListObj?.vendor?.rating?.toBigDecimal()?.setScale(1, RoundingMode.UP)?.toDouble().toString()
        }else{
            bindingInfo.outRatingtxt.text = "0.0"
        }

        //bindingInfo.outRatingtxt.text = designListObj?.vendor?.rating?.toBigDecimal()?.setScale(1, RoundingMode.UP)?.toDouble().toString()
        bindingInfo.outFromRatingtxt.text= "/5"

        bindingInfo.designMixTitleTxt.text = title+" - "+designListObj?.product_name.toString()

        bindingInfo.cementValueTxt.text = designListObj?.cement_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.sandValueTxt.text = designListObj?.sand_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.flyashValueTxt.text = designListObj?.fly_ash_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.admixureValueTxt.text = designListObj?.ad_mixture_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.aggregate1TitleTxt.text = designListObj?.aggregate1_sand_category?.sub_category_name+" (Kg) : "
        bindingInfo.aggregate1ValueTxt.text = designListObj?.aggregate1_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.aggregate2TitleTxt.text = designListObj?.aggregate2_sand_category?.sub_category_name+" (Kg) : "
        bindingInfo.aggregate2ValueTxt.text = designListObj?.aggregate2_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.waterValueTxt.text = designListObj?.water_quantity?.let { Utils.setPrecesionFormate(it) }
        bindingInfo.gradeValueTxt.text = title



       // bindingInfo.tvProductCount.text = (designListObj?.quantity?:"3").toString()

        bindingInfo.tvProductCount?.setOnClickListener {
            var updateQty = UpdateQtyDialog(bindingInfo.tvProductCount.text.toString().toInt(),0)
            updateQty.setListener(this)
            updateQty.show(supportFragmentManager, "updateQty")
        }

        /* holder.tvKm.setOnClickListener {
           val  pickupLocation = dta?.

                 Utils.redirectToMap(context, pickupLocation)
         }*/


        binding.tmPopUpmenu.setOnClickListener {

            val fragment = TMCPBreakAlertDialog(object : TMCPBreakAlertDialog.ClickListener {
                override fun onDoneClicked() {

                }

                override fun onCancelClicked() {


                }
            }, String.format(getString(R.string.tmPricePopUpTxt),Utils.setPrecesionFormate(designListObj?.TM_price)), "Ok")


            fragment.show(this.supportFragmentManager, "alert")
            fragment.isCancelable = false


        }

        binding.cpPopUpmenu.setOnClickListener {
            val fragment = TMCPBreakAlertDialog(object : TMCPBreakAlertDialog.ClickListener {
                override fun onDoneClicked() {

                }

                override fun onCancelClicked() {


                }
            }, String.format(getString(R.string.cpPricePopUpTxt),Utils.setPrecesionFormate(designListObj?.CP_price)), "Ok")


            fragment.show(this.supportFragmentManager, "alert")
            fragment.isCancelable = false
        }




        bindingInfo.icAdd.setOnClickListener {
            var totalMaxQty = 100
            var productQty = (designListObj?.quantity?:"3").toString()?.toInt()
            if (productQty >= totalMaxQty) {
                Utils.showToast(
                        this,
                    getString(R.string.maxmumqty100),
                        Toast.LENGTH_LONG
                )
            } else {
                productQty = bindingInfo.tvProductCount.text.toString().toInt()
                productQty += 1
                bindingInfo.tvProductCount.setText(productQty.toString())
                designListObj?.quantity = productQty
            }
        }

        bindingInfo.icRemove.setOnClickListener {

            var productQty = bindingInfo.tvProductCount.text.toString()?.toInt()
            if (productQty <= 3) {
                Utils.showToast(
                        this,
                        getString(R.string.mimmumqty3),
                        Toast.LENGTH_LONG
                )
            } else {
                productQty = bindingInfo.tvProductCount.text.toString().toInt()
                productQty -= 1
                bindingInfo.tvProductCount.setText(productQty.toString())
                designListObj?.quantity = productQty

            }
        }


       // bindingInfo.designMixDescValue.text = "Cement Brand : "+designListObj?.cement_brand?.name+" , Sand Source : "+designListObj?.sand_source?.sand_source_name+" , Aggregate Source : "+designListObj?.aggregate_source?.aggregate_source_name+" , Admixture Brand : "+designListObj?.admixture_brand?.name+" , Fly Ash Source Name : "+designListObj?.fly_ash_source?.fly_ash_source_name

        val stringBuffer = StringBuffer()
        stringBuffer.append("Cement Brand : ")
        stringBuffer.append(designListObj?.cement_brand?.name?:" - ")
        stringBuffer.append("; Sand Source : ")
        stringBuffer.append(designListObj?.sand_source?.sand_source_name?:" - ")
        stringBuffer.append("; Aggregate Source : ")
        stringBuffer.append(designListObj?.aggregate_source?.aggregate_source_name?:" - ")
        stringBuffer.append("; Admixture Brand : ")
        stringBuffer.append(designListObj?.admixture_brand?.name?:" - ")
        stringBuffer.append("; Fly Ash Source Name : ")
        stringBuffer.append(designListObj?.fly_ash_source?.fly_ash_source_name?:" - ")

        bindingInfo.designMixDescValue.text = stringBuffer.toString()


   /*     if(!designListObj?.cement_brand?.name.isNullOrEmpty()){
            bindingInfo.cementSourceLNLY?.visibility = View.VISIBLE
            bindingInfo.cementSourceTxt.text = "Cement Brand : "+designListObj?.cement_brand?.name
        }

        if(!designListObj?.sand_source?.sand_source_name.isNullOrEmpty()){
            bindingInfo.sandSourceLNLY?.visibility = View.VISIBLE
            bindingInfo.sandSourceTxt.text = "Sand Brand : "+designListObj?.sand_source?.sand_source_name
        }

        if(!designListObj?.aggregate_source?.aggregate_source_name.isNullOrEmpty()){
            bindingInfo.aggSourceLNLY?.visibility = View.VISIBLE
            bindingInfo.aggSourceTxt.text = "Aggregate Source : "+designListObj?.aggregate_source?.aggregate_source_name
        }
        if(!designListObj?.admixture_brand?.name.isNullOrEmpty()){
            bindingInfo.admixSourceLNLY?.visibility = View.VISIBLE
            bindingInfo.admixtureSourceTxt.text = "Admixture Brand : "+designListObj?.admixture_brand?.name
        }
        if(!designListObj?.fly_ash_source?.fly_ash_source_name.isNullOrEmpty()){
            bindingInfo.flyashSourceLNLY?.visibility = View.VISIBLE
            bindingInfo.flyashSourceTxt.text = "Fly Ash Source Name : "+designListObj?.fly_ash_source?.fly_ash_source_name
        }

            bindingInfo.waterSourceLNLY?.visibility = View.VISIBLE
            bindingInfo.waterSourceTxt.text = "Water : Regular"*/




    //  binding.designMixInfoDescValue.text = designListObj?.description
      //  binding.designMixInfoDescValue.text = getString(R.string.static_description_txt)

        binding.designMixInfoDescValue.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(designListObj?.description, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(designListObj?.description)
        }

        setUpReviewUI()
    }

    private fun setUpReviewUI() {

        //bindingEmptyReview?.constrainEmptyReview.visibility = View.VISIBLE



       /* if (productReviewsLst.size > 0) {
           // reviewListt = reviewList
            binding.rvReviews.visibility = View.VISIBLE
            //rvReviews.visibility = View.VISIBLE
            reviewdapter = ProductReviewdapter(this, productReviewsLst)
            binding.rvReviews.adapter = reviewdapter
            bindingEmptyReview.constrainEmptyReview.visibility = View.GONE
        } else {
            bindingEmptyReview.constrainEmptyReview.visibility = View.VISIBLE
            binding.rvReviews.visibility = View.GONE
           // tvReview.visibility = View.GONE
        }

        if (productReviewsLst.size > 3) {
            binding.tvReviewAll.visibility = View.VISIBLE
           // binding.tvReviewAll.setOnClickListener(this)
        } else {
            binding.tvReviewAll.visibility = View.GONE
        }*/

        if (vendorReviewlst?.reviews?.size?:0 > 0) {
            // reviewListt = reviewList
            binding.rvReviews.visibility = View.VISIBLE
            //rvReviews.visibility = View.VISIBLE
            venderReviewAdapter = VenderReviewAdapter(this, vendorReviewlst)
            binding.rvReviews.adapter = venderReviewAdapter
            bindingEmptyReview.constrainEmptyReview.visibility = View.GONE
        } else {
            bindingEmptyReview.constrainEmptyReview.visibility = View.VISIBLE
            bindingEmptyReview.tvNoData.text = getString(R.string.grade_dtl_review_err)
            binding.rvReviews.visibility = View.GONE
            // tvReview.visibility = View.GONE
        }

        if (vendorReviewlst?.reviews?.size?:0 > 3) {
            binding.tvReviewAll.visibility = View.VISIBLE
            // binding.tvReviewAll.setOnClickListener(this)
        } else {
            binding.tvReviewAll.visibility = View.GONE
        }


        binding.tvReviewAll?.setOnClickListener {
            val intent = Intent(this, ReviewActivity::class.java)
            intent.putExtra(CoreConstants.Intent.INTENT_VENDER_ID, venderID)
            intent.putParcelableArrayListExtra(CoreConstants.Intent.INTENT_REVIEW_LIST,vendorReviewlst?.reviews)
            startActivity(intent)
            Bungee.slideLeft(this)



        }

    }

    override fun dialogDismiss() {

    }

    override fun dialogSave(count: Int, position: Int) {
        designListObj?.quantity = count
        bindingInfo.tvProductCount.text = count.toString()
    }

    override fun dialogChangeDateSave(date: String,endt:String) {

        deliveryDate = date
        isOpenDialog = true
        withTm = true

        if(SharedPrefrence.getLogin(this)){
            getConcrateGradeObjData(designMixId,
                SharedPrefrence.getSiteId(this),
                SharedPrefrence.getlang(this),
                SharedPrefrence.getlat(this),date,SharedPrefrence.getStateId(this),withTm,withCp)
        }else{
            getConcrateGradeObjData(designMixId,null,
                SharedPrefrence.getlang(this),
                SharedPrefrence.getlat(this),date,SharedPrefrence.getStateId(this),withTm,withCp)
        }

    }


    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()

                                if (response.body()?.data != null) {

                                    SharedPrefrence.setTRANSISTNo(this,null)
                                    SharedPrefrence.setDriverName(this,null)
                                    SharedPrefrence.setDriverMobile(this,null)
                                    SharedPrefrence.setOPRETORName(this,null)
                                    SharedPrefrence.setOPRETORMobile(this,null)

                                    Utils.showToast(this,getString(R.string.cart_added_successfully),Toast.LENGTH_SHORT)
                                    val intent = Intent(this, CartActivity::class.java)
                                    startActivity(intent)
                                    Bungee.slideLeft(this)

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        binding.avLoading.visibility = View.GONE
                        binding.avLoading.hide()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        /*binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()*/
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 202) {
            if (resultCode == Activity.RESULT_OK) {
                isOpenDialog = false
               // withTm = false
                withTm = true
                binding.checkbox1.isChecked = false
                binding.checkbox2.isChecked = false
                withCp = false

                if(SharedPrefrence.getLogin(this)){
                    getConcrateGradeObjData(designMixId,
                        SharedPrefrence.getSiteId(this),
                        SharedPrefrence.getlang(this),
                        SharedPrefrence.getlat(this),deliveryDate,SharedPrefrence.getStateId(this),withTm,withCp)
                }else{
                    getConcrateGradeObjData(designMixId,null,
                        SharedPrefrence.getlang(this),
                        SharedPrefrence.getlat(this),deliveryDate,SharedPrefrence.getStateId(this),withTm,withCp)
                }
            }
        }
    }
}