package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.adapter.ProductCategoryAdapter
import com.conmix.app.data.ProductCategoryWiseDetailsProductCategory
import com.conmix.app.databinding.CategorysellallactivityBinding
import com.conmix.app.services.interfaces.RecyclerOnSubItemClickListener
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 04,August,2021
 */
class CategorySeeAllActivity:BaseActivity(), RecyclerOnSubItemClickListener {

    var catlst: ArrayList<ProductCategoryWiseDetailsProductCategory>? = ArrayList()

    private lateinit var binding:CategorysellallactivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CategorysellallactivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        catlst = intent.getParcelableArrayListExtra("catlst")?: ArrayList()

        binding.imgBack.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        val adapter = catlst?.let { ProductCategoryAdapter(this, it,false) }
        val gridLayoutManager = GridLayoutManager(this, 3, RecyclerView.VERTICAL, false)
        val layoutParams = gridLayoutManager.spanSizeLookup
        binding.rvdashCommonList?.layoutManager = gridLayoutManager
        adapter!!.setListner(this@CategorySeeAllActivity)

        binding.rvdashCommonList.adapter = adapter
    }

    override fun onItemClick(childView: View?, id: String?, subId: String?, categoryName: String?) {
        var intent =  Intent(this, ConcrateGradeListActivity::class.java)
        intent.putExtra(CoreConstants.Intent.INTENT_GRADE_ID,id)
        intent.putExtra(CoreConstants.Intent.INTENT_CONCRETE_GRADE_TITLE,categoryName)
        startActivity(intent)
        Bungee.slideLeft(this)
    }

    override fun onBackPressed() {

        binding.imgBack.performClick()
    }
}