package com.conmix.app.activities

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.text.*
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.adapter.*
import com.conmix.app.data.*
import com.conmix.app.databinding.CustomDesignMix1Binding
import com.conmix.app.dialogs.*
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.SlideAnimationUtil
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.CustomConmixViewModel
import kotlinx.android.parcel.Parcelize

import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 22,March,2021
 */
class CustomDesignMix1Activity: BaseActivity(), CementBandSelectionDialog.DialogToFragment, SandSourceSelectionDialog.DialogToFragment,
    AggregateSourceSelectionDialog.DialogToFragment, FlyAshSelectionDialog.DialogToFragment {

    private lateinit var  binding: CustomDesignMix1Binding
    private lateinit var customConmixViewModel: CustomConmixViewModel
    lateinit var productCategory: ArrayList<ProductCategoryWiseDetailsProductCategory>
    var cMFOD:CustomMixFixObjData? = null
    var flyashsourcelst: ArrayList<FlyAshSourceObj> = ArrayList<FlyAshSourceObj>()
    var sandSourcelst:ArrayList<SandSourceDataObj> = ArrayList<SandSourceDataObj>()
    var cementGradelst:ArrayList<CementGradeDataObj> = ArrayList<CementGradeDataObj>()
    var cementBrandlst:ArrayList<CementBrandDataObj> = ArrayList<CementBrandDataObj>()
    var aggregateCategorylst:ArrayList<AggregateCategoryDataObj> = ArrayList<AggregateCategoryDataObj>()
    var aggregateSubCategory1lst:ArrayList<AggregateSubCategoryDataObj> = ArrayList<AggregateSubCategoryDataObj>()
    var aggregateSubCategory2lst:ArrayList<AggregateSubCategoryDataObj> = ArrayList<AggregateSubCategoryDataObj>()
    var aggregateSourcelst:ArrayList<AggregateSourceObj> = ArrayList<AggregateSourceObj>()
    var addmixureBrandlst:ArrayList<AddmixureBrandObj> = ArrayList<AddmixureBrandObj>()
    var gradeTypeSelectTxt:String? = null
    var gradeTypeSelectedId:String? = null
    var isWaterRatio:Boolean = false
    var isCementRatio:Boolean = false
    var aggregateSubCategorylst1:ArrayList<AggregateSubCategoryDataObj> = ArrayList()
    var aggregateSubCategorylst2:ArrayList<AggregateSubCategoryDataObj> = ArrayList()

    var addmixureCategorylst:ArrayList<AddmixureCategoryObj> = ArrayList()


    var cementBrandids: ArrayList<String> = ArrayList()
    var sandSourceids: ArrayList<String> = ArrayList()
    var aggregateSourceids: ArrayList<String> = ArrayList()
    var aggregate1Sourceids: ArrayList<String> = ArrayList()
    var aggregate2Sourceids: ArrayList<String> = ArrayList()
    var flyAshSourceids: ArrayList<String> = ArrayList()

    var cementGradeidsSelected:ArrayList<String> = ArrayList()
    var cementBrandidsSelected:ArrayList<String> = ArrayList()
    var sandSourceidsSelected: ArrayList<String> = ArrayList()
    var aggregateSourceidsSelected: ArrayList<String> = ArrayList()
    var aggregate1SourceidsSelected: ArrayList<String> = ArrayList()
    var aggregate2SourceidsSelected: ArrayList<String> = ArrayList()
    var flyAshSourceidsSelected: ArrayList<String> = ArrayList()
    var admixtureBrandidsSelected: ArrayList<String> = ArrayList()
    var admixtureCateidsSelected: ArrayList<String> = ArrayList()



    var cementGradeTypeSelectTxt:String? = null
    var addmixureBrandTypeSelectTxt:String?= null
    var addmixureCodeSelecTxt:String? = null
    var transistMixurArray:ArrayList<String> = ArrayList<String>()
    var words = arrayOf("Yes", "No")
    var transistMixureTypeSelectTxt:Boolean = false
    var withConcreatePumpSelecttxt:Boolean = false

    var selectedAggregateSource1Txt:String? = null
    var selectedAggregateSource2Txt:String? = null


    var gradeTypeId:String? = null
    var cementGradeTypeSelectID:String? = null
    var cementkgValue:String? = null
    var sandKgValue:String?= null
    var aggregate1KgValue:String? = null
    var aggregate2KgValue:String? = null
    var admixtureKgValue:String? = null
    var flyashKgValue:String? = null
    var waterKgValue:String? = null

    var customMixReslst:ArrayList<CustomMixResObjData> = ArrayList()
    var deliveryDate:String? = null
    var latitue:Double?= null
    var longitude:Double?= null
    var siteId:String?= null
    var isAdmixureAdded:Boolean = false

    var isfromReapeatOrder:Boolean = false
    var orderItem: OrderDetailItemDta? = null


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CustomDesignMix1Binding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewModel()
        startAnim()

        isfromReapeatOrder = intent.getBooleanExtra("isfromReapeatOrder",false)
        orderItem = intent.getParcelableExtra("orderitem")

        val isLoogedIn = SharedPrefrence.getLogin(this)
        val siteName = SharedPrefrence.getSiteName(this)
        siteId = SharedPrefrence.getSiteId(this)
        val stateName = SharedPrefrence.getStateName(this)
        val cityname = SharedPrefrence.getCityname(this)
        latitue = (SharedPrefrence.getlat(this)?:"0.0").toString().toDouble()
        longitude = (SharedPrefrence.getlang(this)?:"0.0").toString().toDouble()

        binding.datelnly.isClickable = false
        binding.datelnly.isEnabled = false

        val selectedDate = SharedPrefrence.getSelectedDate(this)
        val selectedEndDate = SharedPrefrence.getSelectedENDDate(this)
        binding.selectDeliverydate.setText("$selectedDate - $selectedEndDate")

        if (isLoogedIn && !siteId.isNullOrEmpty() && !siteName.isNullOrEmpty()) {
            binding?.llLocation?.visibility = View.VISIBLE
            binding?.tvLocation?.text = siteName
        } else {


            if (!stateName.isNullOrEmpty() && !cityname.isNullOrEmpty()) {

                val strBuffer = StringBuffer()
                strBuffer.append((cityname))
                strBuffer.append(",")
                strBuffer.append(" ")
                strBuffer.append((stateName))

                binding?.llLocation?.visibility = View.VISIBLE
                binding?.tvLocation?.text = strBuffer.toString()
            } else {
                binding?.llLocation?.visibility = View.GONE
            }
        }


       // binding.selectDeliverydate.text = SharedPrefrence.getSelectedDate(this)
        changeTextColor("(1/2)")

        runOnUiThread {
            getConcreateGrade()
            // getAggregateSandCategoryData
            // ()

        }

        binding.datelnly.setOnClickListener {

            val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(this, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    // val selectedDate = year.toString() + "-" + getNumString(month + 1) + "-" + getNumString(dayOfMonth)
                    // binding.selectDeliverydate.text = selectedDate

                    // binding.selectDeliverydate.setText(Utils.getConvertDateWithoutTime(selectedDate!!))

                    val   datelng = Utils.getDateTime(year, month, dayOfMonth)

                    val selectedDate = Utils.getIsoDate(datelng)
                    binding.selectDeliverydate.setText(selectedDate)
                }

            }, newDate.get(Calendar.YEAR), newDate.get(Calendar.MONTH), newDate.get(Calendar.DAY_OF_MONTH))
            val c = Calendar.getInstance()
            datePicker.datePicker.minDate = c.timeInMillis
            datePicker.show()
        }


        binding.backRegistration.setOnClickListener {
            val fragment = BreakAlertDialog(
                object : BreakAlertDialog.ClickListener {
                    override fun onDoneClicked() {
                        finish()
                        Bungee.slideRight(this@CustomDesignMix1Activity)
                    }

                    override fun onCancelClicked() {


                    }
                },
                getString(R.string.are_you_sure_you_want_to_remove),
                getString(R.string.txt_yes),
                getString(R.string.txt_cancel)
            )

            fragment.show(supportFragmentManager, "alert")
            fragment.isCancelable = false
        }

        binding.forwardToNext.setOnClickListener {

            if(validComixDesignApplyData()){



                cementBrandidsSelected.clear()
                cementBrandlst.forEach {
                    if (it.isSelected) {
                        cementBrandidsSelected.add(it._id!!)
                    }
                }
                sandSourceidsSelected.clear()
                sandSourcelst.forEach {

                    if (it.isSelected) {
                        sandSourceidsSelected.add(it._id!!)
                    }
                }
                aggregateSourceidsSelected.clear()
                aggregateSourcelst.forEach {

                    if (it.isSelected) {
                        aggregateSourceidsSelected.add(it._id!!)
                    }
                }


                flyAshSourceidsSelected.clear()
                flyashsourcelst.forEach {
                    if (it.isSelected) {
                        flyAshSourceidsSelected.add(it._id!!)
                    }
                }


                binding.scroll2.visibility = View.GONE
                //binding.frmLy1.visibility = View.GONE
                binding.onlyTxt.visibility = View.GONE
                binding.forwardToNext.visibility = View.GONE

                SlideAnimationUtil.slideInFromRight(this, binding.scroll1)

                binding.scroll1.visibility = View.VISIBLE

                binding.btnApply.visibility = View.VISIBLE

                binding.backbtn2.visibility = View.VISIBLE



                /* val spannableString = SpannableString("(2/2)")
                 val foregroundSpan = ForegroundColorSpan(getColor(R.color.login_btn_color))
                 spannableString.setSpan(
                     foregroundSpan,
                     2,
                     3,
                     Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                 )



                 binding.tvTitle2.text = spannableString*/

                changeTextColor("(2/2)")



            }

        }

        binding.backbtn2.setOnClickListener {


            // binding.tvTitle2.text = "(1/2)"
            changeTextColor("(1/2)")
            binding.scroll1.visibility = View.GONE
            binding.btnApply.visibility = View.GONE

            binding.backbtn2.visibility = View.GONE
            SlideAnimationUtil.slideInFromLeft(this, binding.scroll2)

            binding.scroll2.visibility = View.VISIBLE

            binding.onlyTxt.visibility = View.VISIBLE
            binding.forwardToNext.visibility = View.VISIBLE

        }


        binding.cementKgValue?.addTextChangedListener(object : TextWatcher {

            // var isPercentage = true
            // var isWaterLtr = true
            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int,before: Int, count: Int) {



                binding.addmixturePercentageValue.text?.clear()
                binding.waterLtrValue.text?.clear()
                binding.addmixtureKgValue.text?.clear()
                isCementRatio = false

            }
        })

        binding.addmixturePercentageValue.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if(binding.cementKgValue.text?.trim()?.isEmpty() == true){
                    Utils.showToast(
                        this@CustomDesignMix1Activity,
                        getString(R.string.cement_kg_select_empty),
                        Toast.LENGTH_SHORT
                    )

                    binding.cementKgValue.setError( getString(R.string.cement_kg_select_empty))

                }else{
                    if(s.length > 0){
                        var ratio = 0.0

                        if(s.startsWith(".")){
                            val str = "0."
                            binding.addmixturePercentageValue?.setText(str)
                            binding.addmixturePercentageValue?.setSelection(str.length)
                            ratio = ((binding.cementKgValue.text.toString().toDouble())* 0.0)/100
                        }else{
                            ratio = ((binding.cementKgValue.text.toString().toDouble())*(s.toString().toDouble()))/100
                        }

                        //binding.addmixturePercentageValue.setText(getString(R.string.addmixture_percentage_title)+" Ratio = "+ratio.toString())

                        if(ratio in 1.0..5.0){
                            isCementRatio = true
                            val rationdecimal = Utils.roundOffDecimal(ratio)
                            binding.addmixtureKgValue.setText(rationdecimal.toString())

                        }else{
                            binding.addmixtureKgValue.setText("")
                            /* Utils.showToast(
                                     this@CustomDesignMix1Activity,
                                     getString(R.string.admixtureqty_error),
                                     Toast.LENGTH_SHORT
                             )*/


                            isCementRatio = false
                            binding.addmixturePercentageValue.setError( getString(R.string.admixtureqty_error))
                        }
                    }else{
                        isCementRatio = false
                    }
                }

            }
        })



        binding.waterLtrValue?.addTextChangedListener(object : TextWatcher {


            override fun afterTextChanged(s: Editable) {


            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if(binding.cementKgValue.text?.trim()?.isEmpty() == true){
                    Utils.showToast(
                        this@CustomDesignMix1Activity,
                        getString(R.string.cement_kg_select_empty),
                        Toast.LENGTH_SHORT
                    )

                    binding.cementKgValue.setError( getString(R.string.cement_kg_select_empty))

                }else{

                    val minratio = binding.cementKgValue.text?.trim().toString().toDouble()*0.35
                    val maxratio = binding.cementKgValue.text?.trim().toString().toDouble()*0.55
                    binding.watersucessTxt.visibility = View.VISIBLE
                    binding.watersucessTxt.setTextColor(ContextCompat.getColor(this@CustomDesignMix1Activity,R.color.date_not_avilable_color))
                    binding.watersucessTxt.text = String.format(
                        getString(R.string.water_percentage_ratio_error_new),
                        Utils.setPrecesionFormate(minratio),
                        Utils.setPrecesionFormate(maxratio)
                    )

                    if(s.length > 0){
                        var ratio = 0.0

                        if(s.startsWith(".")){
                            val str = "0."
                            binding.waterLtrValue?.setText(str)
                            binding.waterLtrValue?.setSelection(str.length)
                            ratio = 0.0/binding.cementKgValue.text?.trim().toString().toDouble()
                        }else{
                            ratio = (s.toString().toDouble())/binding.cementKgValue.text?.trim().toString().toDouble()
                        }



                        if(ratio >= 0.35 && ratio <= 0.55){

                            isWaterRatio = true
                            binding.watersucessTxt.setTextColor(ContextCompat.getColor(this@CustomDesignMix1Activity,R.color.order_status_success))
                            binding.watersucessTxt.text = String.format(
                                getString(R.string.water_percentage_ratio_ok_new),
                                Utils.setPrecesionFormate(ratio)
                            )


                        }else{

                            isWaterRatio = false
                            binding.watersucessTxt.setTextColor(ContextCompat.getColor(this@CustomDesignMix1Activity,R.color.date_not_avilable_color))
                            binding.watersucessTxt.text = String.format(
                                getString(R.string.water_percentage_ratio_error_new),
                                Utils.setPrecesionFormate(minratio),
                                Utils.setPrecesionFormate(maxratio)
                            )

                        }
                    }else{
                        isWaterRatio = false
                        binding.watersucessTxt.setTextColor(ContextCompat.getColor(this@CustomDesignMix1Activity,R.color.date_not_avilable_color))
                        binding.watersucessTxt.text = String.format(
                            getString(R.string.water_percentage_ratio_error_new),
                            Utils.setPrecesionFormate(minratio),
                            Utils.setPrecesionFormate(maxratio)
                        )
                    }
                }
            }
        })


        binding.btnApply.setOnClickListener {

            if(validComixDesignData()){

                gradeTypeId = gradeTypeSelectedId
                cementkgValue = binding.cementKgValue.text?.trim().toString()
                sandKgValue = binding.sandKgValue.text?.trim().toString()
                aggregate1KgValue = binding.category1Value.text?.trim().toString()
                aggregate2KgValue = binding.category2Value.text?.trim().toString()
                admixtureKgValue = binding.addmixtureKgValue.text?.trim().toString()
                flyashKgValue = binding.flyashKgValue.text?.trim().toString()
                waterKgValue = binding.waterLtrValue.text?.trim().toString()

                var adBrandSlctId:ArrayList<String>? = null
                if(admixtureBrandidsSelected?.size > 0 ){
                    adBrandSlctId = admixtureBrandidsSelected
                }

                var adBrandSlctCodeId:ArrayList<String>? = null
                if(admixtureCateidsSelected?.size > 0 ){
                    adBrandSlctCodeId = admixtureCateidsSelected
                }

                var aggregateSourceidsSlcID:ArrayList<String>? = null
                if(aggregateSourceidsSelected?.size > 0 ){
                    aggregateSourceidsSlcID = aggregateSourceidsSelected
                }

                var sand_source_id_slect:ArrayList<String>? = null
                if(sandSourceidsSelected?.size > 0 ){
                    sand_source_id_slect = sandSourceidsSelected
                }


                val stateId = SharedPrefrence.getSiteId(this)
                val long = SharedPrefrence.getlang(this)?.toDouble()
                val lat = SharedPrefrence.getlat(this)?.toDouble()
                var deliveryDate = SharedPrefrence.getSelectedDate(this).toString()?.let { it1 ->
                    Utils.sendDeliveryDate(
                        it1
                    )
                }
                var customMixPostObj: CustomMixPostObj?= null
                if(flyAshSourceidsSelected.size > 0 && !flyashKgValue.isNullOrEmpty()){
                    if(isAdmixureAdded){
                        customMixPostObj = CustomMixPostObj(stateId,
                            long,
                            lat,
                            gradeTypeId,
                            cementGradeidsSelected,
                            cementBrandidsSelected,
                            cementkgValue?.toDouble(),
                            sand_source_id_slect,
                            sandKgValue?.toDouble(),
                            aggregate1SourceidsSelected,
                            aggregate2SourceidsSelected,
                            aggregateSourceidsSlcID,
                            aggregate1KgValue?.toDouble(),
                            aggregate2KgValue?.toDouble(),
                            flyAshSourceidsSelected,
                            flyashKgValue?.toDouble(),
                            adBrandSlctId,
                            adBrandSlctCodeId,
                            admixtureKgValue?.toDouble(),
                            waterKgValue?.toDouble(),
                            withConcreatePumpSelecttxt,
                            transistMixureTypeSelectTxt,
                            deliveryDate,null,
                            adBrandSlctCodeId
                        )
                    }else{
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),flyAshSourceidsSelected,
                            flyashKgValue?.toDouble(),adBrandSlctId,adBrandSlctCodeId,null,waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }


                    SharedPrefrence.setCustomMixObj(this,customMixPostObj)
                    postCustomMixData(customMixPostObj)
                }else if(flyAshSourceidsSelected.size >0 && flyashKgValue.isNullOrEmpty()){

                    if(isAdmixureAdded){
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),flyAshSourceidsSelected,
                            null,adBrandSlctId,adBrandSlctCodeId,admixtureKgValue?.toDouble(),waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }else{
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),flyAshSourceidsSelected,
                            null,adBrandSlctId,adBrandSlctCodeId,null,waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }


                    SharedPrefrence.setCustomMixObj(this,customMixPostObj)
                    postCustomMixData(customMixPostObj)
                }else if(flyAshSourceidsSelected.size == 0 && !flyashKgValue.isNullOrEmpty()){

                    if(isAdmixureAdded){
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),null,
                            flyashKgValue?.toDouble(),adBrandSlctId,adBrandSlctCodeId,admixtureKgValue?.toDouble(),waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }else{
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),null,
                            flyashKgValue?.toDouble(),adBrandSlctId,adBrandSlctCodeId,null,waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }


                    SharedPrefrence.setCustomMixObj(this,customMixPostObj)
                    postCustomMixData(customMixPostObj)
                }else if(flyAshSourceidsSelected.size == 0 && flyashKgValue.isNullOrEmpty()){
                    if(isAdmixureAdded){
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),null,
                            null,adBrandSlctId,adBrandSlctCodeId,admixtureKgValue?.toDouble(),waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }else{
                        customMixPostObj = CustomMixPostObj(stateId,long,lat,gradeTypeId,cementGradeidsSelected,cementBrandidsSelected,cementkgValue?.toDouble(),
                            sand_source_id_slect,sandKgValue?.toDouble(),aggregate1SourceidsSelected,aggregate2SourceidsSelected,aggregateSourceidsSlcID,aggregate1KgValue?.toDouble(),aggregate2KgValue?.toDouble(),null,
                            null,adBrandSlctId,adBrandSlctCodeId,null,waterKgValue?.toDouble(),withConcreatePumpSelecttxt,transistMixureTypeSelectTxt,deliveryDate,null,adBrandSlctCodeId
                        )
                    }


                    SharedPrefrence.setCustomMixObj(this,customMixPostObj)
                    postCustomMixData(customMixPostObj)
                }

                Log.v("log_tag","obj"+ SharedPrefrence.getCustomMixObj(this))

            }
        }

        binding.cementBrandValueTxtLyn.setOnClickListener {
            cementBrandlst.forEach{
                it.isSelected = false
            }

            if(cementBrandids.size >0){
                cementBrandlst.forEach {
                    cementBrandids.forEach { c ->
                        if (it.name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }
            val confiemDialog = CementBandSelectionDialog(cementBrandlst)
            confiemDialog.setListner(this@CustomDesignMix1Activity)
            confiemDialog.show(supportFragmentManager, "CementBrandAlertDialog")
        }

        binding.cementSourceNameValueLnLy?.setOnClickListener {
            sandSourcelst?.forEach{
                it.isSelected = false
            }
            if(sandSourceids.size >0){

                sandSourcelst?.forEach {

                    sandSourceids.forEach { c ->
                        if (it.name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }
            val confiemDialog = SandSourceSelectionDialog(sandSourcelst)
            confiemDialog.setListner(this@CustomDesignMix1Activity)
            confiemDialog.show(supportFragmentManager, "SandSourceSelectionDialog")
        }

        binding.cementAggregrateSourceNameValueLnly?.setOnClickListener {
            aggregateSourcelst?.forEach{
                it.isSelected = false
            }

            if(aggregateSourceids.size >0){
                aggregateSourcelst?.forEach {
                    aggregateSourceids.forEach { c ->
                        if (it.aggregate_source_name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }
            val confiemDialog = AggregateSourceSelectionDialog(aggregateSourcelst)
            confiemDialog.setListner(this@CustomDesignMix1Activity)
            confiemDialog.show(supportFragmentManager, "AggregateSourceSelectionDialog")
        }


        binding.cementFlysourceashValueLnLy?.setOnClickListener {
            flyashsourcelst?.forEach{
                it.isSelected = false
            }

            if(flyAshSourceids.size >0){

                flyashsourcelst?.forEach {

                    flyAshSourceids.forEach { c ->
                        if (it.fly_ash_source_name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }
            val confiemDialog = FlyAshSelectionDialog(flyashsourcelst)
            confiemDialog.setListner(this@CustomDesignMix1Activity)
            confiemDialog.show(supportFragmentManager, "flyashsourcelst")
        }


    }



    private fun setConcreteGradeData(data: ArrayList<ProductCategoryWiseDetailsProductCategory>, view: View) {

        val spinnerAdapter: SelectConcreteGradeAdapter = SelectConcreteGradeAdapter(view.context, data)
        binding.concrateGradeSpinner.setAdapter(spinnerAdapter)

        binding.concrateGradeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                gradeTypeSelectTxt = data.get(position).name
                gradeTypeSelectedId = data.get(position)._id


            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}}

    }



    override fun onBackPressed() {

        binding.backRegistration.performClick()
    }

    fun setupViewModel(){
        customConmixViewModel = ViewModelProvider(
            this,
            CustomConmixViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(CustomConmixViewModel::class.java)
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
        //loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE
        }
        //loaderFrame.visibility = View.GONE
    }





    private fun getConcreateGrade() {
        ApiClient.setToken()
        customConmixViewModel.getProductHome(null, null, null).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                if (::productCategory.isInitialized) {
                                    productCategory.clear()
                                }
                                if (response.body()?.data != null) {
                                    productCategory = response.body()?.data!!
                                }

                                getCMFData()
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })

    }

    private fun getCMFData(){
        val cMFGObj= customMixFixObj(siteId,longitude,latitue)
        ApiClient.setToken()
        customConmixViewModel.getCustomMixFixData(cMFGObj).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                if(response.code() == 200){
                                    val cMFOD = response.body()?.data

                                    cementBrandlst.clear()

                                    if (cMFOD?.cement_brand != null) {
                                        cMFOD?.cement_brand.forEach {
                                            val fsl = CementBrandDataObj(
                                                it._id,
                                                it.name,
                                                it.created_at,
                                                it.created_by,
                                                it.is_deleted,
                                                it.deleted_by,
                                                it.deleted_at,
                                                it.image_url,
                                                it.thumbnail_url,
                                                it.isSelected)
                                            cementBrandlst.add(fsl)
                                        }
                                    }

                                    cementGradelst.clear()

                                    if (cMFOD?.cement_grade != null) {
                                        cMFOD?.cement_grade.forEach {
                                            val fsl = CementGradeDataObj(
                                                it._id,
                                                it.name,
                                                it.created_by,
                                                it.created_at,
                                                it.is_deleted)
                                            cementGradelst.add(fsl)
                                        }
                                    }


                                    sandSourcelst.clear()

                                    if (cMFOD?.sand_source != null) {


                                        cMFOD?.sand_source.forEach {
                                            val fsl = SandSourceDataObj(
                                                it._id,
                                                it.region_id,
                                                it.name,
                                                it.created_by_id,
                                                it.created_at,
                                                it.updated_at,
                                                it.updated_by_id,
                                                it.region_name,
                                                it.isSelected,
                                                it.name)
                                            sandSourcelst.add(fsl)
                                        }

                                    }


                                    aggregateSourcelst.clear()

                                    if (cMFOD?.agg_source != null) {
                                        cMFOD?.agg_source.forEach {
                                            val fsl = AggregateSourceObj(
                                                it._id,
                                                it.region_id,
                                                it.name,
                                                it.created_by_id,
                                                it.created_at,
                                                it.updated_at,it.updated_by_id,it.region_name,it.isSelected,it.name)
                                            aggregateSourcelst.add(fsl)
                                        }
                                    }


                                    flyashsourcelst.clear()
                                    if (cMFOD?.fly_ash_source != null) {


                                        cMFOD?.fly_ash_source.forEach {
                                            val fsl = FlyAshSourceObj(it._id,
                                                it.region_id,
                                                it.name,
                                                it.created_by_id,
                                                it.created_at,
                                                it.updated_at,
                                                it.updated_by_id,
                                                it.region_name,
                                                it.isSelected,
                                                it.name)
                                            flyashsourcelst.add(fsl)
                                        }

                                    }




                                    addmixureBrandlst.clear()

                                    if (cMFOD?.admix_brand != null) {
                                        cMFOD?.admix_brand.forEach {
                                            val fsl = AddmixureBrandObj(
                                                it._id,
                                                it.name,
                                                it.created_at,
                                                it.created_by,
                                                it.is_deleted,
                                                it.deleted_by,
                                                it.deleted_at,
                                                it.image_url,
                                                it.thumbnail_url)
                                            addmixureBrandlst.add(fsl)
                                        }
                                    }


                                    aggregateSubCategorylst1.clear()


                                    if (cMFOD?.agg1_sub_cat != null) {

                                        cMFOD?.agg1_sub_cat.forEach {
                                            val fsl = AggregateSubCategoryDataObj(
                                                it._id,
                                                it.name,
                                                it.sequence,
                                                it.image_url,
                                                it.thumbnail_url,
                                                it.quantity_unit_code,
                                                it.quantity_units,
                                                it.gst_slab,
                                                it.margin_rate,
                                                it.selling_unit,
                                                it.min_quantity,
                                                it.created_by,
                                                it.created_at,
                                                it.product_category,
                                                it.isSelected,
                                                it.name)
                                            aggregateSubCategorylst1.add(fsl)
                                        }

                                        setAggregateCat1(aggregateSubCategorylst1,binding.cementAggregrateCat1ValueSpinner)
                                    }
                                    setUpUi()

                                }

                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })
    }


    fun setUpUi(){
        stopAnim()

        //   binding.btnApply.visibility = View.GONE
        // binding.spaceval.visibility = View.GONE
        binding.backbtn2.visibility = View.GONE
        binding.scroll2.visibility = View.VISIBLE
        binding.scroll1.visibility = View.GONE

        // binding.frmLy1.visibility = View.VISIBLE

        binding.onlyTxt.visibility = View.VISIBLE
        binding.forwardToNext.visibility = View.VISIBLE
        binding.flyashKgLnLy.visibility = View.GONE

        setConcreteGradeData(productCategory, binding.concrateGradeSpinner)
        setCementGradeData(cementGradelst, binding.cementGradeSpinner)
        setAddMixureData(addmixureBrandlst,binding.concrateAdmitureBrandSpinner)

        transistMixurArray.addAll(words)
        binding.concratetransitMixerSpinner.isEnabled = false
        binding.concratetransitMixerSpinner.isClickable = false
        setTransistMixureData(transistMixurArray,binding.concratetransitMixerSpinner)
        setwithConcreatePumpData(transistMixurArray,binding.concratePumpSpinner)



        if(isfromReapeatOrder){



            productCategory.forEach {
                if (it._id?.let { it1 -> orderItem?.concrete_grade?._id?.contains(it1) } == true) {
                    val index = productCategory.indexOf(it)
                    binding.concrateGradeSpinner.setSelection(index)
                }
            }


            orderItem?.cement_brand?.name?.let { cementBrandids.add(it) }
            orderItem?.cement_brand?._id?.let { cementBrandidsSelected.add(it) }
            //binding.cementBrandValueTxt.text = orderItem?.cement_brand?.name

            cementBrandlst.forEach{
                it.isSelected = false
            }

            if(cementBrandids.size >0){
                cementBrandlst.forEach {
                    cementBrandids.forEach { c ->
                        if (it.name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }


            cementBandSelectionlistfrmDialog()



            cementGradelst?.forEach {
                if (it._id?.let { it1 -> orderItem?.cement_grade?._id?.contains(it1) } == true) {
                    val index = cementGradelst.indexOf(it)
                    binding.cementGradeSpinner.setSelection(index)
                }
            }

            orderItem?.sand_source?.sand_source_name?.let { sandSourceids.add(it) }
            orderItem?.sand_source?._id?.let { sandSourceidsSelected.add(it) }

            //binding.cementSourceNameValueTxt.text = orderItem?.sand_source?.sand_source_name

            sandSourcelst?.forEach{
                it.isSelected = false
            }
            if(sandSourceids.size >0){

                sandSourcelst?.forEach {

                    sandSourceids.forEach { c ->
                        if (it.name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }
            sandSourceSelectionlistfrmDialog()



            orderItem?.aggregate_source?.aggregate_source_name?.let { aggregateSourceids.add(it) }
            orderItem?.aggregate_source?._id?.let { aggregateSourceidsSelected.add(it) }

           // binding.cementAggregrateSourceNameValueTxt.text = orderItem?.aggregate_source?.aggregate_source_name


            aggregateSourcelst?.forEach{
                it.isSelected = false
            }

            if(aggregateSourceids.size >0){
                aggregateSourcelst?.forEach {
                    aggregateSourceids.forEach { c ->
                        if (it.aggregate_source_name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }
            aggregateSourceSelectionlistfrmDialog()


            aggregateSubCategorylst1?.forEach {
                if (it._id?.let { it1 -> orderItem?.aggregate1_sub_cat?._id?.contains(it1) } == true) {
                    val index = aggregateSubCategorylst1.indexOf(it)
                    binding.cementAggregrateCat1ValueSpinner.setSelection(index)
                }
            }


            aggregateSubCategory2lst?.forEach {
                if (it._id?.let { it1 -> orderItem?.aggregate2_sub_cat?._id?.contains(it1) } == true) {
                    val index = aggregateSubCategory2lst.indexOf(it)
                    binding.cementAggregrateCat2ValueSpinner.setSelection(index)
                }
            }

            addmixureBrandlst?.forEach {
                if (it._id?.let { it1 -> orderItem?.admix_brand?._id?.contains(it1) } == true) {
                    val index = addmixureBrandlst.indexOf(it)
                    binding.concrateAdmitureBrandSpinner.setSelection(index)
                }
            }

            addmixureCategorylst?.forEach {
                if (it._id?.let { it1 -> orderItem?.admix_cat?._id?.contains(it1) } == true) {
                    val index = addmixureCategorylst.indexOf(it)
                    binding.concrateAdmitureGradeSpinner.setSelection(index)
                }
            }


            orderItem?.fly_ash_source?.fly_ash_source_name?.let { flyAshSourceids.add(it) }
            orderItem?.fly_ash_source?._id?.let { flyAshSourceidsSelected.add(it) }

           // binding.cementFlysourceashValueTxt.text = orderItem?.fly_ash_source?.fly_ash_source_name
            flyashsourcelst?.forEach{
                it.isSelected = false
            }

            if(flyAshSourceids.size >0){

                flyashsourcelst?.forEach {

                    flyAshSourceids.forEach { c ->
                        if (it.fly_ash_source_name == c) {
                            it.isSelected = true
                            return@forEach
                        }
                    }
                }
            }

            flyAshSelectionlistfrmDialog()


            if(orderItem?.with_CP?:false == false){
                binding.concratePumpSpinner.setSelection(1)
            }else{
                binding.concratePumpSpinner.setSelection(0)
            }


            orderItem?.cement_quantity?.let {
                binding.cementKgValue.setText(it.toString())
            }

            orderItem?.sand_quantity?.let {
                binding.sandKgValue.setText(it.toString())
            }

            orderItem?.aggregate1_quantity?.let {
                binding.category1Value.setText(it.toString())
            }


            orderItem?.aggregate2_quantity?.let {
                binding.category2Value.setText(it.toString())
            }

            orderItem?.admix_quantity?.let {


                val tyt = ((it?:0.0) * 100) /(binding.cementKgValue?.text.toString().toDouble())
                //val newTyt = tyt / binding.cementKgValue?.text.toString().toDouble()
                val rationdecimal = Utils.roundOffDecimal(tyt)
                binding.addmixturePercentageValue.setText(rationdecimal.toString())

            }

            orderItem?.fly_ash_quantity?.let {
                binding.flyashKgValue.setText(it.toString())
            }


            orderItem?.water_quantity?.let {
                binding.waterLtrValue.setText(it.toString())
            }
        }
    }

    private fun validComixDesignData(): Boolean {



        if(binding.selectDeliverydate.text.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.select_delivery_date_empty),
                Toast.LENGTH_SHORT
            )
        }

        if(gradeTypeSelectTxt.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.concreate_grade_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

        if(binding.cementKgValue.text.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.cement_kg_select_empty),
                Toast.LENGTH_SHORT
            )

            binding.cementKgValue.setError( getString(R.string.cement_kg_select_empty))
            return false
        }

        if(binding.cementKgValue.text?.trim().toString().toDouble()>=1000){

            Utils.showToast(
                this,
                getString(R.string.cement_value_over_1000),
                Toast.LENGTH_SHORT
            )
            binding.cementKgValue.setError( getString(R.string.cement_value_over_1000))
            return false
        }

        if(binding.sandKgValue.text.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.sand_kg_select_empty),
                Toast.LENGTH_SHORT
            )

            binding.sandKgValue.setError( getString(R.string.sand_kg_select_empty))
            return false
        }
        if(binding.sandKgValue.text?.trim().toString().toDouble()>=1000){

            Utils.showToast(
                this,
                getString(R.string.cement_value_over_1000),
                Toast.LENGTH_SHORT
            )
            binding.sandKgValue.setError( getString(R.string.cement_value_over_1000))
            return false
        }
        if(binding.category1Value.text.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.aggregate_cat1_select_empty),
                Toast.LENGTH_SHORT
            )

            binding.category1Value.setError( getString(R.string.aggregate_cat1_select_empty))
            return false
        }
        if(binding.category1Value.text?.trim().toString().toDouble()>=1000){

            Utils.showToast(
                this,
                getString(R.string.cement_value_over_1000),
                Toast.LENGTH_SHORT
            )
            binding.category1Value.setError( getString(R.string.cement_value_over_1000))
            return false
        }
        if(binding.category2Value.text.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.aggregate_cat2_select_empty),
                Toast.LENGTH_SHORT
            )
            binding.category2Value.setError( getString(R.string.aggregate_cat2_select_empty))
            return false
        }
        if(binding.category2Value.text?.trim().toString().toDouble()>=1000){

            Utils.showToast(
                this,
                getString(R.string.cement_value_over_1000),
                Toast.LENGTH_SHORT
            )
            binding.category2Value.setError( getString(R.string.cement_value_over_1000))
            return false
        }


        if(isAdmixureAdded) {

            if (binding.addmixturePercentageValue.text.isNullOrEmpty()) {
                Utils.showToast(
                    this,
                    getString(R.string.addmixure_percentage_empty),
                    Toast.LENGTH_SHORT
                )
                binding.addmixturePercentageValue.setError(getString(R.string.addmixure_percentage_empty))
                return false
            }

            if (binding.addmixturePercentageValue.text?.trim().toString().toDouble() >= 5) {

                Utils.showToast(
                    this,
                    getString(R.string.cement_value_over_1000),
                    Toast.LENGTH_SHORT
                )
                binding.addmixturePercentageValue.setError(getString(R.string.cement_value_over_1000))
                return false
            }

        }




        if(binding.waterLtrValue.text.isNullOrEmpty()){
           /* Utils.showToast(
                this,
                getString(R.string.water_percentage_empty),
                Toast.LENGTH_SHORT
            )*/
            binding.waterLtrValue.setError( getString(R.string.water_percentage_empty))
            return false
        }


        if(isAdmixureAdded) {

            if (!isCementRatio) {
                Utils.showToast(
                    this,
                    getString(R.string.admixtureqty_error),
                    Toast.LENGTH_SHORT
                )
                return false
            }
        }

        if(!isWaterRatio){


            val minratio = binding.cementKgValue.text?.trim().toString().toDouble()*0.35
            val maxratio = binding.cementKgValue.text?.trim().toString().toDouble()*0.55

            Utils.showToast(
                this,
                String.format(
                    getString(R.string.water_percentage_ratio_error_new),
                    Utils.setPrecesionFormate(minratio),
                    Utils.setPrecesionFormate(maxratio)
                ),
                Toast.LENGTH_SHORT
            )



            return false
        }

        if(!binding.flyashKgValue.text.isNullOrEmpty() && flyAshSourceidsSelected.size == 0){
            Utils.showToast(
                this,
                getString(R.string.flyAsh_error),
                Toast.LENGTH_SHORT
            )
            return false
        }

        if(binding.flyashKgValue.text.isNullOrEmpty() && flyAshSourceidsSelected.size > 0){
            Utils.showToast(
                this,
                getString(R.string.flyAsh_kg_error),
                Toast.LENGTH_SHORT
            )
            return false
        }

        var flyAshkgvald = 0.0

        if(!binding.flyashKgValue.text.isNullOrEmpty() && flyAshSourceidsSelected.size > 0){
            flyAshkgvald = (binding.flyashKgValue?.text?.trim().toString().toDouble())
            if(flyAshkgvald >= 1000){

                Utils.showToast(
                    this,
                    getString(R.string.cement_value_over_1000),
                    Toast.LENGTH_SHORT
                )
                binding.flyashKgValue.setError( getString(R.string.cement_value_over_1000))
                return false
            }


        }

        var addMixKg = 0.0
        if(isAdmixureAdded){
            if(!binding.addmixtureKgValue.text.isNullOrEmpty()){
                addMixKg = binding.addmixtureKgValue.text.toString().toDouble()
            }
        }



        val calculation = (binding.cementKgValue.text?.trim().toString().toDouble())+(binding.sandKgValue.text?.trim().toString().toDouble())+
                (binding.category1Value.text?.trim().toString().toDouble())+(binding.category2Value.text?.trim().toString().toDouble())+
                addMixKg+(binding.waterLtrValue.text?.trim().toString().toDouble())+ flyAshkgvald



        if(!(calculation>= 2400 && calculation<=2550)){

            Utils.showToast(
                this,
                getString(R.string.concrete_density_error),
                Toast.LENGTH_SHORT
            )

            return false
        }

        return true

    }


    private fun setCementGradeData(data: ArrayList<CementGradeDataObj>, view: View) {

        val spinnerAdapter: SelectCementGradeAdapter = SelectCementGradeAdapter(view.context, data)
        binding.cementGradeSpinner.adapter = spinnerAdapter

        binding.cementGradeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                cementGradeTypeSelectTxt = data.get(position).name
                cementGradeTypeSelectID = data.get(position)._id
                cementGradeidsSelected.clear()
                cementGradeidsSelected.add(cementGradeTypeSelectID!!)

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setAddMixureData(data: ArrayList<AddmixureBrandObj>, view: View){

        val firstObj =  AddmixureBrandObj("first", "Select Admixture Brand", null,
                                     null,null, null,
                                     null,
                                    null, null)
        data.add(0,firstObj)
        val spinnerAdapter: AddMixureBrandAdapter = AddMixureBrandAdapter(view.context, data)
        binding.concrateAdmitureBrandSpinner.adapter = spinnerAdapter


        binding.concrateAdmitureBrandSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if(position != 0) {
                    isAdmixureAdded = true
                    addmixureBrandTypeSelectTxt = data.get(position).name
                    admixtureBrandidsSelected.clear()
                    addmixureCodeSelecTxt = null
                    addmixureCategorylst.clear()
                    admixtureCateidsSelected.clear()
                    admixtureBrandidsSelected.add(data.get(position)._id)
                    getAddmixureCodeCategoryData(data.get(position)._id)
                    binding.addmixturePercentageTitle.visibility = View.VISIBLE
                    binding.addmixtureKgLnLy.visibility = View.VISIBLE
                    binding.admixurcodetitle.visibility = View.VISIBLE
                    binding.admixurcodeFm.visibility = View.VISIBLE
                    binding.admixurcodeView.visibility = View.VISIBLE
                    binding.addmixturePercentageValue.isEnabled = true
                    //binding.addmixturePercentageValue.text?.clear()
                    //binding.addmixtureKgValue.text?.clear()
                }else if(position == 0){
                    isAdmixureAdded = false
                    admixtureBrandidsSelected.clear()
                    addmixureCodeSelecTxt = null
                    addmixureCategorylst.clear()
                    admixtureCateidsSelected.clear()
                    addmixureCodeSelecTxt = null
                    admixtureCateidsSelected.clear()
                    binding.concrateAdmitureGradeSpinner.adapter = null
                    binding.addmixturePercentageValue.isEnabled = false
                    binding.addmixturePercentageValue.text?.clear()
                    binding.addmixtureKgValue.text?.clear()
                    binding.addmixturePercentageTitle.visibility = View.GONE
                    binding.addmixtureKgLnLy.visibility = View.GONE
                    binding.admixurcodetitle.visibility = View.GONE
                    binding.admixurcodeFm.visibility = View.GONE
                    binding.admixurcodeView.visibility = View.GONE
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }


    private fun setAggregateCat1(data: ArrayList<AggregateSubCategoryDataObj>, view: View){
        val spinnerAdapter: AggregateCat1Adapter = AggregateCat1Adapter(view.context, data)
        binding.cementAggregrateCat1ValueSpinner.adapter = spinnerAdapter

        binding.cementAggregrateCat1ValueSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

                try {
                    selectedAggregateSource1Txt = data.get(position).sub_category_name
                    aggregate1SourceidsSelected.clear()
                    selectedAggregateSource2Txt = null
                   // aggregateSubCategorylst1.clear()
                    aggregate2SourceidsSelected.clear()
                    aggregate1SourceidsSelected.add(data.get(position)._id)
                    binding.category1ValueTIL.setHint(String.format(getString(R.string.tenmm_title), selectedAggregateSource1Txt))
                    getAggregate2Data(data.get(position)._id)
                }catch(e:Exception){

                }


            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    private fun setAggregateCat2(data: ArrayList<AggregateSubCategoryDataObj>, view: View){
        val spinnerAdapter: AggregateCat2Adapter = AggregateCat2Adapter(view.context, data)
        binding.cementAggregrateCat2ValueSpinner.adapter = spinnerAdapter

        binding.cementAggregrateCat2ValueSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                selectedAggregateSource2Txt = data.get(position).sub_category_name
                aggregate2SourceidsSelected.clear()
                aggregate2SourceidsSelected.add(data.get(position)._id)
                binding.category2ValueTIL.setHint(String.format(getString(R.string.twenty_mm_title), selectedAggregateSource2Txt))


            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }
    private fun setTransistMixureData(data: ArrayList<String>,view: View){
        val spinnerAdapter: TransistMixurAdapter = TransistMixurAdapter(view.context, data)
        binding.concratetransitMixerSpinner.adapter = spinnerAdapter

        binding.concratetransitMixerSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

                transistMixureTypeSelectTxt = parent.selectedItem as String == "Yes"

                if(parent.selectedItem == "Yes"){
                    binding.withoutTmError.visibility = View.GONE
                }else{
                    binding.withoutTmError.visibility = View.VISIBLE
                }


            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setwithConcreatePumpData(data: ArrayList<String>,view: View){
        val spinnerAdapter: TransistMixurAdapter = TransistMixurAdapter(view.context, data)
        binding.concratePumpSpinner.adapter = spinnerAdapter

        binding.concratePumpSpinner.setSelection(1)
        withConcreatePumpSelecttxt = false

        binding.concratePumpSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                withConcreatePumpSelecttxt = parent.selectedItem as String == "Yes"
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    private fun setAddMixureCodeData(data: ArrayList<AddmixureCategoryObj>, view: View){
        val spinnerAdapter: AddMixureCodeAdapter = AddMixureCodeAdapter(view.context, data)
        binding.concrateAdmitureGradeSpinner.adapter = spinnerAdapter

        binding.concrateAdmitureGradeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                addmixureCodeSelecTxt = data.get(position).category_name
                admixtureCateidsSelected.clear()
                admixtureCateidsSelected.add(data.get(position)._id!!)


            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    override fun cementBandSelectionlistfrmDialog() {


        cementBrandids.clear()
        cementBrandlst?.forEach {
            if(it.isSelected){
                cementBrandids.add(it.name!!)

            }
        }

        var commaseparatedlist: String = cementBrandids.toString()

        commaseparatedlist = commaseparatedlist.replace("[", "")
            .replace("]", "")
            .replace(" ", "")

        binding.cementBrandValueTxt.text = commaseparatedlist
    }


    override fun sandSourceSelectionlistfrmDialog() {
        sandSourceids.clear()
        sandSourcelst?.forEach {
            if(it.isSelected){
                sandSourceids.add(it.sand_source_name!!)

            }
        }

        var commaseparatedlist: String = sandSourceids.toString()

        commaseparatedlist = commaseparatedlist.replace("[", "")
            .replace("]", "")
            .replace(" ", "")

        binding.cementSourceNameValueTxt.text = commaseparatedlist


    }

    override fun aggregateSourceSelectionlistfrmDialog() {
        aggregateSourceids.clear()
        aggregateSourcelst?.forEach {
            if(it.isSelected){
                aggregateSourceids.add(it.aggregate_source_name!!)

            }
        }

        var commaseparatedlist: String = aggregateSourceids.toString()

        commaseparatedlist = commaseparatedlist.replace("[", "")
            .replace("]", "")
            .replace(" ", "")

        binding.cementAggregrateSourceNameValueTxt.text = commaseparatedlist

    }



    override fun flyAshSelectionlistfrmDialog() {
        flyAshSourceids.clear()
        flyashsourcelst?.forEach {
            if(it.isSelected){
                flyAshSourceids.add(it.name!!)

            }
        }

        var commaseparatedlist: String = flyAshSourceids.toString()

        commaseparatedlist = commaseparatedlist.replace("[", "")
            .replace("]", "")
            .replace(" ", "")

        binding.cementFlysourceashValueTxt.text = commaseparatedlist


        if(commaseparatedlist.isEmpty()){
            binding.flyashKgLnLy.visibility = View.GONE
            binding.flyashKgValue.text?.clear()
        }else{
            binding.flyashKgLnLy.visibility = View.VISIBLE
        }

    }


    fun getAddmixureCodeCategoryData(brandId: String?){
        ApiClient.setToken()
        customConmixViewModel.getAdmixureCategoryData(brandId, null, null, null).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                addmixureCategorylst.clear()
                                if (response.body()?.data != null) {


                                    addmixureCategorylst = response.body()?.data!!
                                    setAddMixureCodeData(addmixureCategorylst,binding.concrateAdmitureGradeSpinner)

                                }else{


                                    addmixureCodeSelecTxt = null
                                    admixtureCateidsSelected.clear()
                                }
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }

    fun getAggregate2Data(categoryId: String?){
        ApiClient.setToken()
        customConmixViewModel.getAggregateSandSubCategory2Data(categoryId, null, null, null).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                aggregateSubCategory2lst.clear()
                                if (response.body()?.data != null) {
                                    aggregateSubCategory2lst = response.body()?.data!!
                                    setAggregateCat2(aggregateSubCategory2lst,binding.cementAggregrateCat2ValueSpinner)

                                }else{
                                    selectedAggregateSource2Txt = null
                                    aggregate2SourceidsSelected.clear()
                                }
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }





    fun validComixDesignApplyData():Boolean{


        if(binding.cementBrandValueTxt.text.trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.cement_brand_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if(cementGradeTypeSelectTxt.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.cement_brand_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }
        if(binding.cementSourceNameValueTxt.text.trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.sand_source_name_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

      /*
        if(binding.cementAggregrateSourceNameValueTxt.text.trim().isEmpty()){
            Utils.showToast(
                this,
                getString(R.string.aggregrate_source_name_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }*/

        if(selectedAggregateSource1Txt.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.aggregrate_cat1_name_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if(selectedAggregateSource2Txt.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.aggregrate_cat2_name_select_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }


        if(isAdmixureAdded) {

            if (addmixureBrandTypeSelectTxt.isNullOrEmpty()) {
                Utils.showToast(
                    this,
                    getString(R.string.admixture_brand_name_select_empty),
                    Toast.LENGTH_SHORT
                )
                return false
            }

            if (addmixureCodeSelecTxt.isNullOrEmpty()) {
                Utils.showToast(
                    this,
                    getString(R.string.admixture_code_name_select_empty),
                    Toast.LENGTH_SHORT
                )
                return false
            }
        }

        /*if(transistMixureTypeSelectTxt.isNullOrEmpty()){
            Utils.showToast(
                    this,
                    getString(R.string.transit_mixer_select_empty),
                    Toast.LENGTH_SHORT
            )
            return false
        }

        if(withConcreatePumpSelecttxt.isNullOrEmpty()){
            Utils.showToast(
                    this,
                    getString(R.string.concrete_pump_select_empty),
                    Toast.LENGTH_SHORT
            )
            return false
        }*/

        return true

    }
    fun postCustomMixData(customMixPostObjRequest: CustomMixPostObj?){


        ApiClient.setToken()
        customConmixViewModel.postCustomMixData(customMixPostObjRequest).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                binding.avLoading.visibility = View.GONE
                                binding.avLoading.hide()
                                customMixReslst.clear()
                                if (response.body()?.data != null) {


                                    //SharedPrefrence.setSelectedDate(this,binding.selectDeliverydate.text?.toString())

                                    customMixReslst.addAll(response.body()?.data!!)
                                    val intent =Intent(this, CustomConmixListActivity::class.java)
                                    intent.putExtra("customMixLst", customMixReslst)
                                    startActivity(intent)
                                    finish()
                                    Bungee.slideLeft(this)

                                }
                            }
                        } else {
                            binding.avLoading.visibility = View.GONE
                            binding.avLoading.hide()
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        binding.avLoading.visibility = View.GONE
                        binding.avLoading.hide()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()
                    }
                }
            }
        })
    }

    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun changeTextColor(str:String){
        val spannableString = SpannableString(str)
        val foregroundSpan = ForegroundColorSpan(getColor(R.color.login_btn_color))
        spannableString.setSpan(
            foregroundSpan,
            1,
            2,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.tvTitle2.text = spannableString
        binding.onlyTxt.text = spannableString
    }

   /* fun PerfectDecimal(str: String, MAX_BEFORE_POINT: Int, MAX_DECIMAL: Int): String? {
        var str = str
        if (str[0] == '.') str = "0$str"
        val max = str.length
        var rFinal = ""
        var after = false
        var i = 0
        var up = 0
        var decimal = 0
        var t: Char
        while (i < max) {
            t = str[i]
            if (t != '.' && after == false) {
                up++
                if (up > MAX_BEFORE_POINT) return rFinal
            } else if (t == '.') {
                after = true
            } else {
                decimal++
                if (decimal > MAX_DECIMAL) return rFinal
            }
            rFinal = rFinal + t
            i++
        }
        return rFinal
    }*/


}