package com.conmix.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.R
import com.conmix.app.data.LoginResponse
import com.conmix.app.databinding.OtpActivityBinding
import com.conmix.app.services.SMSBroadcast.SmsVerifyCatcher
import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.ForgotPasswordViewModel
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee
import java.util.regex.Pattern


/**
 * Created by Hitesh Patel on 04,March,2021
 */
class ForgotPassWordOTPActivity: BaseActivity() {
    private lateinit var binding: OtpActivityBinding
    private var mobNum:String? = null
    private lateinit var forgotPasswordViewModel: ForgotPasswordViewModel
    private var otp: String? = ""
    private var cTimer: CountDownTimer? = null
    var isFromProductDetail: Boolean = false
    private var smsVerifyCatcher: SmsVerifyCatcher? = null
    var isEmail = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OtpActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fullScreen()
        binding.btnVerify.isEnabled = false
        binding.btnVerify.alpha = 0.5f
        stopAnim()
        getIntentdate()
        setupViewModel()
        mobNum = intent.getStringExtra(CoreConstants.Intent.INTENT_MOB_NUMBER)
        //init SmsVerifyCatcher
        smsVerifyCatcher = SmsVerifyCatcher(
                this
        ) { message ->
            val code: String? = parseCode(message) //Parse verification code
            binding.codeEdtpin.setText(code) //set code in edit text
            //then you can send verification code to server
        }


        smsVerifyCatcher?.setPhoneNumberFilter("VM-DSHSMS")
        //init SmsVerifyCatcher
        smsVerifyCatcher = SmsVerifyCatcher(
                this
        ) { message ->
            val code: String? = parseCode(message)

            //Parse verification code
            binding.codeEdtpin.setText(code) //set code in edit text
            //then you can send verification code to server
        }
        if (!mobNum.isNullOrEmpty()) {
            binding.tvDescription.visibility = View.VISIBLE

            if(!isEmail) {

                binding.tvDescription.text = Utils.getMaskMobileNumber(mobNum,isEmail,this)


            }else{
                binding.tvDescription.text = Utils.getMaskMobileNumber(mobNum,isEmail,this)
            }
        } else
            binding.tvDescription.visibility = View.GONE

        reverseTimer(120)

        binding.btnVerify.setOnClickListener {
            stopAnim()
            otp = binding.codeEdtpin.text?.toString()
            if (otp?.isNotEmpty() == true && otp?.length == 6) {
                cTimer?.cancel()
                ForgotPassOtp()
            } else {
                stopAnim()
            }
        }
        binding.codeEdtpin.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                stopAnim()
                otp = binding.codeEdtpin.text?.toString()
                if (otp?.isNotEmpty() ?: false && otp?.length == 6) {
                    cTimer?.cancel()
                    ForgotPassOtp()
                } else {
                    stopAnim()
                }
            }
            false
        }
        binding.codeEdtpin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
               // binding.btnVerify.isEnabled = s?.length == 6
                if(s?.length == 6){
                    binding.btnVerify.isEnabled = true
                    binding.btnVerify.alpha = 1.0f
                }else{
                    binding.btnVerify.isEnabled = false
                    binding.btnVerify.alpha = 0.5f
                }
            }

        })

        binding.resendTxt.setOnClickListener {
            getOtp("sms")
        }
        binding.callWithCodeText.setOnClickListener {
            getOtp("call")
        }

        binding.backBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

    }


    override fun onBackPressed() {
        binding.backBtn.performClick()
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
        //loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE
        }
        //loaderFrame.visibility = View.GONE
    }

    private fun getIntentdate() {
       // isFromProductDetail =intent.getBooleanExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, false)
        isEmail = intent.getBooleanExtra("isEmail",false)
   }

    private fun setupViewModel() {
        forgotPasswordViewModel =
                ViewModelProvider(
                        this,
                        ForgotPasswordViewModel.ViewModelFactory(ApiClient().apiService)
                )
                        .get(ForgotPasswordViewModel::class.java)
    }

    private fun parseCode(message: String): String? {
        val p = Pattern.compile("\\b\\d{6}\\b")
        val m = p.matcher(message)
        var code: String? = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    private fun reverseTimer(Seconds: Int) {
        binding.resendTxt.visibility = View.GONE
        binding.callWithCodeText.visibility = View.GONE
        binding.faltuLine.visibility = View.GONE
        binding.otpTimer.visibility = View.VISIBLE
        binding.otpTimer.text = "00:00"
        cTimer?.cancel()
        cTimer = object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val minutes = seconds / 60
                seconds %= 60
                binding.otpTimer.text =
                        String.format("%02d", minutes) + ":" + String.format("%02d", seconds)

            }

            override fun onFinish() {
                //tvResendCode.text = "00:00"
                binding.otpTimer.visibility = View.GONE
                binding.resendTxt.visibility = View.VISIBLE
                binding.callWithCodeText.visibility = View.GONE
                binding.faltuLine.visibility = View.GONE
                binding.resendTxt.text = getString(R.string.txt_resend_code)
                //callWithCodeText.visibility = View.GONE
                //faltuLine.visibility = View.GONE
                //timer.visibility = View.GONE

            }
        }.start()
    }


    private fun ForgotPassOtp() {
        ApiClient.setToken()
        otp?.let { forgotPasswordViewModel.forgotpasswordOtp(mobNum!!, it) }?.observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {

                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response -> setResponseFromLogin(response.body()?.data) }

                        } else {
                            cTimer?.cancel()
                            binding.resendTxt.visibility = View.VISIBLE
                            binding.otpTimer.visibility = View.GONE
                            binding.callWithCodeText.visibility = View.GONE
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        cTimer?.cancel()
                        binding.resendTxt.visibility = View.VISIBLE
                        binding.otpTimer.visibility = View.GONE
                        binding.callWithCodeText.visibility = View.GONE
                        Utils.showToast(
                                this,
                                "Kindly enter the correct OTP",
                                Toast.LENGTH_SHORT
                        )
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }

            }
        })

    }

    private fun setResponseFromLogin(response: LoginResponse?) {
        val customToken = response?.customToken
        SharedPrefrence.setSessionToken(this, customToken)
        //SharedPrefrence.setLogin(this, true)
        navigateToMainActivity()
    }

    private fun navigateToMainActivity() {

        if (isFromProductDetail) {
            val intent = Intent(this, ActivityForgotConfirm::class.java)
            intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL,isFromProductDetail)
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)
        } else {
            val intent = Intent(this, ActivityForgotConfirm::class.java)
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)
        }
    }

    private fun getOtp(type: String) {
        ApiClient.setToken()
        reverseTimer(120)
        forgotPasswordViewModel.forgotPassword(mobNum!!, type)
                .observe(this, Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                stopAnim()
                                if (resource.data?.isSuccessful!!) {

                                } else {
                                    /* Utils.showToast(
                                        this,
                                        getString(R.string.err_mobile_exits),
                                        Toast.LENGTH_SHORT
                                    )*/

                                }

                            }
                            Status.ERROR -> {
                                stopAnim()
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                            Status.LOADING -> {
                                //startAnim()

                            }

                        }
                    }
                })
    }
    override fun onStart() {
        super.onStart()
        smsVerifyCatcher!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        smsVerifyCatcher!!.onStop()
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!)
        smsVerifyCatcher!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}