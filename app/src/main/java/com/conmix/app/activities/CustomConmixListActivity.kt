package com.conmix.app.activities


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.conmix.app.adapter.CustomMixListAdapter
import com.conmix.app.data.CustomMixResObjData
import com.conmix.app.databinding.CustomMixListActivityBinding
import com.conmix.app.databinding.EmptyConcreteGradeBinding
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 01,April,2021
 */
class CustomConmixListActivity: BaseActivity(), CustomMixListAdapter.SelectCustomMixInterface {
    private lateinit var  binding: CustomMixListActivityBinding
    private lateinit var bindingNodata: EmptyConcreteGradeBinding
    var customMixReslst:ArrayList<CustomMixResObjData> = ArrayList()
    var customMixAdapter: CustomMixListAdapter? = null
    var selectedObj:CustomMixResObjData?= null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CustomMixListActivityBinding.inflate(layoutInflater)
        bindingNodata = binding.noConcrateGrade
        setContentView(binding.root)

        customMixReslst =
            intent.getParcelableArrayListExtra<CustomMixResObjData>("customMixLst") ?: ArrayList()

        if (customMixReslst.size ?: 0 > 0) {
            bindingNodata.constrainEmptySite.visibility = View.GONE
            binding.rvCustomMixListVwNest?.visibility = View.VISIBLE
            binding.rvCustomMixListVw?.visibility = View.VISIBLE
            binding.btnProceed?.visibility = View.GONE

            customMixAdapter = CustomMixListAdapter(this, customMixReslst, "M10")
            val mLayoutManager = LinearLayoutManager(this)
            binding.rvCustomMixListVw.layoutManager = mLayoutManager
            customMixAdapter?.setListener(this)
            binding.rvCustomMixListVw.adapter = customMixAdapter
        } else {
            binding.btnProceed?.visibility = View.GONE
            binding.rvCustomMixListVwNest?.visibility = View.GONE
            binding.rvCustomMixListVw?.visibility = View.GONE
            bindingNodata.constrainEmptySite.visibility = View.VISIBLE
            bindingNodata.tvNoData.text = "No RMC supplier found"

        }
        binding.backConCreteGradeList.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding?.btnProceed?.setOnClickListener {
            if(selectedObj?.isSelected == true){
                val intent = Intent(this, CustomMixlstDeatilActivity::class.java)
                intent.putExtra("vendorId", selectedObj?.vendor_id)
                intent.putExtra("addressId",selectedObj?.address_id)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.backConCreteGradeList.performClick()
    }

    override fun onClickDesignList(cmrd: CustomMixResObjData) {
        /*val intent = Intent(this, CustomMixlstDeatilActivity::class.java)
        intent.putExtra("vendorId", cmrd.vendor_id)
        intent.putExtra("addressId",cmrd.address_id)
        startActivity(intent)
        Bungee.slideLeft(this)*/

        if(cmrd.isSelected == true){
            selectedObj = cmrd
            binding.btnProceed?.visibility = View.VISIBLE
        }else{
            binding.btnProceed?.visibility = View.GONE
        }



    }
}