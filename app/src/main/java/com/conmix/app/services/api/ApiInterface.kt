package com.conmix.app.services.api

import android.os.Parcelable
import com.conmix.app.data.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


/**
 * Created by Hitesh Patel on 24,February,2021
 */

interface ApiInterface {
    @POST("sessions")
    suspend fun getSessionToken(@Body sessionRequest: SessionRequest?): Response<Data<SessionResponse>>

    @GET("buyer/auth/mobiles/{mobileNumber}")
    suspend fun getOtpByMobNum(
            @Path("mobileNumber") mobileNumber: String?,
            @Query("method") method: String?
    ): Response<ResponseBody>

    @GET("buyer/users/requestOTP")
    suspend fun getUserOTP(): Response<ResponseBody>

    @POST("buyer/auth/mobiles/{mobileNumber}/code/{code}")
    suspend fun verifyOtp(
            @Path("mobileNumber") mobileNumber: String?,
            @Path("code") code: String?
    ): Response<Data<LoginResponse>>

    @GET("buyer/auth/email/{email}")
    suspend fun getOtpByEmail(
        @Path("email") email: String?
    ): Response<ResponseBody>

    @POST("buyer/users")
    suspend fun signupRegisterUser(
            @Query("signup_type") signuptype: String,
            @Body signUpRegisterModel: SignUpRegisterModel
    ): Response<Data<LoginResponse>>

    @PATCH("buyer/users")
    suspend fun updateUserProfile(@Body signUpRegisterModel: UpdateUserProfileObj)
            : Response<Data<LoginResponse>>

    @POST("buyer/auth/{identifier}")
    suspend fun loginWithIdentifier(
            @Path("identifier") identifier: String,
            @Body loginReqModel: LoginReqModel
    ): Response<Data<LoginResponse>>

    @GET("buyer/auth/users/{identifier}")
    suspend fun forgotPassword(
            @Path("identifier") identifier: String?,
            @Query("method") method: String?
    ): Response<ResponseBody>

    @POST("buyer/auth/users/{identifier}/codes/{code}")
    suspend fun verifyForgotpasswordOtp(
            @Path("identifier") identifier: String?,
            @Path("code") code: String?
    ): Response<Data<LoginResponse>>

    @PATCH("buyer/users")
    suspend fun updatePassWordProfile(@Body updateUserProfileData: UpdateUserPasswordProfileObj?)
            : Response<Data<LoginResponse>>
    @PATCH("buyer/users/changePassword")
    suspend fun changePassWordProfile(@Body chnagePasswordProfileObj: ChnagePasswordProfileObj?)
            : Response<ResponseBody>

    @GET("concrete_grade")
    suspend fun getProductHome(
            @Query("search") search: String?,
            @Query("before") before: String?,
            @Query("after") after: String?
    ): Response<ProductlistCategory>


    @POST("region")
    suspend fun addState(@Body citydetails: Citydetails): Response<Data<Citydetails>>

    @GET("region/getCountries")
    suspend fun getCountryData(): Response<CountryDataModel>

    @GET("region/getStates")
    suspend fun getStateData(): Response<StateDataModel>

    @GET("region/getStatesByCountryId/{countryId}")
    suspend fun getStateFromCountryData(@Path("countryId") countryId: String?)
            : Response<StateDataModel>

    @GET("region/getCitiesByStateId/{stateId}")
    suspend fun getCityData(@Path("stateId") stateId: String?): Response<CityDataModel>


    @GET("buyer/site")
    suspend fun getSiteAddressListData(
            @Query("before") beforeTxt: String?,
            @Query("after") afterTxt: String?,
            @Query("search") search: String?
    ): Response<SiteAddressListData>

    @GET("buyer/billing_address")
    suspend fun getBillAddressListData():Response<BillAddressListData>


    @POST("buyer/site")
    suspend fun postAddNewSiteAdd(@Body addSiteAddPostData: AddSiteAddPostData): Response<AddSiteAddPostResult>

    @POST("buyer/billing_address")
    suspend fun postBillAddNewAdd(@Body addBillAddPostData:AddBillAddPostObj?):Response<AddBillAddressPostResult>

    @PATCH("buyer/billing_address/{billingId}")
    suspend fun updateBillAddNewAdd(@Path("billingId") id:String?,@Body addBillAddPostData:AddBillAddPostObj?):Response<ResponseBody>

    @POST("buyer/cart/billing_address")
    suspend fun addBillAddressCart(@Body addBillAddressIdCart:AddBillAddressIdCart?):Response<ResponseBody>

    @GET("buyer/users/profile")
    suspend fun getUserProfileData(): Response<UserProfileData>

    @DELETE("buyer/site/{siteId}")
    suspend fun deleteSiteAddProfile(@Path("siteId") siteId: String?): Response<ResponseBody>

    @DELETE("buyer/billing_address/{addressId}")
    suspend fun deleteBillAddProfile(@Path("addressId") addressId: String?): Response<ResponseBody>

    @GET("buyer/site/{siteId}")
    suspend fun getSiteAddPerticulerId(@Path("siteId") siteId: String): Response<SiteAddressObjModel>

    @PATCH("buyer/site/{siteId}")
    suspend fun updateAddProfile(
            @Path("siteId") siteId: String?,
            @Body addSiteAddPostData: AddSiteAddPostData
    ): Response<ResponseBody>

    @Multipart
    @PATCH("buyer/users")
    suspend fun uploadUserPic(@Part("full_name") full_name: RequestBody?,
                              @Part userImg: MultipartBody.Part): Response<Data<LoginResponse>>

    @GET("design_mix")
    suspend fun getDesignMixList(@Query("site_id") site_id: String?,
                                 @Query("long") long: String?,
                                 @Query("lat") lat: String?,
                                 @Query("grade_id") grade_id: String?,
                                 @Query("before") before: String?,
                                 @Query("after") after: String?,
                                 @Query("order_by") order_by: String?,
                                 @Query("page_num") page_num: String?,
                                 @Query("delivery_date") delivery_date: String?,
                                 @Query("end_date") end_date: String?,
                                 @Query("quantity") quantity: Int?
    ): Response<DesignListData>

    @GET("design_mix/{design_mix_id}")
    suspend fun getDesignMixObj(@Path("design_mix_id")design_mix_id:String?,
                                @Query("site_id")site_id:String?,
                                @Query("long") long: String?,
                                @Query("lat") lat: String?,
                                @Query("delivery_date") delivery_date: String?,
                                @Query("state_id")state_id:String?,@Query("with_TM")with_TM:Boolean?,@Query("with_CP")with_CP: Boolean?): Response<DesignListObjMain>
    // Custom Page Api-->

    @GET("admixture_brand")
    suspend fun getAdmixtureBandData(@Query("search")search:String?,
                                     @Query("before")before:String?,
                                     @Query("after")after:String?):Response<AddmixureBrandList>

    @POST("custom_mix/options")
    suspend fun getCustomMixFixData(@Body customMixFixObj:customMixFixObj?):Response<CustomMixFixObjMainData>

    @GET("aggregate-sand-category")
    suspend fun getAggregateSandCategoryData(@Query("search")search:String?,
                                             @Query("before")before:String?,
                                             @Query("after")after:String?):Response<AggregateCategoryList>

    @GET("aggregate-sand-category/subcategory/{categoryId}")
    suspend fun getAggregateSandSubCategoryData(@Path("categoryId")categoryId:String?,@Query("search")search:String?,
                                             @Query("before")before:String?,
                                             @Query("after")after:String?
                                                )
    :Response<AggregateSubCategoryList>

    @GET("aggregate-sand-category/check/{id}")
    suspend fun getAggregateSandSubCategory2Data(@Path("id")id:String?,@Query("search")search:String?,
                                                @Query("before")before:String?,
                                                @Query("after")after:String?
    )
            :Response<AggregateSubCategoryList>

    @GET("cement_brand")
    suspend fun getCementBandData(@Query("search")search:String?,
                                 @Query("before")before:String?,
                                 @Query("after")after:String?):Response<CementBrandList>

    @GET("source/aggregate")
    suspend fun getAggregateSourceData():Response<AggregateSourceList>

    @GET("source/fly_ash")
    suspend fun getFlyAshSourceData():Response<FlyAshSourceList>

    @GET("source/sand")
    suspend fun getSandSourceData():Response<SandSourceList>

    @GET("admixture_category")
    suspend fun getAdmixureCategoryData(@Query("brand_id")brand_id:String?,@Query("search")search:String?,
                                        @Query("before")before:String?,
                                        @Query("after")after:String?
                                        )
            :Response<AddmixureCategoryList>

    @GET("cement_grade")
    suspend fun getCementGradeData():Response<CementGradeDataList>

    @POST("custom_mix")
    suspend fun postCustomMixData(@Body customMixPostObjRequest: CustomMixPostObj?):Response<CustomMixResLstData>

    @POST("custom_mix/details/{vendor_id}")
    suspend fun getCustomMixDetailData(@Path("vendor_id")vendor_id:String?,@Body customMixPostObjRequest: CustomMixPostObj?):Response<CustomMixResLstDetailData>


    @POST("buyer/cart")
    suspend fun postCustomMixAddToCartObj(@Body addToCartCustomMixObj: AddToCartCustomMixObj?):Response<CustomMixAddtoCartRepData>


    @GET("buyer/cart")
    suspend fun getCartData(@Query("state_id") state_id:String?,@Query("long") long:Double?,@Query("lat") lat:Double?,@Query("with_TM") with_TM:Boolean?,@Query("with_CP") with_CP:Boolean?,@Query("delivery_date")delivery_date:String?):Response<CartData>


    @GET("buyer/cart/{cartId}")
    suspend fun getCartDataWithId(@Path("cartId")cartId:String?,@Query("state_id") state_id:String?, @Query("long") long:Double?, @Query("lat") lat:Double?, @Query("with_TM") with_TM:Boolean?, @Query("with_CP") with_CP:Boolean?, @Query("delivery_date")delivery_date:String?):Response<CartData>


    @HTTP(method = "DELETE", path = "buyer/cart/item", hasBody = true)
    suspend fun deleteCartItem(@Body deleteCartObj: DeleteCartObj?):Response<ResponseBody>

    @POST("buyer/cart/address")
    suspend fun cartAddressAdd(@Body cartAddressIdObj: CartAddressIdObj?):Response<ResponseBody>

    @GET("review/getVendorReview/{vendor_id}")
    suspend fun getVenderReview(@Path("vendor_id")vendor_id:String?,@Query("before")before:String?,@Query("after")after:String?):Response<VenderReviewdataRes>


    @POST("buyer/review")
    suspend fun postVenderReviewData(@Body venderWriteReviewObj: VenderWriteReviewObj?):Response<Data<VenderWriteReviewRes>>

    @GET("product-review/{product_id}")
    suspend fun getProductReview(@Path("product_id")product_id:String?, @Query("user_id") user_id:String?,@Query("before")before:String?, @Query("after")after:String?):Response<ProductReviewResData>

    @GET("buyer/cart/coupon")
    suspend fun getCouponsData(): Response<Data<ArrayList<CouponDataModel>>>

    @POST("buyer/cart/coupon")
    suspend fun postCouponCode(@Body couponObj: CouponObj?):Response<ResponseBody>

    @POST("buyer/cart/coupon/{cartId}")
    suspend fun postCouponCodeWithClientId(@Path("cartId") cartId:String?,@Body couponObj: CouponObj?):Response<ResponseBody>


    @POST("buyer/order")
    suspend fun postOrderData(@Body gatewayTranstedIdObj: GatewayTranstedIdObj?):Response<OrderResDta>

    @GET("buyer/order")
    suspend fun getOrderlstData(@Query("user_id")user_id:String?,
                                @Query("before")before:String?,
                                @Query("after")after:String?,
                                @Query("order_status")order_status:String?,
                                @Query("payment_status")payment_status:String?,
                                @Query("gateway_transaction_id")gateway_transaction_id:String?):Response<OrderLstDta>

    @GET("buyer/order/{orderId}")
    suspend fun getOrderDetailLstData(@Path("orderId")orderId:String?):Response<OrderLstDetailDta>

    @POST("buyer/order/{order_item_id}/cancel/{order_id}")
    suspend fun cancelOrderdta(@Body paymentTranstedIdObj: PaymentTranstedIdObj?,@Path("order_item_id")order_item_id:String?, @Path("order_id")order_id:String?):Response<ResponseBody>

    @POST("buyer/order/{orderId}/cancelOrderItem/{itemId}")
    suspend fun cancelOrderItem(@Path("orderId")orderId:String?,@Path("itemId")itemId:String?):Response<ResponseBody>

    @GET("cron/order/process")
    suspend fun processOrder(@Query("order_id") orderId: String): Response<ResponseBody>

    @GET("buyer/order_track/{order_item_id}")
    suspend fun getTrackListOrder(@Path("order_item_id")order_item_id:String?):Response<TrackOrderLst>

    @GET("buyer/order_track/{order_item_id}/{tracking_id}")
    suspend fun getTrackDtlOrder(@Path("order_item_id")order_item_id:String?,@Path("tracking_id")tracking_id:String?):Response<TrackOrderdtl>

    @POST("buyer/fcm-tokens")
    suspend fun registerFCMToken(@Body fcmPostReq: FcmData?): Response<ResponseBody>


    @DELETE("buyer/fcm-tokens/{deviceID}")
    suspend fun removeFCMToken(@Path("deviceID") deviceID: String?): Response<ResponseBody>

    @GET("buyer/notification/count")
    suspend fun getNotificationCount(): Response<Data<NotificationCount>>

    @GET("buyer/notification/all")
    suspend fun getNotification(@Query("before") beforeTxt: String?): Response<Data<NotificationDataList>>

    @PATCH("buyer/notification/{notificationId}")
    suspend fun notificationItemSeen(@Path("notificationId") notificationId: String?): Response<ResponseBody>


    @POST("payments/{payment_id}/capture")
    suspend fun paymentCapture(@Header("Authorization") authheader:String, @Path("payment_id") payment_id:String, @Body paymentCaptureObj: PaymentCaptureObj): Response<ResponseBody>

    @GET("pay_method")
    suspend fun getPaymentTypes(@Query("search") search: String?,@Query("before") before: String?,@Query("after") after: String?):Response<PaymentTypesArr>

    @POST("buyer/site/check")
    suspend fun checkSiteAdd(@Body checkSiteNameObj:CheckSiteNameObj):Response<ResponseBody>

    @POST("buyer/order/CheckTMAvailbility")
    suspend fun checkTMAvailbility(@Body checkTmAvlObj:checkTmAvlObj?):Response<ResponseBody>

    @POST("buyer/order/{order_id}/{order_item_id}")
    suspend fun UpdateAssignTm(@Path("order_id") order_id:String?,@Path("order_item_id") order_item_id:String?,@Body assignTmOBJ:AssignTMOBJ?):Response<ResponseBody>

    @GET("buyer/order_track/CP/{order_item_part_id}")
    suspend fun getAssignTrackOrderDetail(@Path("order_item_part_id")_id:String?):Response<AssignTrackDlData>

    @POST("buyer/users/requestOTPForUpdateProfile")
    suspend fun getUpdateEmailOtp(@Body updateProfileOtpObj:UpdateProfileOtpObj):Response<ResponseBody>

}