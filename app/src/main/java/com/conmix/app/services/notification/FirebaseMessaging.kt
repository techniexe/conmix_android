package com.conmix.app.services.notification

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.conmix.app.R
import com.conmix.app.activities.MainActivity

import com.conmix.app.utils.SharedPrefrence
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.ktx.messaging


class FirebaseMessaging : FirebaseMessagingService() {

    override fun onNewToken(s: String) {
        /*Firebase.messaging.getToken()
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                val id = task.result!!.id
                SharedPrefrence.setFCMtokem(this@FirebaseMessaging, task.result!!.token)
                val isLoogedIn = SharedPrefrence.getLogin(this)
                if (isLoogedIn) {
                    // registerFcmToken(s!!, id)
                }

            })*/


       /* Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            val msg = getString(R.string.msg_token_fmt, token)
            Log.d(TAG, msg)
            Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
        })*/
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage);


        // Check if message contains a notification payload.
        if (remoteMessage.data != null) {

            handleNotification(remoteMessage!!)
            val intent = Intent()
            intent.action = "NEW_Notification"
            sendBroadcast(intent)
            val count: Int = SharedPrefrence.getCount(this)
            SharedPrefrence.setCount(
                this,
                count + 1
            )


        }
    }

    private fun handleNotification(remoteMessage: RemoteMessage) {
        if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            val message = remoteMessage.data.get("body") ?: ""

            val resultIntent = Intent(applicationContext, MainActivity::class.java)
            val hashmap: HashMap<String, String> = HashMap()
            remoteMessage.data.keys.forEach {
                hashmap.put(it, remoteMessage.data.getValue(it).toString())
            }
            resultIntent.putExtra("data", hashmap)
            resultIntent.putExtra("isFromNotification", true)
             resultIntent.putExtra("isFromSplash", true)
            // image is present, show notification with image
            // val title = hashmap.get("title")?:"Yippee"
            showNotificationMessage(
                applicationContext, "ConmixApp", message, "" + System
                    .currentTimeMillis(), resultIntent
            )
        } else {
            // If the app is in background, firebase itself handles the notification
            val message = remoteMessage.data.get("body") ?: ""
            val resultIntent = Intent(applicationContext, MainActivity::class.java)
            val hashmap: HashMap<String, String> = HashMap()
            remoteMessage.data.keys.forEach {
                hashmap.put(it, remoteMessage.data.getValue(it).toString())
            }
            resultIntent.putExtra("data", hashmap)
            Log.d("tag", "background")
            resultIntent.putExtra("isFromNotification", true)
            resultIntent.putExtra("isFromSplash", true)
            //val title = hashmap.get("title")?:"ConmateBuyer"
            showNotificationMessage(
                applicationContext,
                "ConmixApp",
                 message,
                "" + System.currentTimeMillis(),
                resultIntent
            )
        }
    }

    private var notificationUtils: NotificationUtils? = null
    private fun showNotificationMessage(
        context: Context,
        title: String,
        message: String,
        timeStamp: String,
        intent: Intent
    ) {
        notificationUtils = NotificationUtils(context)
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("isFromNotification", true)
        intent.putExtra("isFromSplash", true)
        notificationUtils?.showNotificationMessage(title, message, timeStamp, intent)
    }

    companion object {

        private const val TAG = "MainActivity"
    }

}
