package com.conmix.app.services.api

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.conmix.app.BuildConfig

import com.conmix.app.ConmixApp
import com.conmix.app.activities.LoginActivity
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import comCoreConstants.conmix.utils.CoreConstants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


import okhttp3.Response
import java.util.concurrent.TimeUnit


/**
 * Created by Hitesh Patel on 24,February,2021
 */
class ApiClient {
    companion object  {
        var authToken: String =""
        fun setToken() {
            authToken = "Bearer " + SharedPrefrence.getSessionToken(ConmixApp.instance?.applicationContext)
        }
    }

    private var retrofit: Retrofit? = null
    private  val REQUEST_TIMEOUT = 60
    private var okHttpClient: OkHttpClient? = null


    fun getClient(): Retrofit? {

        if (!Utils.isNetworkAvailable(ConmixApp.instance?.applicationContext) && ConmixApp.instance?.applicationContext != null) {
            Utils.showToast(
                ConmixApp.instance?.applicationContext,
                "Please check your internet connection",
                Toast.LENGTH_SHORT
            )

        }
        authToken = "Bearer " + SharedPrefrence.getSessionToken(ConmixApp.instance?.applicationContext)

        if (okHttpClient == null)
            ConmixApp.instance?.applicationContext?.let { initOkHttp(it) }

        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit

    }

    private fun clientForSearch():Retrofit{
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client =
            OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .client(client)
            .baseUrl("https://api.github.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    }

    private fun clientPaymentCapture():Retrofit{
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl("https://api.razorpay.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

    }

    private fun getClientWithoutAuth(): Retrofit {
        Log.d("ApiClient ","getClientWithoutAuth ")
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client =
            OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            // .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    val apiService: ApiInterface = getClient()!!.create(ApiInterface::class.java)

    val apiServiceWithAuth: ApiInterface = getClientWithoutAuth().create(ApiInterface::class.java)

    val apiServiceForSearch: ApiInterface = clientForSearch().create(ApiInterface::class.java)
    val apiPaymentCapture: ApiInterface = clientPaymentCapture().create(ApiInterface::class.java)


    private fun initOkHttp(context: Context) {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(interceptor)
        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val original = chain.request()
                val requestBuilder = authToken?.let {
                    original.newBuilder()
                        .addHeader("Authorization", it)
                        .addHeader("Accept", "application/json")
                        .addHeader("Request-Type", "Android")
                        .addHeader("Content-Type", "application/json")
                }
                val request = requestBuilder!!.build()
                val response = chain.proceed(request)
                Log.d("request===",request.toString())
                if (response.code == 401) {
                    //new Utils().showToast(context, context.getString(R.string.login_again), Toast.LENGTH_SHORT);
                    val intent = Intent(context, LoginActivity::class.java)
                    intent.putExtra(CoreConstants.Intent.INTENT_AUTH_ERROR, true)
                    SharedPrefrence.setLogin(context,false)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ConmixApp.instance?.startActivity(intent)
                }
                return response
            }
        })
        okHttpClient = httpClient.build()
    }
}