package com.conmix.app.services.api


/**
 * Created by Hitesh Patel on 24,February,2021
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}