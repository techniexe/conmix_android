package com.conmix.app.services.interfaces

import android.view.View


/**
 * Created by Hitesh Patel on 08,March,2021
 */
interface RecyclerOnSubItemClickListener {
    fun onItemClick(childView: View?, id: String?, subId: String?, categoryName:String?)

}
interface RecyclerOnItemAddWishListClickListener {
    fun onItemClick(index:Int,isWishList:Boolean,productId:String)

}