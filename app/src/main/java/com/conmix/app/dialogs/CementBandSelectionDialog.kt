package com.conmix.app.dialogs


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.SelectCementBrandDpDwnAdapter
import com.conmix.app.data.CementBrandDataObj
import com.conmix.app.data.SelectedCementBrandChecked


/**
 * Created by Hitesh Patel on 23,March,2021
 */
class CementBandSelectionDialog(private var cementBrandListDg:ArrayList<CementBrandDataObj>) : DialogFragment(),
    SelectCementBrandDpDwnAdapter.SelectCementBrandInterface {


    var mListener: DialogToFragment? = null
    var sselectedSitedatalst: ArrayList<CementBrandDataObj> = ArrayList<CementBrandDataObj>()
    var selectedCementBrandCheckedlst:ArrayList<SelectedCementBrandChecked> = ArrayList<SelectedCementBrandChecked>()
    var selectCementBrandDpDwnAdapter: SelectCementBrandDpDwnAdapter?= null
    init {
        sselectedSitedatalst.clear()
        sselectedSitedatalst.addAll(cementBrandListDg)
        selectedCementBrandCheckedlst.clear()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is DialogToFragment) {
            context as DialogToFragment
        } else {
            throw ClassCastException(context.toString().toString() +
                    " must implement " + ".MyListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface DialogToFragment {
        fun cementBandSelectionlistfrmDialog()
    }

    fun setListner(listner: DialogToFragment) {
        mListener = listner
    }



    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
        //dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        // mListner!!.dialogDismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.select_cement_brand_dialog, null)
        builder.setView(view)
        setRetainInstance(true)

        val btnCancel: ImageView = view.findViewById(R.id.imgClose)
        val btnSave :Button = view.findViewById(R.id.btnSubmit)
        val rvListPlace:RecyclerView = view.findViewById(R.id.cementBrandlst)
        val title:TextView = view.findViewById(R.id.dialog_title)//Select Cement Brand

        title.text = "Select Cement Brand"

        selectCementBrandDpDwnAdapter = SelectCementBrandDpDwnAdapter(requireContext(), sselectedSitedatalst)
        val mLayoutManager = LinearLayoutManager(context)
        rvListPlace?.layoutManager = mLayoutManager
        selectCementBrandDpDwnAdapter?.setListener(this@CementBandSelectionDialog)
        rvListPlace?.adapter = selectCementBrandDpDwnAdapter

        btnCancel.setOnClickListener {
            Log.v("log_tag","cementBrandListDg" +cementBrandListDg.toString())
            dismiss()

        }

        btnSave.setOnClickListener {

            mListener?.cementBandSelectionlistfrmDialog()
            dismiss()
        }
        return builder.create()
    }

    override fun onAddSelected(ischeck:Boolean,position:String) {

        /*val obj = SelectedCementBrandChecked(ischeck,position)
        selectedCementBrandCheckedlst.forEach {

            if(it.position == position){
                val index = selectedCementBrandCheckedlst.indexOf(it)

                selectedCementBrandCheckedlst.add(index,obj)
            }else{
                selectedCementBrandCheckedlst.add(obj)
            }

        }*/

    }
}
