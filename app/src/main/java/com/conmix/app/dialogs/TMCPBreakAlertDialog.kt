package com.conmix.app.dialogs

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.conmix.app.R


/**
 * Created by Hitesh Patel on 04,May,2021
 */
class TMCPBreakAlertDialog : DialogFragment {
    var mListener: ClickListener? = null
    private var mTitle: String? = null
    private val mMsg: String? = null
    private var mPostiveText: String? = null
    private val mUserImage: String? = null

    @SuppressLint("ValidFragment")
    constructor() {
    }

    @SuppressLint("ValidFragment")
    constructor(
        dialogListener: ClickListener?,
        title: String?,
        positiveBtn: String?

    ) {
        mListener = dialogListener
        mTitle = title
        mPostiveText = positiveBtn

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        val v: View = inflater.inflate(R.layout.tm_cp_alert_dialog, container, false)
        val title =
                v.findViewById<View>(R.id.titleAlertDialog) as TextView
        val doneButton =
                v.findViewById<View>(R.id.postiveBtnAlertDialog) as TextView

        doneButton.setOnClickListener {
            mListener!!.onDoneClicked()
            dismiss()
        }

        title.text = mTitle
        //title.setSelectAllOnFocus(true)
        doneButton.text = mPostiveText

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
       // dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        return v
    }

    interface ClickListener {
        fun onDoneClicked()
        fun onCancelClicked()
    }
}