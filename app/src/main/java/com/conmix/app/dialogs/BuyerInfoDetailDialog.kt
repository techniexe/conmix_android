package com.conmix.app.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import com.conmix.app.R


/**
 * Created by Hitesh Patel on 21,April,2021
 */
class BuyerInfoDetailDialog(var tmNo: String?,var driverName:String?,var driverMobNo:String?) : DialogFragment() {



    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window?.setGravity(Gravity.CENTER)

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
        //dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.buyer_info_dialog, null)
        builder.setView(view)

        val tmNoTxt = view.findViewById<TextView>(R.id.transit_mixer_numer_txt) as TextView
        val driverNameTxt = view.findViewById<TextView>(R.id.driver_name_txt) as TextView
        val driverNoTxt = view.findViewById<TextView>(R.id.driver_mobile_txt) as TextView
       // val opretorNameTxt = view.findViewById<TextView>(R.id.operator_name_txt) as TextView
       // val opretorNoTxt = view.findViewById<TextView>(R.id.operator_mobile_txt) as TextView
        val imgClose= view.findViewById<ImageView>(R.id.imgClose) as ImageView

        tmNoTxt.text = String.format(
                getString(R.string.buyer_info_colon_txt),
                tmNo
        )
        driverNameTxt.text = String.format(
                getString(R.string.buyer_info_colon_txt),
                driverName
        )

        driverNoTxt.text = String.format(
                getString(R.string.buyer_info_colon_txt),
                driverMobNo
        )

       /* opretorNameTxt.text = String.format(
                getString(R.string.buyer_info_colon_txt),
                opretorName
        )

        opretorNoTxt.text = String.format(
                getString(R.string.buyer_info_colon_txt),
                opretorMobNo
        )
*/


        imgClose.setOnClickListener {
            dismiss()

        }
        return builder.create()
    }

}