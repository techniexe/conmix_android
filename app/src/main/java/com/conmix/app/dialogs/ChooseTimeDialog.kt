package com.conmix.app.dialogs

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.conmix.app.R
import com.conmix.app.activities.PlaceAutoCompleteActivity
import com.conmix.app.data.CartItemLst
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import spencerstudios.com.bungeelib.Bungee
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Hitesh Patel on 12,April,2021
 */
class ChooseTimeDialog(var tmValue: Double?,var pumpValue:Double?,var dta: CartItemLst?) : DialogFragment(){

    private var mListner: DialogToFragment? = null
    private var selectedDate: String? = null
    var minDate:Long = 0
    var maxDate :Long = 0
    var isQty:Boolean = false
    private var selectedEndDate:String?= null


    interface DialogToFragment {

        fun dialogChangeDateSave(date:String,endDate:String)
    }

    fun setListener(mListener: DialogToFragment) {
        this.mListner = mListener


    }

    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window?.setGravity(Gravity.CENTER)

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
        //dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.choose_time_dialog, null)
        builder.setView(view)

        val tm_value = view.findViewById<TextView>(R.id.tm_value) as TextView
        val pump_value = view.findViewById<TextView>(R.id.pump_value) as TextView
        val imgClose= view.findViewById<ImageView>(R.id.imgClose) as ImageView
        val datetxt = view.findViewById<TextView>(R.id.datetxt) as TextView
        val calendarlnly = view.findViewById<LinearLayout>(R.id.calendarlnly) as LinearLayout
        val btnSubmit = view.findViewById<Button>(R.id.btnSubmit) as Button
        val pumpLnly = view.findViewById<LinearLayout>(R.id.pumpLnly) as LinearLayout
        val tmLnly = view.findViewById<LinearLayout>(R.id.tmLnly) as LinearLayout
        val tmAndPumpTxt = view.findViewById<TextView>(R.id.tmAndPumpTxt) as TextView
        val dateInfoTxt = view.findViewById<TextView>(R.id.dateInfoTxt) as TextView



       /* val  calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE,2)
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val date = simpleDateFormat.format(calendar.time)
        selectedDate = Utils.getConvertDateWithoutTime(date)
        selectedEndDate = Utils.getConvertDateWithoutTime(date)
        datetxt.setText("$selectedDate - $selectedEndDate")
        isQty = true*/


        selectedDate = Utils.getConvertDateWithoutTime(dta?.delivery_date?:"")
        selectedEndDate = Utils.getConvertDateWithoutTime(dta?.end_date?:"")

        val isEndDatevalid = Utils.comparedateToday(selectedEndDate!!,selectedDate!!)
        if(isEndDatevalid){
            datetxt.setText("$selectedDate - $selectedEndDate")
            isQty = true
        }else{
            selectedEndDate = selectedDate
            datetxt.setText("$selectedDate - $selectedEndDate")
            isQty = true
        }



        if(isQty) {
            val qtytxt = dta?.quantity.toString()
            val result = Utils.comparenddateToStartday(selectedEndDate!!, selectedDate!!)
            val ratio = qtytxt.toDouble() / 3.0
            val rationdecimal = Math.ceil(ratio).toInt()

            if (result > rationdecimal) {
                dateInfoTxt.visibility = View.VISIBLE
               /* dateInfoTxt.text =
                    String.format(getString(R.string.date_range_info_error), rationdecimal)*/

                Utils.dateRangeError(requireContext(),rationdecimal,dateInfoTxt)

                dateInfoTxt.setTextColor(Color.parseColor("#B94A48"))
            } else {
                dateInfoTxt.visibility = View.VISIBLE
                /*dateInfoTxt.text =
                    String.format(getString(R.string.date_range_info), rationdecimal)*/
                Utils.dateRangeError(requireContext(),rationdecimal,dateInfoTxt)
                dateInfoTxt.setTextColor(Color.parseColor("#19A42C"))
            }
        }else{
            //Utils.showToast(this,getString(R.string.qty_value_error),Toast.LENGTH_SHORT)
            dateInfoTxt.visibility = View.GONE
        }

        val builderM = MaterialDatePicker.Builder.dateRangePicker()
        builderM.setTitleText("Select Date")
        val constraintBuilder = CalendarConstraints.Builder()
        val c = Calendar.getInstance()
        val ce = Calendar.getInstance()
        c.add(Calendar.DATE,1)
        ce.add(Calendar.DATE,32)

        val calendarStart = c
        val calendarEnd = ce

        minDate = calendarStart.timeInMillis
        maxDate = calendarEnd.timeInMillis

        var title  = getString(R.string.static_tm_cp_date_no_avilable)
        //if(pumpValue != null && pumpValue?:0.0 >0){
        if(dta?.with_TM == true && dta?.with_CP == true){
            tmLnly.visibility = View.VISIBLE
            pumpLnly.visibility = View.VISIBLE
            val tmPrice = Utils.setPrecesionFormate(tmValue)
            tm_value.setText(" ₹"+tmPrice+" Per Cu.Mtr / Km")
            pump_value.setText(" ₹"+ Utils.setPrecesionFormate(pumpValue)+" Per day")
        }else if(dta?.with_TM == true && dta?.with_CP == false){
            tmLnly.visibility = View.VISIBLE
            pumpLnly.visibility = View.GONE
            val tmPrice = Utils.setPrecesionFormate(tmValue)
            tm_value.setText(" ₹"+tmPrice+" Per Cu.Mtr / Km")
            //title  = getString(R.string.static_tm_date_title)

        }else if(dta?.with_TM == true && dta?.with_CP == false){
            tmLnly.visibility = View.GONE
            pumpLnly.visibility = View.VISIBLE
            pump_value.setText(" ₹"+ Utils.setPrecesionFormate(pumpValue)+" Per day")
            //title  = getString(R.string.static_cp_date_title)
        }else{
            tmLnly.visibility = View.GONE
            pumpLnly.visibility = View.GONE
           // title = getString(R.string.static_tm_cp_date_no_avilable)
            pumpLnly.visibility = View.GONE
        }

        tmAndPumpTxt.text = title



        calendarlnly.setOnClickListener {

            constraintBuilder.setStart(minDate)
            constraintBuilder.setEnd(maxDate)
            constraintBuilder.setValidator(
                PlaceAutoCompleteActivity.RangeValidator(
                    minDate,
                    maxDate
                )
            )
            builderM.setSelection(androidx.core.util.Pair(Utils.getTimeInLong(selectedDate!!),Utils.getTimeInLong(selectedEndDate!!)))
            builderM.setCalendarConstraints(constraintBuilder.build())

            val picker = builderM.build()
            activity?.supportFragmentManager?.let { it1 -> picker.show(it1, "date_picker_tag") }

            picker.addOnPositiveButtonClickListener {
                val formater = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val awal = formater.format(Date(it.first ?: 0))
                val akhit = formater.format(Date(it.second ?: 0))
                selectedDate = awal
                selectedEndDate = akhit
                datetxt.setText("$awal - $akhit")


                if(isQty) {
                    val qtytxt = dta?.quantity.toString()
                    val result = Utils.comparenddateToStartday(selectedEndDate!!, selectedDate!!)
                    val ratio = qtytxt.toDouble() / 3.0
                    val rationdecimal = Math.ceil(ratio).toInt()

                    if (result > rationdecimal) {
                        dateInfoTxt.visibility = View.VISIBLE
                       /* dateInfoTxt.text =
                            String.format(getString(R.string.date_range_info_error), rationdecimal)*/

                        Utils.dateRangeError(requireContext(),rationdecimal,dateInfoTxt)
                        dateInfoTxt.setTextColor(Color.parseColor("#B94A48"))
                    } else {
                        dateInfoTxt.visibility = View.VISIBLE
                        /*dateInfoTxt.text =
                            String.format(getString(R.string.date_range_info), rationdecimal)*/
                        Utils.dateRangeError(requireContext(),rationdecimal,dateInfoTxt)
                        dateInfoTxt.setTextColor(Color.parseColor("#19A42C"))
                    }
                }else{
                    //Utils.showToast(this,getString(R.string.qty_value_error),Toast.LENGTH_SHORT)
                    dateInfoTxt.visibility = View.GONE
                }

            }

            picker.addOnNegativeButtonClickListener {
                picker.dismiss()
            }
        }


        btnSubmit?.setOnClickListener {
           // mListner!!.dialogChangeDateSave(selectedDate!!)

            if(isQty) {

                val qtytxt = dta?.quantity.toString()
                val result = Utils.comparenddateToStartday(selectedEndDate!!, selectedDate!!)
                val ratio = qtytxt.toDouble() / 3.0
                val rationdecimal = Math.ceil(ratio).toInt()

                if(result > rationdecimal){
                    dateInfoTxt.visibility = View.VISIBLE
                    /*dateInfoTxt.text = String.format(getString(R.string.date_range_info), rationdecimal)
                    Utils.showToast(requireContext(),String.format(getString(R.string.date_range_info), rationdecimal),Toast.LENGTH_SHORT)*/

                    Utils.dateRangeError(requireContext(),rationdecimal,dateInfoTxt)
                    Utils.dateRangeErrorTost(requireContext(),rationdecimal,dateInfoTxt)


                }else {

                    /*SharedPrefrence.setSELECTEDQTY(this, binding.qtytxt.text.toString().toInt())
                    SharedPrefrence.setSelectedDate(
                        this@LoginSelectSiteAddressActivity,
                        selectedDate
                    )
                    SharedPrefrence.setSelectedENDDate(
                        this@LoginSelectSiteAddressActivity,
                        selectedEndDate
                    )
                    finish()
                    Bungee.slideRight(this)*/


                    mListner!!.dialogChangeDateSave(selectedDate!!,selectedEndDate!!)
                    dismiss()

                }
            }else{
                Utils.showToast(requireContext(),getString(R.string.qty_value_error),Toast.LENGTH_SHORT)
            }

        }

        imgClose.setOnClickListener {
            dismiss()

        }


        return builder.create()
    }

    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }
}