package com.conmix.app.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent

import android.os.Bundle
import android.provider.Settings
import android.text.InputFilter
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.Log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*

import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.conmix.app.BuildConfig

import com.conmix.app.R
import com.conmix.app.activities.CartActivity
import com.conmix.app.activities.ForgotPassWordActivity
import com.conmix.app.activities.MainActivity
import com.conmix.app.activities.SignUpActivity
import com.conmix.app.data.*

import com.conmix.app.services.api.ApiClient
import com.conmix.app.services.api.Status
import com.conmix.app.utils.SharedPrefrence
import com.conmix.app.utils.Utils
import com.conmix.app.viewmodels.CustomMixAddToCartViewModel
import com.conmix.app.viewmodels.LoginViewModel
import com.conmix.app.viewmodels.NotificationViewModel
import com.conmix.app.viewmodels.SessionViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import comCoreConstants.conmix.utils.CoreConstants
import spencerstudios.com.bungeelib.Bungee

import java.util.*


/**
 * Created by Hitesh Patel on 05,April,2021
 */
class LoginDialog(val isFromProductDetail: Boolean,val obj:AddToCartCustomMixObj?) : DialogFragment() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    var avLoading:com.wang.avi.AVLoadingIndicatorView?= null
    var etMobileNo:EditText? = null
    var ccpL:com.hbb20.CountryCodePicker?= null
    var etPassword:EditText?= null
    var forgotpassword:TextView? =null
    var btnSignIn:Button? = null
    var signUpTxt:TextView? = null
    private lateinit var customMixAddToCartViewModel: CustomMixAddToCartViewModel



    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.setBackgroundDrawable(null);
        dialog!!.setCanceledOnTouchOutside(false)
       // dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
        setupViewModel()

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater

        val view = inflater.inflate(R.layout.login_dialog, null)
        builder.setView(view)

        val imgClose = view.findViewById<ImageView>(R.id.imgClose)
        avLoading = view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.avLoading)
        etMobileNo = view.findViewById<EditText>(R.id.etMobileNo)
        ccpL = view.findViewById(R.id.ccpL)
        etPassword = view.findViewById(R.id.etPassword)
        forgotpassword = view.findViewById(R.id.forgotpassword)
        btnSignIn = view.findViewById<Button>(R.id.btnSignIn)
        signUpTxt = view.findViewById(R.id.signUpTxt)
        setupUI()

        /*signUpTxt?.setOnClickListener {
            val intent = Intent(requireContext(), SignUpActivity::class.java)
            intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, true)
            intent.putExtra("cartObj",obj)
            startActivity(intent)
            dismiss()

        }*/
        forgotpassword?.setOnClickListener {

            val intent = Intent(requireContext(), ForgotPassWordActivity::class.java)
            intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, true)
            startActivity(intent)
            Bungee.slideLeft(requireContext())

        }

        getSessionToken(null, null)

       etPassword?.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signIn(etPassword!!)
            }
            false
        }
        btnSignIn?.setOnClickListener { v ->

            signIn(v)

        }



        imgClose.setOnClickListener {
            dismiss()

        }

        return builder.create()
    }

    private fun setupViewModel() {

        sessionViewModel =
                ViewModelProvider(
                        this,
                        SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
                ).get(SessionViewModel::class.java)

        loginViewModel =
                ViewModelProvider(this, LoginViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(LoginViewModel::class.java)

        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)

        customMixAddToCartViewModel = ViewModelProvider(
            this,
            CustomMixAddToCartViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(CustomMixAddToCartViewModel::class.java)

    }
    private fun setupUI() {
        etMobileNo?.filters = arrayOf(
                Utils.ignoreFirstWhiteSpace()

                )
       etPassword?.filters = arrayOf(
                Utils.ignoreFirstWhiteSpace(),
                InputFilter.LengthFilter(20)

        )

        val signupLinkClickSpan = object : ClickableSpan() {
            override fun onClick(view: View) {


                val intent = Intent(requireContext(), SignUpActivity::class.java)
                intent.putExtra(CoreConstants.Intent.ISFROMPRODUCTDETAIL, true)
                intent.putExtra("cartObj",obj)
                startActivity(intent)
                Bungee.slideLeft(requireActivity())
                dismiss()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val clickableText = arrayOf(getString(R.string.txt_account_sub_msg))
        val clickableSpan: Array<ClickableSpan> = arrayOf(signupLinkClickSpan)
        Utils.setStringSpanable(signUpTxt!!, clickableText, clickableSpan)

    }
    private fun startAnim() {
        if (avLoading != null)
            avLoading?.show()

    }

    private fun stopAnim() {
        if (avLoading != null)
           avLoading?.hide()

    }
    private fun validateFields(identifier: String, password: String): Boolean {
        if (identifier.isEmpty()) {
            Utils.showToast(
                    requireContext(),
                    getString(R.string.err_ragister_mobile_number),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        if (password.isEmpty()) {
            Utils.showToast(requireContext(), getString(R.string.err_password), Toast.LENGTH_SHORT)
            return false
        }
        if (password.length < 6) {

            Utils.showToast(
                    requireContext(),
                    getString(R.string.err_password_length),
                    Toast.LENGTH_SHORT
            )
            return false
        }
        return true
    }

    private fun login(userName: String, password: String) {
        ApiClient.setToken()
        loginViewModel.loginWithIdentifier(
                userName = userName,
                loginReqModel = LoginReqModel(password)
        ).observe(this, androidx.lifecycle.Observer {

            it?.let {

                resource ->

                when (resource.status) {

                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {

                            Utils.showToast(
                                    requireContext(),
                                    getString(R.string.sucess_login),
                                    Toast.LENGTH_SHORT
                            )
                            setResponseFromLogin(resource.data.body()!!.data)
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })
    }

    private fun signIn(v: View) {
        Utils.hideKeyboard(v)
        if (Utils.isNetworkAvailable(requireContext())) {
            val userName = etMobileNo?.text.toString().trim()
            val password = etPassword?.text.toString().trim()
            val phoneUtil = PhoneNumberUtil.getInstance()

            if (validateFields(userName, password)) {
                try {

                    val numberProto = phoneUtil.parse(userName, ccpL?.selectedCountryNameCode)
                    val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                                    numberProto
                            ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {
                        getSessionToken(mobNum, password)
                    } else {

                        Utils.showToast(
                                requireContext(),
                                getString(R.string.err_invalid_number),
                                Toast.LENGTH_SHORT
                        )
                    }
                } catch (e: NumberParseException) {

                    if (userName.isNotEmpty()) {

                        val emval = Utils.isValidEmail(userName)
                        if (emval) {
                            getSessionToken(userName, password)
                        } else {
                            Utils.showToast(
                                    requireContext(),
                                    getString(R.string.err_ragister_mobileemail_number),
                                    Toast.LENGTH_SHORT
                            )

                        }
                    }
                }
            }
        } else {
            Utils.showToast(requireContext(), getString(R.string.no_internet_msg), Toast.LENGTH_SHORT)
        }
    }


    private fun setResponseFromLogin(loginResponse: LoginResponse) {
        val customToken = loginResponse.customToken
        SharedPrefrence.setSessionToken(requireContext(), customToken)
        SharedPrefrence.setLogin(requireContext(), true)
       // navigateToMainActivity()

        registerFCMToken()
    }

    private fun registerFCMToken() {

        Firebase.messaging.getToken().addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            val secureId = Settings.Secure.getString(requireContext().getContentResolver(), Settings.Secure.ANDROID_ID)
            SharedPrefrence.setDeviceID(requireContext(), secureId)
            SharedPrefrence.setFCMtokem(requireContext(), token)
            registerFcmTokenToServer(token, secureId ?: "")
        })
    }

    private fun registerFcmTokenToServer(token: String, deviceId: String) {
        ApiClient.setToken()
        var fcmData = FcmData(token, deviceId, "android")
        notificationViewModel.ragisterToken(fcmData).observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            Utils.showToast(
                                requireContext() ,
                                getString(R.string.sucess_login),
                                Toast.LENGTH_SHORT
                            )
                            navigateToMainActivity()

                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                }

            }
        })
    }


    private fun navigateToMainActivity() {
        postCustomMixData(obj)
    }


    private fun getSessionToken(userName: String?, password: String?) {
        ApiClient.setToken()
        val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

        sessionViewModel.getSession(sessionRequest = sessionRequest)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                if (resource.data?.isSuccessful!!) {
                                    resource.data?.let { response ->
                                        setData(
                                                response.body()?.data,
                                                userName,
                                                password
                                        )
                                    }
                                } else {
                                    Utils.setErrorData(requireContext(), resource.data.errorBody())
                                }

                            }
                            Status.ERROR -> {
                                Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                            }
                            Status.LOADING -> {

                            }

                        }
                    }
                })
    }

    private fun setData(response: SessionResponse?, userName: String?, password: String?) {
        if (response != null) {
            SharedPrefrence.setSessionToken(requireContext(), response.token)
        }

        if (userName != null && password != null) {
            login(userName, password)
        }
    }


    fun postCustomMixData(addToCartCustomMixObj: AddToCartCustomMixObj?){


        ApiClient.setToken()
        customMixAddToCartViewModel.postCustomMixAddToCartObj(addToCartCustomMixObj).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->

                                if (response.body()?.data != null) {

                                    SharedPrefrence.setTRANSISTNo(requireContext(),null)
                                    SharedPrefrence.setDriverName(requireContext(),null)
                                    SharedPrefrence.setDriverMobile(requireContext(),null)
                                    SharedPrefrence.setOPRETORName(requireContext(),null)
                                    SharedPrefrence.setOPRETORMobile(requireContext(),null)

                                    Utils.showToast(requireContext(),getString(R.string.cart_added_successfully),Toast.LENGTH_SHORT)
                                    dismiss()
                                    val intent = Intent(requireContext(), CartActivity::class.java)
                                    requireContext().startActivity(intent)
                                    Bungee.slideLeft(requireContext())

                                }
                            }
                        } else {

                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        /*binding.avLoading.visibility = View.VISIBLE
                        binding.avLoading.show()*/
                    }
                }
            }
        })
    }

}