package com.conmix.app.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import com.conmix.app.R
import com.conmix.app.utils.Utils
import com.conmix.app.utils.Utils.ignoreFirstWhiteSpace
import java.lang.NumberFormatException


/**
 * Created by Hitesh Patel on 05,April,2021
 */
class UpdateQtyDialog(var count: Int,var position:Int) : DialogFragment() {

    private var mListner: DialogToFragment? = null
    var isQty:Boolean = false


    interface DialogToFragment {
        fun dialogDismiss()
        fun dialogSave( count: Int, position:Int)
    }

    fun setListener(mListener: DialogToFragment) {
        this.mListner = mListener


    }

    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window?.setGravity(Gravity.CENTER)

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
       // dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mListner!!.dialogDismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.update_qty_dailog, null)
        builder.setView(view)

        val etQty = view.findViewById<EditText>(R.id.etQty) as EditText
        val btnUpdate = view.findViewById<Button>(R.id.btnUpdate) as Button
        val imgClose= view.findViewById<ImageView>(R.id.imgClose) as ImageView
        val qtyErrorTxt = view.findViewById<TextView>(R.id.qtyErrorTxt) as TextView

        etQty.filters = arrayOf<InputFilter>(ignoreFirstWhiteSpace())
        var totalMinQty = 3
        var totalMaxQty = 100
        etQty.setText(count.toString())
        etQty.setSelection(etQty.text.length)
        etQty.setFilters(arrayOf<InputFilter>(LengthFilter(3)))

        etQty?.addTextChangedListener(object : TextWatcher {


            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int,before: Int, count: Int) {



                    if(s.length == 0 ){

                        qtyErrorTxt?.visibility = View.VISIBLE
                        qtyErrorTxt.text =  getString(R.string.qty_empty_erro)

                        isQty = false

                    }else{
                        if(s.length > 0){

                            try{
                                if(s.toString().toInt() >=3 && s.toString().toInt() <=100){
                                    isQty = true
                                    qtyErrorTxt?.visibility = View.GONE

                                }else{
                                    isQty = false
                                    qtyErrorTxt?.visibility = View.VISIBLE
                                    qtyErrorTxt.text =  getString(R.string.qty_value_error)

                                }
                            }catch (e:NumberFormatException){
                                isQty = false
                               qtyErrorTxt?.visibility = View.VISIBLE
                                qtyErrorTxt.text =  getString(R.string.qty_value_error)

                            }


                        }else{
                            isQty = false
                            qtyErrorTxt?.visibility = View.VISIBLE
                            qtyErrorTxt.text =  getString(R.string.qty_empty_erro)

                        }
                    }


            }
        })


        btnUpdate.setOnClickListener {



            if (!etQty.text.isNullOrEmpty()) {
                var qty = Integer.parseInt(etQty.text.toString());
                if (qty >= totalMinQty && qty <= totalMaxQty) {
                    count = qty

                    this.dismiss()

                    mListner!!.dialogSave(count,position)

                } else {

                    if (qty < totalMinQty) {
                        Utils.showToast(
                                context,
                               // String.format(getString(R.string.txt_minimum), totalMinQty),
                            getString(R.string.qty_empty_erro),
                                Toast.LENGTH_SHORT
                        )
                    } else if (qty > totalMaxQty) {
                        Utils.showToast(
                                context,
                                //String.format(getString(R.string.txt_maximunm), totalMaxQty),
                            getString(R.string.qty_empty_erro),
                                Toast.LENGTH_SHORT
                        )
                    }
                }

            }else{
                Utils.showToast(
                        context,
                        //String.format(getString(R.string.txt_minimum), totalMinQty),
                        getString(R.string.qty_empty_erro),
                        Toast.LENGTH_SHORT
                )
            }
           // closeKeyboard()

        }

     imgClose.setOnClickListener {
            dismiss()
            mListner!!.dialogDismiss()
        }
        return builder.create()
    }



   /* @RequiresApi(Build.VERSION_CODES.M)
    private fun closeKeyboard() {
        // this will give us the view
        // which is currently focus
        // in this layout
        val view = requireActivity().currentFocus

        // if nothing is currently
        // focus then this will protect
        // the app from crash
        if (view != null) {

            // now assign the system
            // service to InputMethodManager
            val manager: InputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }*/


}