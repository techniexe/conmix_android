package com.conmix.app.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmix.app.R
import com.conmix.app.adapter.SelectAggregate2SourceDpDwnAdapter
import com.conmix.app.data.AggregateSubCategoryDataObj
import com.conmix.app.data.SelectedCementBrandChecked


/**
 * Created by Hitesh Patel on 30,March,2021
 */
class AggregateSource2SelectionDialog(private var aggregateSource2ListDg:ArrayList<AggregateSubCategoryDataObj>) : DialogFragment(), SelectAggregate2SourceDpDwnAdapter.SelectAggregate2SourceInterface {


    var mListener: DialogToFragment? = null
    var sselectedSitedatalst: ArrayList<AggregateSubCategoryDataObj> = ArrayList<AggregateSubCategoryDataObj>()
    var selectedCementBrandCheckedlst:ArrayList<SelectedCementBrandChecked> = ArrayList<SelectedCementBrandChecked>()
    var selectaggregate2SourceDpDwnAdapter: SelectAggregate2SourceDpDwnAdapter?= null
    init {
        sselectedSitedatalst.clear()
        sselectedSitedatalst.addAll(aggregateSource2ListDg)
        selectedCementBrandCheckedlst.clear()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is DialogToFragment) {
            context as DialogToFragment
        } else {
            throw ClassCastException(context.toString().toString() +
                    " must implement " + ".MyListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface DialogToFragment {
        fun aggregateSource2SelectionlistfrmDialog()
    }

    fun setListner(listner: DialogToFragment) {
        mListener = listner
    }



    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
       // dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        // mListner!!.dialogDismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.select_cement_brand_dialog, null)
        builder.setView(view)
        setRetainInstance(true)

        val btnCancel: ImageView = view.findViewById(R.id.imgClose)
        val btnSave : Button = view.findViewById(R.id.btnSubmit)
        val rvListPlace: RecyclerView = view.findViewById(R.id.cementBrandlst)
        val title: TextView = view.findViewById(R.id.dialog_title)//Select Cement Brand

        title.text = requireContext().getString(R.string.custom_mix_select_aggregrate_category_2_title)

        selectaggregate2SourceDpDwnAdapter = SelectAggregate2SourceDpDwnAdapter(requireContext(), sselectedSitedatalst)
        val mLayoutManager = LinearLayoutManager(context)
        rvListPlace?.layoutManager = mLayoutManager
        selectaggregate2SourceDpDwnAdapter?.setListener(this@AggregateSource2SelectionDialog)
        rvListPlace?.adapter = selectaggregate2SourceDpDwnAdapter

        btnCancel.setOnClickListener {

            dismiss()

        }

        btnSave.setOnClickListener {

            mListener?.aggregateSource2SelectionlistfrmDialog()
            dismiss()
        }
        return builder.create()
    }








    override fun onAddSelectedAggregateSource2(ischecked: Boolean, position: String) {
        /*val obj = SelectedCementBrandChecked(ischeck,position)
         selectedCementBrandCheckedlst.forEach {

             if(it.position == position){
                 val index = selectedCementBrandCheckedlst.indexOf(it)

                 selectedCementBrandCheckedlst.add(index,obj)
             }else{
                 selectedCementBrandCheckedlst.add(obj)
             }

         }*/
    }
}