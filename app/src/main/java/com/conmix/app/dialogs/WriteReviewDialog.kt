package com.conmix.app.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.conmix.app.R
import com.conmix.app.data.VenderReviewData
import com.conmix.app.utils.Utils


/**
 * Created by Hitesh Patel on 13,April,2021
 */
class WriteReviewDialog(var orderId: String?, var review: VenderReviewData?) : DialogFragment() {

    private var mListner: DialogToFragment? = null
    var reviewId: String = ""
    var ratingbar : RatingBar ? = null
    var btnSubmit : Button? = null
    var imgClose:ImageView? = null
    var etReview: EditText? = null

    interface DialogToFragment {
        fun dialogDismiss()
        fun dialogSave(review: VenderReviewData, vendorId:String?)
    }

    fun setListener(mListener: DialogToFragment) {
        this.mListner = mListener


    }
    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window?.setGravity(Gravity.CENTER)

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(false)
       // dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mListner!!.dialogDismiss()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.write_review_dialog, null)
        builder.setView(view)


         ratingbar = view.findViewById<RatingBar>(R.id.rating) as RatingBar
         btnSubmit = view.findViewById<Button>(R.id.btnSubmit) as Button
         imgClose= view.findViewById<ImageView>(R.id.imgClose) as ImageView
         etReview =view.findViewById<EditText>(R.id.etReview) as EditText


        //view.etReview.filters = arrayOf<InputFilter>(ignoreFirstWhiteSpace())
        //review?.let { setValue(it, view) }
        btnSubmit?.setOnClickListener {
            val review = VenderReviewData()
            if (ratingbar?.rating != null && etReview?.length()?:0 > 0) {
                review.rating = ratingbar?.rating?.toDouble()
                review.review_text = etReview?.text.toString()
                review._id = reviewId
              //  orderId?.let { it1 -> mListner!!.dialogSave(review, it1) }

                mListner?.dialogSave(review,orderId)
                dismiss()
            } else {
                if (ratingbar?.rating != null) {
                    Utils.showToast(context, getString(R.string.err_no_ratting), Toast.LENGTH_LONG)
                } else if (etReview?.text?.isEmpty() == true) {
                    Utils.showToast(
                            context,
                            getString(R.string.err_no_ratting_text),
                            Toast.LENGTH_LONG
                    )
                }
            }
        }

       imgClose?.setOnClickListener {
            dismiss()
            mListner!!.dialogDismiss()
        }
        return builder.create()
    }



    private fun setValue(review: VenderReviewData, view: View) {
        var text = review.review_text
        var num = review.rating
        reviewId = review._id!!

        if (text!!.isNotEmpty()) {
           etReview?.setText(text)
        }
        if (num != null) {
            if (num > 0)
                ratingbar?.rating = num.toFloat()
        }

    }

}