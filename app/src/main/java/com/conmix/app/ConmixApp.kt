package com.conmix.app

import android.app.Application
import com.google.firebase.FirebaseApp


/**
 * Created by Hitesh Patel on 24,February,2021
 */
class ConmixApp : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        //FirebaseApp.initializeApp(this)

    }

    companion object {
        var instance: ConmixApp? = null
            private set
    }
}